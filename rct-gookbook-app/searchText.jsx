import React, { Component } from "react";
import book from "./book.jpg";
import config from "./config.json";
import axios from "axios";

class SearchText extends Component {
  state = {
    searchText: "",
    bookDataList: {},
    view: 0,
  };
  handleSubmit = (e) => {
    e.preventDefault();
    this.calURL("", this.state.searchText, "0", "8");
  };
  async getDataFromGoogleBook(searchString) {
    const { data: bookDataList } = await axios.get(
      config.apiEndPoint + "?q=" + searchString
    );
    this.setState({ bookDataList });
  }
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  calURL = (params, searchText, startIndex, maxResults) => {
    let path = "/books";
    params = this.addToParams(params, "searchText", searchText);
    params = this.addToParams(params, "startIndex", startIndex);
    params = this.addToParams(params, "maxResults", maxResults);

    this.props.history.push({
      pathname: path,
      search: params,
    });
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    this.setState({ searchText: input.value });
  };
  render() {
    return (
      <div className="container">
        <img
          src={book}
          className="rounded-circle mx-auto d-block mt-3"
          style={{
            marginLeft: "auto",
            marginRight: "auto",
            width: "35%",
            height: "50%",
          }}
          alt="Responsive image"
        ></img>
        <form onSubmit={this.handleSubmit}>
          <div className="form-inline text-center">
            <input
              value={this.state.searchText}
              onChange={this.handleChange}
              name="searchText"
              type="text"
              className="form-control col-8"
              placeholder="Search"
            />
            <button className="btn btn-primary m-5">Search</button>
          </div>
        </form>
      </div>
    );
  }
}

export default SearchText;
