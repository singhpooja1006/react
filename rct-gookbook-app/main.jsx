import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import NavBar from "./navbar";
import SearchText from "./searchText";

import GoogleBooksList from "./books";
class MainComp extends Component {
  state = {};
  render() {
    return (
      <div>
        <NavBar />
        <Switch>
          <Route path="/books" component={GoogleBooksList} />
          <Route exact path="/" component={SearchText} />
        </Switch>
      </div>
    );
  }
}

export default MainComp;
