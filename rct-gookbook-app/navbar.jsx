import React, { Component } from "react";
import { NavDropdown } from "react-bootstrap";
import { faBook } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
class NavBar extends Component {
  state = {};
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <FontAwesomeIcon icon={faBook} className="fa-3x" />
            <li className="nav-item">
              <NavDropdown title="Options" id="basic-nav-dropdown">
                <NavDropdown.Item href="/books?searchText=Harry Potter Books&startIndex=0&maxResults=8">
                  Harry Potter Books
                </NavDropdown.Item>
                <NavDropdown.Item href="/books?searchText=Books by Agatha Christie&startIndex=0&maxResults=8">
                  Books by Agatha Christie
                </NavDropdown.Item>
                <NavDropdown.Item href="/books?searchText=Books by Premchand&startIndex=0&maxResults=8">
                  Books by Premchand
                </NavDropdown.Item>
                <NavDropdown.Item href="/books?searchText=Love Stories by Jane&startIndex=0&maxResults=8">
                  Love Stories by Jane
                </NavDropdown.Item>
                <NavDropdown.Item href="/books?searchText=Biography on Lincoln&startIndex=0&maxResults=8">
                  Biography on Lincoln
                </NavDropdown.Item>
              </NavDropdown>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavBar;
