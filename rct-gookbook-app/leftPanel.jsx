import React, { Component } from "react";
class LeftPanel extends Component {
  state = {};

  handleChange = (e) => {
    const { currentTarget: input } = e;
    const { languages, filterValues } = this.props;
    if (input.name === "langRestrict") {
      languages[input.name] = input.id;
    } else {
      filterValues[input.name] = input.id;
    }
    this.props.onOptionChange(languages, filterValues);
  };

  render() {
    return (
      <React.Fragment>
        <div className="container">
          <div className="mt-3 ml-3 row border bg-light">
            <div class="col-8 mb-2 mt-2 h6">Language</div>
          </div>
          {this.props.languages.languages.map((language) => (
            <div className="row ml-3 border">
              <div className=" col-8 mb-2 mt-2 ml-2 form-check">
                <input
                  value={language.value}
                  onChange={this.handleChange}
                  id={language.id}
                  name="langRestrict"
                  type="Radio"
                  checked={language.id === this.props.languages.langRestrict}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={language.value}>
                  {language.value}
                </label>
              </div>
            </div>
          ))}
          <br />
          <div className="row ml-3">
            <hr className="col-10" />
          </div>
          <br />
          <div className="row ml-3 border bg-light">
            <div class="col-8 mb-2 mt-2 h6">Filter</div>
          </div>
          {this.props.filterValues.filterValues.map((filterValue) => (
            <div className="row ml-3 border">
              <div className=" col-8 mb-2 mt-2 ml-2 form-check">
                <input
                  value={filterValue.value}
                  onChange={this.handleChange}
                  id={filterValue.id}
                  name="filter"
                  type="Radio"
                  checked={filterValue.id === this.props.filterValues.filter}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={filterValue.value}>
                  {filterValue.value}
                </label>
              </div>
            </div>
          ))}
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPanel;
