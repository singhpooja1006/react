import React, { Component } from "react";
import LeftPanel from "./leftPanel";
import queryString from "query-string";
import axios from "axios";
import config from "./config.json";
class GoogleBooksList extends Component {
  state = {
    bookDataList: [],
    languages: {
      languages: [
        { id: "en", value: "English" },
        { id: "fr", value: "French" },
        { id: "hi", value: "Hindi" },
      ],
      langRestrict: "",
    },
    filterValues: {
      filterValues: [
        { id: "full", value: "Full Volume" },
        { id: "free-ebooks", value: "Free Google e-Books" },
        { id: "paid-ebooks", value: "Paid Google e-Books" },
      ],
      filter: "",
    },
    page: 1,
  };
  calURL = (
    params,
    searchText,
    startIndex,
    maxResults,
    langRestrict,
    filter
  ) => {
    let path = "/books";
    params = this.addToParams(params, "searchText", searchText);
    params = this.addToParams(params, "startIndex", startIndex);
    params = this.addToParams(params, "maxResults", maxResults);
    params = this.addToParams(params, "langRestrict", langRestrict);
    params = this.addToParams(params, "filter", filter);
    this.props.history.push({
      pathname: path,
      search: params,
    });
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }

  pageNavigate = (value) => {
    let { searchText, startIndex, maxResults } = queryString.parse(
      this.props.location.search
    );
    if (value > 0) {
      startIndex = +startIndex + +maxResults;
    } else {
      startIndex =
        +startIndex - +maxResults === 0 ? "0" : +startIndex - +maxResults;
    }
    this.setState({ page: this.state.page + value });
    this.calURL(
      "",
      searchText,
      startIndex,
      maxResults,
      this.state.languages.langRestrict,
      this.state.filterValues.filter
    );
  };
  async componentDidMount() {
    let url = config.apiEndPoint + this.props.location.search;
    url = url.replace("searchText", "q");
    const { data: bookDataList } = await axios.get(url);
    this.setState({ bookDataList: bookDataList.items });
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.location.search !== this.props.location.search) {
      let url = config.apiEndPoint + this.props.location.search;
      url = url.replace("searchText", "q");
      const { data: bookDataList } = await axios.get(url);
      this.setState({ bookDataList: bookDataList.items });
    }
  }
  handleOptionChange = (languages, filterValues) => {
    const { searchText, maxResults } = queryString.parse(
      this.props.location.search
    );

    this.calURL(
      "",
      searchText,
      "0",
      maxResults,
      languages.langRestrict,
      filterValues.filter
    );
    this.setState({ languages, filterValues, page: 1 });
  };
  render() {
    let { startIndex, maxResults } = queryString.parse(
      this.props.location.search
    );
    maxResults = +maxResults;
    let endIndex = +startIndex + +maxResults;
    startIndex = startIndex === "0" ? "1" : +startIndex + 1;
    console.log("Page NO", this.state.page);
    let msg = startIndex + " - " + endIndex + " entries";
    return (
      <div>
        <div className="row">
          <div className="col-3">
            <LeftPanel
              languages={this.state.languages}
              filterValues={this.state.filterValues}
              onOptionChange={this.handleOptionChange}
            />
          </div>
          <div className="col-8">
            <div className="row ">
              <div className="col-12 text-center h3 text-warning">
                All Books
              </div>
            </div>
            <div className="row ">
              <div className="col-12 text-success h6">
                {this.state.bookDataList && this.state.bookDataList.length > 0
                  ? msg
                  : "No Data found"}
              </div>
            </div>
            {this.renderBookView()}
            <div className="row mt-3">
              <div className="col-6">
                {this.state.page > 1 ? (
                  <button
                    className="btn btn-warning"
                    onClick={() => this.pageNavigate(-1)}
                  >
                    Prev
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div className="col-6">
                {this.state.bookDataList &&
                this.state.bookDataList.length >= maxResults ? (
                  <button
                    className="btn btn-warning float-right"
                    onClick={() => this.pageNavigate(1)}
                  >
                    Next
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  renderBookView() {
    if (this.state.bookDataList) {
      return (
        <React.Fragment>
          <div className="row ">
            {this.renderBookData(this.state.bookDataList[0])}
            {this.renderBookData(this.state.bookDataList[1])}
            {this.renderBookData(this.state.bookDataList[2])}
            {this.renderBookData(this.state.bookDataList[3])}
          </div>
          <div className="row">
            {this.renderBookData(this.state.bookDataList[4])}
            {this.renderBookData(this.state.bookDataList[5])}
            {this.renderBookData(this.state.bookDataList[6])}
            {this.renderBookData(this.state.bookDataList[7])}
          </div>
        </React.Fragment>
      );
    }
  }
  renderBookData(bookData) {
    if (bookData) {
      return (
        <div className="col-md-6 col-lg-3 bg-success border">
          {bookData.volumeInfo.imageLinks ? (
            <div className="card">
              <img
                className="card-img-top text-center"
                src={bookData.volumeInfo.imageLinks.thumbnail}
                style={{ width: "100%", height: "15vw", objectFit: "cover" }}
              />
            </div>
          ) : (
            <div
              className="bg-success"
              style={{ width: "100%", height: "15vw" }}
            >
              &nbsp;
            </div>
          )}

          <div className="card-block">
            <div className="card-title row m-1 h3">
              <div className="col-12 text-center">
                {bookData.volumeInfo.title}
              </div>
            </div>
            <div className="row m-1">
              <div className="col-12 text-center">
                {bookData.volumeInfo.authors}
              </div>
            </div>
            <div className="row m-1">
              <div className="col-12 text-center">
                {bookData.volumeInfo.categories
                  ? bookData.volumeInfo.categories
                  : "NA"}
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default GoogleBooksList;
