import React, { Component } from "react";
class LeftPanel extends Component {
  state = {};
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let { fuelsData, typesData, sortsData } = { ...this.props };
    if (input.name === "fuel") {
      fuelsData.selected = input.value;
    }
    if (input.name === "type") {
      typesData.selected = input.value;
    }
    if (input.name === "sort") {
      sortsData.selected = input.value;
    }
    console.log(fuelsData, typesData, sortsData);
    this.props.onRadioButtonEvent(fuelsData, typesData, sortsData);
  };
  render() {
    const { fuelsData, typesData, sortsData } = this.props;
    return (
      <React.Fragment>
        <div className="row border mt-5">
          <div className="col-12 bg-light">
            <h6>Fuel</h6>
          </div>
          <div className="col-12 m-2">
            {fuelsData.fuels.map((fuel) => (
              <div className="form-check mt-1">
                <input
                  value={fuel}
                  onChange={this.handleChange}
                  id="fuel"
                  name="fuel"
                  type="radio"
                  checked={fuelsData.selected === fuel ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={fuel}>
                  {fuel}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 bg-light">
            <h6>Sort</h6>
          </div>
          <div className="col-12 m-2">
            {typesData.types.map((type) => (
              <div className="form-check mt-1">
                <input
                  value={type}
                  onChange={this.handleChange}
                  id="type"
                  name="type"
                  type="radio"
                  checked={typesData.selected === type ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={type}>
                  {type}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 bg-light">
            <h6>Type</h6>
          </div>
          <div className="col-12 m-2">
            {sortsData.sorts.map((sort) => (
              <div className="form-check mt-1">
                <input
                  value={sort}
                  onChange={this.handleChange}
                  id="sort"
                  name="sort"
                  type="radio"
                  checked={sortsData.selected === sort ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={sort}>
                  {sort}
                </label>
              </div>
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPanel;
