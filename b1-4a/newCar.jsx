import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
class NewCar extends Component {
  state = {
    data: { id: "", price: "", kms: "", year: "", model: "", color: "" },
    colors: ["Silver Grey", "Metallic Blue", "Black"],
    models: [],
    carMaster: [],
    errors: {},
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { data } = this.state;
    data[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data, errors });
  };
  async componentDidMount() {
    const { id } = this.props.match.params;
    if (id) {
      const apiEndpoint = config.apiEndPoint + "/cars/" + id;
      const { data } = await http.get(apiEndpoint);
      this.setState({ data });
    }
    const { data: carMaster } = await http.get(
      config.apiEndPoint + "/carmaster"
    );
    let models = [];
    carMaster.forEach((car) => models.push(car.model));
    this.setState({ carMaster, models });
  }
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "id":
        if (!e.currentTarget.value.trim()) return "ID is required";
        break;
      case "price":
        if (!e.currentTarget.value.trim()) return "Price is required";
        break;
      case "kms":
        if (!e.currentTarget.value.trim()) return "Kms is required";
        break;
      case "year":
        if (!e.currentTarget.value.trim()) return "Year is required";
        break;
      case "model":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Enter Model"
        )
          return "Model is required";
        break;
      case "color":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Enter Color"
        )
          return "Color is required";
        break;
      default:
        break;
    }
    return "";
  };
  validate = () => {
    let errs = {};
    if (!this.state.data.id.trim()) errs.id = "ID is required";
    if (!this.state.data.price < 0) errs.price = "Price is required";
    if (!this.state.data.kms < 0) errs.kms = "Kms is required";
    if (!this.state.data.year < 0) errs.year = "Year is required";
    if (
      !this.state.data.model.trim() ||
      this.state.data.model === "Enter Model"
    )
      errs.model = "Model is required";
    if (
      !this.state.data.color.trim() ||
      this.state.data.color === "Enter Color"
    )
      errs.color = "Color is required";

    return errs;
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const { data } = this.state;
      const { id } = this.props.match.params;
      if (id) {
        const apiEndpoint = config.apiEndPoint + "/cars/" + id;
        await http.put(apiEndpoint, data);
      } else {
        const apiEndpoint = config.apiEndPoint + "/cars";
        await http.post(apiEndpoint, data);
      }

      window.location = "/cars";
    } catch (ex) {}
  };

  render() {
    const { data, colors, models, errors } = this.state;
    return (
      <div className="container">
        <div className="col-12 text-center h1">Car Details</div>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="id">Car ID</label>
            <input
              value={data.id}
              onChange={this.handleChange}
              type="text"
              id="id"
              name="id"
              placeholder="Enter Id"
              className="form-control"
              disabled={data.id ? true : false}
            />
            {errors.id ? <div className="text-danger">{errors.id}</div> : ""}
          </div>
          <div className="form-group">
            <label htmlFor="price">Price</label>
            <input
              value={data.price}
              onChange={this.handleChange}
              type="number"
              id="price"
              name="price"
              placeholder="Enter price"
              className="form-control"
            />
            {errors.price ? (
              <div className="text-danger">{errors.price}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="kms">Mileage in Kms</label>
            <input
              value={data.kms}
              onChange={this.handleChange}
              type="number"
              id="kms"
              name="kms"
              placeholder="Enter mileage"
              className="form-control"
            />
            {errors.kms ? <div className="text-danger">{errors.kms}</div> : ""}
          </div>
          <div className="form-group">
            <label htmlFor="year">Year of Manufacture</label>
            <input
              value={data.year}
              onChange={this.handleChange}
              type="number"
              id="year"
              name="year"
              placeholder="Enter year"
              className="form-control"
            />
            {errors.year ? (
              <div className="text-danger">{errors.year}</div>
            ) : (
              ""
            )}
          </div>
          <div className="row">
            <div className="col-6">
              <div className="form-group">
                <label htmlFor="model">Model</label>
                <select
                  value={data.model}
                  onChange={this.handleChange}
                  id="model"
                  name="model"
                  className="browser-default custom-select mb-1"
                >
                  <option>Enter Model</option>
                  {models.map((model) => (
                    <option key={model}>{model}</option>
                  ))}
                </select>
                {errors.model ? (
                  <div className="text-danger">{errors.model}</div>
                ) : (
                  ""
                )}
              </div>
            </div>
            <div className="col-6">
              <div className="form-group">
                <label htmlFor="color">Color</label>
                <select
                  value={data.color ? data.color.trim() : "Enter Color"}
                  onChange={this.handleChange}
                  id="color"
                  name="color"
                  className="browser-default custom-select mb-1"
                >
                  <option>Enter Color</option>
                  {models ? this.renderColorDropDown() : ""}
                </select>
                {errors.color ? (
                  <div className="text-danger">{errors.color}</div>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
          <div className="col-12 text-center">
            <button className="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    );
  }

  renderColorDropDown() {
    const { data, carMaster } = this.state;
    let colors = carMaster.find((car) => data.model === car.model);
    if (colors) {
      return (
        <React.Fragment>
          {colors.colors.map((color) => (
            <option key={color}>{color}</option>
          ))}
        </React.Fragment>
      );
    }
  }
}

export default NewCar;
