import React, { Component } from "react";
import LeftPanel from "./leftPanel";
import config from "./config.json";
import http from "./services/httpService";
class Car extends Component {
  state = {
    carsData: [],
    //carMasterData: [],
    fuelsData: {
      fuels: ["Diesel", "Petrol"],
      selected: "",
    },
    typesData: {
      types: ["Hatchback", "Sedan"],
      selected: "",
    },
    sortsData: {
      sorts: ["kms", "price", "year"],
      selected: "",
    },
    minPrice: "",
    maxPrice: "",
  };
  async componentDidMount() {
    let apiEndpoint = config.apiEndPoint + "/cars";
    const { data: carsData } = await http.get(apiEndpoint);

    this.setState({ carsData });
  }
  handleRadioButtonEvent = (fuelsData, typesData, sortsData) => {
    this.setState({ fuelsData, typesData, sortsData });

    this.calURL(
      "",
      fuelsData.selected,
      typesData.selected,
      sortsData.selected,
      this.state.minPrice,
      this.state.maxPrice
    );
  };

  calURL = (params, fuel, type, sort, minPrice, maxPrice) => {
    let path = "/cars";
    params = this.addToParams(params, "fuel", fuel);
    params = this.addToParams(params, "type", type);
    params = this.addToParams(params, "minprice", minPrice);
    params = this.addToParams(params, "maxprice", maxPrice);
    params = this.addToParams(params, "sort", sort);
    this.props.history.push({
      pathname: path,
      search: params,
    });
  };

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      try {
        let apiEndpoint =
          config.apiEndPoint + "/cars" + this.props.location.search;
        const { data: carsData } = await http.get(apiEndpoint);
        this.setState({ carsData });
      } catch (ex) {
        if (ex.response >= 400 && ex.response <= 500) {
          alert("No Data Found with given query param");
        }
      }
    }
  }
  handleEditDetails = (id) => {
    window.location = "/cars/" + id;
  };
  handleDeleteDetails = async (id) => {
    try {
      const apiEndpoint = config.apiEndPoint + "/cars/" + id;
      await http.delete(apiEndpoint);
    } catch (ex) {}
  };
  handleChange = (ele) => {
    let { minPrice, maxPrice } = this.state;
    if (ele.currentTarget.name === "minPrice") {
      minPrice = ele.currentTarget.value;
    }
    if (ele.currentTarget.name === "maxPrice") {
      maxPrice = ele.currentTarget.value;
    }
    this.setState({ maxPrice, minPrice });
  };
  handleKeyPress = (event) => {
    if (event.key === "Enter") {
      this.calURL(
        "",
        this.state.fuelsData.selected,
        this.state.typesData.selected,
        this.state.sortsData.selected,
        this.state.minPrice,
        this.state.maxPrice
      );
    }
  };
  render() {
    return (
      <div className="container">
        <div className="row m-1">
          <div className="col-2 mr-1">
            <LeftPanel
              fuelsData={this.state.fuelsData}
              typesData={this.state.typesData}
              sortsData={this.state.sortsData}
              onRadioButtonEvent={this.handleRadioButtonEvent}
            />
          </div>
          <div className="col-9 ml-2">
            <div className="text-center h1">All Cars</div>
            <form className="form-inline">
              <div className="form-group mb-2">
                <label className="sr-only">PriceRange :</label>
                <input
                  type="text"
                  readonly
                  className="form-control-plaintext h6"
                  value=" Price Range :"
                />
              </div>
              <div className="form-group mx-sm-3 mb-2">
                <input
                  onChange={this.handleChange}
                  onKeyPress={this.handleKeyPress}
                  type="number"
                  className="form-control"
                  id="minPrice"
                  name="minPrice"
                  placeholder="MinPrice"
                  value={this.state.minPrice}
                />
              </div>
              <input
                onChange={this.handleChange}
                onKeyPress={this.handleKeyPress}
                value={this.state.maxPrice}
                type="number"
                className="form-control mb-2"
                id="maxPrice"
                name="maxPrice"
                placeholder="MaxPrice"
              />
            </form>
            {this.renderCarData()}
          </div>
        </div>
      </div>
    );
  }
  renderCarData = () => {
    const { carsData } = this.state;
    if (carsData) {
      return (
        <React.Fragment>
          <div className="row text-center">
            {carsData.map((car) => (
              <div className="col-3 border bg-warning">
                <h5>{car.model}</h5>
                <h6>
                  Price : <i class="fa fa-inr" aria-hidden="true"></i>
                  {car.price}
                </h6>
                <h6>Color : {car.color}</h6>
                <h6>Mileage : {car.kms} Kms</h6>
                <h6>Manufactured in {car.year}</h6>
                <h6>
                  <i
                    className="fa fa-pencil-square-o mr-5"
                    aria-hidden="true"
                    onClick={() => this.handleEditDetails(car.id)}
                  ></i>
                  <i
                    className="fa fa-trash ml-5"
                    aria-hidden="true"
                    onClick={() => this.handleDeleteDetails(car.id)}
                  ></i>
                </h6>
              </div>
            ))}
          </div>
        </React.Fragment>
      );
    }
  };
}

export default Car;
