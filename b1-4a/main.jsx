import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import NavBar from "./navBar";
import NewCar from "./newCar";
import Car from "./car";
class MainComp extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <NavBar />
        <Switch>
          <Route path="/new" component={NewCar} />
          <Route path="/cars/:id" component={NewCar} />
          <Route path="/cars" component={Car} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default MainComp;
