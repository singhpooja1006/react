var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
    next();
});

const port  = 2410;
app.listen(port,()=>console.log("Listening on port : ",port));

let carMaster = [
    {model: "Swift Dzire VXi", make: "Maruti", fuel: "Diesel",
     colors: ["White", "Silver Grey", "Metallic Blue", "Red"], type: "Sedan", transmission: "Manual"},
    {model: "Etios SMi", make: "Toyota", fuel: "Diesel",
     colors: ["White", "Steel Grey", "Black"], type: "Hatchback", transmission: "Manual"},
    {model: "City AXi", make: "Honda", fuel: "Petrol",
     colors: ["Silver Grey", "Metallic Blue", "Black"], type: "Sedan", transmission: "Automatic"},
    {model: "Swift DXi", make: "Maruti", fuel: "Diesel",
     colors: ["White", "Red", "Black"], type: "Hatchback", transmission: "Manual"},
    {model: "Etios VXi", make: "Toyota", fuel: "Diesel",
     colors: ["White", "Silver Grey", "Black"], type: "Sedan", transmission: "Manual"},
    {model: "City ZXi", make: "Honda", fuel: "Petrol",
     colors: ["Silver Grey", "Metallic Blue", "Red"], type: "Sedan", transmission: "Manual"}
    ];
let cars = [
    {id: "ABR12", price: 400000, year: 2015, kms: 25000, model: "Swift Dzire VXi", color: "White"},
    {id: "CBN88", price: 480000, year: 2012, kms: 75000, model: "Etios SMi", color: "Steel Grey"},
    {id: "XER34", price: 300000, year: 2013, kms: 55000, model: "City AXi", color: "Metallic Blue"},
    {id: "MPQ29", price: 400000, year: 2015, kms: 25000, model: "Swift DXi", color: "Black"},
    {id: "PYQ88", price: 480000, year: 2012, kms: 75000, model: "Etios VXi", color: "White"},
    {id: "DFI61", price: 300000, year: 2013, kms: 55000, model: "City ZXi", color: "Red"},
    {id: "JUW88", price: 400000, year: 2015, kms: 25000, model: "Swift Dzire VXi", color: "White"},
    {id: "KPW09", price: 285000, year: 2012, kms: 76321, model: "Swift Dzire VXi", color: "White"},
    {id: "NHH09", price: 725000, year: 2018, kms: 15000, model: "City ZXi", color: "Silver Grey"},
    {id: "CTT26", price: 815000, year: 2016, kms: 42500, model: "City AXi", color: "Metallic Blue"},
    {id: "VAU55", price: 345000, year: 2014, kms: 81559, model: "Swift DXi", color: "Red"},
    {id: "BTR31", price: 184000, year: 2011, kms: 120833, model: "Etios VXi", color: "Silver Grey"}
    ];

app.get("/cars",function(req,res) { 
    let {sort,minprice,maxprice,fuel,type} = req.query;

    console.log("in get request for /cars: ", fuel,type);
    let result = [...cars];
    
    if(minprice) {
        
        result = result.filter((data) => data.price >= +minprice) ;
    }
    if(maxprice){
        result = result.filter((data) => data.price <= +maxprice) ;
    }
    if(fuel && type){
        let carfuelType = carMaster.filter((car) => car.fuel === fuel && car.type === type);
        let modelNames = [];
       
        carfuelType.forEach(car => modelNames.push(car.model));
        let newResult = [];
        for(let index=0;index < modelNames.length; index++){
            let opts =  result.filter((data) => data.model === modelNames[index]) ;
            if(opts)
                opts.forEach(opt => newResult.push(opt));
        }
        if(newResult.length > 0){
            result = [...newResult];
        }else{
            res.status(404).send("No Data Found");
        }
       
    }
    else {
        if(type){
            let carType = carMaster.filter((car) => car.type === type);
            let modelNames = [];
           
            carType.forEach(car => modelNames.push(car.model));
            let newResult = [];
            for(let index=0;index < modelNames.length; index++){
                let opts =  result.filter((data) => data.model === modelNames[index]) ;
                if(opts)
                    opts.forEach(opt => newResult.push(opt));
            }
            if(newResult.length > 0){
                result = [...newResult];
            }
          
        }
        if(fuel){
            let carfuelType = carMaster.filter((car) => car.fuel === fuel);
            let modelNames = [];
        
            carfuelType.forEach(car => modelNames.push(car.model));
            let newResult = [];
            for(let index=0;index < modelNames.length; index++){
                let opts =  result.filter((data) => data.model === modelNames[index]) ;
                if(opts)
                    opts.forEach(opt => newResult.push(opt));
            }
            if(newResult.length > 0){
                result = [...newResult];
            }
          
        }
    }
   
    
    if(sort === "kms"){
        result = result.sort((car1,car2) => car1.kms - car2.kms);
    }
    if(sort === "price"){
        result = result.sort((car1,car2) => car1.price - car2.price);
    }
    if(sort === "year"){
        result = result.sort((car1,car2) => car1.year - car2.year);
    }
    res.send(result);
    
});

app.get("/cars/:id",function(req,res) { 
    let id = req.params.id;
    console.log("in get request for /cars/:id : ",id);
    let index = cars.findIndex((data)=> data.id === id);
    if (index === -1) res.status(404).send(id + " Not Found");
    else {
        res.send(cars[index]);
    }
});

app.post("/cars",function(req,res) { 
    console.log("in post request for /cars ");
    cars.push(req.body)
    res.send(req.body);
});

app.put("/cars/:id",function(req,res) { 
    let id = req.params.id;
    console.log("in put request for /cars/:id ", id);
    let index = cars.findIndex((data)=> data.id === id);
    if (index === -1) res.status(404).send(id + " Not Found");
    else {
        cars[index] = req.body;
        res.send(res.body);
    }
});

app.delete("/cars/:id",function(req,res) { 
    let id = req.params.id;
    console.log("in delete request for /cars/:id", id );
    let index = cars.findIndex((data)=> data.id === id);
    if (index === -1) res.status(404).send(id + " Not Found");
    else {
        cars.splice(index,1);
    }
});

app.get("/carmaster",function(req,res) { 
    console.log("in get request for /carmaster : ");
    res.send(carMaster);
});