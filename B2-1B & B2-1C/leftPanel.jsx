import React, { Component } from "react";
class LeftPanel extends Component {
  state = {};
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let { departments, designations, salaryData, gendersData } = {
      ...this.props,
    };
    if (input.type === "checkbox") {
      let dept = departments.find((n1) => n1.name === input.name);
      if (dept) dept.isSelected = input.checked;

      let desig = designations.find((n1) => n1.name === input.name);
      if (desig) desig.isSelected = input.checked;
    }
    if (input.name === "gender") {
      gendersData.selected = input.value;
    }
    if (input.name === "salary") {
      salaryData.selected = input.value;
    }
    this.props.onCheckBoxButtonEvent(
      departments,
      designations,
      salaryData,
      gendersData
    );
  };
  render() {
    const { departments, designations, salaryData, gendersData } = this.props;
    return (
      <React.Fragment>
        <div className="row border ">
          <div className="col-1 m-3 h6">Options</div>
        </div>
        <div className="row border ">
          <div className="col-12 m-3">
            <h6>Department</h6>
            {departments.map((department) => (
              <div className="form-check mt-1">
                <input
                  value={department.name}
                  onChange={this.handleChange}
                  id={department.name}
                  name={department.name}
                  type="checkbox"
                  checked={department.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={department.name}>
                  {department.name}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>Designation</h6>
            {designations.map((designation) => (
              <div className="form-check mt-1">
                <input
                  value={designation.name}
                  onChange={this.handleChange}
                  name={designation.name}
                  id={designation.name}
                  type="checkbox"
                  checked={designation.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={designation.name}>
                  {designation.name}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>Gender</h6>
            {gendersData.genders.map((gender) => (
              <div className="form-check mt-1">
                <input
                  value={gender}
                  onChange={this.handleChange}
                  id="gender"
                  name="gender"
                  type="radio"
                  checked={gendersData.selected === gender ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={gender}>
                  {gender}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>Salry Range</h6>
            {salaryData.salaries.map((salary) => (
              <div className="form-check mt-1">
                <input
                  value={salary}
                  onChange={this.handleChange}
                  id="salary"
                  name="salary"
                  type="radio"
                  checked={salaryData.selected === salary ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={salary}>
                  {salary}
                </label>
              </div>
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPanel;
