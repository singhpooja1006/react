import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import NavBar from "./navBar";
import Employees from "./employees";
import AddNewEmp from "./newEmp";

class MainComp extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <NavBar />
        <Switch>
          <Route path="/new" component={AddNewEmp} />
          <Route path="/employees/:id" component={AddNewEmp} />
          <Route path="/employees" component={Employees} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default MainComp;
