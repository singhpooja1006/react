import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
import queryString from "query-string";
import LeftPanel from "./leftPanel";

class Employees extends Component {
  state = {
    employeesData: [],
    sortBy: "",
    pageInfo: {},

    departments: [
      { name: "Finance", isSelected: false },
      { name: "Operations", isSelected: false },
      { name: "Technology", isSelected: false },
      { name: "HR", isSelected: false },
    ],

    designations: [
      { name: "Trainee", isSelected: false },
      { name: "Junior Manager", isSelected: false },
      { name: "Manager", isSelected: false },
      { name: "General Manager", isSelected: false },
      { name: "Vice President", isSelected: false },
    ],
    salaryData: {
      salaries: ["<15000", "15000-25000", ">25000"],
      selected: "",
    },
    gendersData: { genders: ["Male", "Female"], selected: "" },
  };
  async componentDidMount() {
    let { sortBy } = queryString.parse(this.props.location.search);
    let apiEndpoint = config.apiEndPoint + "/employees";
    if (sortBy) {
      apiEndpoint = apiEndpoint + "?sortBy=" + sortBy;
    }
    const { data: employeesData } = await http.get(apiEndpoint);
    this.setState({
      employeesData: employeesData.data,
      pageInfo: employeesData.pageInfo,
    });
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      let apiEndpoint =
        config.apiEndPoint + "/employees" + this.props.location.search;
      const { data: employeesData } = await http.get(apiEndpoint);
      this.setState({
        employeesData: employeesData.data,
        pageInfo: employeesData.pageInfo,
      });
    }
  }
  handleSortBy = (sortBy) => {
    this.setState({ sortBy });
    let department = this.buildQueryString(this.state.departments);
    let designation = this.buildQueryString(this.state.designations);
    this.calURL(
      "",
      department,
      designation,
      this.state.salaryData.selected,
      this.state.gendersData.selected,
      sortBy
    );
  };
  calURL = (params, department, designation, salary, gender, sortBy, page) => {
    let path = "/employees";
    params = this.addToParams(params, "department", department);
    params = this.addToParams(params, "designation", designation);
    params = this.addToParams(params, "salary", salary);
    params = this.addToParams(params, "gender", gender);
    params = this.addToParams(params, "sortBy", sortBy);
    params = this.addToParams(params, "page", page);
    this.props.history.push({
      pathname: path,
      search: params,
    });
  };

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  handleEditDetails = (id) => {
    window.location = "/employees/" + id;
  };
  handleDeleteDetails = async (id) => {
    const originalEmployees = this.state.employeesData;
    try {
      const apiEndpoint = config.apiEndPoint + "/employees";
      await http.delete(apiEndpoint + "/" + id);
      const { data: employeesData } = await http.get(apiEndpoint);
      this.setState({
        employeesData: employeesData.data,
        pageInfo: employeesData.pageInfo,
      });
    } catch (ex) {
      if (ex.response && ex.response.status !== 404) {
        this.setState({ employeesData: originalEmployees });
      }
    }
  };
  handleCheckBoxEvent = (
    departments,
    designations,
    salaryData,
    gendersData
  ) => {
    this.setState({ departments, designations, salaryData, gendersData });
    let department = this.buildQueryString(departments);
    let designation = this.buildQueryString(designations);
    this.calURL(
      "",
      department,
      designation,
      salaryData.selected,
      gendersData.selected,
      this.state.sortBy,
      1
    );
  };
  buildQueryString(optionsData) {
    let filterData = optionsData.filter((n1) => n1.isSelected);
    let arrayData = filterData.map((n1) => n1.name);
    return arrayData.join(",");
  }
  pageNavigate = (value) => {
    const { pageInfo } = { ...this.state };
    let { department, designation, salary, gender, sortBy } = queryString.parse(
      this.props.location.search
    );
    let currPage = pageInfo.pageNumber + value;
    this.calURL("", department, designation, salary, gender, sortBy, currPage);
  };
  render() {
    return (
      <div className="container">
        <div className="row m-1">
          <div className="col-2 mr-1">
            <LeftPanel
              departments={this.state.departments}
              designations={this.state.designations}
              salaryData={this.state.salaryData}
              gendersData={this.state.gendersData}
              onCheckBoxButtonEvent={this.handleCheckBoxEvent}
            />
          </div>
          <div className="col-9 ml-2">{this.renderEmployeesData()}</div>
        </div>
      </div>
    );
  }
  renderEmployeesData = () => {
    const { employeesData, pageInfo } = this.state;

    if (employeesData) {
      let { page } = queryString.parse(this.props.location.search);
      page = page ? +page : 1;
      return (
        <React.Fragment>
          <div className="row">
            {pageInfo.pageNumber === 1
              ? 1
              : pageInfo.numOfItems * pageInfo.pageNumber -
                pageInfo.numOfItems +
                1}{" "}
            to{" "}
            {pageInfo.numOfItems * pageInfo.pageNumber < pageInfo.totalItemCount
              ? pageInfo.numOfItems * pageInfo.pageNumber
              : pageInfo.totalItemCount}{" "}
            of {pageInfo.totalItemCount}
          </div>
          <div className="row border bg-dark text-center text-white mt-2">
            <div
              className="col-1 border"
              onClick={() => this.handleSortBy("id")}
            >
              ID
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("name")}
            >
              Name
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("department")}
            >
              Department
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("designation")}
            >
              Designation
            </div>
            <div
              className="col-1 border"
              onClick={() => this.handleSortBy("salary")}
            >
              Salary
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("gender")}
            >
              Gender
            </div>
            <div className="col-2 border"></div>
          </div>
          {employeesData.map((employee) => (
            <div className="row border text-center bg-light" key={employee.id}>
              <div className="col-1 border">{employee.id}</div>
              <div className="col-2 border">{employee.name}</div>
              <div className="col-2 border">{employee.department}</div>
              <div className="col-2 border">{employee.designation}</div>
              <div className="col-1 border">{employee.salary}</div>
              <div className="col-2 border">{employee.gender}</div>
              <div className="col-1 border">
                <i
                  className="fa fa-pencil-square-o"
                  aria-hidden="true"
                  onClick={() => this.handleEditDetails(employee.id)}
                ></i>
              </div>
              <div className="col-1 border">
                <i
                  className="fa fa-trash"
                  aria-hidden="true"
                  onClick={() => this.handleDeleteDetails(employee.id)}
                ></i>
              </div>
            </div>
          ))}
          <div className="row m-1">
            <div>
              {page > 1 ? (
                <button
                  className="btn btn-primary m-1"
                  onClick={() => this.pageNavigate(-1)}
                >
                  Previous
                </button>
              ) : (
                ""
              )}
            </div>
            <div>
              {page < pageInfo.numberOfPages ? (
                <button
                  className="btn btn-primary m-1"
                  onClick={() => this.pageNavigate(1)}
                >
                  Next
                </button>
              ) : (
                ""
              )}
            </div>
          </div>
        </React.Fragment>
      );
    }
  };
}

export default Employees;
