import React, { Component } from "react";
import LeftPanel from "./leftPanel";
class FoodData extends Component {
  state = {
    productsData: [],
    carts: [],
  };
  componentDidMount() {
    let { productsData } = { ...this.props };
    this.setState({ productsData });
  }
  addProductToCart = (product) => {
    const productsData = [...this.state.productsData];
    let prdIndex = productsData.findIndex((prd) => prd.id === product.id);
    if (prdIndex !== -1) {
      productsData[prdIndex].qty = productsData[prdIndex].qty + 1;
    }

    this.setState({
      productsData,
    });
    this.props.onTotalQtyChange(
      productsData.reduce((acc, curr) => acc + curr.qty, 0),
      productsData
    );
  };

  handleDecrement = (product) => {
    const productsData = [...this.state.productsData];
    let prdIndex = productsData.findIndex((prd) => prd.id === product.id);
    if (prdIndex !== -1) {
      productsData[prdIndex].qty = productsData[prdIndex].qty - 1;
    }

    this.setState({
      productsData,
    });
    this.props.onTotalQtyChange(
      productsData.reduce((acc, curr) => acc + curr.qty, 0),
      productsData
    );
  };
  handleOptionChange = (categoryData) => {
    this.props.onCategoryChange(categoryData);
  };

  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-2">
            <LeftPanel
              categoryData={this.props.categoryData}
              onFilter={this.handleOptionChange}
            />
          </div>
          <div className="col-lg-9 col-12 mt-2">
            {this.renderProductsData()}
          </div>
        </div>
      </div>
    );
  }
  renderProductsData = () => {
    let { productsData } = this.state;
    if (this.props.categoryData.selected !== "") {
      productsData = productsData.filter(
        (prd) => prd.category === this.props.categoryData.selected
      );
    }
    return (
      <React.Fragment>
        <div className="row">
          {productsData
            ? productsData.map((product) => this.renderProductViewData(product))
            : ""}
        </div>
      </React.Fragment>
    );
  };
  renderProductViewData = (product) => {
    return (
      <div className="col-md-6 col-lg-6">
        <div className="ml-3 text-center">
          <img
            className="card-img-top img-fluid"
            src={product.imgLink}
            style={{
              width: "75%",
              height: "150px",
            }}
          />
          <div className="card-block">
            <h5 className="card-title text-center">{product.name}</h5>
            <div className="text-center">${product.price}</div>
            {product.qty === 0 ? (
              <button
                className="btn btn-lg btn-block btn-secondary mb-4"
                onClick={() => this.addProductToCart(product)}
              >
                Add to Cart
              </button>
            ) : (
              <div>
                <button
                  className="btn  btn-secondary mb-4 float-left ml-3"
                  onClick={() => this.addProductToCart(product)}
                >
                  +
                </button>
                {product.qty} in cart
                <button
                  className="btn  btn-secondary float-right mr-3"
                  onClick={() => this.handleDecrement(product)}
                >
                  -
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  };
}

export default FoodData;
