import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
class ShoppinCart extends Component {
  state = {
    productsData: this.props.productsData ? [...this.props.productsData] : [],
    cardCheckOut: -1,
  };
  handleDecrement = (product) => {
    const productsData = [...this.state.productsData];
    let prdIndex = productsData.findIndex((prd) => prd.id === product.id);
    if (prdIndex !== -1) {
      productsData[prdIndex].qty = productsData[prdIndex].qty - 1;
    }
    this.setState({
      productsData,
    });
    this.props.onTotalQtyChange(
      productsData.reduce((acc, curr) => acc + curr.qty, 0),
      productsData
    );
  };
  handleIecrement = (product) => {
    const productsData = [...this.state.productsData];
    let prdIndex = productsData.findIndex((prd) => prd.id === product.id);
    if (prdIndex !== -1) {
      productsData[prdIndex].qty = productsData[prdIndex].qty + 1;
    }
    this.setState({
      productsData,
    });
    this.props.onTotalQtyChange(
      productsData.reduce((acc, curr) => acc + curr.qty, 0),
      productsData
    );
  };
  addCredentialData = () => {
    const user = localStorage.getItem("user");
    if (!user) {
      this.setState({ cardCheckOut: 0 });
    } else {
      this.setState({ cardCheckOut: 1 });
    }
  };
  render() {
    const { productsData } = this.state;
    if (this.state.cardCheckOut === 0) {
      return (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: "/check-out", productsData: productsData },
          }}
        />
      );
    } else if (this.state.cardCheckOut === 1) {
      return (
        <Redirect
          to={{
            pathname: "/check-out",
            state: { productsData: productsData },
          }}
        />
      );
    } else {
      return (
        <div className="container">
          <h2 className="text-center">
            You have {this.props.totalQty} items in your cart
          </h2>
          {this.props.totalQty > 0 ? (
            <div className="row border">
              <div className="col-2"></div>
              <div className="col-2">Product</div>
              <div className="col-2"></div>
              <div className="col-3">Quantity</div>
              <div className="col-2"></div>
              <div className="col-1">Price</div>
            </div>
          ) : (
            ""
          )}
          {productsData.map((product) =>
            product.qty > 0 ? (
              <React.Fragment>
                <div className="row border">
                  <div className="col-3">
                    <img
                      className="card-img-top mb-2"
                      src={product.imgLink}
                      alt=""
                      style={{
                        height: "190px",
                        width: "90%",
                        borderRadius: "50%",
                      }}
                      className="img-circle"
                    />
                  </div>
                  <div className="col-1 h6">{product.name}</div>
                  <div className="col-1 ml-5">
                    <button
                      className="btn btn-secondary"
                      onClick={() => this.handleIecrement(product)}
                    >
                      +
                    </button>
                  </div>
                  <div className="col-3 h6 ml-4">{product.qty} in cart</div>
                  <div className="col-1">
                    <button
                      className="btn btn-secondary"
                      onClick={() => this.handleDecrement(product)}
                    >
                      -
                    </button>
                  </div>
                  <div className="col-2 h6 text-right">${product.price}</div>
                </div>
              </React.Fragment>
            ) : (
              ""
            )
          )}
          {this.props.totalQty > 0 ? (
            <React.Fragment>
              <div className="row border">
                <div className="col-12 h6 text-right">
                  $
                  {productsData.reduce(
                    (acc, curr) => acc + curr.qty * curr.price,
                    0
                  )}
                </div>
              </div>

              <div className="row border">
                <div className="col-3 m-2">
                  <button
                    className="btn btn-success"
                    onClick={() => this.addCredentialData()}
                  >
                    Check Out
                  </button>
                </div>
              </div>
            </React.Fragment>
          ) : (
            ""
          )}
        </div>
      );
    }
  }
}

export default ShoppinCart;
