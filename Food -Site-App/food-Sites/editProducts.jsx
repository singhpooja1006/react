import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
class EditProducts extends Component {
  state = {
    data: this.props.product,
    categories: [
      "Bread",
      "Dairy",
      "Fruits",
      "Seasoning and Spices",
      "Vegetables",
    ],
  };
  handleChange = (e) => {
    const formdata = { ...this.state.data };
    formdata[e.currentTarget.id] = e.currentTarget.value;
    this.setState({ data: formdata });
  };
  handleEditProduct = async () => {
    const user = JSON.parse(localStorage.getItem("user"));
    let apiEndPoint = config.refreshTokenKeyApi;
    const { data: userToken } = await axios.post(apiEndPoint, {
      grant_type: "refresh_token",
      refresh_token: user.refreshToken,
    });
    apiEndPoint =
      config.apiEndPoint +
      "products/" +
      this.state.data.id +
      ".json?auth=" +
      userToken.id_token;

    try {
      const { data: product } = await axios.put(apiEndPoint, {
        category: this.state.data.category,
        imgLink: this.state.data.imgLink,
        name: this.state.data.name,
        price: this.state.data.price,
      });
      alert("Product Modified Successfully", product);
      window.location = "/";
    } catch (ex) {
      console.log("Error", ex);
    }
  };
  handleDeleteProduct = async () => {
    const user = JSON.parse(localStorage.getItem("user"));
    let apiEndPoint = config.refreshTokenKeyApi;
    const { data: userToken } = await axios.post(apiEndPoint, {
      grant_type: "refresh_token",
      refresh_token: user.refreshToken,
    });
    apiEndPoint =
      config.apiEndPoint +
      "products/" +
      this.state.productsData[this.state.editProductIndex].id +
      ".json?auth=" +
      userToken.id_token;
    await axios.delete(apiEndPoint);
    alert("Product Deleted Successfullly");
    window.location = "/";
  };
  render() {
    const { data, categories } = this.state;
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-6 ">
            <div>
              <div className="form-group">
                <label htmlFor="name">Title</label>
                <input
                  name="name"
                  id="name"
                  type="text"
                  className="form-control"
                  value={data.name}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="price">Price</label>
                <input
                  value={data.price}
                  onChange={this.handleChange}
                  type="number"
                  id="price"
                  name="price"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label htmlFor="category">Category</label>
                <select
                  value={data.category}
                  onChange={this.handleChange}
                  id="category"
                  name="category"
                  className="form-control"
                >
                  <option>Selected category</option>
                  {categories.map((category) => (
                    <option key={category}>{category}</option>
                  ))}
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="imgLink">Image Url</label>
                <input
                  value={data.imgLink}
                  onChange={this.handleChange}
                  type="text"
                  id="imgLink"
                  name="imgLink"
                  className="form-control"
                />
              </div>
              <div>
                <button
                  className="btn btn-success"
                  onClick={() => this.handleEditProduct()}
                >
                  Edit
                </button>
                <button
                  className="btn btn-danger ml-2"
                  onClick={() => this.handleDeleteProduct()}
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
          {data.imgLink ? (
            <div className="col-lg-4 col-12 mt-4 ml-5">
              <div className="card">
                <img
                  className="card-img-top text-center"
                  alt=""
                  src={data.imgLink}
                  style={{
                    width: "100%",
                    height: "200px",
                    objectFit: "cover",
                  }}
                />
                <div className="card-block">
                  <div className="ml-2 h5">{data.name}</div>
                  <div className="ml-2 h6">${data.price}</div>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}

export default EditProducts;
