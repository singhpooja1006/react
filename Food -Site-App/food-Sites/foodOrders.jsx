import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
import { Link } from "react-router-dom";
class FoodOrders extends Component {
  state = {
    ordersData: [],
  };

  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    let apiEndPoint = config.refreshTokenKeyApi;
    const { data: userToken } = await axios.post(apiEndPoint, {
      grant_type: "refresh_token",
      refresh_token: user.refreshToken,
    });
    apiEndPoint =
      config.apiEndPoint +
      "orders/" +
      userToken.user_id +
      ".json?auth=" +
      userToken.id_token;
    const { data: orderData } = await axios.get(apiEndPoint);
    let ordersData = [];
    for (const [key, value] of Object.entries(orderData)) {
      let order = { ...value };
      order.id = key;
      ordersData.push(order);
    }
    this.setState({
      ordersData,
    });
    console.log(ordersData);
  }
  render() {
    const { ordersData } = this.state;
    return (
      <React.Fragment>
        <div className="container-fluid">
          <div className="row ml-4">
            <h1>Orders</h1>
          </div>
          <table className="table ml-4">
            <thead>
              <tr>
                <th>Name</th>
                <th>City</th>
                <th>Amount</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {ordersData.map((orders) => (
                <tr>
                  <td>{orders.name}</td>
                  <td>{orders.city}</td>
                  <td>{orders.totalPrice}</td>
                  <td>
                    <Link to="/">View</Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default FoodOrders;
