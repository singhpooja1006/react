import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./dropdown.css";
class NavBar extends Component {
  componentDidMount() {
    console.log("Did mount");
  }
  render() {
    return (
      <nav className="navbar navbar-expand-sm navbar-light bg-light">
        <Link className="navbar-brand" to="/">
          <i className="fa fa-leaf" style={{ color: "green" }}></i>
        </Link>
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link to="/shopping-cart" className="nav-link">
              <i className="fa fa-shopping-cart fa-6x" aria-hidden="true"></i>
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/" className="nav-link">
              <span className="badge badge-pill badge-warning">
                {this.props.totalQty ? this.props.totalQty : 0}
              </span>
            </Link>
          </li>
        </ul>

        <ul className="navbar-nav ml-auto">
          {this.props.user ? (
            <React.Fragment>
              <form className="form-inline my-2 my-lg-0">
                <a className="navbar-brand">
                  <div className="dropdown">
                    <div className="dropbtn">
                      {this.props.user.email}&nbsp;
                      <i
                        className="fa fa-chevron-down"
                        id="onhover"
                        style={{ fontSize: "10px" }}
                      ></i>
                    </div>
                    <div className="dropdown-content">
                      <div>
                        <Link to="/orders">My Orders</Link>
                      </div>
                      <div>
                        <Link to="/products">Manage Products</Link>
                      </div>
                      <div>
                        <Link to="/logout">Logout</Link>
                      </div>
                    </div>
                  </div>
                </a>
              </form>
            </React.Fragment>
          ) : (
            <li className="nav-item">
              <Link to="/login" className="nav-link">
                Login
              </Link>
            </li>
          )}
        </ul>
      </nav>
    );
  }
}

export default NavBar;
