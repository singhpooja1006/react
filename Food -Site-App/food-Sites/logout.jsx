import React, { Component } from "react";
class Logout extends Component {
  componentDidMount() {
    localStorage.removeItem("user");
    localStorage.removeItem("prvState");
    window.location = "/login";
  }
  render() {
    return null;
  }
}

export default Logout;
