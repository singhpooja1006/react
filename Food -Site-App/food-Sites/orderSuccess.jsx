import React, { Component } from "react";
class OrderSuccess extends Component {
  state = {};
  render() {
    return (
      <div className="container">
        <div className="row">
          <h3 className="col-12 text-center">Thank You!</h3>
          <div className="col-12 text-center h6">
            We received your order and will process it within next 24 hours!
          </div>
        </div>
      </div>
    );
  }
}

export default OrderSuccess;
