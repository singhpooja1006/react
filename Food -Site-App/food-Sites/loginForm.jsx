import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import config from "./config.json";
import axios from "axios";
import { Redirect } from "react-router-dom";
class LoginForm extends React.Component {
  state = {
    checkOutFlag: false,
  };
  render() {
    if (this.state.checkOutFlag) {
      return (
        <Redirect
          to={{
            pathname: "/check-out",
            state: { productsData: this.props.location.state.productsData },
          }}
        />
      );
    }
    return (
      <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        validationSchema={Yup.object().shape({
          email: Yup.string()
            .email("Email is invalid")
            .required("Email is required"),
          password: Yup.string()
            .min(6, "Password must be at least 6 characters")
            .required("Password is required"),
        })}
        onSubmit={async (fields) => {
          let userData = {
            email: fields.email,
            password: fields.password,
            returnSecureToken: true,
          };
          try {
            const { data: loginData } = await axios.post(
              config.loginEndPoint,
              userData
            );
            localStorage.setItem("user", JSON.stringify(loginData));
            console.log("props", this.props);
            const { state } = this.props.location;
            if (!state) {
              window.location = "/admin";
            } else {
              this.setState({ checkOutFlag: true });
            }
          } catch (ex) {
            alert("Wrong Credentials. Enter correct Username and password");
          }
        }}
        render={({ errors, status, touched }) => (
          <div className="container mt-3">
            <Form>
              <h2>Enter Your Credentials</h2>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <Field
                  name="email"
                  type="text"
                  className={
                    "form-control" +
                    (errors.email && touched.email ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="email"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <Field
                  name="password"
                  type="password"
                  className={
                    "form-control" +
                    (errors.password && touched.password ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="password"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <button type="submit" className="btn btn-success mr-2">
                  Login
                </button>
              </div>
            </Form>
          </div>
        )}
      />
    );
  }
}

export default LoginForm;
