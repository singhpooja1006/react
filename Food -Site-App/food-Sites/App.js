import React, { Component } from 'react';
import { Switch, Route, Redirect } from "react-router-dom";
import FoodData from './components/food-Sites/showFoodData';
import ShoppinCart from './components/food-Sites/shoppingCart';
import NavBar from './components/food-Sites/navBar';
import config from "./components/food-Sites/config.json";
import axios from "axios";
import LoginForm from './components/food-Sites/loginForm';
import CheckOut from './components/food-Sites/checkOut';
import Logout from './components/food-Sites/logout';
import OrderSuccess from './components/food-Sites/orderSuccess';
import FoodOrders from './components/food-Sites/foodOrders';
import Products from './components/food-Sites/manageProducts';
import NewProduct from './components/food-Sites/newProduct';


class App extends Component {
  state = {
    totalQty:0,
    categoryData: {
      categories: [
        "Bread",
        "Dairy",
        "Fruits",
        "Seasoning and Spices",
        "Vegetables",
      ],
      selected: "",
    },
  };
  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
  
    const { data: products } = await axios.get(
      config.apiEndPoint + "products.json"
    );
    let productsData = [];
    for (const [key, value] of Object.entries(products)) {
      let product = { ...value };
      product.id = key;
      product.qty = 0;
      productsData.push(product);
    }
    this.setState({
      productsData,user
    });
  }
  handleTotalQty = (totalQty,productsData)=>{
    this.setState({totalQty,productsData})
  }

  handleCategory = (categoryData)=>{
    this.setState({categoryData})
  }

  render() { 
    const { user } = this.state;
    return (
      <React.Fragment>
         <NavBar totalQty={this.state.totalQty} user={user}/>
        <Switch>
        <Route path="/login" component={LoginForm}/>
        <Route path="/logout" component={Logout}/>
        <Route path="/shopping-cart"  component={() => <ShoppinCart productsData={this.state.productsData} 
        totalQty={this.state.totalQty} onTotalQtyChange={this.handleTotalQty} />} />
        <Route path="/newProduct" component={NewProduct}/>
       
        <Route path="/orders" component={FoodOrders}/>
        <Route path="/products" component={Products} />
        <Route path="/order-success" component={OrderSuccess}/>
       
        <Route path="/check-out" component={CheckOut}/>
        <Route path="/admin" component={() => <FoodData productsData={this.state.productsData} 
        totalQty={this.state.totalQty} onTotalQtyChange={this.handleTotalQty} onCategoryChange={this.handleCategory} categoryData={this.state.categoryData}/>}/> 
        
        <Route path="/" component={() => <FoodData productsData={this.state.productsData} 
        totalQty={this.state.totalQty} onTotalQtyChange={this.handleTotalQty} onCategoryChange={this.handleCategory} categoryData={this.state.categoryData}/>}/> 
        
        </Switch>
            
      </React.Fragment>
     );
  }
}
 
export default App;
