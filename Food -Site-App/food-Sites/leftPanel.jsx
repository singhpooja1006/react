import React, { Component } from "react";
class LeftPanel extends Component {
  filterCategoryData = (category) => {
    let { categoryData } = { ...this.props };
    categoryData.selected = category;

    this.props.onFilter(categoryData);
  };
  render() {
    const { categoryData } = this.props;
    return (
      <React.Fragment>
        <div className="row border bg-success">
          <div
            className="col-12 m-3 h6"
            onClick={() => this.filterCategoryData("")}
          >
            All Categories
          </div>
        </div>
        {categoryData.categories.map((category) => (
          <div
            className="row border"
            onClick={() => this.filterCategoryData(category)}
          >
            <div className="col-12 m-2">
              <h6>{category}</h6>
            </div>
          </div>
        ))}
      </React.Fragment>
    );
  }
}

export default LeftPanel;
