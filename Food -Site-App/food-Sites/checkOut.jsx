import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
class CheckOut extends Component {
  state = {
    data: { name: "", line1: "", line2: "", city: "" },
    productsData: [],
  };
  componentDidMount() {
    this.setState({ productsData: this.props.location.state.productsData });
  }
  handleSubmit = async (e) => {
    e.preventDefault();
    let orderData = {};

    const selectedProduct = [
      ...this.state.productsData.map((prd) => ({
        prodKey: prd.id,
        quantity: prd.qty,
      })),
    ];

    orderData.name = this.state.data.name;
    orderData.line1 = this.state.data.line1;
    orderData.line2 = this.state.data.line2;
    orderData.city = this.state.data.city;

    orderData.items = selectedProduct.filter((prd) => prd.quantity > 0);
    orderData.totalPrice = this.state.productsData.reduce(
      (acc, curr) => acc + curr.qty * curr.price,
      0
    );

    const user = JSON.parse(localStorage.getItem("user"));

    let apiEndPoint = config.refreshTokenKeyApi;
    const { data: userToken } = await axios.post(apiEndPoint, {
      grant_type: "refresh_token",
      refresh_token: user.refreshToken,
    });
    apiEndPoint =
      config.apiEndPoint +
      "orders/" +
      userToken.user_id +
      ".json?auth=" +
      userToken.id_token;
    const { data: orderOutput } = await axios.post(apiEndPoint, orderData);
    window.location = "/order-success";
  };
  handleChange = (e) => {
    const formdata = { ...this.state.data };
    formdata[e.currentTarget.id] = e.currentTarget.value;
    this.setState({ data: formdata });
  };
  addPlaceOrder = () => {
    let orderData = {};
    orderData.name = this.state.data.name;
    orderData.line1 = this.state.data.line1;
    orderData.line2 = this.state.data.line2;
    orderData.city = this.state.data.city;
  };
  render() {
    const { data, productsData } = this.state;
    return (
      <div className="container-fluid">
        <div className="row ml-1">
          <div className="col-6">
            <div className="row ml-1">
              <h2>Shipping</h2>
            </div>
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input
                  value={data.name}
                  onChange={this.handleChange}
                  type="text"
                  id="name"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label htmlFor="line1">Address</label>
                <input
                  value={data.line1}
                  onChange={this.handleChange}
                  type="text"
                  id="line1"
                  placeholder="Line1"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <input
                  value={data.line2}
                  onChange={this.handleChange}
                  type="text"
                  id="line2"
                  placeholder="Line2"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label htmlFor="city">City</label>
                <input
                  value={data.city}
                  onChange={this.handleChange}
                  type="text"
                  id="city"
                  className="form-control"
                />
              </div>
              <button className="btn btn-success" type="submit">
                Place Order
              </button>
            </form>
          </div>
          <div className="col-5 ml-2 mt-5">
            <div className="row border">
              <div className="col-12">
                <h4>Order Summary</h4>
              </div>
              <div className="col-12 mt-2">
                <h5>
                  You have{" "}
                  {productsData.reduce((acc, curr) => acc + curr.qty, 0)} items
                  in your cart
                </h5>{" "}
              </div>
              <div className="col-12 mt-2">
                <hr />
              </div>
              {productsData.map((product) =>
                product.qty > 0 ? (
                  <React.Fragment>
                    <div className="col-8 h6">
                      {product.qty} X {product.name}
                    </div>
                    <div className="col-3 text-right h6">${product.price}</div>
                    <div className="col-12 border-bottom mt-2 mb-2"></div>
                  </React.Fragment>
                ) : (
                  ""
                )
              )}
              <div className="col-12 mt-2">
                <hr />
              </div>
              <div className="col-8 h5 mt-3">Total</div>
              <div className="col-3 text-right h5 mt-3">
                $
                {productsData.reduce(
                  (acc, curr) => acc + curr.qty * curr.price,
                  0
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CheckOut;
