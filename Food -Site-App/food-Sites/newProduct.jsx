import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
class NewProduct extends Component {
  state = {
    data: { name: "", price: "", category: "", imgLink: "" },
    categories: [
      "Bread",
      "Dairy",
      "Fruits",
      "Seasoning and Spices",
      "Vegetables",
    ],
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const { data } = this.state;
      const user = JSON.parse(localStorage.getItem("user"));
      let apiEndPoint = config.refreshTokenKeyApi;
      const { data: userToken } = await axios.post(apiEndPoint, {
        grant_type: "refresh_token",
        refresh_token: user.refreshToken,
      });
      apiEndPoint =
        config.apiEndPoint + "products.json?auth=" + userToken.id_token;
      console.log("apiEndPoint", apiEndPoint);
      await axios.post(apiEndPoint, {
        category: data.category,
        imgLink: data.imgLink,
        name: data.name,
        price: data.price,
      });
      window.location = "/products";
    } catch (ex) {}
  };

  handleChange = (e) => {
    const formdata = { ...this.state.data };
    formdata[e.currentTarget.id] = e.currentTarget.value;
    this.setState({ data: formdata });
  };
  render() {
    const { data, categories } = this.state;
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="name">Title</label>
            <input
              value={data.name}
              onChange={this.handleChange}
              type="text"
              name="name"
              id="name"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label htmlFor="price">Price</label>
            <input
              value={data.price}
              onChange={this.handleChange}
              type="number"
              id="price"
              name="price"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label htmlFor="category">Category</label>
            <select
              value={data.category}
              onChange={this.handleChange}
              id="category"
              name="category"
              className="form-control"
            >
              <option>Selected category</option>
              {categories.map((category) => (
                <option key={category}>{category}</option>
              ))}
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="imgLink">Image Url</label>
            <input
              value={data.imgLink}
              onChange={this.handleChange}
              type="text"
              id="imgLink"
              name="imgLink"
              className="form-control"
            />
          </div>
          <button className="btn btn-success" type="submit">
            Create
          </button>
        </form>
      </div>
    );
  }
}

export default NewProduct;
