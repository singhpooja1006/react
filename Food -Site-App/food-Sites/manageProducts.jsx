import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
import EditProducts from "./editProducts";
class Products extends Component {
  state = {
    searchText: "",
    productsData: [],
    editProductIndex: -1,
  };
  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));

    const { data: products } = await axios.get(
      config.apiEndPoint + "products.json"
    );
    let productsData = [];
    for (const [key, value] of Object.entries(products)) {
      let product = { ...value };
      product.id = key;
      product.qty = 0;
      productsData.push(product);
    }
    this.setState({
      productsData,
      user,
    });
    console.log("Props");
  }

  handleChange = (e) => {
    const { currentTarget: input } = e;
    this.setState({ searchText: input.value });
  };

  handleKeyDown = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      const searchText = this.state.searchText;
      if (searchText === "") {
        this.setState({ productsData: this.state.productsData });
      } else {
        let filteredProductsData = [
          ...this.state.productsData.filter(
            (product) =>
              product.name.toUpperCase() === this.state.searchText.toUpperCase()
          ),
        ];
        this.setState({ productsData: filteredProductsData });
      }
    }
  };

  addNewProduct = () => {
    window.location = "/newProduct";
  };
  editProducts = (index) => {
    this.setState({ editProductIndex: index });
  };
  handleDeleteProduct = async () => {
    const user = JSON.parse(localStorage.getItem("user"));
    let apiBaseURL =
      config.apiEndPoint +
      "products/" +
      this.state.productsData[this.state.editProductIndex].id +
      ".json?auth=" +
      user.idToken;
    console.log(apiBaseURL);
    const { data: result } = await axios.delete(apiBaseURL);
    alert(result);
    window.location = "/";
  };
  render() {
    return (
      <div className="container-fluid">
        {this.state.editProductIndex === -1
          ? this.renderAllProduct()
          : this.renderEditProduct()}
      </div>
    );
  }
  renderAllProduct() {
    const { productsData } = this.state;
    return (
      <React.Fragment>
        <div className="row ml-4">
          <button
            className="btn btn-success"
            onClick={() => this.addNewProduct()}
          >
            New Product
          </button>
        </div>
        <input
          value={this.state.searchText}
          onChange={this.handleChange}
          onKeyDown={this.handleKeyDown}
          name="searchText"
          type="text"
          className="form-control col-12 mt-2"
          placeholder="Search"
        />
        <div className="row border mt-2 ml-2 mr-2">
          <div className="col-1 border"></div>
          <div className="col-4 border">Name</div>
          <div className="col-4 border">Price</div>
          <div className="col-3 border"></div>
        </div>
        {productsData.map((product, index) => (
          <div className="row border  ml-2 mr-2">
            <div className="col-1 border">{index + 1}</div>
            <div className="col-4 border">{product.name}</div>
            <div className="col-4 border">${product.price}</div>
            <div
              className="col-3 border text-primary"
              onClick={() => this.editProducts(index)}
            >
              Edit
            </div>
          </div>
        ))}
      </React.Fragment>
    );
  }
  renderEditProduct() {
    const product = this.state.productsData[this.state.editProductIndex];
    return (
      <EditProducts product={product} onDelete={this.handleDeleteProduct} />
    );
  }
}

export default Products;
