import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
import queryString from "query-string";
import LeftPanel from "./leftPanel";
class Students extends Component {
  state = {
    studnetsData: [],
    pageInfo: {},
    sortBy: "",
    maths: [
      { name: "Excellent", isSelected: false },
      { name: "Good", isSelected: false },
      { name: "Average", isSelected: false },
      { name: "Poor", isSelected: false },
    ],
    english: [
      { name: "Excellent", isSelected: false },
      { name: "Good", isSelected: false },
      { name: "Average", isSelected: false },
      { name: "Poor", isSelected: false },
    ],
    computers: [
      { name: "Excellent", isSelected: false },
      { name: "Good", isSelected: false },
      { name: "Average", isSelected: false },
      { name: "Poor", isSelected: false },
    ],

    sections: [
      { name: "A", isSelected: false },
      { name: "B", isSelected: false },
      { name: "C", isSelected: false },
      { name: "D", isSelected: false },
    ],
  };
  async componentDidMount() {
    let apiEndpoint = config.apiEndPoint + "/students";
    const { data: studnetsData } = await http.get(apiEndpoint);
    this.setState({
      studnetsData: studnetsData.data,
      pageInfo: studnetsData.pageInfo,
    });
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      let apiEndpoint =
        config.apiEndPoint + "/students" + this.props.location.search;
      console.log("URL", apiEndpoint);
      const { data: studnetsData } = await http.get(apiEndpoint);
      this.setState({
        studnetsData: studnetsData.data,
        pageInfo: studnetsData.pageInfo,
      });
    }
  }
  handleEditDetails = (id) => {
    window.location = "/students/" + id;
  };
  handleDeleteDetails = async (id) => {
    const originalstudnetsData = this.state.studnetsData;
    try {
      const apiEndpoint = config.apiEndPoint + "/students";
      await http.delete(apiEndpoint + "/" + id);
      const { data: studnetsData } = await http.get(apiEndpoint);
      this.setState({
        studnetsData: studnetsData.data,
        pageInfo: studnetsData.pageInfo,
      });
    } catch (ex) {
      if (ex.response && ex.response.status !== 404) {
        this.setState({ studnetsData: originalstudnetsData });
      }
    }
  };
  calURL = (params, maths, english, computers, sections, sortBy, page) => {
    let path = "/students";
    params = this.addToParams(params, "maths", maths);
    params = this.addToParams(params, "english", english);
    params = this.addToParams(params, "computers", computers);
    params = this.addToParams(params, "sections", sections);
    params = this.addToParams(params, "sortBy", sortBy);
    params = this.addToParams(params, "page", page);

    this.props.history.push({
      pathname: path,
      search: params,
    });
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  handleSortBy = (sortBy) => {
    this.setState({ sortBy });
    let { maths, english, computers, sections } = queryString.parse(
      this.props.location.search
    );
    this.calURL("", maths, english, computers, sections, sortBy, 1);
  };
  pageNavigate = (value) => {
    const { pageInfo } = { ...this.state };
    let { maths, english, computers, sections, sortBy } = queryString.parse(
      this.props.location.search
    );
    let currPage = pageInfo.pageNumber + value;
    this.calURL("", maths, english, computers, sections, sortBy, currPage);
  };

  handleCheckBoxEvent = (maths, english, computers, sections) => {
    this.setState({ maths, english, computers, sections });
    let mathsString = this.buildQueryString(maths);
    let englishString = this.buildQueryString(english);
    let computersString = this.buildQueryString(computers);
    let sectionString = this.buildQueryString(sections);
    console.log("Section", sectionString);
    this.calURL(
      "",
      mathsString,
      englishString,
      computersString,
      sectionString,
      this.state.sortBy,
      1
    );
  };
  buildQueryString(optionsData) {
    let filterData = optionsData.filter((n1) => n1.isSelected);
    let arrayData = filterData.map((n1) => n1.name);
    return arrayData.join(",");
  }

  render() {
    return (
      <div className="container">
        <div className="row m-1">
          <div className="col-2 mr-1">
            <LeftPanel
              mathsData={this.state.maths}
              englishData={this.state.english}
              computersData={this.state.computers}
              sectionsData={this.state.sections}
              onCheckBoxButtonEvent={this.handleCheckBoxEvent}
            />
          </div>
          <div className="col-8 ml-2">{this.renderstudnetsData()}</div>
        </div>
      </div>
    );
  }
  renderstudnetsData = () => {
    const { studnetsData, pageInfo } = this.state;
    if (studnetsData) {
      let { page } = queryString.parse(this.props.location.search);
      page = page ? +page : 1;
      return (
        <React.Fragment>
          <div className="row">
            {pageInfo.pageNumber === 1
              ? 1
              : pageInfo.numOfItems * pageInfo.pageNumber -
                pageInfo.numOfItems +
                1}{" "}
            to{" "}
            {pageInfo.numOfItems * pageInfo.pageNumber < pageInfo.totalItemCount
              ? pageInfo.numOfItems * pageInfo.pageNumber
              : pageInfo.totalItemCount}{" "}
            of {pageInfo.totalItemCount}
          </div>
          <div className="row border bg-dark text-center text-white mt-2">
            <div
              className="col-1 border"
              onClick={() => this.handleSortBy("id")}
            >
              ID
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("name")}
            >
              Name
            </div>
            <div
              className="col-1 border"
              onClick={() => this.handleSortBy("section")}
            >
              Sec
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("maths")}
            >
              Maths
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("english")}
            >
              English
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("computers")}
            >
              Computers
            </div>
            <div className="col-2 border"></div>
          </div>
          {studnetsData.map((student) => (
            <div className="row border text-center bg-light" key={student.id}>
              <div className="col-1 border">{student.id}</div>
              <div className="col-2 border">{student.name}</div>
              <div className="col-1 border">{student.section}</div>
              <div className="col-2 border">{student.maths}</div>
              <div className="col-2 border">{student.english}</div>
              <div className="col-2 border">{student.computers}</div>
              <div className="col-1 border">
                <i
                  className="fa fa-pencil-square-o"
                  aria-hidden="true"
                  onClick={() => this.handleEditDetails(student.id)}
                ></i>
              </div>
              <div className="col-1 border">
                <i
                  className="fa fa-trash"
                  aria-hidden="true"
                  onClick={() => this.handleDeleteDetails(student.id)}
                ></i>
              </div>
            </div>
          ))}
          <div className="row m-1">
            <div>
              {page > 1 ? (
                <button
                  className="btn btn-primary m-1"
                  onClick={() => this.pageNavigate(-1)}
                >
                  Previous
                </button>
              ) : (
                ""
              )}
            </div>
            <div>
              {page < pageInfo.numberOfPages ? (
                <button
                  className="btn btn-primary m-1"
                  onClick={() => this.pageNavigate(1)}
                >
                  Next
                </button>
              ) : (
                ""
              )}
            </div>
          </div>
        </React.Fragment>
      );
    }
  };
}
export default Students;
