import React, { Component } from "react";
class LeftPanel extends Component {
  state = {};
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let { mathsData, englishData, computersData, sectionsData } = {
      ...this.props,
    };
    console.log(input.id);
    if (input.id.includes("maths")) {
      let maths = mathsData.find((n1) => n1.name === input.name);
      if (maths) maths.isSelected = input.checked;
    }
    if (input.id.includes("english")) {
      let english = englishData.find((n1) => n1.name === input.name);
      if (english) english.isSelected = input.checked;
    }
    if (input.id.includes("computers")) {
      let computers = computersData.find((n1) => n1.name === input.name);
      if (computers) computers.isSelected = input.checked;
    }
    if (input.id.includes("maths")) {
      let maths = mathsData.find((n1) => n1.name === input.name);
      if (maths) maths.isSelected = input.checked;
    }
    if (input.id.includes("section")) {
      let section = sectionsData.find((n1) => n1.name === input.name);
      if (section) section.isSelected = input.checked;
    }
    console.log("handleChange", sectionsData);
    this.props.onCheckBoxButtonEvent(
      mathsData,
      englishData,
      computersData,
      sectionsData
    );
  };
  render() {
    const { mathsData, englishData, computersData, sectionsData } = this.props;
    return (
      <React.Fragment>
        <div className="row border ">
          <div className="col-1 m-3 h6">Options</div>
        </div>
        <div className="row border ">
          <div className="col-12 m-3">
            <h6>Section</h6>
            {sectionsData.map((section) => (
              <div className="form-check mt-1">
                <input
                  value={section.name}
                  onChange={this.handleChange}
                  id={"section" + section.name}
                  name={section.name}
                  type="checkbox"
                  checked={section.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={section.name}>
                  {section.name}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border ">
          <div className="col-12 m-3">
            <h6>Maths</h6>
            {mathsData.map((maths) => (
              <div className="form-check mt-1">
                <input
                  value={maths.name}
                  onChange={this.handleChange}
                  id={"maths" + maths.name}
                  name={maths.name}
                  type="checkbox"
                  checked={maths.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={maths.name}>
                  {maths.name}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border ">
          <div className="col-12 m-3">
            <h6>English</h6>
            {englishData.map((english) => (
              <div className="form-check mt-1">
                <input
                  value={english.name}
                  onChange={this.handleChange}
                  id={"english" + english.name}
                  name={english.name}
                  type="checkbox"
                  checked={english.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={english.name}>
                  {english.name}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border ">
          <div className="col-12 m-3">
            <h6>Computers</h6>
            {computersData.map((computers) => (
              <div className="form-check mt-1">
                <input
                  value={computers.name}
                  onChange={this.handleChange}
                  id={"computers" + computers.name}
                  name={computers.name}
                  type="checkbox"
                  checked={computers.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={computers.name}>
                  {computers.name}
                </label>
              </div>
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPanel;
