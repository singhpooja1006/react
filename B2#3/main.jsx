import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import NavBar from "./navBar";
import Studnets from "./students";
import AddNewStudent from "./newStudent";

class MainComp extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <NavBar />
        <Switch>
          <Route path="/new" component={AddNewStudent} />
          <Route path="/students/:id" component={AddNewStudent} />
          <Route path="/students" component={Studnets} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default MainComp;
