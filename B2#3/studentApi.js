var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
    next();
});

const port  = 2410;
app.listen(port,()=>console.log("Listening on port : ",port))
let studentsData =[{id: "ST01",name:"Vishal", section:"A", maths:89, english:75, computers:56},
{id: "ST02",name:"Meghna", section:"B", maths:90, english:65, computers:70},
{id: "ST03",name:"Rohit Varma", section:"A", maths:81, english:89, computers:90},
{id: "ST04",name:"Kunal Singh", section:"C", maths:58, english:75, computers:55},
{id: "ST05",name:"Nutan Singh", section:"D", maths:76, english:56, computers:78},
{id: "ST06",name:"Deepak Gupta", section:"B", maths:37, english:54, computers:67},
{id: "ST07",name:"Alok Sharma", section:"C", maths:76, english:56, computers:90},
{id: "ST08",name:"Nupur Gupta", section:"A", maths:56, english:56, computers:67},
{id: "ST09",name:"Alok Singh", section:"A", maths:72, english:75, computers:56},
{id: "ST10",name:"Vishal Kolhi", section:"B", maths:45, english:65, computers:60},
{id: "ST11",name:"Anushka", section:"D", maths:55, english:56, computers:60},
{id: "ST12",name:"Sachin Sharma", section:"D", maths:78, english:48, computers:64},
{id: "ST13",name:"Anup Singh", section:"C", maths:85, english:58, computers:70},
{id: "ST14",name:"Vishal Nehra", section:"C", maths:80, english:70, computers:60},
{id: "ST15",name:"Shreya", section:"A", maths:66, english:47, computers:87},
{id: "ST16",name:"Vinod Singh", section:"A", maths:60, english:60, computers:60},
{id: "ST17",name:"Ajay Kumar", section:"B", maths:68, english:59, computers:75},
{id: "ST18",name:"Manmohan", section:"B", maths:81, english:83, computers:70},
{id: "ST19",name:"Ghanshyam", section:"C", maths:74, english:76, computers:60},
{id: "ST20",name:"Anupama", section:"D", maths:39, english:37, computers:50},
{id: "ST21",name:"Brijesh", section:"A", maths:92, english:93, computers:91},
{id: "ST22",name:"Deepak Kohli", section:"D", maths:56, english:56, computers:70},
{id: "ST23",name:"Rishu Kumari", section:"B", maths:76, english:46, computers:67},
{id: "ST24",name:"Pradeep", section:"C", maths:88, english:86, computers:76},
{id: "ST25",name:"Swati Kumari", section:"A", maths:67, english:88, computers:78},
{id: "ST26",name:"Preeti Kumari", section:"B", maths:78, english:56, computers:87},
{id: "ST27",name:"Vipin Kumar", section:"B", maths:74, english:83, computers:75},
{id: "ST28",name:"Pankaj Singh", section:"D", maths:67, english:56, computers:49},
{id: "ST29",name:"Priyanka", section:"C", maths:57, english:56, computers:70},
{id: "ST30",name:"Kumal Khemu", section:"A", maths:63, english:59, computers:80}];

function pagination(obj, page) {
    var resArr = obj;
    resArr = resArr.slice(page * 6 - 6, page * 6);
    return resArr;
  }
app.get("/students",function(req,res) { 
    let {sortBy,sections,maths,english,computers} = req.query;
    let page = +req.query.page;
    page = isNaN(page) ? 1 : page;
    console.log("in get request for /students: ");
    let result = [...studentsData];
    if(sections){
        let sectionList = sections.split(",");
        result = result.filter((data) => sectionList.find(section => section === data.section)) ;
    }
    if(maths){
        let mathsList = maths.split(",");
        let ressArr = [];
        let dataFound = false;
        for(let index=0;index < mathsList.length ;index++){
            let objs;
            switch (mathsList[index]){
                case "Excellent":
                    objs = result.filter((data) => data.maths >= 80) ;
                    break;
                case "Good":
                    objs = result.filter((data) => data.maths >= 60 && data.maths < 80) ;
                    break;
                case "Average":
                    objs = result.filter((data) => data.maths >= 40 && data.maths < 60) ;
                    break;
                case "Poor":
                    objs = result.filter((data) => data.maths < 40) ;
                    break;
            }
            if(objs){
                objs.forEach(obj => ressArr.push(obj));
                dataFound = true;
            }

        }
        if(dataFound === true){
            result = [...ressArr]
        }
    }
    if(english){
        let englishList = english.split(",");
        let ressArr = [];
        let dataFound = false;
        for(let index=0;index < englishList.length ;index++){
            let objs;
            switch (englishList[index]){
                case "Excellent":
                    objs = result.filter((data) => data.english >= 80) ;
                    break;
                case "Good":
                    objs = result.filter((data) => data.english >= 60 && data.english < 80) ;
                    break;
                case "Average":
                    objs = result.filter((data) => data.english >= 40 && data.english < 60) ;
                    break;
                case "Poor":
                    objs = result.filter((data) => data.english < 40) ;
                    break;
            }
            if(objs){
                objs.forEach(obj => ressArr.push(obj));
                dataFound = true;
            }

        }
        if(dataFound === true){
            result = [...ressArr]
        }
    }
    if(computers){
        let computersList = computers.split(",");
        let ressArr = [];
        let dataFound = false;
        for(let index=0;index < computersList.length ;index++){
            let objs;
            switch (computersList[index]){
                case "Excellent":
                    objs = result.filter((data) => data.computers >= 80) ;
                    break;
                case "Good":
                    objs = result.filter((data) => data.computers >= 60 && data.computers < 80) ;
                    break;
                case "Average":
                    objs = result.filter((data) => data.computers >= 40 && data.computers < 60) ;
                    break;
                case "Poor":
                    objs = result.filter((data) => data.computers < 40) ;
                    break;
            }
            if(objs){
                objs.forEach(obj => ressArr.push(obj));
                dataFound = true;
            }

        }
        if(dataFound === true){
            result = [...ressArr]
        }
    }
    if(sortBy === "id"){
        result = result.sort((student1,student2) => 
            (student1.id > student2.id)  ? 
                1
            : ((student1.id < student2.id)  ? 
              -1 : 0)
        );
    }
    if(sortBy === "name"){
        result = result.sort((student1,student2) => 
            (student1.name > student2.name)  ? 
                1
            : ((student1.name < student2.name)  ? 
              -1 : 0)
        );
    }
    if(sortBy === "section"){
        result = result.sort((student1,student2) => 
        (student1.section > student2.section)  ? 
            1
        : ((student1.section < student2.section)  ? 
          -1 : 0)
    );
    }
    if(sortBy === "maths"){
        result = result.sort((student1,student2) => student1.maths - student2.maths);
    }
    if(sortBy === "english"){
        result = result.sort((student1,student2) => student1.english - student2.english);
    }
    if(sortBy === "computers"){
        result = result.sort((student1,student2) => student1.computers - student2.computers);
    }
    respArr = pagination(result, page);
    let len = result.length;
    let quo = Math.floor(len / 6);
    let rem = len % 6;
    let extra = rem === 0 ? 0 : 1;
    let numofpages = quo + extra;
    let pageInfo = {
        pageNumber: page,
        numberOfPages: numofpages,
        numOfItems: 6,
        totalItemCount: result.length,
    };
    res.json({
        data: respArr,
        pageInfo: pageInfo,
      });
});

app.get("/students/:id",function(req,res) { 
    let id = req.params.id;
    console.log("in get request for /students/:id : ",id);
    let index = studentsData.findIndex((data)=> data.id === id);
    if (index === -1) res.status(404).send(id + " Not Found");
    else {
        res.send(studentsData[index]);
    }
});

app.post("/students",function(req,res) { 
    console.log("in post request for /students ");

    let name = req.body.name;
    let id = req.body.id;
    let index = studentsData.findIndex((data)=> data.name === name || data.id === id);
    if(index !== -1) res.status(500).send("Student with given ID or Name already exist");
    else{
        studentsData.push(req.body)
      res.send(req.body);
    }
    
});

app.put("/students/:id",function(req,res) { 
    let id = req.params.id;
    console.log("in put request for /students/:id ", id);
    let index = studentsData.findIndex((data)=> data.id === id);
    if (index === -1) res.status(404).send(id + " Not Found");
    else {
        studentsData[index] = req.body;
        res.send(res.body);
    }
});
app.delete("/students/:id",function(req,res) { 
    let id = req.params.id;
    console.log("in delete request for /students/:id ", id);
    let studentIndex = studentsData.findIndex((data)=> data.id === id);
    const student = studentsData[studentIndex];
    if (studentIndex === -1) 
        return res.status(404).send("The student with the given Id was not found.");
        studentsData.splice(studentIndex,1);
    res.send(student);
});