import { Alert } from "bootstrap";
import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
class AddNewMobile extends Component {
  state = {
    data: {
      id: "",
      name: "",
      section: "",
      maths: "",
      english: "",
      computers: "",
    },
    sections: ["A", "B", "C", "D"],

    errors: {},
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { data } = this.state;
    data[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data, errors });
  };
  async componentDidMount() {
    const { id } = this.props.match.params;
    if (id) {
      const apiEndpoint = config.apiEndPoint + "/students/" + id;
      const { data } = await http.get(apiEndpoint);
      this.setState({ data });
    }
  }
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "id":
        if (!e.currentTarget.value.trim()) return "ID is required";
        else if (e.currentTarget.value.trim().length > 4)
          return "Id length should not be more than 4 Character";
        break;
      case "name":
        if (!e.currentTarget.value.trim()) return "Name is required";
        else if (e.currentTarget.value.trim().length < 6)
          return "Name should be minimum 6 character";
        break;
      case "section":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select Section"
        )
          return "Section is required";
        break;
      case "maths":
        if (!e.currentTarget.value.trim()) return "Maths Number is required";
        else if (+e.currentTarget.value <= 0)
          return "Maths Number is grater than 0";
        break;
      case "english":
        if (!e.currentTarget.value.trim()) return "English Number is required";
        else if (+e.currentTarget.value <= 0)
          return "English Number is grater than 0";
        break;
      case "computers":
        if (!e.currentTarget.value.trim())
          return "Computers Number is required";
        else if (+e.currentTarget.value <= 0)
          return "Computers Number is grater than 0";
        break;

      default:
        break;
    }
    return "";
  };
  validate = () => {
    let errs = {};
    console.log("len", this.state.data.id.length);
    if (!this.state.data.id.trim()) errs.id = "ID is required";
    else if (this.state.data.id.trim().length > 4)
      errs.id = "Id length should not be more than 4 Character";
    else if (this.state.data.id.trim().length === 4) {
      console.log(this.state.data.id.trim().substring(0, 2));
      var upperletters = /^[A-Z]+$/;
      if (
        Number.isInteger(+this.state.data.id.trim().substring(0, 2)) === true
      ) {
        errs.id = "First two charater of ID should be in Character";
      }
      if (!this.state.data.id.trim().substring(0, 2).match(upperletters)) {
        errs.id = "First two charater of ID should be in Upper Case";
      }
      if (
        Number.isInteger(+this.state.data.id.trim().substring(2, 4)) === false
      ) {
        errs.id = "last two charater of ID should be in Number";
      }
    }
    if (!this.state.data.name.trim()) errs.name = "Name is required";
    else if (this.state.data.name.trim().length < 6)
      return "Name should be minimum 6 character";
    if (
      !this.state.data.section.trim() ||
      this.state.data.section === "Select Section"
    )
      errs.section = "Section is required";

    if (+this.state.data.maths <= 0)
      errs.maths = "Maths Number is grater than 0";
    if (+this.state.data.english <= 0)
      errs.english = "English Number is grater than 0";
    if (+this.state.data.computers <= 0)
      errs.computers = "Computers Number is grater than 0";
    return errs;
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    const { data } = this.state;
    try {
      const { id } = this.props.match.params;
      if (id) {
        const apiEndpoint = config.apiEndPoint + "/students/" + id;
        await http.put(apiEndpoint, data);
      } else {
        const apiEndpoint = config.apiEndPoint + "/students";
        await http.post(apiEndpoint, data);
      }

      window.location = "/students";
    } catch (ex) {
      console.log("Ex", ex.response);
      if (ex.response.status >= 400 && ex.response.status <= 500) {
        alert(ex.response.data + " , Please provide correct Data");
      }
    }
  };
  render() {
    const { data, sections, errors } = this.state;
    const { id } = this.props.match.params;
    return (
      <div className="container">
        <h2>Student Details</h2>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="id">Student ID</label>
            <input
              value={data.id}
              onChange={this.handleChange}
              type="text"
              id="id"
              name="id"
              placeholder="Enter Student ID"
              className="form-control"
              disabled={id ? true : false}
            />
            {errors.id ? <div className="text-danger">{errors.id}</div> : ""}
          </div>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              value={data.name}
              onChange={this.handleChange}
              type="text"
              id="name"
              name="name"
              placeholder="Enter Name"
              className="form-control"
            />
            {errors.name ? (
              <div className="text-danger">{errors.name}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="section">Section</label>
            <select
              value={data.section}
              onChange={this.handleChange}
              id="section"
              name="section"
              className="browser-default custom-select mb-1"
            >
              <option>Select Section</option>
              {sections.map((section) => (
                <option key={section}>{section}</option>
              ))}
            </select>
            {errors.section ? (
              <div className="text-danger">{errors.section}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="maths">Maths</label>
            <input
              value={data.maths}
              onChange={this.handleChange}
              type="number"
              id="maths"
              name="maths"
              placeholder="Enter Maths Number"
              className="form-control"
            />
            {errors.maths ? (
              <div className="text-danger">{errors.maths}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="english">English</label>
            <input
              value={data.english}
              onChange={this.handleChange}
              type="number"
              id="english"
              name="english"
              placeholder="Enter English Number"
              className="form-control"
            />
            {errors.english ? (
              <div className="text-danger">{errors.english}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="computers">Computers</label>
            <input
              value={data.computers}
              onChange={this.handleChange}
              type="number"
              id="computers"
              name="computers"
              placeholder="Enter Computers Number"
              className="form-control"
            />
            {errors.computers ? (
              <div className="text-danger">{errors.computers}</div>
            ) : (
              ""
            )}
          </div>

          <button className="btn btn-primary">Submit</button>
        </form>
      </div>
    );
  }
}

export default AddNewMobile;
