var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
    next();
});
const port  = 2410;
app.listen(port,()=>console.log("Listening on port : ",port))

let dataLists = [
{"id":"H1","name":"Hotel 69","location":"Saligao, Goa","city" : "Goa","prevPrice":1250,"price":1138,"yatraOffer":111,"Couple Friendly": true,
"Free Cancellation":true, "Free WiFi":true, 
"Swimming pool":true,
"fullLoc":"7/32, Salmona Saligaon Bardez, Near Sharvani Temple, Goa, 403511",
"tripadvisorRating":"3.5/5 Very Good","rating":"★★★",
"payment":"UPI",
"CHECKIN":"12:00 PM",
"CHECKOUT": "11:00 AM",
"service":"Superior Service 4.0/5",
"value":"Superb Value 4.0/5",
"imgList":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1490339922/Hotel/Goa/00083887/Deluxe_Room_(2)_fSJVl0.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1490340350/Hotel/Goa/00083887/Super_Deluxe_Room_tWktbI.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1542355392/Hotel/00083887/DSC_1059_z4gJDD.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1542355446/Hotel/00083887/DSC_1061_pgrW2e.jpg"],
"roomlist" : [{"type" : "Classic Room","bedSize":"1 King Bed","roomId":"r1","roomtype":[{"type":"Classic Room only","prevPrice":2000,"price":1776,"yatraOffer":224, "eCash" : 250},
{"type":"Classic Room With Breakfast","prevPrice":4800,"price":4260,"yatraOffer":538, "eCash" : 300}], 
"imgDetail":
["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542355446/Hotel/00083887/DSC_1061_pgrW2e.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542355392/Hotel/00083887/DSC_1059_z4gJDD.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542355496/Hotel/00083887/DSC_1065_yZWsju.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1490340350/Hotel/Goa/00083887/Super_Deluxe_Room_tWktbI.jpg"]},
{"type" : "Suite","roomId":"r2","bedSize":"Extra Bed","roomtype":[{"type":"Suite Room only","prevPrice":6000,"price":5328,"yatraOffer":672, "eCash" : 300},{"type":"Suite with Breakfast","prevPrice":6800,"price":6038,"yatraOffer":762, "eCash" : 500}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v0820502131/Hotel/00083887/2c44aa26_a42e_4128_b596_093e7bfa7dd0_N8I9EB.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1967355167/Hotel/00083887/46dd5d02_02dd_4b0d_aed7_e9f2c40d9302_001_0H8j8N.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542355735/Hotel/00083887/DSC_1026_n3quZ0.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542355695/Hotel/00083887/DSC_1030_NjWi0Y.jpg"]},
{"type" : "Deluxe Room","roomId":"r3","bedSize":"Double Bed","roomtype":[{"type":"Deluxe Room only","prevPrice":6000,"price":5328,"yatraOffer":672, "eCash" : 500},
{"type":"Deluxe Room with Breakfast","prevPrice":6800,"price":6038,"yatraOffer":762, "eCash" : 500}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v4916429217/Hotel/00083887/2e9c4bc5_1240_442b_9c47_0d8fb4a9dba2_w48NTb.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v8270866475/Hotel/00083887/5f60e48d_68dd_4cce_b137_2b58df88ec67_HALf56.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v8849616394/Hotel/00083887/9096c58d_7004_4a23_a274_e3f16ce5a3a4_RJoROP.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1490339922/Hotel/Goa/00083887/Deluxe_Room_(2)_fSJVl0.jpg"]}]},
{"id":"H11","name":"Neelams The Grand","location":"Calangute,Goa","city" : "Goa","prevPrice":6900,"price":4796,"yatraOffer":2111,"Couple Friendly": true,
"Free Cancellation":true, "Free WiFi":true, 
"Swimming pool":true,
"fullLoc":"Post Office Road ,Calangute, Bardez, Goa, 403516",
"tripadvisorRating":"3.5/5 Very Good","rating":"★★★★",
"payment":"UPI",
"CHECKIN":"2:00 PM",
"CHECKOUT": "10:00 AM",
"service":"Impressive Location 4.0/5",
"value":"Good Service 3.5/5",
"imgList":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1545908408/Hotel/New%20Delhi/00000732/Untitled-1_KsQLOH.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1575446355/Hotel/00000732/Sea_Shell_banquet_hall_[HDTV_(1080)]_aKUjTC.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1575446403/Hotel/00000732/Lobby_2_[HDTV_(1080)]_YMhT1C.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1575446437/Hotel/00000732/Grand_Pool_3_[HDTV_(1080)]_NIehZg.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1575446455/Hotel/00000732/Grand_Pool_1_[HDTV_(1080)]_6mHuVk.jpg"],
"roomlist" : [{"type" : "DELUXE ROOM-Non Refundable","bedSize":"Double Bed","roomtype":[{"type":"Deluxe Room Only - Non Refundable","prevPrice":6900,"price":4796,"yatraOffer":2104, "eCash" : 250},
{"type":"Deluxe Room-Bed & Breakfast Non Refundable","prevPrice":7200,"price":5004,"yatraOffer":2196, "eCash" : 300}], 
"imgDetail":
["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1575454897/Hotel/00000732/5M1A5703_[HDTV_(1080)]_aFb58R.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1575454909/Hotel/00000732/5M1A5706_[HDTV_(1080)]_aHGwdx.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1575454920/Hotel/00000732/5M1A5721_[HDTV_(1080)]_9iT55e.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1575454936/Hotel/00000732/5M1A5727_[HDTV_(1080)]_RyXHJ6.jpg"]},
{"type" : "Suite","roomId":"r2","bedSize":"Extra Bed","roomtype":[{"type":"Suite Room only","prevPrice":6000,"price":5328,"yatraOffer":672, "eCash" : 300},{"type":"Suite with Breakfast","prevPrice":6800,"price":6038,"yatraOffer":762, "eCash" : 500}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v0820502131/Hotel/00083887/2c44aa26_a42e_4128_b596_093e7bfa7dd0_N8I9EB.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1967355167/Hotel/00083887/46dd5d02_02dd_4b0d_aed7_e9f2c40d9302_001_0H8j8N.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542355735/Hotel/00083887/DSC_1026_n3quZ0.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542355695/Hotel/00083887/DSC_1030_NjWi0Y.jpg"]},
{"type" : "Premium Suite Room","bedSize":"Double Bed","roomtype":[{"type":"Premium Suite Room Only - Non Refundable","prevPrice":6000,"price":5328,"yatraOffer":672, "eCash" : 500},
{"type":"Premium Suite-Bed & Breakfast Non Refundable","prevPrice":6800,"price":6038,"yatraOffer":762, "eCash" : 500}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1575454636/Hotel/00000732/5M1A4678_[HDTV_(1080)]_QRPDU6.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1575454654/Hotel/00000732/5M1A4685_[HDTV_(1080)]_OTwvVp.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1575454715/Hotel/00000732/Grand_Premium_Suite_2_[HDTV_(1080)]_yprZQ9.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1575454741/Hotel/00000732/Grand_Premium_Suite_Room_1_[HDTV_(1080)]_C27p1M.jpg"]}]},
{"id":"H2","name":"Radisson Blu Plaza Delhi Airport","location":"Mahipalpur, New Delhi","city" : "New Delhi","prevPrice":5600,"price":4483,"yatraOffer":1117,"Couple Friendly": true,
"Free Cancellation":true, "Free WiFi":true, 
"Swimming pool":true,
"fullLoc":"Near Mahipalpur, Extension NH 8,, Opposite Intl Airport, New Delhi, 110037",
"tripadvisorRating":"4.5/5 Excellent","rating":"★★★★★",
"payment":"Credit Card","CHECKIN":"12:00 PM",
"CHECKOUT": "11:00 AM",
"service":"Wonderful Location 4.5/5",
"value":"Impressive Service 4.5/5",
"imgList":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1467127374/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Superior%20Double%20Room/Superior_Double_Room_RdZtwR.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1467127371/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Premium%20Double%20Room/Premium_Double_Room_UIcrw8.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1467127368/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/CLOSED/CLOSED_B78UVn.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1467127374/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Superior%20Double%20Room/Superior_Double_Room_RdZtwR.jpg"],
"roomlist" : [{"type" : "Deluxe Double Room","bedSize":"Double Bed","roomtype":[{"type":"Deluxe Double Room Only","prevPrice":7800,"price":6336,"yatraOffer":1464},
{"type":"Deluxe Double Room with breakfast","prevPrice":9876,"price":8995,"yatraOffer":4000}], 
"imgDetail":
["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1399621148/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Super%20Room.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1431775189/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Executive_Suite.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1399621099/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Living%20Room%201.jpg"]},

{"type" : "Business Room","bedSize":"Double Bed","roomtype":[{"type":"Business Room only","prevPrice":8000,"price":7149,"yatraOffer":1651},{"type":"Business Room Double with Breakfast ","prevPrice":9000,"price":8150,"yatraOffer":2000}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1570769539/Hotel/New%20Delhi/00000007/Suite_mt55If.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1399621068/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Lobby.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1399621140/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Premium%20Room.jpg"]},
{"type" : "Suite","roomId":"r2","bedSize":"Extra Bed","roomtype":[{"type":"Suite Room only","prevPrice":6000,"price":5328,"yatraOffer":672},{"type":"Suite with Breakfast","prevPrice":6800,"price":6038,"yatraOffer":762}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v0820502131/Hotel/00083887/2c44aa26_a42e_4128_b596_093e7bfa7dd0_N8I9EB.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1967355167/Hotel/00083887/46dd5d02_02dd_4b0d_aed7_e9f2c40d9302_001_0H8j8N.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542355735/Hotel/00083887/DSC_1026_n3quZ0.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542355695/Hotel/00083887/DSC_1030_NjWi0Y.jpg"]}]},
{"id":"H3","name":"Ramada Encore Bangalore Domlur","location":"Domlur,Bangalore","city" : "Bangalore","prevPrice":5320,"price":4254,"yatraOffer":1066,"Couple Friendly": true,
"Free Cancellation":true, "Free WiFi":true, 
"fullLoc":"282-285, Amarjyothi Layout, Domlur 1st stage , Bangalore, 560071",
"tripadvisorRating":"4.5/5 Excellent","rating":"★★★★",
"payment":"Debit Card","CHECKIN":"12:00 PM",
"CHECKOUT": "11:00 AM",
"service":"Wonderful Location 4.5/5",
"value":"Impressive Service 4.5/5",
"imgList":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1431950261/Domestic%20Hotels/Hotels_Bangalore/Ramada%20Encore/Cozy_Bedroom.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1431950263/Domestic%20Hotels/Hotels_Bangalore/Ramada%20Encore/Bedroom_5.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1431950258/Domestic%20Hotels/Hotels_Bangalore/Ramada%20Encore/Bedroom_2.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1431950260/Domestic%20Hotels/Hotels_Bangalore/Ramada%20Encore/Restaurant.jpg"],
"roomlist" : [{"type" : "Deluxe Twin Bed room","bedSize":"1 King Bed","roomtype":[{"type":"Deluxe Twin Bed Double Room Only","prevPrice":4744,"price":3656,"yatraOffer":1088},
{"type":"Deluxe Twin Bed Double Room","prevPrice":5545,"price":4271,"yatraOffer":1274}], 
"imgDetail":
["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1402376205/Domestic%20Hotels/Hotels_Bangalore/Ramada%20Encore/Executive%20Room.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509016275/Hotel/00017090/13_JJA2GK.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509016345/Hotel/00017090/14_3FTtZq.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1467525593/Domestic%20Hotels/Hotels_Bangalore/Ramada%20Encore%20Bangalore%20Domlur/Executive%20Double/Executive_Double_F5vLpz.jpg"]},
{"type" : "Deluxe Queen Bed room","bedSize":"1 King Bed","roomtype":[{"type":"Deluxe Queen Bed Double Room Only","prevPrice":4744,"price":3040,"yatraOffer":1704},{"type":"Deluxe Queen Bed Double Room","prevPrice":5545,"price":3552,"yatraOffer":1993}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509008090/Hotel/00017090/12_Lt2nWS.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509016155/Hotel/00017090/9_1sJRU0.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1467525591/Domestic%20Hotels/Hotels_Bangalore/Ramada%20Encore%20Bangalore%20Domlur/Executive%20Single/Executive_Single_RhMcEy.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509016215/Hotel/00017090/8_Z6ap5E.jpg"]},
{"type" : "Business Room","bedSize":"Double Bed","roomtype":[{"type":"Business Room only","prevPrice":8000,"price":7149,"yatraOffer":1651},{"type":"Business Room Double with Breakfast ","prevPrice":9000,"price":8150,"yatraOffer":2000}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1570769539/Hotel/New%20Delhi/00000007/Suite_mt55If.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1399621068/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Lobby.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1399621140/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Premium%20Room.jpg"]}
]},
{"id":"H7","name":"Park Inn by Radisson New Delhi Lajpat Nagar","location":"Lajpat Nagar,New Delhi","city" : "New Delhi","prevPrice":6748,"price":4606,"yatraOffer":2142,"Couple Friendly": true,
"Free Cancellation":true, "Free WiFi":true, 
"Swimming pool":true,
"fullLoc":"1 & 2, Ring Road,, Lajpat Nagar 4, New Delhi, 110024",
"tripadvisorRating":"4/5 Very Good","rating":"★★★★",
"payment":"Debit Card",
"CHECKIN":"2:00 PM",
"CHECKOUT": "12:00 PM",
"service":"Wonderful Location 4.5/5",
"value":"Impressive Cleanliness 4.5/5",
"imgList":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1484800621/Domestic%20Hotels/Hotels_New%20Delhi/Park%20Inn%20by%20Radisson%20New%20Delhi%20Lajpat%20Nagar/Superior_Room.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1484800658/Domestic%20Hotels/Hotels_New%20Delhi/Park%20Inn%20by%20Radisson%20New%20Delhi%20Lajpat%20Nagar/Reception.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1484800623/Domestic%20Hotels/Hotels_New%20Delhi/Park%20Inn%20by%20Radisson%20New%20Delhi%20Lajpat%20Nagar/Superior_Room_-_1.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1484800533/Domestic%20Hotels/Hotels_New%20Delhi/Park%20Inn%20by%20Radisson%20New%20Delhi%20Lajpat%20Nagar/Living_Room_2.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1484800609/Domestic%20Hotels/Hotels_New%20Delhi/Park%20Inn%20by%20Radisson%20New%20Delhi%20Lajpat%20Nagar/Deluxe_Room_-_1.jpg"],
"roomlist" : [{"type" : "Superior Double Room","bedSize":"Extra Bed","roomtype":[{"type":"Superior Double Room Only","prevPrice":6748,"price":4606,"yatraOffer":2142, "eCash" : 500},
{"type":"Superior Double Room with breakfast","prevPrice":8250,"price":5631,"yatraOffer":2619, "eCash" : 300}], 
"imgDetail":
["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1498645872/Hotel/New%20Delhi/00082693/Superior_Room_WZdRa7.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1511861042/Hotel/New%20Delhi/00009243/Umrao__Deluxe_Single_and_Doube_6Bqvax.jpg"]},
{"type" : "Deluxe Double Room","bedSize":"Double Bed","roomtype":[{"type":"Deluxe Double Room Only","prevPrice":9000,"price":5480,"yatraOffer":1257, "eCash" : 300},{"type":"Deluxe Double Room With Breakfast","prevPrice":9800,"price":6038,"yatraOffer":1267, "eCash" : 500}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1498645744/Hotel/New%20Delhi/00082693/Deluxe_room_yEW4zh.jpg"
,"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1511861175/Hotel/New%20Delhi/00009243/Umrao_Executive_Single_and_Double_BgK2Jw.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1467587950/Domestic%20Hotels/Hotels_New%20Delhi/The%20Umrao/Superior%20Room/Superior_Room_tBHoER.jpg"]},
{"type" : "Business Class Double","bedSize":"Double Bed","roomtype":[{"type":"Business Class Double Room only","prevPrice":7000,"price":4500,"yatraOffer":131, "eCash" : 500},
{"type":"Business Class Double Room with Breakfast","prevPrice":8000,"price":5990,"yatraOffer":172, "eCash" : 500}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1498645686/Hotel/New%20Delhi/00082693/Business_Class_PkPH0f.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1539764038/Hotel/00003951/Super_Deluxe_Room%7Bbedroom%7D_(2)_oU5K25.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1540448229/Hotel/00003951/_MG_8324_chNNgj.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1540448270/Hotel/00003951/_MG_8297_Z9gbsQ.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1540448378/Hotel/00003951/_MG_8289_4KvQZu.jpg"]}]},
{"id":"H4","name":"Ginger Hotel, Mumbai, Andheri","location":"Andheri East,Mumbai","city":"Mumbai","prevPrice":9498,"price":8121,"yatraOffer":1377,"Couple Friendly": true,
"Free Cancellation":true, "Free WiFi":true, 
"fullLoc":"Off Western Express Highway, Teligali, Parsi Colony, Andheri (E), Mumbai, 400069",
"tripadvisorRating":"3.5/5 very Good","rating":"★★★",
"payment":"Net Banking","CHECKIN":"12:00 PM",
"CHECKOUT": "11:00 AM",
"service":"Impressive Location 4.0/5",
"value":"Superior Service 4.0/5",
"imgList":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1499863317/Hotel/Thane/00084101/Roomview_(1)_AWWBne.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1499863345/Hotel/Mumbai/00084101/Roomview_(2)_RYM6pY.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1499863360/Hotel/Mumbai/00084101/Roomview_(3)_lzh9Dk.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1499863377/Hotel/Mumbai/00084101/Net_xRC9Zr.jpg"],
"roomlist" : [{"type" : "Superior Room Double","bedSize":"200 Sq. Ft","roomtype":[{"type":"Superior Double Room only","prevPrice":9797,"price":7886,"yatraOffer":2111},
{"type":"Superior Double Room with Breakfast","prevPrice":5545,"price":4271,"yatraOffer":1274}], 
"imgDetail":
["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1499928686/Hotel/Mumbai/00084101/Roomview_(2)_wgquC1.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1402376205/Domestic%20Hotels/Hotels_Bangalore/Ramada%20Encore/Executive%20Room.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509016275/Hotel/00017090/13_JJA2GK.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509016345/Hotel/00017090/14_3FTtZq.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1467525593/Domestic%20Hotels/Hotels_Bangalore/Ramada%20Encore%20Bangalore%20Domlur/Executive%20Double/Executive_Double_F5vLpz.jpg"]},
{"type" : "Deluxe Queen Bed room","bedSize":"1 King Bed","roomtype":[{"type":"Deluxe Queen Bed Double Room Only","prevPrice":4744,"price":3040,"yatraOffer":1704},{"type":"Deluxe Queen Bed Double Room","prevPrice":5545,"price":3552,"yatraOffer":1993}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509008090/Hotel/00017090/12_Lt2nWS.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509016155/Hotel/00017090/9_1sJRU0.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1467525591/Domestic%20Hotels/Hotels_Bangalore/Ramada%20Encore%20Bangalore%20Domlur/Executive%20Single/Executive_Single_RhMcEy.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509016215/Hotel/00017090/8_Z6ap5E.jpg"]},
{"type" : "Luxe - Double","bedSize":"Double Bed","roomtype":[{"type":"Luxe Double Room only","prevPrice":8000,"price":7149,"yatraOffer":1651},{"type":"Luxe Double Room with Breakfast","prevPrice":9000,"price":8150,"yatraOffer":2000}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1499928621/Hotel/Mumbai/00084101/Roomview_(3)_eQ29v6.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1570769539/Hotel/New%20Delhi/00000007/Suite_mt55If.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1399621068/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Lobby.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1399621140/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Premium%20Room.jpg"]}
]},
{"id":"H9","name":"Hilton Mumbai International Airport","location":"Sahar Airport Road,Mumbai","city" : "Mumbai","prevPrice":9904,"price":8306,"yatraOffer":598,"Couple Friendly": true,
"Free Cancellation":true, "Free WiFi":true, 
"Swimming pool":true,
"fullLoc":"Sahar Airport Road, Sahar International Airport , Mumbai, 400099",
"tripadvisorRating":"4.5/5 Excellent","rating":"★★★★★",
"payment":"UPI",
"CHECKIN":"3:00 PM",
"CHECKOUT": "12:00 PM",
"service":"Impressive Service 4.5/5",
"value":"Impressive Cleanliness 4.5/5",
"imgList":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1431955850/Domestic%20Hotels/Hotels_Mumbai/Hilton%20Mumbai%20International%20Airport/esprit-health-club.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1431955876/Domestic%20Hotels/Hotels_Mumbai/Hilton%20Mumbai%20International%20Airport/victoria-meeting-room.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1431955854/Domestic%20Hotels/Hotels_Mumbai/Hilton%20Mumbai%20International%20Airport/executive-lounge.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1431955862/Domestic%20Hotels/Hotels_Mumbai/Hilton%20Mumbai%20International%20Airport/imperial-china.jpg"],
"roomlist" : [{"type" : "Room,2 Twin Beds, Pool View","bedSize":"2 Twins Bed","roomtype":[{"type":"Room, 2 Twin Beds, Pool View","prevPrice":10000,"price":9358,"yatraOffer":1142, "eCash" : 500},
{"type":"Room, 2 Twin Beds,Pool View with breakfast","prevPrice":12000,"price":9865,"yatraOffer":2135, "eCash" : 300}], 
"imgDetail":
["https://i.travelapi.com/hotels/1000000/540000/533200/533142/51058a5f_z.jpg","https://i.travelapi.com/hotels/1000000/540000/533200/533142/80510f14_z.jpg",
"https://i.travelapi.com/hotels/1000000/540000/533200/533142/9de07169_z.jpg","https://i.travelapi.com/hotels/1000000/540000/533200/533142/8994f9d9_z.jpg"]},
{"type" :"Hilton,Executive Room,Business Lounge Access,City View","bedSize":"1 King Bed","roomtype":[{"type":"Hilton,Executive Room,Business Lounge Access, City View","prevPrice":11000,"price":9800,"yatraOffer":3000, "eCash" : 500},{"type":"Hilton, Executive Room, Business Lounge Access, City View","prevPrice":11968,"price":9195,"yatraOffer":1806, "eCash" : 300}],
"imgDetail":["https://i.travelapi.com/hotels/1000000/540000/533200/533142/020f449a_z.jpg","https://i.travelapi.com/hotels/1000000/540000/533200/533142/80510f14_z.jpg","https://i.travelapi.com/hotels/1000000/540000/533200/533142/efae678a_z.jpg","https://i.travelapi.com/hotels/1000000/540000/533200/533142/2b1f46fd_z.jpg"]},
{"type" :"Hilton, Luxury Suite, 1 King Bed, Business Lounge Access, Pool View","bedSize":"1 King Bed","roomtype":[{"type":"Hilton,Luxury Suite,1 King Bed, Business Lounge Access, Pool View","prevPrice":15000,"price":10000,"yatraOffer":5000, "eCash" : 500},
{"type":"Hilton,Luxury Suite,1 King Bed,Business Lounge Access,Pool View","prevPrice":20000,"price":15000,"yatraOffer":1700, "eCash" : 500}],
"imgDetail":["https://i.travelapi.com/hotels/1000000/540000/533200/533142/80510f14_z.jpg","https://i.travelapi.com/hotels/1000000/540000/533200/533142/70471169_z.jpg","https://i.travelapi.com/hotels/1000000/540000/533200/533142/9e8faeed_z.jpg","https://i.travelapi.com/hotels/1000000/540000/533200/533142/2b1f46fd_z.jpg"]}]},{"id":"H8","name":"Sterlings Mac Hotel","location":"Old Airport Road,Bangalore","city" : "Bangalore","prevPrice":7904,"price":6606,"yatraOffer":1298,"Couple Friendly": true,
"Free Cancellation":true, "Free WiFi":true, 
"Swimming pool":true,
"fullLoc":"#134, HAL Airport Road, Kodihalli,Next to Manipal Hospital, Bangalore, 560017",
"tripadvisorRating":"4/5 Very Good","rating":"★★★★★",
"payment":"UPI",
"CHECKIN":"12:00 PM",
"CHECKOUT": "12:00 PM",
"service":"Wonderful Location 4.5/5",
"value":"Impressive Cleanliness 4.5/5",
"imgList":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1421739892/Domestic%20Hotels/Hotels_Bangalore/Sterlings%20Mac%20Hotel/Overview1.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1594017926/Hotel/00008767/staycation-offer_GlINMc.jpeg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1405344006/Domestic%20Hotels/Hotels_Bangalore/Sterlings%20Mac%20Hotel/Bathtub%201.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1405344089/Domestic%20Hotels/Hotels_Bangalore/Sterlings%20Mac%20Hotel/Executive%20Single.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1405344047/Domestic%20Hotels/Hotels_Bangalore/Sterlings%20Mac%20Hotel/Exterior.jpg"],
"roomlist" : [{"type" : "Premium Double Room King Size Bed","bedSize":"1 King Bed","roomtype":[{"type":"Premium Double Room Only King Size Bed","prevPrice":10000,"price":8358,"yatraOffer":1142, "eCash" : 500},
{"type":"Premium Double Room King Size Bed with breakfast","prevPrice":13000,"price":10865,"yatraOffer":2135, "eCash" : 300}], 
"imgDetail":
["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1593424693/Hotel/00008767/Premium-room_dL5vBB.png","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1498645872/Hotel/New%20Delhi/00082693/Superior_Room_WZdRa7.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1511861042/Hotel/New%20Delhi/00009243/Umrao__Deluxe_Single_and_Doube_6Bqvax.jpg"]},
{"type" : "Deluxe Twin Bed Double","bedSize":"Double Bed","roomtype":[{"type":"Deluxe Twin Bed Double Room Only","prevPrice":40000,"price":35000,"yatraOffer":5000, "eCash" : 500},{"type":"Deluxe Twin Bed Double With Breakfast","prevPrice":11001,"price":9195,"yatraOffer":1806, "eCash" : 300}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1467585000/Domestic%20Hotels/Hotels_Bangalore/Sterlings%20Mac%20Hotel/Deluxe%20Double/Deluxe_Double_EWCs8u.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1593424495/Hotel/00008767/Deluxe_Twin_Bed_3-min_r0qhJG.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1498645744/Hotel/New%20Delhi/00082693/Deluxe_room_yEW4zh.jpg"
,"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1511861175/Hotel/New%20Delhi/00009243/Umrao_Executive_Single_and_Double_BgK2Jw.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1467587950/Domestic%20Hotels/Hotels_New%20Delhi/The%20Umrao/Superior%20Room/Superior_Room_tBHoER.jpg"]},
{"type" :"Suite Double","bedSize":"Double Bed","roomtype":[{"type":"Suite Double Room only","prevPrice":7000,"price":4500,"yatraOffer":131, "eCash" : 500},
{"type":"Suite Double with Breakfast","prevPrice":8000,"price":5990,"yatraOffer":172, "eCash" : 500}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1559543377/Hotel/00008767/Suite_1_kn1GIY.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1559544125/Hotel/00008767/Suite_5_RNeXHH.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1559544163/Hotel/00008767/Suite_4_gbakDf.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1559544205/Hotel/00008767/Suite_6_78ZfSj.jpg"]}]},
{"id":"H6","name":"Resort Terra Paraiso","location":"Anjuna,Goa","city" : "Goa","prevPrice":10600,"price":7786,"yatraOffer":2815,"Couple Friendly": true,
"Free Cancellation":true, "Free WiFi":true, 
"Swimming pool":true,
"fullLoc":"7/32, Salmona Saligaon Bardez, Near Sharvani Temple, Goa, 403511",
"tripadvisorRating":"3.5/5 Very Good","rating":"★★★★",
"payment":"UPI",
"CHECKIN":"12:00 PM",
"CHECKOUT": "11:00 AM",
"service":"Superior Service 4.0/5",
"value":"Superb Value 4.0/5",
"imgList":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1390450829/Domestic%20Hotels/Hotels_Goa/Hotel%20Resort%20Terra%20Paraiso/Pool~54.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1390450730/Domestic%20Hotels/Hotels_Goa/Hotel%20Resort%20Terra%20Paraiso/Lobby~302.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1390450775/Domestic%20Hotels/Hotels_Goa/Hotel%20Resort%20Terra%20Paraiso/LobbyAndReception~6.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1390450688/Domestic%20Hotels/Hotels_Goa/Hotel%20Resort%20Terra%20Paraiso/Facade~535.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1391030996/Domestic%20Hotels/Hotels_Goa/Hotel%20Resort%20Terra%20Paraiso/restaurant~0.jpg"],
"roomlist" : [{"type" : "Deluxe Room","bedSize":"Double Bed","roomtype":[{"type":"Deluxe Room only","prevPrice":10600,"price":10439,"yatraOffer":107, "eCash" : 250},
{"type":"Deluxe Room With Breakfast","prevPrice":11300,"price":11188,"yatraOffer":538, "eCash" : 300}], 
"imgDetail":
["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1539763695/Hotel/00003951/Deluxe_Bed_Room_(1)_sGjFks.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1540448470/Hotel/00003951/_MG_8278_LFEoqO.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1540448518/Hotel/00003951/P1322477_dcnNww.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1540448623/Hotel/00003951/_MG_8287_6yNuBz.jpg"]},
{"type" : "Executive Deluxe Room","bedSize":"Double Bed","roomtype":[{"type":"Executive - Room Only","prevPrice":6000,"price":5480,"yatraOffer":127, "eCash" : 300},{"type":"Executive Room With Breakfast","prevPrice":6800,"price":6038,"yatraOffer":127, "eCash" : 500}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1540448151/Hotel/00003951/_MG_8340-HDR_xDXyLQ.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1540448100/Hotel/00003951/Ex_Dlx_Washroom_W3tFyH.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1540448073/Hotel/00003951/Exec_Dlx_2_0W7HG6.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1540448049/Hotel/00003951/Exec_Dlx_3_M5X11v.jpg"]},
{"type" : "Super Deluxe Room","bedSize":"Double Bed","roomtype":[{"type":"Super Deluxe Room only","prevPrice":7000,"price":4500,"yatraOffer":131, "eCash" : 500},
{"type":"Super Deluxe Room with Breakfast","prevPrice":8000,"price":5990,"yatraOffer":172, "eCash" : 500}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1539764038/Hotel/00003951/Super_Deluxe_Room%7Bbedroom%7D_(2)_oU5K25.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1540448229/Hotel/00003951/_MG_8324_chNNgj.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1540448270/Hotel/00003951/_MG_8297_Z9gbsQ.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1540448378/Hotel/00003951/_MG_8289_4KvQZu.jpg"]}]},
{"id":"H10","name":"Royal Orchid Golden Suites","location":"Kalyani Nagar,Pune","city" : "Pune","prevPrice":5398,"price":3279,"yatraOffer":2101,"Couple Friendly": true,
"Free Cancellation":true, "Free WiFi":true, 
"Swimming pool":true,
"fullLoc":"SKalyan Nagar, Pune, 411014",
"tripadvisorRating":"4.5/5 Excellent","rating":"★★★★",
"payment":"UPI",
"CHECKIN":"2:00 PM",
"CHECKOUT": "12:00 PM",
"service":"Wonderful Location 4.5/5",
"value":"Impressive Service 4.5/5",
"imgList":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v5761881135/Hotel/Pune/00002624/Orchid_2_kN7CIt.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v9937236868/Hotel/Pune/00002624/Orchid_3_O14I6n.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v4846184584/Hotel/Pune/00002624/Orchid_5_FMeO4B.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v6018574470/Hotel/Pune/00002624/Orchid_6_Ra4xpx.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1441455883/Hotel/00002624/IMG_3638_HC0mZ0.JPG"],
"roomlist" : [{"type" : "Suite","roomId":"r2","bedSize":"Extra Bed","roomtype":[{"type":"Suite Room only","prevPrice":6000,"price":5328,"yatraOffer":672, "eCash" : 300},{"type":"Suite Room with Breakfast","prevPrice":6800,"price":6038,"yatraOffer":762, "eCash" : 500}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v0820502131/Hotel/00083887/2c44aa26_a42e_4128_b596_093e7bfa7dd0_N8I9EB.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1967355167/Hotel/00083887/46dd5d02_02dd_4b0d_aed7_e9f2c40d9302_001_0H8j8N.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542355735/Hotel/00083887/DSC_1026_n3quZ0.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542355695/Hotel/00083887/DSC_1030_NjWi0Y.jpg"]},
{"type" :"Executive Suite - Double","bedSize":"Double Bed","roomtype":[{"type":"Executive Suite Double Room Only","prevPrice":5657,"price":3881,"yatraOffer":2179, "eCash" : 500},
{"type":"Executive Suite Double Room With Breakfast","prevPrice":7098,"price":4435,"yatraOffer":2776, "eCash" : 300}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v8541978580/Hotel/00002624/IMG_3623_yYvXnc.JPG",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1467547782/Domestic%20Hotels/Hotels_Pune/Royal%20Orchid%20Central%20Pune/Deluxe%20Room%20-%20Double/Deluxe_Room_-_Double_EVx3FA.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542004434/Hotel/00003320/IMG_20181102_183510_BS7S2i.jpg"]},
{"type" :"Deluxe Suite-Double","bedSize":"Double Bed","roomtype":[{"type":"Deluxe Suite-Double Room Only","prevPrice":10000,"price":9000,"yatraOffer":3000, "eCash" : 500},
{"type":"Deluxe Suite-Double Room With Breakfast","prevPrice":10500,"price":9900,"yatraOffer":4000, "eCash" : 500}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542004486/Hotel/00003320/IMG_20181102_163154_4ApGxZ.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1441455883/Hotel/00002624/IMG_3638_HC0mZ0.JPG",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1542004358/Hotel/00003320/IMG_20181102_162019_brwULe.jpg"]}]},
{"id":"H5","name":"Kyriad Hotel Chinchwad by OTHPL","location":"Pimpri Chinchwad, Pune","city":"Pune","prevPrice":6860,"price":5894,"yatraOffer":966,"Couple Friendly": true,
"Free Cancellation":true, "Free WiFi":true, 
"fullLoc":"D-1 Block, MIDC Chinchwad Telco Road (Thermax Chowk), Pune, 411019",
"tripadvisorRating":"4.5/5 Excellent","rating":"★★★★",
"payment":"PayPals","CHECKIN":"12:00 PM",
"CHECKOUT": "11:00 AM",
"service":"Wonderful Location 4.0/5",
"value":"Impressive Service 4.0/5",
"imgList":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1480339917/Domestic%20Hotels/Hotels_Pune/Citrus%20Hotel%20Chinchwad/Bedroom_4.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1480339915/Domestic%20Hotels/Hotels_Pune/Citrus%20Hotel%20Chinchwad/Bedroom_2.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1480339916/Domestic%20Hotels/Hotels_Pune/Citrus%20Hotel%20Chinchwad/Bedroom_3.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1480339915/Domestic%20Hotels/Hotels_Pune/Citrus%20Hotel%20Chinchwad/Bedroom_1.jpg"],
"roomlist" : [{"type" : "Superior Double Room","bedSize":"Double Bed","roomtype":[{"type":"Superior Double Room only","prevPrice":6860,"price":6216,"yatraOffer":644},
{"type":"Superior Double Room with Breakfast","prevPrice":6860,"price":6216,"yatraOffer":644}], 
"imgDetail":
["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1467577992/Domestic%20Hotels/Hotels_Pune/Ramee%20Grand%20Hotel%20Pune/Suite%20Room/Suite_Room_Oywzff.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1499928686/Hotel/Mumbai/00084101/Roomview_(2)_wgquC1.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1402376205/Domestic%20Hotels/Hotels_Bangalore/Ramada%20Encore/Executive%20Room.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509016275/Hotel/00017090/13_JJA2GK.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509016345/Hotel/00017090/14_3FTtZq.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1467525593/Domestic%20Hotels/Hotels_Bangalore/Ramada%20Encore%20Bangalore%20Domlur/Executive%20Double/Executive_Double_F5vLpz.jpg"]},
{"type" : "Deluxe Double Room","bedSize":"1 King Bed","roomtype":[{"type":"Deluxe Double Room Only","prevPrice":8861,"price":8030,"yatraOffer":831},{"type":"Deluxe Double Room With Breakfast","prevPrice":8845,"price":6754,"yatraOffer":993}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1480339915/Domestic%20Hotels/Hotels_Pune/Citrus%20Hotel%20Chinchwad/Bedroom_2.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509008090/Hotel/00017090/12_Lt2nWS.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509016155/Hotel/00017090/9_1sJRU0.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1467525591/Domestic%20Hotels/Hotels_Bangalore/Ramada%20Encore%20Bangalore%20Domlur/Executive%20Single/Executive_Single_RhMcEy.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1509016215/Hotel/00017090/8_Z6ap5E.jpg"]},
{"type" : "Suite Double Room","bedSize":"Extra Bed","roomtype":[{"type":"Suite Double Room only","prevPrice":9990,"price":8745,"yatraOffer":951},{"type":"Suite Double Room with Breakfast","prevPrice":9678,"price":8740,"yatraOffer":578}],
"imgDetail":["https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1480339916/Domestic%20Hotels/Hotels_Pune/Citrus%20Hotel%20Chinchwad/Bedroom_3.jpg",
"https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1499928621/Hotel/Mumbai/00084101/Roomview_(3)_eQ29v6.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_srplist/v1570769539/Hotel/New%20Delhi/00000007/Suite_mt55If.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1399621068/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Lobby.jpg","https://imgcld.yatra.com/ytimages/image/upload/t_hotel_yatra_city_desktop/v1399621140/Domestic%20Hotels/Hotels_New%20Delhi/Radisson%20Blu%20Plaza%20Delhi/Premium%20Room.jpg"]}
]}];

app.get("/getCityHotelCount",function(req,res) { 
    let countByCity = [];
    for(let index = 0; index < dataLists.length; index++){
        let findIndex = countByCity.findIndex((obj)=> obj.city === dataLists[index].city)
        if(findIndex === -1){
            countByCity.push({city : dataLists[index].city, count : 1})
        }else{
            countByCity[findIndex].count += 1;  
        }
    }
    res.send(countByCity);
})
app.get("/hotels",function(req,res) { 
    let {sort,payment,rating,price,city} = req.query;
    let results = [...dataLists]
    if(city){
        results = results.filter((result) => result.city === city)
    }
    if(sort === 'price'){
        results = results.sort((a,b) => (a.price > b.price) ? 1 : ((b.price > a.price) ? -1 : 0))
    }
    if(sort === 'rating'){
        results = results.sort((a,b) => (a.rating > b.rating) ? 1 : ((b.rating > a.rating) ? -1 : 0))
    }
    if(sort === 'taipadvisorRating'){
        results = results.sort((a,b) => (a.tripadvisorRating > b.tripadvisorRating) ? 1 : ((b.tripadvisorRating > a.tripadvisorRating) ? -1 : 0))
    }
    if(price){
        console.log("Price",price)
        if(price === "<1000")
            results = results.filter((data) => data.price < 1000) ;
        if(price === "1001-2000")
            results = results.filter((data) => data.price >= 1001 && data.price < 2000) ;
            if(price === "2001-4000")
            results = results.filter((data) => data.price >= 2001 && data.price < 4000) ;
            if(price === "4001-7000")
            results = results.filter((data) => data.price >= 4001 && data.price < 7000) ;
            if(price === "7001-10000")
            results = results.filter((data) => data.price >= 7001 && data.price < 10000) ;
        if(price === ">10001")
            results = results.filter((data) => data.price > 10001) ;
      }
      if(payment){
        results = results.filter((data) => data.payment === payment) ;
      }
      if(rating){
        if(rating === "1")
        results = results.filter((data) => data.rating === "★") ;
        if(rating === "2")
        results = results.filter((data) => data.rating === "★★") ;
        if(rating === "3")
        results = results.filter((data) => data.rating === "★★★") ;
        if(rating === "4")
        results = results.filter((data) => data.rating === "★★★★") ;
        if(rating === "5")
        results = results.filter((data) => data.rating === "★★★★★") ;
      }
      
    console.log(results)
    res.send(results);
});