import React, { Component } from 'react';
import './App.css';
import {Switch, Route, Redirect} from 'react-router-dom';
import RightPanel from './components/rightPanel';
import YatrairSearch from './components/airSearch';
import BookFlight from './components/bookFlight';
import Payment from './components/payment';
import HotelSearch from './components/hotelSearch';
import ChooseRoom from './components/chooseRoom';
import HotelBooking from './components/hotelBooking';
import HotelPayment from './components/hotelPayment';
class App extends Component {
  state = { departure : "", arival : ""}
  handleSerachFlight = (departure, arival) => {
    this.setState({departure, arival})
    console.log("Calling onSerachFlight", departure, arival)
  }
  render() { 
    return (
      <Switch>
        <Route path="/checkOut" component={HotelPayment}/>
           <Route path="/hotelBooking" component={HotelBooking}/>
        <Route path="/yatra/hotelSearch" component={HotelSearch}/>
        <Route path="/roomDetails" component={ChooseRoom}/>
      <Route path="/payment" component={Payment}/>
        <Route path="/booking" component={BookFlight}/>
        <Route path="/yatra/airSearch" component={YatrairSearch}/>
       
        <Route
          exact path='/yatra'
         component={RightPanel}
        />
            <Route exact path={'/'} render={() => {
              return <Redirect to={'/yatra'}/>
              }}/>
      </Switch>
     
      
    );
  }
}
 
export default App;
  