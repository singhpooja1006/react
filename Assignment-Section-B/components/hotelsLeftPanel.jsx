import React, { Component } from "react";
class HotelsLeftPanel extends Component {
  state = {};
  handleChange = (e) => {
    const { currentTarget: input } = e;
    const { priceData, starRatings, paymentMode } = this.props;
    if (input.name === "price") {
      priceData.selected = input.value;
    }
    if (input.name === "rating") {
      starRatings.selected = input.value;
    }
    if (input.name === "payment") {
      paymentMode.selected = input.value;
    }
    this.props.onOptionChange(priceData, starRatings, paymentMode);
  };
  render() {
    const { priceData, starRatings, paymentMode } = this.props;
    return (
      <React.Fragment>
        <div className="mt-5 ml-3 mr-2 row border" id="payment">
          <div class="col-8 mb-2 mt-2 h6 text-center">OPTIONS</div>
        </div>
        <br />
        <div className="ml-3 mr-2 row border" id="payment">
          <div class="col-8 mb-2 mt-2 h6">PRICE</div>
        </div>
        {priceData.prices.map((price) => (
          <div className="row ml-3 mr-2 border" id="payment">
            <div className=" col-8 mb-2 mt-2 ml-2 form-check">
              <input
                checked={priceData.selected === price.value ? true : false}
                value={price.value}
                className="form-check-input ng-untouched ng-pristine ng-valid"
                name="price"
                type="radio"
                onChange={this.handleChange}
                id="price"
              />
              <label className="form-check-label" for={price.value}>
                {price.name}
              </label>
            </div>
          </div>
        ))}
        <br />
        <div className="ml-3 mr-2 row border" id="payment">
          <div class="col-8 mb-2 mt-2 h6">STAR RATING</div>
        </div>
        {starRatings.ratings.map((rating) => (
          <div className="row ml-3 mr-2 border" id="payment">
            <div className=" col-8 mb-2 mt-2 ml-2 form-check">
              <input
                checked={starRatings.selected === rating.value ? true : false}
                value={rating.value}
                className="form-check-input ng-untouched ng-pristine ng-valid"
                name="rating"
                type="radio"
                onChange={this.handleChange}
                id="rating"
              />
              <label className="form-check-label" for={rating.value}>
                {rating.name}
              </label>
            </div>
          </div>
        ))}
        <br />
        <div className="ml-3 mr-2 row border" id="payment">
          <div class="col-8 mb-2 mt-2 h6">PAYMENT MODE</div>
        </div>
        {paymentMode.payments.map((payment) => (
          <div className="row ml-3 mr-2 border" id="payment">
            <div className=" col-8 mb-2 mt-2 ml-2 form-check">
              <input
                checked={paymentMode.selected === payment ? true : false}
                value={payment}
                className="form-check-input ng-untouched ng-pristine ng-valid"
                name="payment"
                type="radio"
                onChange={this.handleChange}
                id="payment"
              />
              <label className="form-check-label" for={payment}>
                {payment}
              </label>
            </div>
          </div>
        ))}
      </React.Fragment>
    );
  }
}

export default HotelsLeftPanel;
