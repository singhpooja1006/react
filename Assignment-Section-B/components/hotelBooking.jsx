import React, { Component } from "react";
import Navbar from "./navbar";
import { Link } from "react-router-dom";
import "./icon.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faReceipt, faMouse } from "@fortawesome/free-solid-svg-icons";

class HotelBooking extends Component {
  state = {
    formdata: { email: "", mobileNo: "", request: "" },
    errors: {},
    errorsTravler: {},
  };
  componentDidMount() {
    let hotel = {};

    if (this.props.location.state) {
      hotel = this.props.location.state.hotel;
    }

    this.setState({ hotel });
    console.log("roomDetail", hotel);
  }
  /*getNoOfDays(checkInDate, checkOutDate) {
    let one_day = 1000 * 60 * 60 * 24;
    return (
      Math.round(checkOutDate.getTime() - checkInDate.getTime()) / one_day
    ).toFixed(0);
  }*/
  handleChangeRoom = () => {
    const { hotel } = this.state;
    this.props.history.push({
      pathname: "/roomDetails",
      state: {
        hotel: hotel,
      },
    });
  };
  handleInputChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const data = { ...this.state.formdata };
    data[e.currentTarget.name] = e.currentTarget.value;

    this.setState({ formdata: data, errors: errors });
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "email":
        if (!e.currentTarget.value.trim()) return "Email address is required.";
        else if (e.currentTarget.value.includes("@") === false)
          return "Provide valid email address";
        break;
      case "mobileNo":
        if (!e.currentTarget.value.trim()) return "Mobile number is required.";
        else if (e.currentTarget.value.trim().length < 10)
          return "Mobile number must be 10 Digits";
        break;
      default:
        break;
    }
    return "";
  };
  handleSubmit = (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    console.log(errors, errCount);
    if (errCount > 0) return;
    let errorsTravler = this.validateTravlerDetails();
    this.setState({ errorsTravler: errorsTravler });
    if (Object.keys(errorsTravler).length > 0) return;
    console.log("Submitted");
    const { hotel, formdata } = this.state;
    hotel.email = formdata.email;
    hotel.mobileNo = formdata.mobileNo;
    hotel.request = formdata.request;
    //hotel.noOfNights = +this.getNoOfDays(hotel.checkInDate, hotel.checkOutDate);
    this.props.history.push({
      pathname: "/checkOut",
      state: {
        hotel: hotel,
      },
    });
  };
  validate = () => {
    let errs = {};

    if (!this.state.formdata.email.trim())
      errs.email = "Email address is required.";
    else if (this.state.formdata.email.includes("@") === false)
      errs.email = "Provide valid email address";

    if (!this.state.formdata.mobileNo.trim())
      errs.mobileNo = "Mobile number is required";
    else if (this.state.formdata.mobileNo.trim().length < 10)
      errs.mobileNo = "Mobile number must be 10 Digits.";

    return errs;
  };
  isFormvalid = () => {
    let errs = this.validate();

    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };
  validateTravlerDetails() {
    let errs = {};
    const { rooms } = this.state.hotel;

    let roomIndex = rooms.findIndex(
      (room) => room.fname === "" || room.lname === ""
    );
    if (roomIndex !== -1) {
      errs.msg = "First name & Last name is required.";
    }
    return errs;
  }
  handleChange = (index, elementName, e) => {
    const { currentTarget: input } = e;
    const { rooms } = this.state.hotel;
    if (input.name.includes("adult")) {
      if (elementName === "firstName") rooms[index].fname = input.value;
      if (elementName === "lastName") rooms[index].lname = input.value;
    }
    const { hotel } = this.state;
    hotel.rooms = rooms;
    this.setState({ hotel });
  };

  render() {
    const { formdata, errors, hotel } = this.state;
    /*let noOfNights = this.getNoOfDays(
      hotel ? hotel.checkInDate : new Date(),
      hotel ? hotel.checkOutDate : new Date()
    );*/
    let noOfNights = hotel ? hotel.noOfNights : "";
    let noOfDays = +noOfNights + 1;
    return (
      <div className="container-fluid" style={{ backgroundColor: "lightgray" }}>
        <Navbar />
        <div className="row mt-4 ml-1">
          <div className="col-lg-9 col-12">
            <div className="row">
              <div
                className="col-1"
                style={{ color: "#ea2331", fontSize: "25px" }}
              >
                <FontAwesomeIcon icon={faReceipt} />
              </div>
              <div
                className="col-lg-8 col-10"
                style={{
                  color: "#ea2331",
                  fontWeight: "500",
                  fontSize: "1.429rem",
                }}
              >
                {" "}
                Review Your Booking
              </div>
            </div>
            <div className="row bg-white" id="box2">
              <div className="col-12">
                <div className="row bg-white" style={{ paddingTop: "12px" }}>
                  <div className="col-lg-3 col-12 text-center d-none d-lg-block">
                    <div className="row pl-1">
                      <div className="col-lg-12 col-12">
                        <img
                          style={{ width: "200px", height: "180px" }}
                          src={hotel ? hotel.selectedRoom.imgDetail[0] : ""}
                        />
                        <span
                          ng-if="selectionSummary.roomType"
                          class="img-grad text-truncate ng-scope"
                        >
                          <a
                            title={
                              hotel
                                ? hotel.selectedRoom.roomtype[
                                    hotel.selectedRoomIndex
                                  ].type
                                : ""
                            }
                            class="text-white ng-binding"
                          >
                            {hotel
                              ? hotel.selectedRoom.roomtype[
                                  hotel.selectedRoomIndex
                                ].type
                              : ""}
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-1 col-4"></div>
                  <div className="col-lg-8 col-12">
                    <div className="row">
                      <div
                        className="col-lg-12 col-12"
                        id="fs12"
                        style={{
                          fontSize: "1.286rem",
                          color: "#333",
                          maxWidth: "460px",
                          display: "inline-block",
                          verticalAlign: "top",
                        }}
                      >
                        {hotel ? hotel.name : ""}
                      </div>
                      <div className="col-lg-12 col-12" id="sh1">
                        {hotel ? hotel.rating : ""}
                      </div>
                    </div>
                    <div className="row d-none d-lg-block">
                      <div
                        className="col-lg-12 col-12"
                        id="fs12"
                        style={{
                          fontSize: "1rem",
                          color: "#333",
                          display: "inline-block",
                          verticalAlign: "top",
                        }}
                      >
                        {hotel ? hotel.fullLoc : ""}
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-6 col-12">
                        <div className="cal-block">
                          <div className="cb-top">
                            <span className="checkin-text">Check-In</span>
                            <span className="bkg-date bold">
                              {hotel ? hotel.checkInDate.getDate() : ""}
                            </span>
                          </div>
                          <div class="cb-footer ng-binding">
                            Feb &nbsp;&nbsp;|&nbsp;&nbsp; 12:00 PM
                          </div>
                        </div>
                        <div className="cal-block">
                          <div className="cb-top">
                            <span className="checkin-text">Check-Out</span>
                            <span className="bkg-date bold">
                              {hotel ? hotel.checkOutDate.getDate() : ""}
                            </span>
                          </div>
                          <div class="cb-footer ng-binding">
                            Feb &nbsp;&nbsp;|&nbsp;&nbsp; 11:00 AM
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-6 col-12">
                        <div className="row">
                          <div className="col-6">
                            <span
                              className="bold"
                              style={{
                                fontSize: "1rem",
                                color: "#333",
                              }}
                            >
                              {noOfDays +
                                (noOfDays > 1 ? " Days & " : " Day & ") +
                                noOfNights +
                                (noOfNights > 1 ? " Nights" : " Night")}
                            </span>{" "}
                          </div>
                          <div
                            className="text-primary h6 col-6"
                            onClick={() => this.handleChangeRoom()}
                            style={{ cursor: "pointer" }}
                          >
                            Change Room
                          </div>
                        </div>
                        <div className="row">
                          <hr className="col-12" />
                        </div>
                        {hotel
                          ? hotel.rooms.map((room, index) => (
                              <div className="row">
                                <div className="col-4 h6">
                                  Room {index + 1} &nbsp;:
                                </div>
                                <div className="col-8 h6">
                                  &nbsp;{room.adult}{" "}
                                  {room.adult > 1 ? "Adults" : "Adult"}{" "}
                                  {room.child > 0
                                    ? "& " +
                                      room.child +
                                      (room.child > 1 ? " Childs" : " Child")
                                    : ""}
                                </div>
                              </div>
                            ))
                          : ""}

                        <div className="row">
                          <hr className="col-12" />
                        </div>
                      </div>
                      <div className="row mt-2">
                        <div
                          className="col text-center d-none d-lg-block"
                          id="fs12"
                        >
                          Inclusion:
                          <span className="ml-4">
                            <i
                              className="fa fa-check mr-1"
                              style={{ color: "lightgreen" }}
                            ></i>{" "}
                            Complimentary Wi-Fi Internet
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row mt-4">
                <div className="col-12 ml-2 h6" id="fs12">
                  Cancellation Policy: No refund if you cancel this booking.{" "}
                </div>
              </div>
            </div>
            <div className="row">
              <i
                className="fa fa-user-plus"
                style={{ fontSize: "3rem", color: "#ea2331" }}
              ></i>
              <h4 style={{ color: "#ea2331" }} className="mt-3 ml-1">
                Enter Traveller Details
              </h4>
              <div className="mt-3 text-secondary h5">&nbsp;&nbsp;|</div>
              <a href="#" className="text-primary h4 mt-3">
                &nbsp;&nbsp;Sign in
              </a>
              <div className="mt-4 h4" id="fs12">
                &nbsp;to book faster and use eCash
              </div>
            </div>
            <form onSubmit={this.handleSubmit}>
              <div className="row bg-white" id="box2">
                <div className="col-12">
                  <div className="row bg-white ">
                    <div className="col-lg-2 col-4" id="fs14">
                      <b>Contact Details</b>
                    </div>
                    <form className="ng-invalid ng-dirty ng-touched">
                      <div className="row">
                        <div className="col-6 form-group">
                          <input
                            className="form-control ng-invalid ng-dirty ng-touched"
                            name="email"
                            id="email"
                            placeholder="Email"
                            type="text"
                            value={formdata.email}
                            onChange={this.handleInputChange}
                          />
                          {errors.email ? (
                            <div className="text-danger">{errors.email}</div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="col-6 form-group">
                          <input
                            className="form-control ng-untouched ng-pristine ng-invalid"
                            placeholder="Mobile Number"
                            type="number"
                            name="mobileNo"
                            id="mobileNo"
                            value={formdata.mobileNo}
                            onChange={this.handleInputChange}
                          />
                          {errors.mobileNo ? (
                            <div className="text-danger">{errors.mobileNo}</div>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                      <div className="row">
                        Your booking details will be sent to this email address
                        and mobile number.
                      </div>
                      <hr />
                    </form>
                  </div>
                </div>

                <div className="col-12">
                  {this.state.errorsTravler.msg ? (
                    <div className="text-danger text-center">
                      {this.state.errorsTravler.msg}
                    </div>
                  ) : (
                    ""
                  )}
                  {hotel
                    ? hotel.rooms.map((room, index) => (
                        <div className="row ng-untouched ng-pristine ng-invalid mb-4">
                          <div className="col-lg-2 col-4" id="fs14">
                            <label>Room {index + 1}</label>
                          </div>
                          <div className="col-5">
                            <input
                              className="form-control ng-untouched ng-pristine ng-invalid"
                              name={"adultfirstName" + index}
                              placeholder="First Name and Middle Name"
                              type="text"
                              id="firstName"
                              value={room.fname}
                              onChange={(e) =>
                                this.handleChange(index, "firstName", e)
                              }
                            />
                          </div>
                          <div className="col-4">
                            <input
                              className="form-control ng-untouched ng-pristine ng-invalid"
                              placeholder="Last Name"
                              name={"adultlastName" + index}
                              type="text"
                              id="lastName"
                              value={room.lname}
                              onChange={(e) =>
                                this.handleChange(index, "lastName", e)
                              }
                            />
                          </div>
                        </div>
                      ))
                    : ""}
                </div>

                <div className="col-12">
                  <div className="row bg-white mt-5">
                    <div className="col-lg-2 col-4" id="fs14">
                      <b>Special Request:</b>
                    </div>
                    <div className="row">
                      <textarea
                        name="request"
                        id="request"
                        type="text"
                        value={formdata.request}
                        onChange={this.handleInputChange}
                        rows="2"
                        cols="75"
                        placeholder="e.g. I want early check-in or specify the time you will check-in."
                      ></textarea>
                    </div>
                  </div>
                </div>
                <div className="row ml-5">
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Special
                  request can't be guaranteed. We will pass these requests to
                  the hotels.
                </div>
              </div>
              <div className="col-12 text-center mt-3">
                <button
                  style={{ minWidth: "270px", fontSize: "1.286rem" }}
                  className="btn btn-danger btn-lg"
                  type="submit"
                  disabled={this.isFormvalid()}
                >
                  Continue
                </button>
              </div>
            </form>
            <div className="row mt-5"></div>
            <div className="row mt-5"></div>
            <div
              className="row fixed-bottom pt-3 pb-3 mt-5"
              style={{ backgroundColor: "#e6f2ff" }}
            >
              <div className="row">
                <div
                  className="col-12 ml-5"
                  id="fs12"
                  style={{
                    fontSize: ".789rem",
                    color: "#333",
                    display: "inline-block",
                    verticalAlign: "top",
                  }}
                >
                  <FontAwesomeIcon className="text-secondary" icon={faMouse} />{" "}
                  NET BANKING &nbsp;&nbsp;<i className="fa fa-credit-card"></i>
                  &nbsp;&nbsp;EASY EMI OPTION
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <i className="fa fa-cc-visa"></i>&nbsp;&nbsp;
                  <i className="fa fa-cc-mastercard"></i>&nbsp;&nbsp;
                  <i className="fa fa-cc-discover"></i>&nbsp;&nbsp;
                  <i className="fa fa-cc-amex"></i>&nbsp;&nbsp;
                  <i className="fa fa-cc-paypal"></i>&nbsp;&nbsp;
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-3 col-12">
            <div className="row mt-3">
              <div
                className="col"
                style={{ color: "black", fontWeight: "500" }}
              >
                {" "}
                Tariff Details
              </div>
            </div>
            <div className="row bg-white" id="box3">
              <div className="col-12">
                <div className="row bg-white">
                  <div className="col-8 text-left" id="fs11">
                    {" "}
                    Hotel Charges
                  </div>
                  <div className="col-4 text-right" id="fs11">
                    {" "}
                    ₹
                    {hotel
                      ? hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                          .prevPrice * noOfNights
                      : ""}
                  </div>

                  <div className="col-8 text-left" id="fs11">
                    Yatra Promo Offer(-)
                  </div>
                  <div className="col-4 text-right" id="fs11">
                    {" "}
                    ₹
                    {hotel
                      ? hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                          .yatraOffer * noOfNights
                      : ""}
                  </div>

                  <div className="col-12 text-center">
                    <hr />
                  </div>
                  <div className="col-8 text-left h5">Sub Total</div>
                  <div className="col-4 text-right h6">
                    {" "}
                    ₹{" "}
                    {hotel
                      ? hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                          .price * noOfNights
                      : ""}
                  </div>
                  <div className="col-8 text-left" id="fs11">
                    Hotel GST
                  </div>
                  <div className="col-4 text-right" id="fs11">
                    {" "}
                    ₹{" "}
                    {hotel
                      ? (hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                          .prevPrice *
                          noOfNights *
                          12) /
                        100
                      : ""}
                  </div>
                  <div className="col-8 text-left" id="fs12">
                    You Pay
                  </div>
                  <div className="col-4 text-left" id="fs12">
                    ₹{" "}
                    {hotel
                      ? hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                          .price *
                          noOfNights +
                        (hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                          .prevPrice *
                          noOfNights *
                          12) /
                          100
                      : ""}
                  </div>
                  <div className="col-8 text-left text-success" id="fs11">
                    Total Savings:
                  </div>
                  <div className="col-4 text-right text-success" id="fs11">
                    ₹{" "}
                    {hotel
                      ? hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                          .yatraOffer * noOfNights
                      : ""}
                  </div>
                  <div className="col-2 text-left" id="fs11">
                    Earn
                  </div>
                  <div className="col-6  text-warning" id="fs11">
                    eCash
                  </div>
                  <div className="col-4 text-right" id="fs11">
                    ₹{" "}
                    {hotel
                      ? hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                          .eCash *
                          noOfNights >=
                        500
                        ? 500
                        : hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                            .eCash * noOfNights
                      : ""}
                  </div>
                </div>
              </div>
            </div>
            <br />
          </div>
        </div>
      </div>
    );
  }
}

export default HotelBooking;
