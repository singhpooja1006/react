import React, { Component } from "react";
import Navbar from "./navbar";
import config from "./config.json";
import axios from "axios";
class HotelPayment extends Component {
  state = {
    value: "UPI",
    selectedPaymentCss: "border bg-white",
  };
  showTextField = (value) => {
    this.setState({ value });
  };
  componentDidMount() {
    let hotel = {};
    if (this.props.location.state) {
      hotel = this.props.location.state.hotel;
    }
    console.log("Payment", hotel);
    this.setState({ hotel });
  }
  handlePayNow = async (e) => {
    let apiEndPoint = config.apiEndPoint + "/checkOut";
    await axios.post(apiEndPoint, {
      ...this.state.hotel,
    });
  };

  render() {
    const { hotel } = this.state;
    let noOfNights = hotel ? hotel.noOfNights : "";
    return (
      <div className="container-fluid" style={{ backgroundColor: "lightgray" }}>
        <Navbar />
        <div className="row">
          <div className="col-lg-9 col-12">
            <div className="row mt-2 mb-2">
              <div className="col-9 ml-1" style={{ fontSize: "20px" }}>
                <strong>
                  <i className="fa fa-credit-card"></i> Payment Method
                </strong>
              </div>
            </div>
            <div className="row bg-white ml-1" id="payment">
              <div className="col-lg-2 col-4 text-center" id="fs20">
                <div
                  style={{ cursor: "pointer" }}
                  className={
                    this.state.value === "UPI"
                      ? this.state.selectedPaymentCss
                      : "border bg-light"
                  }
                  onClick={() => this.showTextField("UPI")}
                >
                  <div className="mt-2 mb-2">UPI</div>
                </div>

                <div
                  style={{ cursor: "pointer" }}
                  className={
                    this.state.value === "Credit Card"
                      ? this.state.selectedPaymentCss
                      : "border bg-light"
                  }
                  onClick={() => this.showTextField("Credit Card")}
                >
                  <div className="mt-2 mb-2">Credit Card</div>
                </div>
                <div
                  style={{ cursor: "pointer" }}
                  className={
                    this.state.value === "Debit Card"
                      ? this.state.selectedPaymentCss
                      : "border bg-light"
                  }
                  onClick={() => this.showTextField("Debit Card")}
                >
                  <div className="mt-2 mb-2">Debit Card</div>
                </div>
                <div
                  style={{ cursor: "pointer" }}
                  className={
                    this.state.value === "Net Banking"
                      ? this.state.selectedPaymentCss
                      : "border bg-light"
                  }
                  onClick={() => this.showTextField("Net Banking")}
                >
                  <div className="mt-2 mb-2">Net Banking</div>
                </div>
                <div
                  style={{ cursor: "pointer" }}
                  className={
                    this.state.value === "PayPal"
                      ? this.state.selectedPaymentCss
                      : "border bg-light"
                  }
                  onClick={() => this.showTextField("PayPal")}
                >
                  <div className="mt-2 mb-2">PayPal</div>
                </div>
              </div>
              <div className="col-8" id="fs20">
                <br />
                <br />
                <div className="row">
                  <div className="col ml-4">
                    <strong>Pay with {this.state.value}</strong>
                  </div>
                </div>
                <br />
                <br />
                <div className="row">
                  <div
                    className="col-lg-4 col-5 ml-4"
                    style={{ fontSize: "25px" }}
                  >
                    <strong>
                      ₹
                      {hotel
                        ? hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                            .prevPrice * +noOfNights
                        : ""}
                    </strong>
                  </div>
                  <div className="col-6">
                    <button
                      className="btn btn-danger text-white btn-md"
                      onClick={() => this.handlePayNow()}
                    >
                      Pay Now
                    </button>
                  </div>
                </div>
                <div className="row">
                  <div
                    className="col text-secondary text-center"
                    style={{ fontSize: "12px" }}
                  >
                    By clicking on Pay Now, you are agreeing to our Terms &
                    Conditions , Privacy policy, User Agreement Guidelines.
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-12">
            <div className="row mt-2 mb-2">
              <div
                className="col-11 ml-1 text-left"
                style={{ fontSize: "16px" }}
              >
                Payment Details
              </div>
            </div>
            <div className="row bg-white ml-1 mr-1" id="flight">
              <div className="col">
                <div className="row">
                  <div className="col-8 text-left" style={{ fontSize: "1rem" }}>
                    Hotel Charges
                  </div>
                  <div
                    className="col-4 text-right"
                    style={{ fontSize: "1rem" }}
                  >
                    ₹{" "}
                    {hotel
                      ? hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                          .prevPrice * +noOfNights
                      : ""}
                  </div>
                </div>
                <div className="row">
                  <div className="col-8 text-left" style={{ fontSize: "1rem" }}>
                    Hotel GST
                  </div>
                  <div
                    className="col-4 text-right"
                    style={{ fontSize: "1rem" }}
                  >
                    ₹{" "}
                    {hotel
                      ? (hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                          .prevPrice *
                          noOfNights *
                          12) /
                        100
                      : ""}
                  </div>
                </div>
                <div className="row">
                  <div className="col-8 text-left" style={{ fontSize: "1rem" }}>
                    Yatra Promo Offer (-)
                  </div>
                  <div
                    className="col-4 text-right"
                    style={{ fontSize: "1rem" }}
                  >
                    {" "}
                    ₹{" "}
                    {hotel
                      ? hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                          .yatraOffer * noOfNights
                      : ""}
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div
                    className="col-6 text-left"
                    style={{ fontSize: "1.7rem" }}
                  >
                    <strong>You Pay</strong>
                  </div>
                  <div
                    className="col-6 text-right"
                    style={{ fontSize: "1.7rem" }}
                  >
                    <strong>
                      ₹
                      {hotel
                        ? hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                            .price *
                            noOfNights +
                          (hotel.selectedRoom.roomtype[hotel.selectedRoomIndex]
                            .prevPrice *
                            noOfNights *
                            12) /
                            100
                        : ""}
                    </strong>
                  </div>
                </div>
              </div>
            </div>
            <div _ngcontent-rjw-c11="" class="row mt-1 mb-1">
              <div _ngcontent-rjw-c11="" class="col ml-1">
                {" "}
                Booking summary{" "}
              </div>
            </div>
            <div className="row bg-white ml-1 mr-1" id="flight">
              <div className="col">
                <div class="row">
                  <div className="col-12 h5">{hotel ? hotel.name : ""}</div>
                  <div className="col-12">{hotel ? hotel.location : ""}</div>
                </div>
                <div className="row">
                  <div className="col-12 text-left">
                    {hotel
                      ? hotel.rooms.map((room, index) => (
                          <div className="row">
                            <div className="col-12 h6">
                              Room: {index + 1}&nbsp;
                              {room.adult > 1 ? "Adults" : "Adult"} :{" "}
                              {room.adult}
                              {room.child > 0
                                ? "& " +
                                  room.child +
                                  (room.child > 1 ? " Childs" : " Child")
                                : ""}
                            </div>
                          </div>
                        ))
                      : ""}
                  </div>
                </div>
              </div>
            </div>

            <div className="row ml-1 mr-1 mt-1 mb-1">
              <div className="col"> Contact Details </div>
            </div>
            <div className="row bg-white ml-1 mr-1" id="detail">
              <div className="col">
                <div className="row">
                  {hotel
                    ? hotel.rooms.map((room) => (
                        <div className="col" id="fs16">
                          {room.fname} {room.lname}
                        </div>
                      ))
                    : ""}
                </div>

                <hr />
                <div className="row" id="fs16">
                  <div className="col-4 text-secondary">Email</div>
                  <div className="col-8 text-secondary">
                    {hotel ? hotel.email : ""}
                  </div>
                </div>
                <div className="row" id="fs16">
                  <div className="col-4 text-secondary">Phone</div>
                  <div className="col-8 text-secondary">
                    {" "}
                    {hotel ? hotel.mobileNo : ""}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HotelPayment;
