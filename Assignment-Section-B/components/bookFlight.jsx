import React, { Component } from "react";
import "./icon.css";
import {
  faPlaneDeparture,
  faUserEdit,
  faSuitcaseRolling,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Navbar from "./navbar";
class BookFlight extends Component {
  state = {
    formdata: { email: "", mobileNo: "" },
    errors: {},
    returnFlight: {},
    errorsTravler: {},
    flight: {},
    fareDetails: {},
    travellersDetails: {},
    addOns: [
      { name: "cancellation", value: 747, check: false },
      { name: "insurance", value: 269, check: false },
    ],
    promoCode: {
      codes: ["NEWPAY", "YTAMZ19"],
      selected: "",
    },
  };

  componentDidMount() {
    let flight = {},
      travellersDetails = {},
      returnFlight = {};
    let fareDetails = { surcharge: 622, cancelPolicy: 0, travelProtection: 0 };
    if (this.props.location.state) {
      flight = this.props.location.state.flight;
      fareDetails.baseFare = flight.Price;
      if (this.props.location.state.returnFlight) {
        returnFlight = this.props.location.state.returnFlight;
        fareDetails.baseFare += returnFlight.Price;
      }
      let travellersDetailsCount = this.props.location.state.travellersDetails;
      if (travellersDetailsCount.adultCount > 0) {
        travellersDetails.adults = [];
        for (
          let index = 0;
          index < travellersDetailsCount.adultCount;
          index++
        ) {
          let adultDetil = { firstName: "", lastName: "" };
          travellersDetails.adults.push(adultDetil);
        }
      }
      if (travellersDetailsCount.childCount > 0) {
        travellersDetails.childs = [];
        for (
          let index = 0;
          index < travellersDetailsCount.childCount;
          index++
        ) {
          let childDetil = { firstName: "", lastName: "" };
          travellersDetails.childs.push(childDetil);
        }
      }
      if (travellersDetailsCount.infantCount > 0) {
        travellersDetails.infants = [];
        for (
          let index = 0;
          index < travellersDetailsCount.infantCount;
          index++
        ) {
          let infantDetil = { firstName: "", lastName: "" };
          travellersDetails.infants.push(infantDetil);
        }
      }
    }
    this.setState({ flight, fareDetails, travellersDetails, returnFlight });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    console.log(errors, errCount);
    if (errCount > 0) return;
    let errorsTravler = this.validateTravlerDetails();
    this.setState({ errorsTravler: errorsTravler });
    if (Object.keys(errorsTravler).length > 0) return;
    console.log("Submitted");
    const {
      flight,
      fareDetails,
      addOns,
      promoCode,
      formdata,
      travellersDetails,
      returnFlight,
    } = this.state;
    let promo =
      promoCode.selected === promoCode.codes[0]
        ? 1400
        : promoCode.selected === promoCode.codes[1]
        ? 2000
        : 0;
    let departure = { ...flight };
    delete departure.flightDetails;
    let arrival = { ...returnFlight };
    delete arrival.returnFlightDetails;
    let total = fareDetails
      ? fareDetails.surcharge -
        promo +
        fareDetails.baseFare * departure.count +
        (addOns[0].check === true ? addOns[0].value : 0) +
        (addOns[1].check === true ? addOns[1].value * departure.count : 0)
      : 0;
    let passengers = [];
    if (travellersDetails.adults) {
      travellersDetails.adults.map((adult) => passengers.push(adult));
    }
    if (travellersDetails.childs) {
      travellersDetails.childs.map((child) => passengers.push(child));
    }
    if (travellersDetails.infants) {
      travellersDetails.infants.map((infant) => passengers.push(infant));
    }
    this.props.history.push({
      pathname: "/payment",
      state: {
        departure: departure,
        total: total,
        email: formdata.email,
        mobile: formdata.mobileNo,
        travellers: departure.count,
        passengers: passengers,
        arrival: arrival,
      },
    });
  };

  validateTravlerDetails() {
    let errs = {};
    const { travellersDetails } = this.state;

    if (travellersDetails.adults) {
      let adultIndex = travellersDetails.adults.findIndex(
        (adult) => adult.firstName === "" || adult.lastName === ""
      );
      if (adultIndex !== -1) {
        errs.msg = "Please fill all the travler details ";
      }
    }

    if (travellersDetails.childs) {
      let childIndex = travellersDetails.childs.findIndex(
        (child) => child.firstName === "" || child.lastName === ""
      );
      if (childIndex !== -1) {
        errs.msg = "Please fill all the travler details ";
      }
    }

    if (travellersDetails.infants) {
      let infantIndex = travellersDetails.infants.find(
        (infant) => infant.firstName === "" || infant.lastName === ""
      );
      if (infantIndex !== -1) {
        errs.msg = "Please fill all the travler details ";
      }
    }
    return errs;
  }
  validate = () => {
    let errs = {};

    if (!this.state.formdata.email.trim()) errs.email = "Email is required";
    else if (this.state.formdata.email.includes("@") === false)
      errs.email = "Enter a valid email";

    if (!this.state.formdata.mobileNo.trim())
      errs.mobileNo = "Mobile No is required";
    else if (this.state.formdata.mobileNo.trim().length < 10)
      errs.mobileNo = "Mobile No must be 10 Digits.";

    return errs;
  };
  isFormvalid = () => {
    let errs = this.validate();

    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };

  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "email":
        if (!e.currentTarget.value.trim()) return "Email is required";
        else if (e.currentTarget.value.includes("@") === false)
          return "Enter a valid email";
        break;
      case "mobileNo":
        if (!e.currentTarget.value.trim()) return "Mobile No is required";
        else if (e.currentTarget.value.trim().length < 10)
          return "Mobile No must be 10 Digits";
        break;
      default:
        break;
    }
    return "";
  };
  handleInputChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const data = { ...this.state.formdata };
    data[e.currentTarget.name] = e.currentTarget.value;

    this.setState({ formdata: data, errors: errors });
  };
  handleChange = (index, elementName, e) => {
    const { currentTarget: input } = e;
    const { travellersDetails } = this.state;
    if (input.name.includes("adult")) {
      if (elementName === "firstName")
        travellersDetails.adults[index].firstName = input.value;
      if (elementName === "lastName")
        travellersDetails.adults[index].lastName = input.value;
    }
    if (input.name.includes("child")) {
      if (elementName === "firstName")
        travellersDetails.childs[index].firstName = input.value;
      if (elementName === "lastName")
        travellersDetails.childs[index].lastName = input.value;
    }
    if (input.name.includes("infant")) {
      if (elementName === "firstName")
        travellersDetails.infants[index].firstName = input.value;
      if (elementName === "lastName")
        travellersDetails.infants[index].lastName = input.value;
    }
    this.setState({ travellersDetails });
  };

  handleCheckBox = (e) => {
    const { currentTarget: input } = e;
    const { addOns, fareDetails, promoCode } = this.state;
    promoCode.selected = "";
    if (input.name === "cancellation") {
      addOns[0].check = input.checked;
      if (input.checked === true) {
        fareDetails.cancelPolicy = addOns[0].value;
      } else {
        fareDetails.cancelPolicy = 0;
      }
    }
    if (input.name === "insurance") {
      addOns[1].check = input.checked;
      if (input.checked === true) {
        fareDetails.travelProtection = addOns[1].value;
      } else {
        fareDetails.travelProtection = 0;
      }
    }

    this.setState({ addOns, fareDetails, promoCode });
  };
  handleRadioChange = (ele) => {
    const { currentTarget: input } = ele;
    const { promoCode } = this.state;
    promoCode.selected = input.value;
    this.setState({ promoCode });
  };
  render() {
    const {
      formdata,
      errors,
      flight,
      returnFlight,
      fareDetails,
      travellersDetails,
      addOns,
      promoCode,
    } = this.state;
    let totalTravler = 0;
    let promo =
      promoCode.selected === promoCode.codes[0]
        ? 1400
        : promoCode.selected === promoCode.codes[1]
        ? 2000
        : 0;
    totalTravler +=
      travellersDetails.adults && travellersDetails.adults.length > 0
        ? travellersDetails.adults.length
        : 0;
    totalTravler +=
      travellersDetails.childs && travellersDetails.childs.length > 0
        ? travellersDetails.childs.length
        : 0;

    totalTravler +=
      travellersDetails.infants && travellersDetails.infants.length > 0
        ? travellersDetails.infants.length
        : 0;
    return (
      <div className="container-fluid" style={{ backgroundColor: "lightgray" }}>
        <Navbar />
        <form onSubmit={this.handleSubmit}>
          <div className="row mt-4 ml-1">
            <div className="col-lg-9 col-12">
              <div className="row">
                <div className="col-1 text-left" style={{ fontSize: "25px" }}>
                  <i className="fa fa-search"></i>
                </div>
                <div
                  className="col-lg-8 col-10 text-left"
                  style={{
                    color: "#43264e",
                    fontWeight: "500",
                    fontSize: "1.429rem",
                  }}
                >
                  {" "}
                  Review your Bookings
                </div>
              </div>
              <div className="row bg-white" id="box2">
                <div className="col-12">
                  <div className="row bg-white" style={{ paddingTop: "12px" }}>
                    <div className="col-lg-2 col-12 text-center d-none d-lg-block">
                      <div className="row pl-1">
                        <div className="col-lg-12 col-12">
                          <img
                            style={{ width: "40px" }}
                            src={flight ? flight.logo : ""}
                          />
                        </div>
                      </div>
                      <div className="row pl-1">
                        <div className="col-lg-12 col-12">
                          {flight ? flight.name : ""}{" "}
                        </div>
                      </div>
                      <div className="row pl-1 text-secondary">
                        <div className="col-lg-12 col-12">
                          {" "}
                          {flight ? flight.airBus : ""}{" "}
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-2 col-4">
                      <div className="row" id="fs12">
                        {flight ? flight.desDept : ""}
                      </div>
                      <div className="row" id="fs13">
                        {flight ? flight.timeDept : ""}
                      </div>
                      <div className="row text-secondary" id="fs11">
                        {flight ? flight.T1 : ""}
                      </div>
                    </div>
                    <div className="col-lg-6 col-3">
                      <div className="row">
                        <div className="col-lg-4 col-12 text-right" id="fs12">
                          {flight ? flight.total : ""}
                        </div>
                        <div className="col-4 d-none d-lg-block " id="fs12">
                          <span className="text-secondary ">|</span>
                          <span className="text-secondary">
                            {flight ? flight.meal : ""} Meal{" "}
                          </span>
                          <span className="text-secondary ">|</span>
                        </div>
                        <div
                          className="col-4 d-none d-lg-block text-left"
                          id="fs12"
                        >
                          <span className="text-secondary">
                            {flight ? flight.type : ""}
                          </span>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-5 d-none d-lg-block">
                          <hr />
                        </div>
                        <div className="col-lg-2 col-12 mt-1 text-center">
                          <i className="fa fa-fighter-jet" id="ic1"></i>
                        </div>
                        <div className="col-5 d-none d-lg-block">
                          <hr />
                        </div>
                      </div>
                      <div className="row">
                        <div
                          className="col text-center d-none d-lg-block"
                          id="fs12"
                        >
                          {flight ? flight.checkin : ""} kgs |{" "}
                          <span className="text-success">
                            Partially Refundable
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-2 col-5">
                      <div className="row" id="fs12">
                        {flight ? flight.desArr : ""}
                      </div>
                      <div className="row" id="fs13">
                        {flight ? flight.timeArr : ""}
                      </div>
                      <div className="row text-secondary" id="fs11">
                        {flight ? flight.T2 : ""}
                      </div>
                    </div>
                  </div>
                  <div className="row bg-white">
                    <div className="col-2 d-none d-lg-block"></div>
                    <div
                      className="col-lg-10 col-12"
                      style={{
                        backgroundColor: "#fffcc7",
                        padding: "8px 15px 7px 15px",
                        borderRadius: "80px",
                      }}
                    ></div>
                  </div>
                  {returnFlight && returnFlight.id ? (
                    <div
                      className="row bg-white"
                      style={{ paddingTop: "12px" }}
                    >
                      <div className="col-lg-2 col-12 text-center d-none d-lg-block">
                        <div className="row pl-1">
                          <div className="col-lg-12 col-12">
                            <img
                              style={{ width: "40px" }}
                              src={returnFlight ? returnFlight.logo : ""}
                            />
                          </div>
                        </div>
                        <div className="row pl-1">
                          <div className="col-lg-12 col-12">
                            {returnFlight ? returnFlight.name : ""}{" "}
                          </div>
                        </div>
                        <div className="row pl-1 text-secondary">
                          <div className="col-lg-12 col-12">
                            {" "}
                            {returnFlight ? returnFlight.airBus : ""}{" "}
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-2 col-4">
                        <div className="row" id="fs12">
                          {returnFlight ? returnFlight.desDept : ""}
                        </div>
                        <div className="row" id="fs13">
                          {returnFlight ? returnFlight.timeDept : ""}
                        </div>
                        <div className="row text-secondary" id="fs11">
                          {returnFlight ? returnFlight.T1 : ""}
                        </div>
                      </div>
                      <div className="col-lg-6 col-3">
                        <div className="row">
                          <div className="col-lg-4 col-12 text-right" id="fs12">
                            {returnFlight ? returnFlight.total : ""}
                          </div>
                          <div className="col-4 d-none d-lg-block " id="fs12">
                            <span className="text-secondary ">|</span>
                            <span className="text-secondary">
                              {returnFlight ? returnFlight.meal : ""} Meal{" "}
                            </span>
                            <span className="text-secondary ">|</span>
                          </div>
                          <div
                            className="col-4 d-none d-lg-block text-left"
                            id="fs12"
                          >
                            <span className="text-secondary">
                              {returnFlight ? returnFlight.type : ""}
                            </span>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-5 d-none d-lg-block">
                            <hr />
                          </div>
                          <div className="col-lg-2 col-12 mt-1 text-center">
                            <i className="fa fa-fighter-jet" id="ic1"></i>
                          </div>
                          <div className="col-5 d-none d-lg-block">
                            <hr />
                          </div>
                        </div>
                        <div className="row">
                          <div
                            className="col text-center d-none d-lg-block"
                            id="fs12"
                          >
                            {returnFlight ? returnFlight.checkin : ""} kgs |{" "}
                            <span className="text-success">
                              Partially Refundable
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-2 col-5">
                        <div className="row" id="fs12">
                          {returnFlight ? returnFlight.desArr : ""}
                        </div>
                        <div className="row" id="fs13">
                          {returnFlight ? returnFlight.timeArr : ""}
                        </div>
                        <div className="row text-secondary" id="fs11">
                          {returnFlight ? returnFlight.T2 : ""}
                        </div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>

              <br />
              <div className="row bg-white" id="box2">
                <div className="col-12">
                  <div className="row bg-white">
                    <div className="col-1 text-right"></div>
                  </div>
                  <div className="row bg-white">
                    <div className="col-lg-1 col-2 text-right">
                      <input
                        className="form-check-input ng-untouched ng-pristine ng-valid"
                        name={addOns[0].name}
                        type="checkbox"
                        id={addOns[0].name}
                        onChange={this.handleCheckBox}
                        checked={addOns[0].check}
                      />
                    </div>
                    <div className="col-1">
                      <FontAwesomeIcon
                        icon={faPlaneDeparture}
                        style={{ color: "green" }}
                      />
                    </div>
                  </div>
                  <div className="row text-success" id="fs10">
                    <div className="col ml-1 h5">Cancellation Policy</div>
                  </div>
                  <div className="row" id="fs14">
                    <div className="col ml-1">
                      Zero cancellation fee for your tickets when you cancel.
                      Pay additional Rs.747
                    </div>
                  </div>
                  <div className="row bg-white">
                    <div
                      className="col-12 text-center d-none d-lg-block"
                      id="fs14"
                      style={{ backgroundColor: "#FFFCC7" }}
                    >
                      {" "}
                      Travel Smart: Get additional refund of Rs.3,051 in case of
                      cancellation. Terms &amp; Conditions{" "}
                    </div>
                  </div>
                </div>
              </div>
              <div className="row mt-2">
                <div className="col-1 text-right">
                  <FontAwesomeIcon
                    icon={faUserEdit}
                    style={{ fontSize: "25px", color: "red" }}
                  />
                </div>
                <div className="col-11" id="fs10">
                  {" "}
                  Enter Traveller Details |
                  <span
                    className="d-none d-lg-block"
                    style={{
                      fontFamily: "Rubik-Regular,Arial, Helvetica, sans-serif",
                      fontSize: "1rem",
                    }}
                  >
                    Sign in to book faster and use eCash
                  </span>
                </div>
              </div>
              <div className="row bg-white" id="box2">
                <div className="col-12">
                  <div className="row bg-white ">
                    <div className="col-lg-2 col-4" id="fs14">
                      <b>Contact :</b>
                    </div>
                    <form className="ng-invalid ng-dirty ng-touched">
                      <div className="row">
                        <div className="col-6 form-group">
                          <input
                            className="form-control ng-invalid ng-dirty ng-touched"
                            name="email"
                            id="email"
                            placeholder="Enter email"
                            type="text"
                            value={formdata.email}
                            onChange={this.handleInputChange}
                          />
                          {errors.email ? (
                            <div className="text-danger">{errors.email}</div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="col-6 form-group">
                          <input
                            className="form-control ng-untouched ng-pristine ng-invalid"
                            placeholder="Mobile Number"
                            type="number"
                            name="mobileNo"
                            id="mobileNo"
                            value={formdata.mobileNo}
                            onChange={this.handleInputChange}
                          />
                          {errors.mobileNo ? (
                            <div className="text-danger">{errors.mobileNo}</div>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                    </form>
                  </div>
                  <div
                    className="row bg-white"
                    style={{ color: "#666", fontSize: "1rem" }}
                  >
                    <div className="col-2 d-none d-lg-block"></div>
                    <div className="col-lg-10 col-12" id="fs14">
                      {" "}
                      Your booking details will be sent to this email address
                      and mobile number.{" "}
                    </div>
                  </div>
                  <div className="row bg-white">
                    <div className="col-2 d-none d-lg-block"></div>
                    <div className="col-lg-10 col-12" id="fs14">
                      {" "}
                      Also send my booking details on WhatsApp{" "}
                      <i
                        className="fab fa-whatsapp-square"
                        style={{ color: "green", fontSize: "18px" }}
                      ></i>
                    </div>
                  </div>
                  <div className="row bg-white">
                    <div className="col-2 d-none d-lg-block"></div>
                    <div className="col-lg-10 col-12">
                      <hr />
                    </div>
                  </div>
                  <div className="row bg-white">
                    <div className="col-2 d-none d-lg-block"></div>
                    <div className="col-10" id="fs14">
                      <b>Traveller Information</b>
                    </div>
                  </div>
                  <div className="row bg-white" style={{ fontSize: "1rem" }}>
                    <div className="col-2 d-none d-lg-block"></div>
                    <div className="col-lg-10 col-12" id="fs14">
                      <b>Important Note:</b> Please ensure that the names of the
                      passengers on the travel documents is the same as on their
                      government issued identity proof.{" "}
                    </div>
                  </div>
                  <form className="ng-untouched ng-pristine ng-invalid">
                    <div
                      className="row bg-white ng-untouched ng-pristine ng-invalid"
                      name="arr"
                    >
                      <div className="col-12">
                        {this.state.errorsTravler.msg ? (
                          <div className="text-danger text-center">
                            {this.state.errorsTravler.msg}
                          </div>
                        ) : (
                          ""
                        )}
                        {travellersDetails.adults &&
                        travellersDetails.adults.length > 0
                          ? travellersDetails.adults.map((adult, index) => (
                              <div className="row ng-untouched ng-pristine ng-invalid">
                                <div className="col-lg-2 col-12" id="fs14">
                                  <label>Adult {index + 1} :</label>
                                </div>
                                <div className="col-5">
                                  <input
                                    className="form-control ng-untouched ng-pristine ng-invalid"
                                    name={"adultfirstName" + index}
                                    placeholder="First Name"
                                    type="text"
                                    id="firstName"
                                    value={adult.firstName}
                                    onChange={(e) =>
                                      this.handleChange(index, "firstName", e)
                                    }
                                  />
                                </div>
                                <div className="col-5">
                                  <input
                                    className="form-control ng-untouched ng-pristine ng-invalid"
                                    placeholder="Last Name"
                                    name={"adultlastName" + index}
                                    type="text"
                                    id="lastName"
                                    value={adult.lastName}
                                    onChange={(e) =>
                                      this.handleChange(index, "lastName", e)
                                    }
                                  />
                                </div>
                              </div>
                            ))
                          : ""}
                        {travellersDetails.childs &&
                        travellersDetails.childs.length > 0
                          ? travellersDetails.childs.map((child, index) => (
                              <div className="row ng-untouched ng-pristine ng-invalid">
                                <div className="col-lg-2 col-12" id="fs14">
                                  <label>Child {index + 1} :</label>
                                </div>
                                <div className="col-5">
                                  <input
                                    className="form-control ng-untouched ng-pristine ng-invalid"
                                    placeholder="First Name"
                                    type="text"
                                    id="firstName"
                                    value={child.firstName}
                                    name={"childfirstName" + index}
                                    onChange={(e) =>
                                      this.handleChange(index, "firstName", e)
                                    }
                                  />
                                </div>
                                <div className="col-5">
                                  <input
                                    className="form-control ng-untouched ng-pristine ng-invalid"
                                    placeholder="Last Name"
                                    type="text"
                                    id="lastName"
                                    value={child.lastName}
                                    name={"childlastName" + index}
                                    onChange={(e) =>
                                      this.handleChange(index, "lastName", e)
                                    }
                                  />
                                </div>
                              </div>
                            ))
                          : ""}
                        {travellersDetails.infants &&
                        travellersDetails.infants.length > 0
                          ? travellersDetails.infants.map((infant, index) => (
                              <div className="row ng-untouched ng-pristine ng-invalid">
                                <div className="col-lg-2 col-12" id="fs14">
                                  <label>Infant {index + 1} :</label>
                                </div>
                                <div className="col-5">
                                  <input
                                    className="form-control ng-untouched ng-pristine ng-invalid"
                                    placeholder="First Name"
                                    type="text"
                                    id="firstName"
                                    value={infant.firstName}
                                    name={"infantfirstName" + index}
                                    onChange={(e) =>
                                      this.handleChange(index, "firstName", e)
                                    }
                                  />
                                </div>
                                <div className="col-5">
                                  <input
                                    className="form-control ng-untouched ng-pristine ng-invalid"
                                    placeholder="Last Name"
                                    type="text"
                                    id="lastName"
                                    value={infant.lastName}
                                    name={"infantlastName" + index}
                                    onChange={(e) =>
                                      this.handleChange(index, "lastName", e)
                                    }
                                  />
                                </div>
                              </div>
                            ))
                          : ""}
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div className="row bg-white" id="box2">
                <div className="col-12 d-none d-lg-block">
                  <div
                    className="row bg-white"
                    style={{ color: "#666", fontSize: "1rem" }}
                  >
                    <div className="col-1 text-center mt-1">
                      <i
                        class="fa fa-university"
                        style={{ fontSize: "35px" }}
                      ></i>
                    </div>
                    <div className="col-10 text-left">
                      <div className="row" id="fs14">
                        <b>Add your GST Details (Optional)</b>
                      </div>
                      <div className="row" id="fs14">
                        {" "}
                        Claim credit of GST charges. Your taxes may get updated
                        post submitting your GST details.{" "}
                      </div>
                    </div>
                  </div>
                  <div
                    className="row bg-white"
                    style={{ color: "#666", fontSize: "1rem" }}
                  >
                    <div className="col-1 text-center">
                      <FontAwesomeIcon
                        icon={faSuitcaseRolling}
                        style={{ fontSize: "35px" }}
                      />
                    </div>
                    <div className="col-10 text-left">
                      <div className="row" id="fs14">
                        <b>Travelling for work?</b>
                      </div>
                      <div className="row" id="fs14">
                        {" "}
                        Join Yatra for Business. View Benefits{" "}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row mt-2">
                <div
                  className="col-1 text-right"
                  style={{ fontSize: "30px", color: "red" }}
                >
                  <i className="fa fa-umbrella"></i>
                </div>
                <div className="col-11" id="fs10">
                  {" "}
                  Travel Protection
                  <span id="fs14">(Recommended)</span>
                </div>
              </div>
              <div className="row bg-white" id="box2">
                <div className="col-12">
                  <div className="row bg-white">
                    <div className="col-2 text-center">
                      <input
                        className="form-check-input ng-untouched ng-pristine ng-valid"
                        name={addOns[1].name}
                        type="checkbox"
                        id={addOns[1].name}
                        onChange={this.handleCheckBox}
                        checked={addOns[1].check}
                      />
                    </div>
                    <div className="col-10 mt-1" id="fs14">
                      {" "}
                      Yes, Add Travel Protection to protect my trip (Rs.269 per
                      traveller){" "}
                    </div>
                  </div>
                  <div
                    className="row bg-white"
                    style={{ color: "#DB9A00", fontSize: "1.143re" }}
                  >
                    <div className="col-1 d-none d-lg-block"></div>
                    <div className="col-11 d-none d-lg-block" id="fs14">
                      {" "}
                      6000+ travellers on Yatra protect their trip daily.
                      <span className="text-primary">Learn More</span>
                    </div>
                  </div>
                  <div className="row bg-white">
                    <div
                      className="col-12 text-left text-muted d-none d-lg-block"
                      id="fs14"
                    >
                      {" "}
                      Cover Includes:{" "}
                    </div>
                  </div>
                  <div className="row bg-white">
                    <div
                      className="col-4 text-center d-none d-lg-block"
                      id="fs14"
                    >
                      <div className="row">
                        <div className="col text-left">
                          <span className="fa-stack fa-2x">
                            <i className="fa fa-circle fa-stack-2x"></i>
                            <FontAwesomeIcon
                              icon={faPlaneDeparture}
                              className="fa-stack-1x fa-inverse"
                            />
                          </span>
                        </div>
                      </div>
                      <div className="row" style={{ fontSize: "0.87rem" }}>
                        {" "}
                        Trip Cancellation{" "}
                      </div>
                      <div className="row" style={{ fontSize: "0.87rem" }}>
                        {" "}
                        Claim upto Rs.25,000{" "}
                      </div>
                    </div>
                    <div
                      className="col-4 text-center d-none d-lg-block"
                      style={{ fontSize: "1rem" }}
                    >
                      <div className="row mb-1">
                        <div className="col text-left">
                          <span className="fa-stack fa-2x">
                            <i className="fa fa-circle fa-stack-2x"></i>
                            <FontAwesomeIcon
                              icon={faSuitcaseRolling}
                              className="fa-stack-1x fa-inverse"
                            />
                          </span>
                        </div>
                      </div>
                      <div className="row" style={{ fontSize: "0.87rem" }}>
                        {" "}
                        Loss of Baggage{" "}
                      </div>
                      <div className="row" style={{ fontSize: "0.87rem" }}>
                        {" "}
                        Claim upto Rs.25,000{" "}
                      </div>
                    </div>
                    <div
                      className="col-4 text-center d-none d-lg-block"
                      style={{
                        fontSize: "0.87rem",
                        fontFamily:
                          "Rubik-Regular,Arial, Helvetica, sans-serif",
                      }}
                    >
                      <div className="row">
                        <div className="col text-left">
                          <span className="fa-stack fa-2x">
                            <i className="fa fa-circle fa-stack-2x"></i>
                            <i className="fa fa-ambulance fa-stack-1x fa-inverse"></i>
                          </span>
                        </div>
                      </div>
                      <div className="row" style={{ fontSize: "0.87rem" }}>
                        {" "}
                        Medical Emergency{" "}
                      </div>
                      <div className="row" style={{ fontSize: "0.87rem" }}>
                        {" "}
                        Claim upto Rs.25,000{" "}
                      </div>
                    </div>
                  </div>
                  <div className="row bg-white">
                    <div
                      className="col-12 text-center d-none d-lg-block"
                      style={{ fontSize: "0.87rem" }}
                    >
                      {" "}
                      Note: Travel Protection is applicable only for Indian
                      citizens below the age of 70 years.{" "}
                      <span className="text-primary">
                        Terms &amp; Conditions
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-12">
              <div className="row">
                <div
                  className="col"
                  style={{ color: "black", fontWeight: "500" }}
                >
                  {" "}
                  Fare Details{" "}
                </div>
              </div>
              <div className="row bg-white" id="box3">
                <div className="col-12">
                  <div className="row bg-white">
                    <div className="col-8 text-left" id="fs11">
                      {" "}
                      Base Fare(1 Traveller){" "}
                    </div>
                    <div className="col-4 text-right" id="fs11">
                      {" "}
                      ₹ {fareDetails ? fareDetails.baseFare : 0}
                    </div>
                  </div>
                  <div className="row bg-white">
                    <div className="col-8 text-left" id="fs11">
                      {" "}
                      Fees &amp; Surcharge{" "}
                    </div>
                    <div className="col-4 text-right" id="fs11">
                      {" "}
                      ₹ {fareDetails ? fareDetails.surcharge : 0}
                    </div>
                  </div>
                  <div className="row bg-white">
                    <div className="col-12 text-center">
                      <hr />
                    </div>
                  </div>
                  <div className="row bg-white">
                    <div className="col-6 text-left" id="fs12">
                      {" "}
                      Total Fare{" "}
                    </div>
                    <div className="col-6 text-right" id="fs12">
                      {" "}
                      ₹{" "}
                      {fareDetails
                        ? fareDetails.surcharge -
                          promo +
                          fareDetails.baseFare * totalTravler +
                          (addOns[0].check === true ? addOns[0].value : 0) +
                          (addOns[1].check === true
                            ? addOns[1].value * totalTravler
                            : 0)
                        : 0}
                    </div>
                  </div>

                  {addOns[0].check === true || addOns[1].check === true ? (
                    <div className="row bg-white">
                      <div className="col-6 text-left" id="fs12">
                        Add Ons(
                        {(addOns[0].check === true ? 1 : 0) +
                          (addOns[1].check === true ? 1 : 0)}
                        )
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
              <div className="row" id="box4">
                <div className="col-7 text-left" id="fs13">
                  You Pay
                </div>
                <div className="col-5 text-right" id="fs13">
                  ₹{" "}
                  {fareDetails
                    ? fareDetails.surcharge -
                      promo +
                      fareDetails.baseFare * totalTravler +
                      (addOns[0].check === true ? addOns[0].value : 0) +
                      (addOns[1].check === true
                        ? addOns[1].value * totalTravler
                        : 0)
                    : 0}
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col">Promo</div>
              </div>
              <div className="row bg-white ml-1" id="box3">
                <div className="col-12">
                  <div className="row">
                    <div className="col-12"> Select A Promo Code </div>
                  </div>
                  <div className="row">
                    <div className="col-12">
                      <div className="form-check ml-3">
                        <input
                          className="form-check-input ng-untouched ng-pristine ng-valid"
                          type="radio"
                          name="promo"
                          value={promoCode.codes[0]}
                          onChange={this.handleRadioChange}
                          checked={
                            promoCode.selected === promoCode.codes[0]
                              ? true
                              : false
                          }
                        />
                        <label className="form-check-label" id="promobox">
                          <span style={{ fontSize: "12px", color: "#02CB66" }}>
                            {" "}
                            &nbsp;NEWPAY
                          </span>
                        </label>
                        <div className="row">
                          <div
                            className="col-10"
                            style={{ fontSize: "12px", color: "#999" }}
                          >
                            Pay with PayPal to save upto Rs.1400 on Domestic
                            flights (Max. discount Rs. 600 + 50% cashback up to
                            Rs. 800).
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12">
                      <div className="form-check ml-3">
                        <input
                          className="form-check-input ng-untouched ng-pristine ng-valid"
                          type="radio"
                          name="promo"
                          value={promoCode.codes[1]}
                          onChange={this.handleRadioChange}
                          checked={
                            promoCode.selected === promoCode.codes[1]
                              ? true
                              : false
                          }
                        />
                        <label
                          className="form-check-label"
                          //for="exampleRadios1"
                          id="promobox"
                        >
                          <span style={{ fontSize: "12px", color: "#02CB66" }}>
                            {" "}
                            &nbsp;YTAMZ19
                          </span>
                        </label>
                        <div className="row">
                          <div
                            className="col-10"
                            style={{ fontSize: "12px", color: "#999" }}
                          >
                            Save up to Rs.2,000 (Flat 6% (max Rs. 1,000) instant
                            OFF + Flat 5% (max Rs. 1,000) cashback).
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-12 text-center">
              <button
                className="btn btn-danger text-white"
                style={{ borderRadius: "2px" }}
                type="submit"
                disabled={this.isFormvalid()}
              >
                Proceed to Payment
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default BookFlight;
