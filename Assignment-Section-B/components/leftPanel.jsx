import React, { Component } from "react";
import "./icon.css";
import { faPlane } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Calendar from "react-calendar";
import ShowHotels from "./hotels";
class LeftPanel extends Component {
  state = {
    value: "Flights",
    dest: [
      "New Delhi (DEL)",
      "Mumbai (BOM)",
      "Banglore (BLR)",
      "Kolkata (CCU)",
    ],
    selectedFrom: "",
    selectedTo: "",
    travelClass: {
      travelersData: ["Economy", "Premium Economy", "Business"],
      selected: "Economy",
    },
    errors: {},
    departureDateFlag: false,
    returnDateFlag: false,
    travellers: { css: "fa fa-chevron-down", isShow: false },
    travellersDetails: { adultCount: 1, childCount: 0, infantCount: 0 },
    months: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
    weeks: [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ],
    departureDate: new Date(),
    returnDate: this.getReturnDate(),
    selectedBtn: "oneWay",
    isReturnFlight: false,
  };
  getReturnDate() {
    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);
    return tomorrow;
  }
  handleRadioChange = (e) => {
    const { currentTarget: input } = e;
    let { travelClass } = { ...this.state };
    if (input.name === "traveler") {
      travelClass.selected = input.value;
    }
    this.setState(travelClass);
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    if (
      input.name === "selectedFrom" &&
      input.value !== this.state.selectedTo
    ) {
      this.setState({ selectedFrom: input.value });
    } else if (
      input.name === "selectedTo" &&
      input.value !== this.state.selectedFrom
    ) {
      this.setState({ selectedTo: input.value });
    } else {
      alert("Destination and Arrival have to be different");
    }
  };
  handletravellers() {
    let { travellers } = this.state;
    if (travellers.css.includes("up")) {
      travellers.css = "fa fa-chevron-down";
      travellers.isShow = false;
    } else {
      travellers.css = "fa fa-chevron-up";
      travellers.isShow = true;
    }

    this.setState({ travellers });
  }
  handleDepDate = () => {
    this.setState({
      departureDateFlag: this.state.departureDateFlag === true ? false : true,
    });
  };

  getDepartureDate = (value) => {
    this.setState({ departureDate: value, departureDateFlag: false });
  };
  handleReturnDate = () => {
    this.setState({
      returnDateFlag: this.state.returnDateFlag === true ? false : true,
    });
  };
  getReturnDate = (value) => {
    if (value < this.state.departureDate) {
      alert("Select Return Date after Departure Date");
    } else {
      this.setState({ returnDate: value, returnDateFlag: false });
    }
  };
  handleIecrement = (generation) => {
    let { travellersDetails } = this.state;
    if (generation === "Adult") {
      travellersDetails.adultCount++;
    }
    if (generation === "Child") {
      travellersDetails.childCount++;
    }
    if (generation === "Infant") {
      travellersDetails.infantCount++;
    }
    this.setState({ travellersDetails });
  };
  handleDecrement = (generation) => {
    let { travellersDetails } = this.state;
    if (generation === "Adult") {
      travellersDetails.adultCount--;
    }
    if (generation === "Child") {
      travellersDetails.childCount--;
    }
    if (generation === "Infant") {
      travellersDetails.infantCount--;
    }
    this.setState({ travellersDetails });
  };
  validate = () => {
    let errs = {};
    if (
      !this.state.selectedFrom.trim() ||
      this.state.selectedFrom === "Select City"
    )
      errs.msg = "Please enter a valid Source";
    else if (
      !this.state.selectedTo.trim() ||
      this.state.selectedTo === "Select City"
    )
      errs.msg = "Please enter a valid Destination";
    else if (this.state.selectedTo.trim() === this.state.selectedFrom.trim())
      errs.msg = "Destination and Arrival have to be different";
    return errs;
  };
  isFormvalid = () => {
    let errs = this.validate();

    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };
  handleSearchFlights = () => {
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    const {
      selectedTo,
      selectedFrom,
      departureDate,
      months,
      travellersDetails,
      travelClass,
      returnDate,
      isReturnFlight,
    } = this.state;

    let deptDate =
      departureDate.getDate() +
      " " +
      months[departureDate.getMonth()] +
      " " +
      departureDate.getFullYear();
    let arvlDate =
      isReturnFlight === true
        ? returnDate.getDate() +
          " " +
          months[returnDate.getMonth()] +
          " " +
          returnDate.getFullYear()
        : "";

    this.props.onSerachFlight(
      selectedFrom,
      selectedTo,
      deptDate,
      arvlDate,
      travellersDetails,
      travelClass.selected
    );
  };
  handleReturn = () => {
    this.setState({ selectedBtn: "twoWay", isReturnFlight: true });
  };
  handleDeparture = () => {
    this.setState({ selectedBtn: "oneWay", isReturnFlight: false });
  };
  showTextField = (value) => {
    this.setState({ value });
  };
  handleSerachHotel = (
    selectedLoc,
    checkInDate,
    checkOutDate,
    travellersCount,
    rooms
  ) => {
    this.props.onSerachHotel(
      selectedLoc,
      checkInDate,
      checkOutDate,
      travellersCount,
      rooms
    );
  };
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-3">
            <span
              className="fa-stack fa-2x"
              style={{
                color: this.state.value === "Flights" ? "red" : "black",
              }}
              onClick={() => this.showTextField("Flights")}
            >
              <i className="fa fa-circle fa-stack-2x"></i>
              <FontAwesomeIcon
                icon={faPlane}
                className="fa-stack-1x fa-inverse"
              />
            </span>
          </div>
          <div className="col-3">
            <span
              className="fa-stack fa-2x"
              style={{ color: this.state.value === "Hotels" ? "red" : "black" }}
              onClick={() => this.showTextField("Hotels")}
            >
              <i className="fa fa-circle  fa-stack-2x"></i>
              <i className="fa fa-bed fa-stack-1x fa-inverse"></i>
            </span>
          </div>
          <div className="col-3">
            <span
              className="fa-stack fa-2x"
              style={{ color: this.state.value === "Buses" ? "red" : "black" }}
              onClick={() => this.showTextField("Buses")}
            >
              <i className="fa fa-circle  fa-stack-2x"></i>
              <i className="fa fa-bus fa-stack-1x fa-inverse"></i>
            </span>
          </div>
          <div className="col-3">
            <span
              className="fa-stack fa-2x"
              style={{ color: this.state.value === "Taxis" ? "red" : "black" }}
              onClick={() => this.showTextField("Taxis")}
            >
              <i className="fa fa-circle fa-stack-2x"></i>
              <i className="fa fa-taxi fa-stack-1x fa-inverse"></i>
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col text-center text-dark">
            <h4>{this.state.value}</h4>
          </div>
          {this.state.value === "Hotels" ? (
            <div className="row">
              <ShowHotels onSerachHotel={this.handleSerachHotel} />
            </div>
          ) : (
            ""
          )}
        </div>
        {this.state.value === "Flights" ? (
          <div className="row">{this.renderFlight()}</div>
        ) : (
          ""
        )}
      </div>
    );
  }

  renderFlight() {
    const {
      travelClass,
      departureDate,
      returnDate,
      travellersDetails,
      errors,
    } = this.state;
    return (
      <div className="container">
        <div className="row">
          <div className="col-12 text-center">
            <button
              type="button"
              className="btn btn-outline-primary btn-sm"
              style={{
                backgroundColor:
                  this.state.selectedBtn === "oneWay" ? "#1a53ff" : "#fff",
                color: this.state.selectedBtn === "oneWay" ? "#fff" : "#1a53ff",
              }}
              onClick={() => this.handleDeparture()}
            >
              One Way
            </button>
            <button
              type="button"
              className="btn btn-outline-primary btn-sm ml-1"
              onClick={() => this.handleReturn()}
              style={{
                backgroundColor:
                  this.state.selectedBtn === "twoWay" ? "#1a53ff" : "#fff",
                color: this.state.selectedBtn === "twoWay" ? "#fff" : "#1a53ff",
              }}
            >
              Return
            </button>
          </div>
        </div>
        <form onSubmit={this.handleSearchFlights}>
          <div className="row mt-2">
            <div className="col-5">
              <h6>Depart Form</h6>
              <div>
                <select
                  className="browser-default custom-select"
                  value={this.state.selectedFrom}
                  onChange={this.handleChange}
                  id="selectedFrom"
                  name="selectedFrom"
                >
                  <option>Select city</option>
                  {this.state.dest.map((item) => (
                    <option key={item}>{item}</option>
                  ))}
                </select>
              </div>
            </div>

            <div className="col-lg-2 col-1 mt-4 d-none d-lg-block">
              <i
                className="fa fa-arrow-right"
                style={{ fontSize: "40px", color: "#9fdbac" }}
              ></i>
            </div>

            <div className="col-5">
              <h6>Going To</h6>
              <div>
                <select
                  className="browser-default custom-select"
                  value={this.state.selectedTo}
                  onChange={this.handleChange}
                  id="selectedTo"
                  name="selectedTo"
                >
                  <option selected>Select city</option>
                  {this.state.dest.map((item) => (
                    <option key={item}>{item}</option>
                  ))}
                </select>
              </div>
            </div>
          </div>
          <div className="row mt-2">
            <div className="text-danger col-12">
              {errors.msg ? errors.msg : ""}
            </div>
          </div>
          <hr />
          <div className="row mt-2">
            <div className="col-6">
              <h6>Departure Date</h6>
              <div className="h6" onClick={() => this.handleDepDate()}>
                {departureDate.getDate() +
                  " " +
                  this.state.months[departureDate.getMonth()] +
                  ", " +
                  departureDate.getFullYear()}
              </div>
              <div>{this.state.weeks[departureDate.getDay()]}</div>
              {this.state.departureDateFlag === true ? (
                <div>
                  <Calendar
                    height={280}
                    width={280}
                    tileHeight={35}
                    style={{ alignSelf: "center" }}
                    topbarVisible={true}
                    showOtherDates="false"
                    datesSelection={"single"}
                    onClickDay={this.getDepartureDate}
                  />
                </div>
              ) : (
                ""
              )}
            </div>
            <div className="col-6">
              <h6>Return Date</h6>
              {this.state.selectedBtn === "twoWay" ? (
                ""
              ) : (
                <p>
                  <a href="#">
                    <small>Book round trip to save extra</small>
                  </a>
                </p>
              )}
              {this.state.selectedBtn === "twoWay" ? (
                <React.Fragment>
                  <div className="h6" onClick={() => this.handleReturnDate()}>
                    {returnDate.getDate() +
                      " " +
                      this.state.months[returnDate.getMonth()] +
                      ", " +
                      returnDate.getFullYear()}
                  </div>
                  <div>{this.state.weeks[returnDate.getDay()]}</div>
                  {this.state.returnDateFlag === true ? (
                    <div>
                      <Calendar
                        height={280}
                        width={280}
                        tileHeight={35}
                        style={{ alignSelf: "center" }}
                        topbarVisible={true}
                        showOtherDates="false"
                        datesSelection={"single"}
                        onClickDay={this.getReturnDate}
                      />
                    </div>
                  ) : (
                    ""
                  )}
                </React.Fragment>
              ) : (
                ""
              )}
            </div>
          </div>
          <hr />
          <div className="row mt-2">
            <div className="col-5">
              <h6>Travellers, class</h6>
              <div>
                {travellersDetails.adultCount +
                  travellersDetails.infantCount +
                  travellersDetails.childCount}{" "}
                Traveller,{this.state.travelClass.selected}
              </div>
            </div>
            <div className="col-6"></div>
            <div className="col-1">
              <i
                className={this.state.travellers.css}
                onClick={() => this.handletravellers()}
              ></i>
            </div>
          </div>
          {this.state.travellers.isShow ? (
            <div>
              <div className="row mt-2">
                <div className="col-4">
                  <h6>Adult</h6>
                  <div className="row ml-1">
                    <h6 className="col-1 border">
                      <small
                        className="text-muted"
                        style={{ cursor: "pointer" }}
                        onClick={() => this.handleDecrement("Adult")}
                      >
                        -
                      </small>
                    </h6>
                    <h6 className="col-1 border bg-light">
                      <small className="text-muted">
                        {travellersDetails.adultCount}
                      </small>
                    </h6>
                    <h6 className="col-1 border">
                      <small
                        className="text-muted"
                        style={{ cursor: "pointer" }}
                        onClick={() => this.handleIecrement("Adult")}
                      >
                        +
                      </small>
                    </h6>
                  </div>
                </div>
                <div className="col-4">
                  <h6>Child(2-12 yrs)</h6>
                  <div className="row ml-1">
                    <h6 className="col-1 border">
                      <small
                        className="text-muted"
                        style={{ cursor: "pointer" }}
                        onClick={() => this.handleDecrement("Child")}
                      >
                        -
                      </small>
                    </h6>
                    <h6 className="col-1 border bg-light">
                      <small className="text-muted">
                        {travellersDetails.childCount}
                      </small>
                    </h6>
                    <h6 className="col-1 border">
                      <small
                        className="text-muted"
                        style={{ cursor: "pointer" }}
                        onClick={() => this.handleIecrement("Child")}
                      >
                        +
                      </small>
                    </h6>
                  </div>
                </div>
                <div className="col-4">
                  <h6>Infant(Below 2 yrs)</h6>
                  <div className="row ml-1">
                    <h6 className="col-1 border">
                      <small
                        className="text-muted"
                        style={{ cursor: "pointer" }}
                        onClick={() => this.handleDecrement("Infant")}
                      >
                        -
                      </small>
                    </h6>
                    <h6 className="col-1 border bg-light">
                      <small className="text-muted">
                        {travellersDetails.infantCount}
                      </small>
                    </h6>
                    <h6 className="col-1 border">
                      <small
                        className="text-muted"
                        style={{ cursor: "pointer" }}
                        onClick={() => this.handleIecrement("Infant")}
                      >
                        +
                      </small>
                    </h6>
                  </div>
                </div>
              </div>
              <div className="row mt-2">
                <div className="ml-2">
                  {travelClass.travelersData.map((traveler) => (
                    <div className="form-check mt-1">
                      <input
                        value={traveler}
                        onChange={this.handleRadioChange}
                        name="traveler"
                        id="traveler"
                        type="radio"
                        checked={
                          travelClass.selected === traveler ? true : false
                        }
                        className="form-check-input"
                      />
                      <label className="form-check-label" htmlFor={traveler}>
                        {traveler}
                      </label>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
          <hr />
          <div className="row mt-2">
            <div className="col-8"></div>
            <div className="col-3">
              <button
                className="btn btn-danger"
                type="submit"
                disabled={this.isFormvalid()}
              >
                Search Flights
                <i className="fa fa-arrow-right ml-1" aria-hidden="true"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default LeftPanel;
