import React, { Component } from "react";
import { faPlane } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import config from "./config.json";
import axios from "axios";
import "./icon.css";
import queryString from "query-string";
import FilterComponent from "./filterComponent";
import Navbar from "./navbar";
class YatrairSearch extends Component {
  state = {
    searchFlights: [],
    searchReturnFlights: [],
    filterHide: false,
    returnFlightID: "",
    flightID: "",
    priceData: {
      prices: ["0-5000", "5000-10000", "10000-15000", "15000-20000"],
      selected: "",
    },
    timesData: {
      times: ["0-6", "6-12", "12-18", "18-00"],
      selected: "",
    },

    airLines: [
      { name: "Go Air", value: "Go Air", check: false },
      { name: "Indigo", value: "Indigo", check: false },
      { name: "SpiceJet", value: "SpiceJet", check: false },
      { name: "Air India", value: "Air India", check: false },
    ],
    airCrafts: [
      { name: "Airbus A320 Neo", value: "Airbus A320 Neo", check: false },
      { name: "AirbusA320", value: "AirbusA320", check: false },
      { name: "Boeing737", value: "Boeing737", check: false },
      { name: "Airbus A320-100", value: "Airbus A320-100", check: false },
    ],
    arrival: "",
    departure: "",
    departureDate: "",
    arrivalDate: "",
    travellersDetails: {},
    type: "",
    isClearFilter: false,
    isReturnFlight: false,
  };

  async componentDidMount() {
    let departure = "",
      departureDate = "",
      arrivalDate = "",
      travellersDetails = {},
      type = "",
      returnFlightID = "",
      flightID = "";
    let arrival = "";
    if (this.props.location.state) {
      departure = this.props.location.state.departure;
      departureDate = this.props.location.state.departureDate;
      arrivalDate = this.props.location.state.arrivalDate;
      arrival = this.props.location.state.arrival;
      travellersDetails = this.props.location.state.travellersDetails;
      type = this.props.location.state.type;
    }
    let apiEndPoint =
      config.apiEndPoint + "/flights/" + departure + "/" + arrival;
    const { data: searchFlights } = await axios.get(apiEndPoint);
    const { data: searchReturnFlights } = await axios.get(
      config.apiEndPoint + "/flights/" + arrival + "/" + departure
    );
    for (let index = 0; index < searchFlights.length; index++) {
      searchFlights[index].flightDetails = {
        css: "fa fa-chevron-down",
        isShow: false,
      };
      if (index === 0) {
        flightID = searchFlights[index].id;
      }
    }
    for (let index = 0; index < searchReturnFlights.length; index++) {
      searchReturnFlights[index].flightDetails = {
        css: "fa fa-chevron-down",
        isShow: false,
      };
      if (index === 0) {
        returnFlightID = searchReturnFlights[index].id;
      }
    }
    this.setState({
      searchFlights: searchFlights,
      arrival: arrival,
      departure: departure,
      departureDate: departureDate,
      arrivalDate: arrivalDate,
      travellersDetails: travellersDetails,
      type: type,
      isReturnFlight: arrivalDate === "" ? false : true,
      searchReturnFlights: searchReturnFlights,
      returnFlightID: returnFlightID,
      flightID: flightID,
    });
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      const { departure, arrival } = this.state;
      let apiEndPoint =
        config.apiEndPoint + "/flights/" + departure + "/" + arrival;
      apiEndPoint = apiEndPoint + this.props.location.search;
      const { data: searchFlights } = await axios.get(apiEndPoint);
      const { data: searchReturnFlights } = await axios.get(
        config.apiEndPoint +
          "/flights/" +
          arrival +
          "/" +
          departure +
          this.props.location.search
      );
      for (let index = 0; index < searchFlights.length; index++) {
        searchFlights[index].flightDetails = {
          css: "fa fa-chevron-down",
          isShow: false,
        };
      }
      for (let index = 0; index < searchReturnFlights.length; index++) {
        searchReturnFlights[index].flightDetails = {
          css: "fa fa-chevron-down",
          isShow: false,
        };
      }
      this.setState({
        searchFlights: searchFlights,
        searchReturnFlights: searchReturnFlights,
      });
    }
  }
  handleSearchAgain = () => {
    window.location = "/yatra";
  };
  handleBookFlight = (flight) => {
    const { travellersDetails } = this.state;
    flight.pickedDate = this.state.departureDate;
    flight.type = this.state.type;
    flight.dept = flight.desDept;
    flight.arr = flight.desArr;
    flight.count = travellersDetails
      ? travellersDetails.adultCount +
        travellersDetails.childCount +
        travellersDetails.infantCount
      : 0;
    flight.travellers = [
      { name: "Adult", count: travellersDetails.adultCount },
      { name: "Child", count: travellersDetails.childCount },
      { name: "Infant", count: travellersDetails.infantCount },
    ];
    this.props.history.push({
      pathname: "/booking",
      state: {
        flight: flight,
        travellersDetails: this.state.travellersDetails,
      },
    });
  };
  handleFilterData = () => {
    this.setState({ filterHide: true });
  };
  handleCancelBtn = () => {
    this.setState({ filterHide: false });
  };
  handleClearFilter = () => {
    const { priceData, timesData, airLines, airCrafts } = this.state;
    priceData.selected = "";
    timesData.selected = "";
    airLines.map((airLine) => (airLine.check = false));
    airCrafts.map((airCraft) => (airCraft.check = false));
    let { sort } = queryString.parse(this.props.location.search);
    this.setState({
      filterHide: false,
      priceData,
      timesData,
      isClearFilter: false,
      airCrafts,
      airLines,
    });
    this.calURL("", sort, "", "", "", "");
  };
  handleOptionChange = (priceData, timesData, airLines, airCrafts) => {
    let flight = this.buildQueryString(airLines);
    let airbus = this.buildQueryString(airCrafts);
    let isClearFilter = this.state.isClearFilter;
    if (
      flight !== "" ||
      airbus !== "" ||
      priceData.selected !== "" ||
      timesData.selected !== ""
    ) {
      isClearFilter = true;
    } else {
      isClearFilter = false;
    }
    this.setState({
      priceData,
      timesData,
      airLines,
      airCrafts,
      filterHide: false,
      isClearFilter: isClearFilter,
    });
    let { sort } = queryString.parse(this.props.location.search);
    this.calURL(
      "",
      sort,
      priceData.selected,
      timesData.selected,
      flight,
      airbus
    );
  };
  buildQueryString(optionsData) {
    let filterData = optionsData.filter((n1) => n1.check);
    let arrayData = filterData.map((n1) => n1.value);
    return arrayData.join(",");
  }
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  calURL = (params, sort, price, time, flight, airbus) => {
    let path = "/yatra/airSearch";
    params = this.addToParams(params, "price", price);
    params = this.addToParams(params, "time", time);
    params = this.addToParams(params, "flight", flight);
    params = this.addToParams(params, "airbus", airbus);
    params = this.addToParams(params, "sort", sort);

    this.props.history.push({
      pathname: path,
      search: params,
    });
  };
  handleSortBy = (sortBy) => {
    let { price, time, flight, airbus, sort } = queryString.parse(
      this.props.location.search
    );
    if (sort && sort === sortBy) {
      sort = "";
    } else {
      sort = sortBy;
    }

    this.calURL("", sort, price, time, flight, airbus);
  };
  handleFlight = (flight, way) => {
    if (way === "return") {
      const { searchReturnFlights } = this.state;
      const flights = [...searchReturnFlights];
      const index = flights.indexOf(flight);
      if (flights[index].flightDetails.css.includes("up")) {
        flights[index].flightDetails.css = "fa fa-chevron-down";
        flights[index].flightDetails.isShow = false;
      } else {
        flights[index].flightDetails.css = "fa fa-chevron-up";
        flights[index].flightDetails.isShow = true;
      }
      this.setState({ searchReturnFlights: flights });
    } else {
      const { searchFlights } = this.state;
      const flights = [...searchFlights];
      const index = flights.indexOf(flight);
      if (flights[index].flightDetails.css.includes("up")) {
        flights[index].flightDetails.css = "fa fa-chevron-down";
        flights[index].flightDetails.isShow = false;
      } else {
        flights[index].flightDetails.css = "fa fa-chevron-up";
        flights[index].flightDetails.isShow = true;
      }
      this.setState({ searchFlights: flights });
    }
  };
  handleFlightRadioChange = (flightName, e) => {
    const { currentTarget: input } = e;
    let { searchFlights, searchReturnFlights } = this.state;
    if (flightName === "flight") {
      let flight = searchFlights.find((flight) => flight.id === input.name);
      if (flight) {
        this.setState({ flightID: flight.id });
      }
    }
    if (flightName === "returnFlight") {
      let flight = searchReturnFlights.find(
        (returnflight) => returnflight.id === input.name
      );
      if (flight) {
        this.setState({ returnFlightID: flight.id });
      }
    }
  };
  handleBookreturnFlight = (singleFlight, returnFlight) => {
    const { travellersDetails } = this.state;
    singleFlight.pickedDate = this.state.departureDate;
    singleFlight.type = this.state.type;
    singleFlight.dept = singleFlight.desDept;
    singleFlight.arr = singleFlight.desArr;
    singleFlight.count = travellersDetails
      ? travellersDetails.adultCount +
        travellersDetails.childCount +
        travellersDetails.infantCount
      : 0;
    singleFlight.travellers = [
      { name: "Adult", count: travellersDetails.adultCount },
      { name: "Child", count: travellersDetails.childCount },
      { name: "Infant", count: travellersDetails.infantCount },
    ];
    returnFlight.pickedDate = this.state.departureDate;
    returnFlight.type = this.state.type;
    returnFlight.dept = returnFlight.desDept;
    returnFlight.arr = returnFlight.desArr;
    this.props.history.push({
      pathname: "/booking",
      state: {
        flight: singleFlight,
        travellersDetails: this.state.travellersDetails,
        returnFlight: returnFlight,
      },
    });
  };
  render() {
    const {
      searchFlights,
      arrival,
      departure,
      departureDate,
      type,
      travellersDetails,
    } = this.state;
    let sort = "";
    if (this.props.location) {
      sort = queryString.parse(this.props.location.search).sort;
    }
    let sortByCss = "col-lg-2 col-3 text-right";
    return (
      <div className="container-fluid">
        <Navbar />
        <div
          className="row mt-4 pt-2 pb-2 "
          style={{
            background: "linear-gradient(to right, #b7243a,#132522)",
            color: "white",
          }}
        >
          <div className="col-1 text-center mt-1 d-none d-lg-block">
            <i className="fa fa-fighter-jet" style={{ fontSize: "26px" }}></i>
          </div>
          <div className="col-lg-2 col-5">
            <div className="row" id="abc10">
              <div className="col">From</div>
            </div>
            <div className="row" id="abc">
              <div className="col">
                <b>{departure}</b>
              </div>
            </div>
          </div>
          <div className="col-1 mt-1 ">
            <i className="fa fa-exchange"></i>
          </div>
          <div className="col-lg-2 col-5 ">
            <div className="row" id="abc10">
              <div className="col">To</div>
            </div>
            <div className="row" id="abc">
              <div className="col">
                <b>{arrival}</b>
              </div>
            </div>
          </div>
          <div className="col-2 d-none d-lg-block">
            <div
              className="row"
              style={{ fontSize: ".71429rem", fontFamily: "Rubik-Medium" }}
            >
              Date
            </div>
            <div
              className="row"
              style={{ fontSize: "1.14286rem", fontFamily: "Rubik-Medium" }}
            >
              <b>{departureDate}</b>
            </div>
          </div>
          <div className="col-2 d-none d-lg-block">
            <div
              className="row"
              style={{ fontSize: ".71429rem", fontFamily: "Rubik-Medium" }}
            >
              Traveller(s)
            </div>
            <div
              className="row"
              style={{ fontSize: "1.14286rem", fontFamily: "Rubik-Medium" }}
            >
              <b>
                {travellersDetails
                  ? travellersDetails.adultCount +
                    travellersDetails.childCount +
                    travellersDetails.infantCount
                  : 0}
                , {type ? type : ""}
              </b>
            </div>
          </div>
          <div className="col-1 mt-1 d-none d-lg-block">
            <button
              className="btn btn-danger text-white router-link-active"
              routerlinkactive="router-link-active"
              tabindex="0"
              onClick={() => this.handleSearchAgain()}
            >
              Search Again
            </button>
          </div>
        </div>

        <div className="mainExpand">
          <div
            className="row pt-2 pb-2"
            id="id1"
            style={{ boxShadow: "0 2px 4px 0 #ffa2a2" }}
          >
            <div
              className="col-1 text-center d-none d-lg-block"
              style={{ fontSize: "14px" }}
            >
              <i className="fa fa-filter"></i> Filter{" "}
            </div>
            {this.state.filterHide === false ? (
              <React.Fragment>
                <div
                  className="col-lg-2 col-3 text-center"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.handleFilterData()}
                >
                  {" "}
                  Price <i className="fa fa-angle-down"></i>
                </div>
                <div
                  className="col-lg-2 col-3 text-center"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.handleFilterData()}
                >
                  {" "}
                  Depart
                  <i className="fa fa-angle-down"></i>
                </div>
                <div
                  className="col-lg-2 col-3 text-center"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.handleFilterData()}
                >
                  {" "}
                  Airline <i className="fa fa-angle-down"></i>
                </div>
                <div
                  className="col-lg-2 col-3 text-center"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.handleFilterData()}
                >
                  {" "}
                  Aircraft
                  <i className="fa fa-angle-down"></i>
                </div>
                {this.state.isClearFilter ? (
                  <div
                    className="col-1"
                    onClick={() => this.handleClearFilter()}
                  >
                    <button className="btn btn-danger text-white">
                      <strong>Clear Filters</strong>
                    </button>
                  </div>
                ) : (
                  ""
                )}
              </React.Fragment>
            ) : (
              <React.Fragment>
                <div
                  className="col-lg-8 col-3 text-right"
                  onClick={() => this.handleCancelBtn()}
                >
                  <button className="btn btn-outline-dark text-secondary">
                    <strong>Cancel</strong>
                  </button>
                </div>
                {this.state.isClearFilter ? (
                  <div
                    className="col-1"
                    onClick={() => this.handleClearFilter()}
                  >
                    <button className="btn btn-danger text-white">
                      <strong>Clear Filters</strong>
                    </button>
                  </div>
                ) : (
                  ""
                )}
              </React.Fragment>
            )}
          </div>
          {this.state.filterHide === true ? (
            <FilterComponent
              priceData={this.state.priceData}
              airLines={this.state.airLines}
              timesData={this.state.timesData}
              airCrafts={this.state.airCrafts}
              onOptionChange={this.handleOptionChange}
            />
          ) : (
            ""
          )}
        </div>
        <br />
        <div className="row">
          <div className="col-lg-9 col-12">
            <div
              className="row pl-4 pt-1 pb-1 mb-4"
              id="id1"
              style={{
                backgroundColor: "#E5E7EB",
                borderRadius: "3px",
                marginLeft: "7px",
              }}
            >
              <div className="col-2">
                <strong>Sort By:</strong>
              </div>
              <div
                style={{ cursor: "pointer", fontSize: ".78571rem" }}
                className={
                  sort === "arrival" ? sortByCss + " text-primary" : sortByCss
                }
                onClick={() => this.handleSortBy("arrival")}
              >
                ARRIVE{" "}
                {sort === "arrival" ? <i class="fa fa-arrow-up"></i> : ""}
              </div>
              <div
                style={{ cursor: "pointer", fontSize: ".78571rem" }}
                className={
                  sort === "departure" ? sortByCss + " text-primary" : sortByCss
                }
                onClick={() => this.handleSortBy("departure")}
              >
                DEPART{" "}
                {sort === "departure" ? <i class="fa fa-arrow-up"></i> : ""}
              </div>

              <div
                style={{ cursor: "pointer", fontSize: ".78571rem" }}
                className={
                  sort === "price" ? sortByCss + " text-primary" : sortByCss
                }
                onClick={() => this.handleSortBy("price")}
              >
                {" "}
                PRICE {sort === "price" ? <i class="fa fa-arrow-up"></i> : ""}
              </div>
            </div>
            {this.state.isReturnFlight === false
              ? searchFlights.length > 0
                ? searchFlights.map((flight) => this.renderFlight(flight))
                : ""
              : this.renderRetunFlight()}
          </div>
          <div className="col-2 ml-1 d-none d-lg-block">
            <img
              src="https://i.ibb.co/18cngjz/banner-1575268481164-ICICI-Dom-Flight-New-Homepage-SRP-182-X300.png"
              style={{ width: "auto" }}
            />
          </div>
        </div>
      </div>
    );
  }
  renderFlight = (flight) => {
    return (
      <div className="row mt-1 ml-3 pt-2" id="box1">
        <div className="col" id={flight.id}>
          <div className="row mb-1">
            <div className="col-lg-1 col-2">
              <img
                style={{
                  width: "28px",
                  height: "28px",
                  borderRadius: "5px",
                }}
                src={flight.logo}
              />
            </div>
            <div className="col-lg-1 col-3">
              <div
                className="row"
                style={{
                  fontSize: ".92857rem",
                  fontFamily: "Rubik-Regular",
                }}
              >
                <div className="col d-none d-lg-block">{flight.name}</div>
              </div>
              <div
                className="row text-muted"
                style={{
                  fontSize: ".78571rem",
                  fontFamily: "Rubik-Regular",
                  color: "#999",
                }}
              >
                {" "}
                {flight.code}
              </div>
            </div>
            <div
              className="col-lg-1 col-3 text-right"
              style={{
                fontSize: "1.07143rem",
                fontFamily: "Rubik-Regular",
                fontWeight: "400",
              }}
            >
              <div className="row">
                <strong>{flight.timeDept}</strong>
              </div>
              <div className="row" style={{ fontSize: "x-small" }}>
                {flight.desDept}
              </div>
            </div>
            <div className="col-1 d-none d-lg-block">
              <hr />
            </div>
            <div
              className="col-lg-1 col-3 text-left"
              style={{
                fontSize: "1.07143rem",
                fontFamily: "Rubik-Regular",
                fontWeight: "400",
              }}
            >
              <div className="row">
                <strong>{flight.timeArr}</strong>
              </div>
              <div className="row" style={{ fontSize: "x-small" }}>
                {flight.desArr}
              </div>
            </div>
            <div className="col-1 pt-1 d-none d-lg-block">
              <span
                style={{
                  borderLeft: "3px solid lightgrey",
                  height: "60px",
                }}
              ></span>
            </div>
            <div
              className="col-1 d-none d-lg-block text-left"
              style={{
                fontSize: ".78571rem",
                fontFamily: "Rubik-Regular",
              }}
            >
              <div className="row">
                <strong>{flight.total}</strong>
              </div>
              <div className="row">Non-Stop</div>
            </div>
            <div className="col-2 d-none d-lg-block"></div>
            <div
              className="col-lg-2 col-5 text-right"
              style={{ fontSize: "1.28571rem" }}
            >
              <strong>₹ {flight.Price}</strong>
            </div>
            <div className="col-lg-1 col-3 text-right">
              <button
                onClick={() => this.handleBookFlight(flight)}
                className="btn btn-sm btn-outline-danger text-danger"
                id="book"
              >
                <strong> Book </strong>
              </button>
            </div>
          </div>
          <hr style={{ padding: "0", margin: "0" }} />
          <div className="row pb-1">
            <div className="col-7 d-none d-lg-block">
              <a
                style={{
                  fontSize: "12px",
                  color: "blue",
                  cursor: "pointer",
                }}
              >
                Flight Details
                <i
                  className={flight.flightDetails.css}
                  onClick={() => this.handleFlight(flight, "oneWay")}
                ></i>
              </a>
            </div>
            <div className="col-5 text-right d-none d-lg-block">
              <span id="rect"> eCash</span>
              <span id="rect1">₹ 250</span>
            </div>
          </div>
          {flight.flightDetails.isShow ? (
            <div className="row" id={flight.id}>
              <div className="col-7 ">
                <div
                  className="row"
                  style={{
                    backgroundColor: "#301b41",
                    fontSize: "20px",
                    paddingTop: "6px",
                    paddingBottom: "6px",
                    paddingLeft: "8px",
                    color: "white",
                    fontWeight: "500",
                  }}
                >
                  {" "}
                  Flight Details{" "}
                </div>
                <div className="row">
                  <div className="col-2">
                    <img style={{ width: "40px" }} src={flight.logo} />
                  </div>
                  <div className="col-7 text-left">
                    <div className="row" style={{ fontSize: "12px" }}>
                      {flight.name}
                    </div>
                    <div
                      className="row"
                      style={{ fontSize: "10px", color: "grey" }}
                    >
                      {" "}
                      {flight.airBus}
                    </div>
                  </div>
                  <div className="col-3 text-right">
                    <img
                      src="https://i.ibb.co/31BTG9K/icons8-food-100.png"
                      style={{ width: "45px" }}
                    />
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-3 ml-1" style={{ fontSize: "14px" }}>
                    <div className="row ml-1">{flight.desDept}</div>
                    <div className="row ml-1">
                      <strong>{flight.timeDept}</strong>
                    </div>
                    <div className="row ml-1" style={{ fontSize: "12px" }}>
                      {flight.T1}
                    </div>
                  </div>
                  <div className="col-5">
                    <div className="row" style={{ fontSize: "12px" }}>
                      <div className="col text-center">Time Taken</div>
                    </div>
                    <div className="row">
                      <div className="col-9">
                        <hr />
                      </div>
                      <div className="col-2 pt-2" style={{ fontSize: "10px" }}>
                        <FontAwesomeIcon icon={faPlane} />
                      </div>
                    </div>
                  </div>
                  <div className="col-3" style={{ fontSize: "14px" }}>
                    <div className="row">{flight.desArr}</div>
                    <div className="row">
                      <strong>{flight.timeArr}</strong>
                    </div>
                    <div className="row" style={{ fontSize: "12px" }}>
                      {flight.T2}
                    </div>
                  </div>
                </div>
                <br />
                <div
                  className="row ml-2 mr-2"
                  style={{
                    fontSize: "12px",
                    borderRadius: "5px",
                    backgroundColor: "#F9F9F9",
                  }}
                >
                  <div className="col text-center">
                    Checkin Baggage:
                    <i
                      className="fa fa-briefcase"
                      style={{ fontSize: "10px" }}
                    ></i>{" "}
                    {flight.checkin}kg
                  </div>
                </div>
              </div>
              <div
                className="col-5"
                style={{ backgroundColor: "#5a426d", color: "white" }}
              >
                <div className="row pl-1 pt-3 pb-3">
                  <div className="col-6 border-bottom">Fare Summary</div>
                  <div className="col-6 ">Fare Rules</div>
                </div>
                <div className="row" style={{ fontSize: "14px" }}>
                  <div className="col-4">Fare Summary</div>
                  <div className="col-4">Base and Fare</div>
                  <div className="col-4">Fees and Taxes</div>
                </div>
                <div
                  className="row"
                  style={{ fontSize: "12px", color: "#fff" }}
                >
                  <div className="col-4">Adult X 1</div>
                  <div className="col-4">₹ {flight.Price}</div>
                  <div className="col-4">₹ 1000</div>
                </div>
                <hr />
                <div className="row" style={{ fontSize: "16px" }}>
                  <div className="col-6">You Pay:</div>
                  <div className="col-6 text-right">₹ {flight.Price}</div>
                </div>
                <div className="row" style={{ fontSize: ".85714rem" }}>
                  <div className="col-12">
                    {" "}
                    Note: Total fare displayed above has been rounded off and
                    may show a slight difference from actual fare.{" "}
                  </div>
                </div>
                <div className="row pt-2 pb-2">
                  <div className="col-12 text-center">
                    <button
                      className="btn btn-danger text-white btn-block"
                      routerlinkactive="router-link-active"
                      tabindex="0"
                      onClick={() => this.handleBookFlight(flight)}
                    >
                      Book
                    </button>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
        </div>
        <br />
      </div>
    );
  };
  renderRetunFlight = () => {
    const {
      arrival,
      departure,
      searchFlights,
      searchReturnFlights,
      returnFlightID,
      flightID,
    } = this.state;
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-6">
            <div
              className="row pl-4"
              style={{
                backgroundColor: "#E5E7EB",
                borderRadius: "3px",
                marginLeft: "7px",
                fontSize: ".78571rem",
              }}
            >
              <div className="col-12">
                {" "}
                {departure} <i className="fa fa-arrow-right"></i> {arrival}
              </div>
            </div>
            <br />
            {searchFlights.map((flight, index) => (
              <div
                className="row  mt-1 ml-3 pt-2"
                id="fs17"
                style={{ borderRadius: "5px" }}
              >
                <div className="col">
                  <div className="row">
                    <div className="col-lg-2 col-3">
                      <img id="img1" src={flight.logo} />
                    </div>
                    <div className="col-lg-1 col-3 text-right" id="fs9">
                      <div className="row">{flight.timeDept}</div>
                      <div className="row" style={{ fontSize: "x-small" }}>
                        {flight.name}
                      </div>
                    </div>
                    <div className="col-lg-2 col-2">
                      <hr style={{ border: "solid 1px lightgrey" }} />
                    </div>
                    <div className="col-lg-1 col-3 text-left" id="fs9">
                      <div className="row">{flight.timeArr}</div>
                    </div>
                    <div className="col-1 pt-1 d-none d-lg-block">
                      <span
                        style={{
                          borderLeft: "3px solid lightgrey",
                          height: "60px",
                        }}
                      ></span>
                    </div>
                    <div className="col-lg-2 col-6 text-right" id="fs8">
                      <div className="row">
                        <div className="col">{flight.total}</div>
                      </div>
                      <div className="row">
                        <div className="col">Non-Stop</div>
                      </div>
                    </div>
                    <div className="col-lg-2 col-3 text-right" id="fs9">
                      <strong>₹{flight.Price}</strong>
                    </div>
                    <div className="col-1 text-right">
                      <div className="row">
                        <div className="col text-right">
                          <input
                            checked={flight.id === flightID}
                            type="radio"
                            id={flight.id}
                            name={flight.id}
                            className="ng-pristine ng-valid ng-touched"
                            onChange={(e) =>
                              this.handleFlightRadioChange("flight", e)
                            }
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr style={{ padding: "0", margin: "0" }} />
                  <div className="row pb-1 pt-0">
                    <div className="col-7 d-none d-lg-block">
                      <a
                        style={{
                          fontSize: "12px",
                          color: "blue",
                          cursor: "pointer",
                        }}
                      >
                        Flight Details
                        <i
                          className={flight.flightDetails.css}
                          onClick={() => this.handleFlight(flight, "oneWay")}
                        ></i>
                      </a>
                    </div>
                    <div className="col-5 text-right d-none d-lg-block">
                      <span id="rect"> eCash</span>
                      <span id="rect1"> &nbsp;₹ 250</span>
                    </div>
                  </div>
                  {flight.flightDetails.isShow ? (
                    <div className="row">
                      <div className="col-7 d-none d-lg-block">
                        <div
                          className="row"
                          style={{
                            backgroundColor: "#301b41",
                            fontSize: "20px",
                            paddingTop: "6px",
                            paddingBottom: "6px",
                            paddingLeft: "8px",
                            color: "white",
                            fontWeight: "500",
                          }}
                        >
                          {" "}
                          Flight Details{" "}
                        </div>
                        <div className="row">
                          <div className="col-3">
                            <img style={{ width: "40px" }} src={flight.logo} />
                          </div>
                          <div className="col-6 text-left">
                            <div className="row" style={{ fontSize: "12px" }}>
                              {flight.name}
                            </div>
                            <div
                              _
                              className="row"
                              style={{ fontSize: "10px", color: "grey" }}
                            >
                              {" "}
                              {flight.airBus}
                            </div>
                          </div>
                          <div className="col-2 text-right">
                            <img
                              src="https://i.ibb.co/31BTG9K/icons8-food-100.png"
                              style={{ width: "45px" }}
                            />
                          </div>
                        </div>
                        <hr />
                        <div className="row">
                          <div
                            className="col-3 ml-1"
                            style={{ fontSize: "14px" }}
                          >
                            <div className="row ml-1"> {flight.desDept}</div>
                            <div className="row ml-1">
                              <strong>{flight.timeDept}</strong>
                            </div>
                            <div
                              className="row ml-1"
                              style={{ fontSize: "12px" }}
                            >
                              {flight.T1}
                            </div>
                          </div>
                          <div className="col-5">
                            <div className="row" style={{ fontSize: "12px" }}>
                              <div className="col text-center">Time Taken</div>
                            </div>
                            <div className="row">
                              <div className="col-9">
                                <hr />
                              </div>
                              <div
                                className="col-2 pt-2"
                                style={{ fontSize: "10px" }}
                              >
                                <FontAwesomeIcon icon={faPlane} />
                              </div>
                            </div>
                          </div>
                          <div className="col-3" style={{ fontSize: "14px" }}>
                            <div className="row">{flight.desArr}</div>
                            <div className="row">
                              <strong>{flight.timeArr}</strong>
                            </div>
                            <div className="row" style={{ fontSize: "12px" }}>
                              {flight.T2}
                            </div>
                          </div>
                        </div>
                        <br />
                        <div
                          className="row ml-2 mr-2"
                          style={{
                            fontSize: "12px",
                            borderRadius: "5px",
                            backgroundColor: "#F9F9F9",
                          }}
                        >
                          <div className="col text-center">
                            Checkin Baggage:
                            <i
                              className="fa fa-briefcase"
                              style={{ fontSize: "10px" }}
                            ></i>{" "}
                            {flight.checkin}kg
                          </div>
                        </div>
                      </div>
                      <div
                        className="col-5 d-none d-lg-block"
                        style={{ backgroundColor: "#5a426d", color: "white" }}
                      >
                        <div className="row pl-1 pt-3 pb-3">
                          <div className="col-6 border-bottom">
                            Fare Summary
                          </div>
                          <div className="col-6 ">Fare Rules</div>
                        </div>
                        <div className="row" style={{ fontSize: "14px" }}>
                          <div className="col-4">Fare Summary</div>
                          <div className="col-4">Base and Fare</div>
                          <div className="col-4">Fees and Taxes</div>
                        </div>
                        <div
                          className="row"
                          style={{ fontSize: "12px", color: "#fff" }}
                        >
                          <div className="col-4">Adult X 1</div>
                          <div className="col-4">₹ {flight.Price}</div>
                          <div className="col-4">₹ 1000</div>
                        </div>
                        <hr />
                        <div className="row" style={{ fontSize: "16px" }}>
                          <div className="col-6">You Pay:</div>
                          <div className="col-6 text-right">
                            ₹ {flight.Price}
                          </div>
                        </div>
                        <div className="row" style={{ fontSize: ".85714rem" }}>
                          <div className="col-12">
                            {" "}
                            Note: Total fare displayed above has been rounded
                            off and may show a slight difference from actual
                            fare.{" "}
                          </div>
                        </div>
                        <div className="row pt-2 pb-2">
                          <div className="col-12 text-center"></div>
                        </div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            ))}
          </div>
          <div className="col-6">
            <div
              className="row pl-4"
              style={{
                backgroundColor: "#E5E7EB",
                borderRadius: "3px",
                marginLeft: "7px",
                fontSize: ".78571rem",
              }}
            >
              <div className="col-12">
                {" "}
                {arrival} <i className="fa fa-arrow-right"></i> {departure}
              </div>
            </div>
            <br />
            {searchReturnFlights.map((returnFlight, index) => (
              <div
                className="row  mt-1 ml-3 pt-2"
                id="fs17"
                style={{ borderRadius: "5px" }}
              >
                <div className="col">
                  <div className="row">
                    <div className="col-lg-2 col-3">
                      <img id="img1" src={returnFlight.logo} />
                    </div>
                    <div className="col-lg-1 col-3 text-right" id="fs9">
                      <div className="row">{returnFlight.timeDept}</div>
                      <div className="row" style={{ fontSize: "x-small" }}>
                        {returnFlight.name}
                      </div>
                    </div>
                    <div className="col-lg-2 col-2">
                      <hr style={{ border: "solid 1px lightgrey" }} />
                    </div>
                    <div className="col-lg-1 col-3 text-left" id="fs9">
                      <div className="row">{returnFlight.timeArr}</div>
                    </div>
                    <div className="col-1 pt-1 d-none d-lg-block">
                      <span
                        style={{
                          borderLeft: "3px solid lightgrey",
                          height: "60px",
                        }}
                      ></span>
                    </div>
                    <div className="col-lg-2 col-6 text-right" id="fs8">
                      <div className="row">
                        <div className="col">{returnFlight.total}</div>
                      </div>
                      <div className="row">
                        <div className="col">Non-Stop</div>
                      </div>
                    </div>
                    <div className="col-lg-2 col-3 text-right" id="fs9">
                      <strong>₹{returnFlight.Price}</strong>
                    </div>
                    <div className="col-1 text-right">
                      <div className="row">
                        <div className="col text-right">
                          <input
                            checked={returnFlight.id === returnFlightID}
                            type="radio"
                            id={returnFlight.id}
                            name={returnFlight.id}
                            className="ng-pristine ng-valid ng-touched"
                            onChange={(e) =>
                              this.handleFlightRadioChange("returnFlight", e)
                            }
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr style={{ padding: "0", margin: "0" }} />
                  <div className="row pb-1 pt-0">
                    <div className="col-7 d-none d-lg-block">
                      <a
                        style={{
                          fontSize: "12px",
                          color: "blue",
                          cursor: "pointer",
                        }}
                      >
                        Flight Details
                        <i
                          className={returnFlight.flightDetails.css}
                          onClick={() =>
                            this.handleFlight(returnFlight, "return")
                          }
                        ></i>
                      </a>
                    </div>
                    <div className="col-5 text-right d-none d-lg-block">
                      <span id="rect"> eCash</span>
                      <span id="rect1"> &nbsp;₹ 250</span>
                    </div>
                  </div>
                  {returnFlight.flightDetails.isShow ? (
                    <div className="row">
                      <div className="col-7 d-none d-lg-block">
                        <div
                          className="row"
                          style={{
                            backgroundColor: "#301b41",
                            fontSize: "20px",
                            paddingTop: "6px",
                            paddingBottom: "6px",
                            paddingLeft: "8px",
                            color: "white",
                            fontWeight: "500",
                          }}
                        >
                          {" "}
                          Flight Details{" "}
                        </div>
                        <div className="row">
                          <div className="col-3">
                            <img
                              style={{ width: "40px" }}
                              src={returnFlight.logo}
                            />
                          </div>
                          <div className="col-6 text-left">
                            <div className="row" style={{ fontSize: "12px" }}>
                              {returnFlight.name}
                            </div>
                            <div
                              _
                              className="row"
                              style={{ fontSize: "10px", color: "grey" }}
                            >
                              {" "}
                              {returnFlight.airBus}
                            </div>
                          </div>
                          <div className="col-2 text-right">
                            <img
                              src="https://i.ibb.co/31BTG9K/icons8-food-100.png"
                              style={{ width: "45px" }}
                            />
                          </div>
                        </div>
                        <hr />
                        <div className="row">
                          <div
                            className="col-3 ml-1"
                            style={{ fontSize: "14px" }}
                          >
                            <div className="row ml-1">
                              {" "}
                              {returnFlight.desDept}
                            </div>
                            <div className="row ml-1">
                              <strong>{returnFlight.timeDept}</strong>
                            </div>
                            <div
                              className="row ml-1"
                              style={{ fontSize: "12px" }}
                            >
                              {returnFlight.T1}
                            </div>
                          </div>
                          <div className="col-5">
                            <div className="row" style={{ fontSize: "12px" }}>
                              <div className="col text-center">Time Taken</div>
                            </div>
                            <div className="row">
                              <div className="col-9">
                                <hr />
                              </div>
                              <div
                                className="col-2 pt-2"
                                style={{ fontSize: "10px" }}
                              >
                                <FontAwesomeIcon icon={faPlane} />
                              </div>
                            </div>
                          </div>
                          <div className="col-3" style={{ fontSize: "14px" }}>
                            <div className="row">{returnFlight.desArr}</div>
                            <div className="row">
                              <strong>{returnFlight.timeArr}</strong>
                            </div>
                            <div className="row" style={{ fontSize: "12px" }}>
                              {returnFlight.T2}
                            </div>
                          </div>
                        </div>
                        <br />
                        <div
                          className="row ml-2 mr-2"
                          style={{
                            fontSize: "12px",
                            borderRadius: "5px",
                            backgroundColor: "#F9F9F9",
                          }}
                        >
                          <div className="col text-center">
                            Checkin Baggage:
                            <i
                              className="fa fa-briefcase"
                              style={{ fontSize: "10px" }}
                            ></i>{" "}
                            {returnFlight.checkin}kg
                          </div>
                        </div>
                      </div>
                      <div
                        className="col-5 d-none d-lg-block"
                        style={{ backgroundColor: "#5a426d", color: "white" }}
                      >
                        <div className="row pl-1 pt-3 pb-3">
                          <div className="col-6 border-bottom">
                            Fare Summary
                          </div>
                          <div className="col-6 ">Fare Rules</div>
                        </div>
                        <div className="row" style={{ fontSize: "14px" }}>
                          <div className="col-4">Fare Summary</div>
                          <div className="col-4">Base and Fare</div>
                          <div className="col-4">Fees and Taxes</div>
                        </div>
                        <div
                          className="row"
                          style={{ fontSize: "12px", color: "#fff" }}
                        >
                          <div className="col-4">Adult X 1</div>
                          <div className="col-4">₹ {returnFlight.Price}</div>
                          <div className="col-4">₹ 1000</div>
                        </div>
                        <hr />
                        <div className="row" style={{ fontSize: "16px" }}>
                          <div className="col-6">You Pay:</div>
                          <div className="col-6 text-right">
                            ₹ {returnFlight.Price}
                          </div>
                        </div>
                        <div className="row" style={{ fontSize: ".85714rem" }}>
                          <div className="col-12">
                            {" "}
                            Note: Total fare displayed above has been rounded
                            off and may show a slight difference from actual
                            fare.{" "}
                          </div>
                        </div>
                        <div className="row pt-2 pb-2">
                          <div className="col-12 text-center"></div>
                        </div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            ))}
          </div>
        </div>
        <br />
        {this.renderReturnFlightFooter()}
      </React.Fragment>
    );
  };
  renderReturnFlightFooter = () => {
    const {
      searchFlights,
      searchReturnFlights,
      returnFlightID,
      flightID,
    } = this.state;
    let singleFlight = searchFlights.find((flight) => flight.id === flightID);
    let returnFlight = searchReturnFlights.find(
      (flight) => flight.id === returnFlightID
    );
    return (
      <div
        className="row fixed-bottom pt-4 pb-4"
        style={{ backgroundColor: "#43264E", color: "white" }}
      >
        <div className="col-4 d-none d-lg-block">
          <div className="row">
            <div className="col-3 text-center">
              <img
                style={{ width: "40px", borderRadius: "6px" }}
                src={singleFlight.logo}
              />
            </div>
            <div
              className="col-2 d-none d-lg-block"
              style={{ fontSize: ".85714rem" }}
            >
              {singleFlight.name}
            </div>
            <div className="col-2 d-none d-lg-block">
              {singleFlight.timeDept}
            </div>
            <div className="col-3 d-none d-lg-block">
              <hr style={{ borderColor: "white" }} />
            </div>
            <div className="col-2 d-none d-lg-block">
              {singleFlight.timeArr}
            </div>
          </div>
        </div>

        <div className="col-4 d-none d-lg-block">
          <div className="row">
            <div className="col-3 text-center">
              <img
                style={{ width: "40px", borderRadius: "6px" }}
                src={returnFlight.logo}
              />
            </div>
            <div className="col-2" style={{ fontSize: ".85714rem" }}>
              {returnFlight.name}
            </div>
            <div className="col-2">{returnFlight.timeDept}</div>
            <div className="col-3">
              <hr style={{ borderColor: "white" }} />
            </div>
            <div className="col-2">{returnFlight.timeArr}</div>
          </div>
        </div>
        <div className="col-lg-4 col-12">
          <div className="row">
            <div className="col-6 ml-lg-0 ml-1">
              <div className="row" style={{ fontSize: ".85714rem" }}>
                <div className="col">Total Fare</div>
              </div>
              <div className="row" style={{ fontSize: "1.57143rem" }}>
                <div className="col">
                  <strong>₹{singleFlight.Price + returnFlight.Price}</strong>
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-4 mt-1">
              <button
                className="btn btn-danger text-white"
                onClick={() =>
                  this.handleBookreturnFlight(singleFlight, returnFlight)
                }
              >
                {" "}
                Book{" "}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  };
}

export default YatrairSearch;
