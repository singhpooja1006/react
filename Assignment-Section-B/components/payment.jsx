import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
import Navbar from "./navbar";
class Payment extends Component {
  state = {
    value: "UPI",
    selectedPaymentCss: "border bg-white",
    flightDetails: {},
    returnFlightDetails: {},
  };
  showTextField = (value) => {
    this.setState({ value });
  };
  componentDidMount() {
    console.log("Props", this.props.location);
    let flightDetails = {};
    if (this.props.location.state) {
      flightDetails = { ...this.props.location.state };
    }
    let returnFlightDetails = {};
    if (this.props.location.state) {
      returnFlightDetails = { ...this.props.location.state };
    }
    this.setState({ flightDetails, returnFlightDetails });
  }
  handlePayNow = async (e) => {
    let apiEndPoint = config.apiEndPoint + "/booking";
    await axios.post(apiEndPoint, {
      ...this.state.flightDetails,
    });
  };
  render() {
    const { flightDetails, returnFlightDetails } = this.state;
    return (
      <div className="container-fluid" style={{ backgroundColor: "lightgray" }}>
        <Navbar />
        <div className="row">
          <div className="col-lg-9 col-12">
            <div className="row mt-2 mb-2">
              <div className="col-9 ml-1" style={{ fontSize: "20px" }}>
                <strong>
                  <i className="fa fa-credit-card"></i> Payment Method
                </strong>
              </div>
            </div>
            <div className="row bg-white ml-1" id="payment">
              <div className="col-lg-2 col-4 text-center" id="fs20">
                <div
                  style={{ cursor: "pointer" }}
                  className={
                    this.state.value === "UPI"
                      ? this.state.selectedPaymentCss
                      : "border bg-light"
                  }
                  onClick={() => this.showTextField("UPI")}
                >
                  <div className="mt-2 mb-2">UPI</div>
                </div>

                <div
                  style={{ cursor: "pointer" }}
                  className={
                    this.state.value === "Credit Card"
                      ? this.state.selectedPaymentCss
                      : "border bg-light"
                  }
                  onClick={() => this.showTextField("Credit Card")}
                >
                  <div className="mt-2 mb-2">Credit Card</div>
                </div>
                <div
                  style={{ cursor: "pointer" }}
                  className={
                    this.state.value === "Debit Card"
                      ? this.state.selectedPaymentCss
                      : "border bg-light"
                  }
                  onClick={() => this.showTextField("Debit Card")}
                >
                  <div className="mt-2 mb-2">Debit Card</div>
                </div>
                <div
                  style={{ cursor: "pointer" }}
                  className={
                    this.state.value === "Net Banking"
                      ? this.state.selectedPaymentCss
                      : "border bg-light"
                  }
                  onClick={() => this.showTextField("Net Banking")}
                >
                  <div className="mt-2 mb-2">Net Banking</div>
                </div>
                <div
                  style={{ cursor: "pointer" }}
                  className={
                    this.state.value === "PayPal"
                      ? this.state.selectedPaymentCss
                      : "border bg-light"
                  }
                  onClick={() => this.showTextField("PayPal")}
                >
                  <div className="mt-2 mb-2">PayPal</div>
                </div>
              </div>
              <div className="col-8" id="fs20">
                <br />
                <br />
                <div className="row">
                  <div className="col ml-4">
                    <strong>Pay with {this.state.value}</strong>
                  </div>
                </div>
                <br />
                <br />
                <div className="row">
                  <div
                    className="col-lg-4 col-5 ml-4"
                    style={{ fontSize: "25px" }}
                  >
                    <strong>
                      ₹ {(flightDetails ? flightDetails.total : 0) + 350}
                    </strong>
                  </div>
                  <div className="col-6">
                    <button
                      className="btn btn-danger text-white btn-md"
                      onClick={() => this.handlePayNow()}
                    >
                      Pay Now
                    </button>
                  </div>
                </div>
                <div className="row">
                  <div
                    className="col text-secondary text-center"
                    style={{ fontSize: "12px" }}
                  >
                    {" "}
                    By clicking Pay Now, you are agreeing to terms and
                    Conditions and Privacy Policy{" "}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-12">
            <div className="row mt-2 mb-2">
              <div
                className="col-11 ml-1 text-left"
                style={{ fontSize: "16px" }}
              >
                Payment Details
              </div>
            </div>
            <div className="row bg-white ml-1 mr-1" id="flight">
              <div className="col">
                <div className="row">
                  <div className="col-8 text-left" style={{ fontSize: "1rem" }}>
                    Total Flight Price
                  </div>
                  <div
                    className="col-4 text-right"
                    style={{ fontSize: "1rem" }}
                  >
                    {" "}
                    ₹ {flightDetails ? flightDetails.total : 0}{" "}
                  </div>
                </div>
                <div className="row">
                  <div className="col-8 text-left" style={{ fontSize: "1rem" }}>
                    Convenience Fees
                  </div>
                  <div
                    className="col-4 text-right"
                    style={{ fontSize: "1rem" }}
                  >
                    {" "}
                    ₹ 350
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div
                    className="col-6 text-left"
                    style={{ fontSize: "1.7rem" }}
                  >
                    <strong>You Pay</strong>
                  </div>
                  <div
                    className="col-6 text-right"
                    style={{ fontSize: "1.7rem" }}
                  >
                    <strong>
                      ₹ {(flightDetails ? flightDetails.total : 0) + 350}
                    </strong>
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-6 text-left">Earn eCash</div>
                  <div
                    className="col-6 text-right"
                    style={{ fontSize: "1rem" }}
                  >
                    ₹ {(flightDetails ? flightDetails.travellers : 0) * 250}
                  </div>
                </div>
              </div>
            </div>
            <div _ngcontent-rjw-c11="" class="row mt-1 mb-1">
              <div _ngcontent-rjw-c11="" class="col ml-1">
                {" "}
                Booking summary{" "}
              </div>
            </div>
            <div className="row bg-white ml-1 mr-1" id="flight">
              <div className="col">
                <div class="row">
                  <div className="col-6">
                    <img
                      style={{
                        width: "28px",
                        height: "28px",
                        borderRadius: "6px",
                      }}
                      src={
                        flightDetails.departure
                          ? flightDetails.departure.logo
                          : ""
                      }
                    />
                  </div>
                  <div className="col-5 mr-1 text-right" id="fs20">
                    {flightDetails.departure
                      ? flightDetails.departure.code
                      : ""}
                  </div>
                </div>
                <div className="row">
                  <div className="col-5 pl-1" id="fs18">
                    {flightDetails.departure
                      ? flightDetails.departure.desDept
                      : ""}
                  </div>
                  <div className="col-2 text-left">
                    <i
                      className="fa fa-fighter-jet"
                      style={{ color: "lightgray" }}
                    ></i>
                  </div>
                  <div className="col-5" id="fs18">
                    {flightDetails.departure
                      ? flightDetails.departure.desArr
                      : ""}
                  </div>
                </div>
                {returnFlightDetails.arrival &&
                returnFlightDetails.arrival.id ? (
                  <React.Fragment>
                    <div class="row mt-3">
                      <div className="col-6">
                        <img
                          style={{
                            width: "28px",
                            height: "28px",
                            borderRadius: "6px",
                          }}
                          src={
                            returnFlightDetails.arrival
                              ? returnFlightDetails.arrival.logo
                              : ""
                          }
                        />
                      </div>
                      <div className="col-5 mr-1 text-right" id="fs20">
                        {returnFlightDetails.arrival
                          ? returnFlightDetails.arrival.code
                          : ""}
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-5 pl-1" id="fs18">
                        {returnFlightDetails.arrival
                          ? returnFlightDetails.arrival.desDept
                          : ""}
                      </div>
                      <div className="col-2 text-left">
                        <i
                          className="fa fa-fighter-jet"
                          style={{ color: "lightgray" }}
                        ></i>
                      </div>
                      <div className="col-5" id="fs18">
                        {returnFlightDetails.arrival
                          ? returnFlightDetails.arrival.desArr
                          : ""}
                      </div>
                    </div>
                  </React.Fragment>
                ) : (
                  ""
                )}
              </div>
            </div>
            <div className="row ml-1 mr-1 mt-1 mb-1">
              <div className="col"> Contact Details </div>
            </div>
            <div className="row bg-white ml-1 mr-1" id="detail">
              <div className="col">
                {flightDetails.passengers
                  ? flightDetails.passengers.map((passenger, index) => (
                      <div className="row">
                        <div className="col" id="fs16">
                          {index + 1}. {passenger.firstName}{" "}
                          {passenger.lastName}
                        </div>
                      </div>
                    ))
                  : ""}

                <hr />
                <div className="row" id="fs16">
                  <div className="col-4 text-secondary">Email</div>
                  <div className="col-8 text-secondary">
                    {flightDetails ? flightDetails.email : ""}
                  </div>
                </div>
                <div className="row" id="fs16">
                  <div className="col-4 text-secondary">Phone</div>
                  <div className="col-8 text-secondary">
                    {flightDetails ? flightDetails.mobile : ""}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Payment;
