const functions = require("firebase-functions");
var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
    next();
});

const port  = 2410;
exports.app = functions.https.onRequest(app);

let mobiles = [{
    name: "Mi A1",
    brand: "Xiaomi",
    RAM: "4 GB",
    ROM: "64 GB",
    price: 9999,
    OS:"Android",
    url: "https://i.ibb.co/0h336fq/MI-A1.jpg"
  },
  {
    name: "Realme 3",
    brand: "Realme",
    RAM: "3 GB",
    ROM: "32 GB",
    price: 4999,
    OS:"Android",
    url:"https://i.ibb.co/SRSqgnj/mi-mi-a1-na-original-imaexg9njdddfphr.jpg",
  },
  {
    name: "On5",
    brand: "Samsung",
    RAM: "3 GB",
    ROM: "64 GB",
    price: 8499,
    OS:"iOS",
    url:"https://i.ibb.co/H7RjXdL/mi-mi-4i-mzb4427in-original-imaeatz4crfanz5g.jpg",
  },
  {
    name: "F5",
    brand: "Oppo",
    RAM: "6 GB",
    ROM: "128 GB",
    price: 16999,
    OS:"Android",
    url:"https://i.ibb.co/cLV4RvX/google-pixel-3.jpg",
  },
  {
    name: "Mi A3",
    brand: "Xiaomi",
    RAM: "6 GB",
    ROM: "128 GB",
    price: 18999,
    OS:"iOS",
    url:"https://i.ibb.co/160YSpk/mi-redmi-5.jpg",
  },
  {
    name: "Realme 4",
    brand: "Realme",
    RAM: "4 GB",
    ROM: "64 GB",
    price: 9999,
    OS:"Android",
    url:"https://i.ibb.co/y6dk6n1/mi-redmi-y2.jpg",
  },
  {
    name: "On7",
    brand: "Samsung",
    RAM: "4 GB",
    ROM: "64 GB",
    price: 11999,
    OS:"iOS",
    url:"https://i.ibb.co/McQMDF8/motorola-moto-e5-plus.jpg",
  },
  {
    name: "F7",
    brand: "Oppo",
    RAM: "6 GB",
    ROM: "128 GB",
    price: 18199,
    OS:"Android",
    url:"https://i.ibb.co/1QGFW28/nokia-6-1.jpg",
  },
  {
    name: "RedMi 5",
    brand: "Xiaomi",
    RAM: "3 GB",
    ROM: "32 GB",
    price: 7499,
    OS:"iOS",
    url:"https://i.ibb.co/vJbK6qM/realme-c1.jpg",
  },
  {
    name: "Realme Pro",
    brand: "Realme",
    RAM: "4 GB",
    ROM: "64 GB",
    price: 10299,
    OS:"Android",
    url:"https://i.ibb.co/F6c80H6/realme-x.jpg",
  },
  {
    name: "Galaxy 9",
    brand: "Samsung",
    RAM: "6 GB",
    ROM: "128 GB",
    price: 35999,
    OS:"Android",
    url:"https://i.ibb.co/TWLNRyW/samsung-galaxy-s10-plus.jpg",
  },
  {
    name: "F3 Basic",
    brand: "Oppo",
    RAM: "3 GB",
    ROM: "32 GB",
    price: 8599,
    OS:"iOS",
    url:"https://i.ibb.co/ScZsMtW/vivo-z1-pro.jpg",
  },
  {
    name: "Realme X50",
    brand: "Apple",
    RAM: "4 GB",
    ROM: "64 GB",
    price: 20000,
    OS:"iOS",
    url:"https://i.ibb.co/FBbgy1N/realme-c3-rmx2027-original-imafucgfngveff4v.jpg",
  },
  {
    name: "Galaxy M21",
    brand: "Samsung",
    RAM: "2 GB",
    ROM: "256 GB",
    price: 8999,
    OS:"iOS",
    url: "https://i.ibb.co/Jch8Hpf/ziox-duopix-f9-duopix-f9-original-imaf6bg5d86hggef.jpg",
  },
  {
    name: "Mi 10",
    brand: "Xiaomi",
    RAM: "6 GB",
    ROM: "128 GB",
    price: 17000,
    OS:"Android",
    url:"https://i.ibb.co/t8dQvMG/poco-m2-mzb9920in-original-imafvdrfhhuzcnyw.jpg",
  },
  {
    name: "iPhone SE",
    brand: "Apple",
    RAM: "6 GB",
    ROM: "256 GB",
    price: 40000,
    OS:"Android",
    url:"https://i.ibb.co/V9gFttB/20-128-a-yal-al00-honor-6-original-imafgh62mxw3ggvg.jpg",
  },
  {
    name: "Vivo V19",
    brand: "Oppo",
    RAM: "8 GB",
    ROM: "256 GB",
    price: 17500,
    OS:"Android",
    url:"https://i.ibb.co/mXRC7Qf/infinix-hot-9-x655d-original-imafuxzbpghez9yu.jpg",
  },
  {
    name: "iPhone 12 Pro",
    brand: "Apple",
    RAM: "8 GB",
    ROM: "128 GB",
    price: 35000,
    OS:"iOS",
    url:"https://i.ibb.co/qBVzqJb/apple-iphone-se-mxvv2hn-a-original-imafrcqmfxhcrpsb.jpg",
  },
  {
    name: "Honor 9X",
    brand: "Xiaomi",
    RAM: "2 GB",
    ROM: "256 GB",
    price: 18000,
    OS:"Android",
    url:"https://i.ibb.co/L5j1hbc/realme-x50-pro-rmx2076-original-imafpbbvwqm4kjnv.jpg",
  },
  {
    name: "POCO M2",
    brand: "RealMe",
    RAM: "4 GB",
    ROM: "64 GB",
    price: 14000,
    OS:"iOS",
    url:"https://i.ibb.co/HXm1SWs/redmi-note-7s-32-b-mzb7746in-mi-3-original-imafe48pmu5q6ygz.jpg",
  },
  {
    name: "Infinix 9",
    brand: "Oppo",
    RAM: "8 GB",
    ROM: "32 GB",
    price: 14999,
    OS:"Android",
    url:"https://i.ibb.co/ncRSw1B/realme-5i-rmx2030-original-imafnsx5bj4yqhas.jpg",
  },
  {
    name: "Honor 20",
    brand: "Oppo",
    RAM: "2 GB",
    ROM: "64 GB",
    price: 17500,
    OS:"iOS",
    url:"https://i.ibb.co/Kzsky2F/realme-6i-rmx2002-original-imaftngrxb8bg9jr.jpg",
  },]

   function pagination(obj, page) {
    var resArr = obj;
    resArr = resArr.slice(page * 5 - 5, page * 5);
    return resArr;
  }
  app.get("/mobiles",function(req,res) { 
    let {RAM,ROM,brand,price,OS} = req.query;
	let page = +req.query.page;
    page = isNaN(page) ? 1 : page;
    let result = [...mobiles];
    if(RAM) {
        let RAMList = RAM.split(",");
        result = result.filter((data) => RAMList.find(ram => ram === data.RAM)) ;
    }
    if(ROM) {
      let ROMList = ROM.split(",");
      result = result.filter((data) => ROMList.find(rom => rom === data.ROM)) ;
  }
  if(brand) {
    let brandList = brand.split(",");
    result = result.filter((data) => brandList.find(brand => brand === data.brand)) ;
  }
  if(price){
    console.log("Price",price)
    if(price === "<5000")
        result = result.filter((data) => data.price < 5000) ;
    if(price === "5000-10000")
        result = result.filter((data) => data.price >= 5000 && data.price <= 10000) ;
        if(price === "10000-20000")
        result = result.filter((data) => data.price >= 10000 && data.price <= 20000) ;
    if(price === ">20000")
        result = result.filter((data) => data.price > 20000) ;
  }
  if(OS){
    result = result.filter((data) => data.OS === OS) ;
  }
	let respArr = pagination(result, page);
    let len = result.length;
    let quo = Math.floor(len / 5);
    let rem = len % 5;
    let extra = rem === 0 ? 0 : 1;
    let numofpages = quo + extra;
    let pageInfo = {
        pageNumber: page,
        numberOfPages: numofpages,
        numOfItems: 5,
        totalItemCount: result.length,
    };
    res.json({
        data: respArr,
        pageInfo: pageInfo,
      });
});

app.get("/mobiles/:name",function(req,res) { 
  let name = req.params.name;
  console.log("in get request for /mobiles/:name : ",name);
  let index = mobiles.findIndex((data)=> data.name === name);
  if (index === -1) res.status(404).send(name + " Not Found");
  else {
      res.send(mobiles[index]);
  }
});

app.post("/mobiles",function(req,res) { 
    console.log("in post request for /mobiles ");
    let name = req.body.name;
    let index = mobiles.findIndex((data)=> data.name === name);
    if(index !== -1) res.status(404).send(name + " already exist");
    else{
      mobiles.push(req.body)
      res.send(req.body);
    }
    
});
app.put("/mobiles/:name",function(req,res) { 
    let name = req.params.name;
    console.log("in put request for /mobiles/:name ", name);
    let index = mobiles.findIndex((data)=> data.name === name);
    if (index === -1) res.status(404).send(name + " Not Found");
    else {
        mobiles[index] = req.body;
        res.send(res.body);
    }
});
app.delete("/mobiles/:name",function(req,res) { 
    let name = req.params.name;
    console.log("in delete request for /mobiles/:name ", name);
    let mobileIndex = mobiles.findIndex((data)=> data.name === name);
    const mobile = mobiles[mobileIndex];
    if (mobileIndex === -1) 
        return res.status(404).send("The mobile with the given Name was not found.");
        mobiles.splice(mobileIndex,1);
    res.send(mobile);
    
});