import { Alert } from "bootstrap";
import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
class AddNewMobile extends Component {
  state = {
    data: { name: "", brand: "", price: "", RAM: "", ROM: "", OS: "", url: "" },
    brands: ["Xiaomi", "RealMe", "Apple", "Oppo"],
    rams: ["2 GB", "3 GB", "4 GB", "6 GB", "8 GB"],
    roms: ["6 GB", "32 GB", "64 GB", "128 GB", "256 GB"],
    oss: ["Android", "iOS"],
    errors: {},
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { data } = this.state;
    data[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data, errors });
  };
  async componentDidMount() {
    const { name } = this.props.match.params;
    if (name) {
      const apiEndpoint = config.apiEndPoint + "/mobiles/" + name;
      const { data } = await http.get(apiEndpoint);
      this.setState({ data });
    }
  }
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "url":
        if (!e.currentTarget.value.trim()) return "URL is required";
        break;
      case "name":
        if (!e.currentTarget.value.trim()) return "Name is required";
        break;
      case "brand":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select Brand"
        )
          return "Brand is required";
        break;
      case "RAM":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select RAM"
        )
          return "RAM is required";
        break;
      case "ROM":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select ROM"
        )
          return "ROM is required";
        break;
      case "OS":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select OS"
        )
          return "OS is required";
        break;
      case "price":
        if (!e.currentTarget.value.trim()) return "Price is required";
        else if (+e.currentTarget.value <= 0) return "Price is grater than 0";
        break;

      default:
        break;
    }
    return "";
  };
  validate = () => {
    let errs = {};
    if (!this.state.data.url.trim()) errs.url = "URL is required";
    if (!this.state.data.name.trim()) errs.gender = "Name is required";
    if (
      !this.state.data.brand.trim() ||
      this.state.data.brand === "Select Brand"
    )
      errs.brand = "Brand is required";
    if (!this.state.data.RAM.trim() || this.state.data.RAM === "Select RAM")
      errs.RAM = "RAM is required";

    if (!this.state.data.ROM.trim() || this.state.data.ROM === "Select ROM")
      errs.ROM = "ROM is required";
    if (!this.state.data.OS.trim() || this.state.data.OS === "Select OS")
      errs.OS = "OS is required";
    if (+this.state.data.price <= 0) errs.price = "Price is grater than 0";
    return errs;
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    const { data } = this.state;
    try {
      const { name } = this.props.match.params;
      if (name) {
        const apiEndpoint = config.apiEndPoint + "/mobiles/" + name;
        await http.put(apiEndpoint, data);
      } else {
        const apiEndpoint = config.apiEndPoint + "/mobiles";
        await http.post(apiEndpoint, data);
      }

      window.location = "/mobiles";
    } catch (ex) {
      console.log("Ex", ex.response);
      if (ex.response.status >= 400 && ex.response.status <= 500) {
        alert(ex.response.data + " already exist , Please provide new name.");
      }
    }
  };
  render() {
    const { data, brands, rams, roms, oss, errors } = this.state;
    const { name } = this.props.match.params;
    return (
      <div className="container">
        <h2>Mobile Details</h2>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              value={data.name}
              onChange={this.handleChange}
              type="text"
              id="name"
              name="name"
              placeholder="Enter Name"
              className="form-control"
              disabled={name ? true : false}
            />
            {errors.name ? (
              <div className="text-danger">{errors.name}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="url">URL</label>
            <input
              value={data.url}
              onChange={this.handleChange}
              type="text"
              id="url"
              name="url"
              placeholder="Enter URL"
              className="form-control"
            />
            {errors.url ? <div className="text-danger">{errors.url}</div> : ""}
          </div>
          <div className="form-group">
            <label htmlFor="price">Price</label>
            <input
              value={data.price}
              onChange={this.handleChange}
              type="number"
              id="price"
              name="price"
              placeholder="Enter price"
              className="form-control"
            />
            {errors.price ? (
              <div className="text-danger">{errors.price}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="brand">Brand</label>
            <select
              value={data.brand}
              onChange={this.handleChange}
              id="brand"
              name="brand"
              className="browser-default custom-select mb-1"
            >
              <option>Select Brand</option>
              {brands.map((brand) => (
                <option key={brand}>{brand}</option>
              ))}
            </select>
            {errors.brand ? (
              <div className="text-danger">{errors.brand}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="RAM">RAM</label>
            <select
              value={data.RAM}
              onChange={this.handleChange}
              id="RAM"
              name="RAM"
              className="browser-default custom-select mb-1"
            >
              <option>Select RAM</option>
              {rams.map((ram) => (
                <option key={ram}>{ram}</option>
              ))}
            </select>
            {errors.RAM ? <div className="text-danger">{errors.RAM}</div> : ""}
          </div>
          <div className="form-group">
            <label htmlFor="ROM">ROM</label>
            <select
              value={data.ROM}
              onChange={this.handleChange}
              id="ROM"
              name="ROM"
              className="browser-default custom-select mb-1"
            >
              <option>Select ROM</option>
              {roms.map((rom) => (
                <option key={rom}>{rom}</option>
              ))}
            </select>
            {errors.ROM ? <div className="text-danger">{errors.ROM}</div> : ""}
          </div>
          <div className="form-group">
            <label htmlFor="OS">OS</label>
            <select
              value={data.OS}
              onChange={this.handleChange}
              id="OS"
              name="OS"
              className="browser-default custom-select mb-1"
            >
              <option>Select OS</option>
              {oss.map((OS) => (
                <option key={OS}>{OS}</option>
              ))}
            </select>
            {errors.OS ? <div className="text-danger">{errors.OS}</div> : ""}
          </div>
          <button className="btn btn-primary">Submit</button>
        </form>
      </div>
    );
  }
}

export default AddNewMobile;
