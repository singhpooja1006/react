import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
import queryString from "query-string";
import LeftPanel from "./leftPanel";
class Mobiles extends Component {
  state = {
    mobilesData: [],
	pageInfo: {},
    rams: [
      { name: "2 GB", isSelected: false },
      { name: "3 GB", isSelected: false },
      { name: "4 GB", isSelected: false },
      { name: "6 GB", isSelected: false },
      { name: "8 GB", isSelected: false },
    ],
    roms: [
      { name: "16 GB", isSelected: false },
      { name: "32 GB", isSelected: false },
      { name: "64 GB", isSelected: false },
      { name: "128 GB", isSelected: false },
      { name: "256 GB", isSelected: false },
    ],
    brands: [
      { name: "Xiaomi", isSelected: false },
      { name: "RealMe", isSelected: false },
      { name: "Apple", isSelected: false },
      { name: "Oppo", isSelected: false },
    ],
    priceData: {
      prices: ["<5000", "5000-10000", "10000-20000", ">20000"],
      selected: "",
    },
    operatingSystem: {
      operatingSys: ["Android", "iOS"],
      selected: "",
    },
  };
  async componentDidMount() {
    let apiEndpoint = config.apiEndPoint + "/mobiles";
    const { data: mobilesData } = await http.get(apiEndpoint);
	this.setState({
      mobilesData: mobilesData.data,
      pageInfo: mobilesData.pageInfo,
    });
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      let apiEndpoint =
        config.apiEndPoint + "/mobiles" + this.props.location.search;
      const { data: mobilesData } = await http.get(apiEndpoint);
		this.setState({
		  mobilesData: mobilesData.data,
		  pageInfo: mobilesData.pageInfo,
		});
	}
  }
  handleEditDetails = (name) => {
    window.location = "/mobiles/" + name;
  };
  handleDeleteDetails = async (name) => {
    const originalMobilesData = this.state.mobilesData;
    try {
      const apiEndpoint = config.apiEndPoint + "/mobiles";
      await http.delete(apiEndpoint + "/" + name);
      const { data: mobilesData } = await http.get(apiEndpoint);
      this.setState({
		  mobilesData: mobilesData.data,
		  pageInfo: mobilesData.pageInfo,
		});
    } catch (ex) {
      if (ex.response && ex.response.status !== 404) {
        this.setState({ mobilesData: originalMobilesData });
      }
    }
  };
  calURL = (params, ram, rom, brand, price, os, page) => {
    let path = "/mobiles";
    params = this.addToParams(params, "RAM", ram);
    params = this.addToParams(params, "ROM", rom);
    params = this.addToParams(params, "brand", brand);
    params = this.addToParams(params, "price", price);
    params = this.addToParams(params, "OS", os);
	params = this.addToParams(params, "page", page);

    this.props.history.push({
      pathname: path,
      search: params,
    });
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  handleOptionChangeEvent = (
    rams,
    roms,
    brands,
    priceData,
    operatingSystem
  ) => {
    this.setState({ rams, roms, brands, priceData, operatingSystem });
    let ram = this.buildQueryString(rams);
    let rom = this.buildQueryString(roms);
    let brand = this.buildQueryString(brands);
    this.calURL(
      "",
      ram,
      rom,
      brand,
      priceData.selected,
      operatingSystem.selected,
	  1,
    );
  };
  buildQueryString(optionsData) {
    let filterData = optionsData.filter((n1) => n1.isSelected);
    let arrayData = filterData.map((n1) => n1.name);
    return arrayData.join(",");
  }
  pageNavigate = (value) => {
    const { pageInfo } = { ...this.state };
    let { RAM, ROM, brand, price, OS } = queryString.parse(
      this.props.location.search
    );
    let currPage = pageInfo.pageNumber + value;
    this.calURL("", RAM, ROM, brand, price, OS, currPage);
  };

  render() {
    return (
      <div className="container">
        <div className="row m-1">
          <div className="col-2 mr-1">
            <LeftPanel
              rams={this.state.rams}
              roms={this.state.roms}
              brands={this.state.brands}
              priceData={this.state.priceData}
              operatingSystem={this.state.operatingSystem}
              onOptionChangeEvent={this.handleOptionChangeEvent}
            />
          </div>
          <div className="col-8 ml-2">{this.rendermobilesData()}</div>
        </div>
      </div>
    );
  }
  rendermobilesData = () => {
    const { mobilesData, pageInfo } = this.state;
    if (mobilesData) {
		let { page } = queryString.parse(this.props.location.search);
      page = page ? +page : 1;
      return (
        <React.Fragment>
		<div className="row">
            {pageInfo.pageNumber === 1
              ? 1
              : pageInfo.numOfItems * pageInfo.pageNumber -
                pageInfo.numOfItems +
                1}{" "}
            to{" "}
            {pageInfo.numOfItems * pageInfo.pageNumber < pageInfo.totalItemCount
              ? pageInfo.numOfItems * pageInfo.pageNumber
              : pageInfo.totalItemCount}{" "}
            of {pageInfo.totalItemCount}
          </div>
          {mobilesData.map((mobile) => (
            <div className="row border">
              <div className="col-6">
                <img
                  alt=""
                  src={mobile.url}
                  style={{
                    height: "100px",
                  }}
                />
              </div>
              <div className="col-6 text-right">
                <div>Name : {mobile.name}</div>
                <div>Brand : {mobile.brand}</div>
                <div>Price : {mobile.price}</div>
                <div>RAM : {mobile.RAM}</div>
                <div>ROM : {mobile.ROM}</div>
                <div>OS : {mobile.OS}</div>
              </div>
              <div className="col-8 text-right">
                <i
                  className="fa fa-pencil-square-o"
                  aria-hidden="true"
                  onClick={() => this.handleEditDetails(mobile.name)}
                ></i>
              </div>
              <div className="col-2 ml-5">
                <i
                  className="fa fa-trash"
                  aria-hidden="true"
                  onClick={() => this.handleDeleteDetails(mobile.name)}
                ></i>
              </div>
            </div>
          ))}
		  <div className="row m-1">
            <div>
              {page > 1 ? (
                <button
                  className="btn btn-primary m-1"
                  onClick={() => this.pageNavigate(-1)}
                >
                  Previous
                </button>
              ) : (
                ""
              )}
            </div>
            <div>
              {page < pageInfo.numberOfPages ? (
                <button
                  className="btn btn-primary m-1"
                  onClick={() => this.pageNavigate(1)}
                >
                  Next
                </button>
              ) : (
                ""
              )}
            </div>
          </div>
        </React.Fragment>
      );
    }
  };
}
export default Mobiles;
