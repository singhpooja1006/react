import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import NavBar from "./navBar";
import Mobiles from "./mobiles";
import AddNewMobile from "./newMobile";

class MainComp extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <NavBar />
        <Switch>
          <Route path="/new" component={AddNewMobile} />
          <Route path="/mobiles/:name" component={AddNewMobile} />
          <Route path="/mobiles" component={Mobiles} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default MainComp;
