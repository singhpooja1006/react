import React, { Component } from "react";
class LeftPanel extends Component {
  state = {};
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let { rams, roms, brands, priceData, operatingSystem } = {
      ...this.props,
    };
    if (input.type === "checkbox") {
      let ram = rams.find((n1) => n1.name === input.name);
      if (ram) ram.isSelected = input.checked;

      let rom = roms.find((n1) => n1.name === input.name);
      if (rom) rom.isSelected = input.checked;
      let brand = brands.find((n1) => n1.name === input.name);
      if (brand) brand.isSelected = input.checked;
    }
    if (input.name === "price") {
      priceData.selected = input.value;
    }
    if (input.name === "os") {
      operatingSystem.selected = input.value;
    }
    this.props.onOptionChangeEvent(
      rams,
      roms,
      brands,
      priceData,
      operatingSystem
    );
  };
  render() {
    const { rams, roms, brands, priceData, operatingSystem } = this.props;
    return (
      <React.Fragment>
        <div className="row border ">
          <div className="col-1 m-3 h6">Options</div>
        </div>
        <div className="row border ">
          <div className="col-12 m-3">
            <h6>RAM</h6>
            {rams.map((ram) => (
              <div className="form-check mt-1">
                <input
                  value={ram.name}
                  onChange={this.handleChange}
                  id={ram.name}
                  name={ram.name}
                  type="checkbox"
                  checked={ram.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={ram.name}>
                  {ram.name}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>ROM</h6>
            {roms.map((rom) => (
              <div className="form-check mt-1">
                <input
                  value={rom.name}
                  onChange={this.handleChange}
                  name={rom.name}
                  id={rom.name}
                  type="checkbox"
                  checked={rom.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={rom.name}>
                  {rom.name}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>Brand</h6>
            {brands.map((brand) => (
              <div className="form-check mt-1">
                <input
                  value={brand.name}
                  onChange={this.handleChange}
                  name={brand.name}
                  id={brand.name}
                  type="checkbox"
                  checked={brand.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={brand.name}>
                  {brand.name}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>Price</h6>
            {priceData.prices.map((price) => (
              <div className="form-check mt-1">
                <input
                  value={price}
                  onChange={this.handleChange}
                  name="price"
                  id="price"
                  type="radio"
                  checked={priceData.selected === price ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={price}>
                  {price}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>OS</h6>
            {operatingSystem.operatingSys.map((os) => (
              <div className="form-check mt-1">
                <input
                  value={os}
                  onChange={this.handleChange}
                  name="os"
                  id="os"
                  type="radio"
                  checked={operatingSystem.selected === os ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={os}>
                  {os}
                </label>
              </div>
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPanel;
