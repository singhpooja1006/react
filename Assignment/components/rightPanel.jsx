import React, { Component } from "react";
import LeftPanel from "./leftPanel";
import Navbar from "./navbar";
class RightPanel extends Component {
  state = {
    domesticsFlight: [
      {
        origin: "Bengaluru",
        dest: "New Delhi",
        date: "Wed, 3 Oct",
        amount: 3590,
      },
      {
        origin: "Mumbai",
        dest: "New Delhi",
        date: "Sun, 13 Oct",
        amount: 2890,
      },
      {
        dest: "Hyderabad",
        origin: "Bengaluru",
        date: "Mon,30 Sep",
        amount: 2150,
      },
      { dest: "Mumbai", origin: "Pune", date: "Sun,6 Oct", amount: 1850 },
    ],
    holidayDestination: [
      {
        img: "https://i.ibb.co/SQ7NSZT/hol1.png",
        place: "Australia",
        price: "177,990",
        days: "9 Nights / 10 Days",
      },
      {
        img: "https://i.ibb.co/Wxj50q1/hol2.png",
        place: "Europe",
        price: "119,990",
        days: "6 Nights / 7 Days",
      },
      {
        img: "https://i.ibb.co/VY3XNZr/hol3.png",
        place: "New Zealand",
        price: "199,990",
        days: "6 Nights / 7 Days",
      },
      {
        img: "https://i.ibb.co/j4NNc35/hol4.jpg",
        place: "Sri Lanka",
        price: "18,999",
        days: "4 Nights / 5 Days",
      },
      {
        img: "https://i.ibb.co/ct6076f/hol5.jpg",
        place: "Kerala",
        price: "12,999",
        days: "4 Nights / 5 Days",
      },
      {
        img: "https://i.ibb.co/vB0CpYK/hol6.jpg",
        place: "Char Dham",
        price: "22,999",
        days: "4 Nights / 5 Days",
      },
    ],
    redirectToFlight: false,
    departure: "",
    arival: "",
  };
  handleSerachFlight = (
    departure,
    arival,
    departureDate,
    arrivalDate,
    travellersDetails,
    type
  ) => {
    console.log("Calling App SearchFlight");
    this.setState({ departure, arival });
    this.props.history.push({
      pathname: "/yatra/airSearch",
      state: {
        departure: departure,
        arrival: arival,
        departureDate: departureDate,
        travellersDetails: travellersDetails,
        arrivalDate: arrivalDate,
        type: type,
      },
    });
  };
  render() {
    const { domesticsFlight, holidayDestination } = this.state;
    return (
      <div className="container-fluid">
        <Navbar />
        <div className="row mt-4">
          <div className="col-lg-5">
            <LeftPanel onSerachFlight={this.handleSerachFlight} />
          </div>
          <div className="col-7 bg-light">
            <h6>Flight Discounts for you</h6>
            <div className="row bg-light">
              <div className="col-4">
                <img
                  src="https://i.ibb.co/qdc2z7Z/ad01.png"
                  alt=""
                  style={{
                    height: "100%",
                  }}
                />
              </div>
              <div className="col-4">
                <img
                  src="https://i.ibb.co/yp0bbgz/ad02.png"
                  alt=""
                  style={{
                    height: "100%",
                  }}
                />
              </div>
              <div className="col-4">
                <img
                  src="https://i.ibb.co/DkrVrkY/ad03.png"
                  alt=""
                  style={{
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </div>
              <div className="row">
                <div className="col-12">
                  <img
                    src="https://i.ibb.co/Rc9qLyT/banner1.jpg"
                    alt=""
                    className="m-2 ml-3"
                    style={{
                      height: "100%",
                    }}
                  />
                </div>
              </div>
              <div className="row m-1 mt-1">
                <div className="col-12 text-muted">
                  <h5>Popular Domestic Flight Routes</h5>
                </div>
              </div>
            </div>

            <div className="row">
              {domesticsFlight.map((domestic) => (
                <div className="col-2 bg-white m-1">
                  <div className="row">
                    <div className="col text-dark text-center">
                      <strong>{domestic.origin}</strong>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col text-muted text-center">
                      {" "}
                      {domestic.date}
                    </div>
                  </div>
                  <br />
                  <div className="row">
                    <div className="col text-dark text-center">
                      <strong> {domestic.dest} </strong>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col text-muted"> Starting From </div>
                  </div>
                  <div className="row">
                    <div className="col text-center">
                      <button className="btn btn-warning">
                        {domestic.origin}
                      </button>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <div className="row">
              <div className="col-12 text-muted">
                <h4>Popular Holiday Destinations</h4>
              </div>
            </div>
            <div className="row">
              {holidayDestination.map((destination) => (
                <div className="col-5 bg-white m-1">
                  <div className="row">
                    <div className="col-2 mt-3">
                      <img className="img-fluid" src={destination.img} />
                    </div>
                    <div className="col-8">
                      <div className="row">
                        <div className="col text-muted text-center">
                          {" "}
                          {destination.place}
                        </div>
                      </div>
                      <div className="row">
                        <div className="col text-center">
                          <span className="text-danger">
                            <strong> Rs.{destination.price}</strong>
                          </span>
                          <span className="text-dark">per person</span>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col text-center text-muted">
                          {" "}
                          {destination.days}
                        </div>
                      </div>
                    </div>
                    <div className="col-1 mt-3">
                      <i className="fa fa-arrow-right"></i>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RightPanel;
