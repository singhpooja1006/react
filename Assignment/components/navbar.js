import React, { Component } from "react";
import { Link } from "react-router-dom";
class Navbar extends Component {
  state = {};
  render() {
    return (
        
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link className="navbar-brand" to="/yatra">
          <img
            src="https://www.yatra.com/content/fresco/beetle/images/newIcons/yatra_logo.svg"
            alt=""
            style={{ height: "40px" }}
          />
        </Link>
        <button aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" 
        className="navbar-toggler" data-target="#navbarNav" data-toggle="collapse" 
        type="button"><span  className="navbar-toggler-icon"></span></button>
      </nav>
      
    );
  }
}

export default Navbar;
