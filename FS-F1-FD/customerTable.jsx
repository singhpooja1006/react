import React, { Component } from "react";
import axios from "axios";
import config from "./config.json";
import queryString from "query-string";
import LeftPanel from "./leftPanel";
class CustomerTable extends Component {
  state = {
    customerData: [],
    pageInfo: {},
    sortBy: "",
    searchText: "",
    cities: [
      { name: "Delhi", isSelected: false },
      { name: "Noida", isSelected: false },
      { name: "Jaipur", isSelected: false },
      { name: "Gurgaon", isSelected: false },
    ],
    payments: [
      { name: "Debit Card", isSelected: false },
      { name: "Credit Card", isSelected: false },
      { name: "Wallet", isSelected: false },
    ],
    gendersData: { genders: ["Male", "Female"], selected: "" },
  };
  async componentDidMount() {
    const { data: customerData } = await axios.get(
      config.apiEndPoint + "/customers"
    );

    this.setState({
      customerData: customerData.data,
      pageInfo: customerData.pageInfo,
    });
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      console.log(this.props.location.search);
      let apiEndpoint =
        config.apiEndPoint + "/customers" + this.props.location.search;
      console.log("URL", apiEndpoint);
      const { data: customerData } = await axios.get(apiEndpoint);
      this.setState({
        customerData: customerData.data,
        pageInfo: customerData.pageInfo,
      });
    }
  }

  calURL = (params, name, city, payment, gender, sortBy, page) => {
    let path = "/customers";
    params = this.addToParams(params, "name", name);
    params = this.addToParams(params, "city", city);
    params = this.addToParams(params, "payment", payment);
    params = this.addToParams(params, "gender", gender);
    params = this.addToParams(params, "sortBy", sortBy);
    params = this.addToParams(params, "page", page);

    this.props.history.push({
      pathname: path,
      search: params,
    });
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  handleSortBy = (sortBy) => {
    this.setState({ sortBy });
    let { city, payment, gender, name } = queryString.parse(
      this.props.location.search
    );
    this.calURL("", name, city, payment, gender, sortBy, 1);
  };

  handleCheckEvent = (cities, payments, gendersData) => {
    this.setState({ cities, payments, gendersData });
    let city = this.buildQueryString(cities);
    let payment = this.buildQueryString(payments);
    this.calURL(
      "",
      this.state.searchText,
      city,
      payment,
      gendersData.selected,
      this.state.sortBy,
      1
    );
  };
  buildQueryString(optionsData) {
    let filterData = optionsData.filter((n1) => n1.isSelected);
    let arrayData = filterData.map((n1) => n1.name);
    return arrayData.join(",");
  }

  pageNavigate = (value) => {
    const { pageInfo } = { ...this.state };
    let { city, payment, gender, sortBy, name } = queryString.parse(
      this.props.location.search
    );
    let currPage = pageInfo.pageNumber + value;
    this.calURL("", name, city, payment, gender, sortBy, currPage);
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    this.setState({ searchText: input.value });
  };
  handleFilterName = (e) => {
    //e.preventDefault();
    let { city, payment, gender, sortBy } = queryString.parse(
      this.props.location.search
    );
    this.calURL("", this.state.searchText, city, payment, gender, sortBy, 1);
  };

  render() {
    return (
      <div className="container">
        <div className="row m-1">
          <div className="col-2 mr-1">
            <LeftPanel
              cities={this.state.cities}
              payments={this.state.payments}
              gendersData={this.state.gendersData}
              onCheckButtonEvent={this.handleCheckEvent}
            />
          </div>
          <div className="col-8 ml-2">{this.renderCustomerData()}</div>
        </div>
      </div>
    );
  }
  renderCustomerData = () => {
    const { customerData, pageInfo } = this.state;
    if (customerData) {
      let { page } = queryString.parse(this.props.location.search);
      page = page ? +page : 1;
      return (
        <React.Fragment>
          <div className="row mt-3">
            <input
              className="col-6"
              type="text"
              name="searchText"
              id="searchText"
              value={this.state.searchText}
              onChange={this.handleChange}
            />
            <button
              className="btn btn-primary ml-3"
              onClick={() => this.handleFilterName()}
            >
              FilterName
            </button>
          </div>
          <div className="row">
            {pageInfo.pageNumber === 1
              ? 1
              : pageInfo.numOfItems * pageInfo.pageNumber -
                pageInfo.numOfItems +
                1}{" "}
            to{" "}
            {pageInfo.numOfItems * pageInfo.pageNumber < pageInfo.totalItemCount
              ? pageInfo.numOfItems * pageInfo.pageNumber
              : pageInfo.totalItemCount}{" "}
            of {pageInfo.totalItemCount}
          </div>

          <div className="row border text-center mt-3 bg-info">
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("id")}
            >
              ID
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("name")}
            >
              Name
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("city")}
            >
              City
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("age")}
            >
              Age
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("gender")}
            >
              Gender
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("payment")}
            >
              Payment
            </div>
          </div>
          {customerData.map((customer) => (
            <div className="row border text-center">
              <div className="col-2 border">{customer.id}</div>
              <div className="col-2 border">{customer.name}</div>
              <div className="col-2 border">{customer.city}</div>
              <div className="col-2 border">{customer.age}</div>
              <div className="col-2 border">{customer.gender}</div>
              <div className="col-2 border">{customer.payment}</div>
            </div>
          ))}
          <div className="row m-1">
            <div>
              {page > 1 ? (
                <button
                  className="btn btn-primary m-1"
                  onClick={() => this.pageNavigate(-1)}
                >
                  Previous
                </button>
              ) : (
                ""
              )}
            </div>
            <div>
              {page < pageInfo.numberOfPages ? (
                <button
                  className="btn btn-primary m-1"
                  onClick={() => this.pageNavigate(1)}
                >
                  Next
                </button>
              ) : (
                ""
              )}
            </div>
          </div>
        </React.Fragment>
      );
    }
  };
}

export default CustomerTable;
