import React, { Component } from "react";
class LeftPanel extends Component {
  state = {};
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let { cities, payments, gendersData } = {
      ...this.props,
    };
    if (input.type === "checkbox") {
      let city = cities.find((n1) => n1.name === input.name);
      if (city) city.isSelected = input.checked;

      let payment = payments.find((n1) => n1.name === input.name);
      if (payment) payment.isSelected = input.checked;
    }
    if (input.name === "gender") {
      gendersData.selected = input.value;
    }

    this.props.onCheckButtonEvent(cities, payments, gendersData);
  };
  render() {
    const { cities, payments, gendersData } = this.props;
    return (
      <React.Fragment>
        <div className="row border ">
          <div className="col-1 m-3 h6">Options</div>
        </div>
        <div className="row border ">
          <div className="col-12 m-3">
            <h6>City</h6>
            {cities.map((city) => (
              <div className="form-check mt-1">
                <input
                  value={city.name}
                  onChange={this.handleChange}
                  id={city.name}
                  name={city.name}
                  type="checkbox"
                  checked={city.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={city.name}>
                  {city.name}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>Payment</h6>
            {payments.map((payment) => (
              <div className="form-check mt-1">
                <input
                  value={payment.name}
                  onChange={this.handleChange}
                  name={payment.name}
                  id={payment.name}
                  type="checkbox"
                  checked={payment.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={payment.name}>
                  {payment.name}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>Gender</h6>
            {gendersData.genders.map((gender) => (
              <div className="form-check mt-1">
                <input
                  value={gender}
                  onChange={this.handleChange}
                  id="gender"
                  name="gender"
                  type="radio"
                  checked={gendersData.selected === gender ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={gender}>
                  {gender}
                </label>
              </div>
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPanel;
