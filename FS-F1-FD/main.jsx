import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import CustomerTable from "./customerTable";

class MainComp extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route path="/customers" component={CustomerTable} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default MainComp;
