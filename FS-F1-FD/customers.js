var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
const port  = 2410;
app.listen(port,()=>console.log("Listening on port : ",port))
const MongoClient = require("mongodb").MongoClient;
let ObjectId = require("mongodb").ObjectId;
const url = "mongodb://localhost:27017";
const dbName = "customerDB";
const client = new MongoClient(url);


var customerData = [
    {id: "DFI61",name:"Vishal", city:"Delhi", age:27, gender:"Male", payment:"Credit Card"},
    {id: "JUW88",name:"Amit", city:"Noida", age:49, gender:"Male", payment:"Debit Card"},
    {id: "KPW09",name:"Pradeep", city:"Gurgaon", age:21, gender:"Male", payment:"Wallet"},
    {id: "ABR12", name:"Rohit", city:"Jaipur", age:34, gender:"Male", payment:"Debit Card"},
    {id: "BR451", name:"Preeti", city:"Delhi", age:29, gender:"Female", payment:"Credit Card"},
    {id: "MKR52", name:"Neha", city:"Noida", age:42, gender:"Female ", payment:"Debit Card"},
    {id: "BTT66", name:"Swati", city:"Gurgaon", age:24, gender:"Female ", payment:"Wallet"},
    {id: "CDP09", name:"Meghna", city:"Jaipur", age:38, gender:"Female ", payment:"Debit Card"},
    {id: "KK562", name:"Irfan", city:"Delhi", age:25, gender:"Male", payment:"Credit Card"},
    {id: "LPR34", name:"Gagan", city:"Noida", age:51, gender:"Female ", payment:"Debit Card"},
    {id: "MQC11", name:"John", city:"Gurgaon", age:24, gender:"Male", payment:"Wallet"},
    {id: "AXY22", name:"Gurmeet", city:"Jaipur", age:31, gender:"Male", payment:"Debit Card"},
    {id: "APW03", name:"Aarti", city:"Delhi", age:33, gender:"Female", payment:"Credit Card"},
    {id: "NNB71", name:"Sandeep", city:"Noida", age:39, gender:"Male", payment:"Debit Card"},
    {id: "UVX51", name:"Suman", city:"Gurgaon", age:45, gender:"Female ", payment:"Wallet"},
    {id: "PSA22", name:"Piyush", city:"Delhi", age:27, gender:"Male", payment:"Credit Card"},
    {id: "ABT88", name:"Payal", city:"Noida", age:40, gender:"Female ", payment:"Debit Card"},
    {id: "CRT61", name:"Richa", city:"Gurgaon", age:28, gender:"Female ", payment:"Wallet"}
  ];


app.post("/customers",function(req,res) { 
    client.connect(function(err,client){
    console.log("Post : /students");
    const db = client.db(dbName);
    db.collection("customers").insertMany(customerData , function(err,result){
        console.log(result.insertedCount);
        res.send(result);
    });
  });
});


app.get("/customers",function(req,res) { 
	let {sortBy,city,payment,gender,name} = req.query;
	let page = +req.query.page;
    page = isNaN(page) ? 1 : page;
	let customerList = [];
    client.connect(function(err,client){
		console.log("Get: /customers",name);
		let filterCondition = {};
		let sorttingBy= {};
		if(name){
			filterCondition.name =  new RegExp(name);
		}
		if(city){
			let cityList = city.split(",");
			filterCondition.city = { $in : cityList};
		}
		if(payment){
			let paymentList = payment.split(",");
			filterCondition.payment = { $in : paymentList};
		}
		if(gender){
			
			filterCondition.gender = { $in : [gender]};
		}
		if(sortBy === "id"){
			sorttingBy.id = 1;
		}
		else if(sortBy === "name"){
			sorttingBy.name = 1;
		}
		else if(sortBy === "city"){
			sorttingBy.city = 1;
		}
		else if(sortBy === "age"){
			sorttingBy.age = 1;
		}
		else if(sortBy === "gender"){
			sorttingBy.gender = 1;
		}
		else if(sortBy === "payment"){
			sorttingBy.payment = 1;
		}
		const db = client.db(dbName);
		db.collection("customers").find(filterCondition).sort(sorttingBy).toArray(function(
			err,result
		){
			console.log(result);
			respArr = pagination(result, page);
			res.json({
				data: respArr,
				pageInfo: getPageInfo(result,page),
			  });
		});
	});	
  });

function getPageInfo(obj, page){
	let len = obj.length;
    let quo = Math.floor(len / 4);
    let rem = len % 4;
    let extra = rem === 0 ? 0 : 1;
    let numofpages = quo + extra;
    let pageInfo = {
        pageNumber: page,
        numberOfPages: numofpages,
        numOfItems: 4,
        totalItemCount: obj.length,
	};
	return pageInfo;
}
	
function pagination(obj, page) {
	var resArr = obj;
	resArr = resArr.slice(page * 4 - 4, page * 4);
	return resArr;
}