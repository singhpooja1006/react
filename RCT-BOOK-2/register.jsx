import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
class RegisterForm extends Component {
  state = {
    data: { name: "", password: "", cnfPwd: "" },
    errors: {},
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const apiEndpoint = config.apiEndPoint + "/newuser";
      const { data: user } = await http.post(apiEndpoint, {
        name: this.state.data.name,
        password: this.state.data.password,
        cnfPwd: this.state.data.cnfPwd,
      });
      window.location = "/login";
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = { ...this.state.errors };
        if (ex.response.data.includes("Bad Request")) {
          errors.msg = "Database Error.";
        } else {
          errors.msg = ex.response.data;
        }

        this.setState({ errors });
      }
    }
  };
  validate = () => {
    let errs = {};

    if (!this.state.data.name.trim()) errs.name = "Username is required";
    else if (this.state.data.name.trim().length < 6)
      errs.name = "Username  should be minimum 6 char long";
    if (!this.state.data.password.trim())
      errs.password = "Password is required";
    else if (this.state.data.password.trim().length < 6)
      errs.password = "Password  should be minimum 6 char long";
    if (!this.state.data.cnfPwd.trim())
      errs.cnfPwd = "Confirm Password is required";
    else if (this.state.data.password !== this.state.data.cnfPwd)
      errs.cnfPwd = "Password and confirm password should match";

    return errs;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "name":
        if (!e.currentTarget.value.trim()) return "Username is required";
        else if (e.currentTarget.value.trim().length < 6)
          return "Username  should be minimum 6 char long";
        break;
      case "password":
        if (!e.currentTarget.value.trim()) return "Password is required";
        else if (e.currentTarget.value.trim().length < 6)
          return "Password  should be minimum 6 char long";
        break;
      case "cnfPwd":
        if (!e.currentTarget.value.trim())
          return "Confirm password is required";
        else if (e.currentTarget.value !== this.state.data.password)
          return "Password and confirm password should match";
        break;

      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const registerData = { ...this.state.data };
    registerData[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data: registerData, errors: errors });
  };
  viewLoginForm = () => {
    window.location = "/login";
  };
  render() {
    const { data, errors } = this.state;
    return (
      <div className="bg-light">
        <form onSubmit={this.handleSubmit}>
          <div className="ml-5 mr-5">
            <h1>Register</h1>
            {errors.msg ? <div className="text-danger">{errors.msg}</div> : ""}
            <div className="form-group">
              <label htmlFor="name">Username</label>
              <input
                value={data.name}
                onChange={this.handleChange}
                type="text"
                id="name"
                name="name"
                className="form-control"
              />
              {errors.name ? (
                <div className="alert alert-danger">{errors.name}</div>
              ) : (
                ""
              )}
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                value={data.password}
                onChange={this.handleChange}
                type="password"
                id="password"
                name="password"
                className="form-control"
              />
              {errors.password ? (
                <div className="alert alert-danger">{errors.password}</div>
              ) : (
                ""
              )}
            </div>
            <div className="form-group">
              <label htmlFor="cnfPwd">Confirm Password</label>
              <input
                value={data.cnfPwd}
                onChange={this.handleChange}
                type="password"
                id="cnfPwd"
                name="cnfPwd"
                className="form-control"
              />
              {errors.cnfPwd ? (
                <div className="alert alert-danger">{errors.cnfPwd}</div>
              ) : (
                ""
              )}
            </div>
            <button className="btn btn-primary">Register</button>
            <div className="text-primary" onClick={() => this.viewLoginForm()}>
              Login Existing User
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default RegisterForm;
