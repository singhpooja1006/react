import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import NavBar from "./navbar";
import BooksData from "./books";
import LoginForm from "./loginForm";
import Logout from "./logout";
import RegisterForm from "./register";
import Favourites from "./favourites";
import ProtectedRoute from "./common/protectedRoute";
import BookDetails from "./bookDetails";
class MainComponent extends Component {
  state = {};
  componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    this.setState({ user });
  }
  render() {
    const { user } = this.state;
    return (
      <div>
        <NavBar user={user} />
        <Switch>
          <Route path="/bookDetails/:bookid" component={BookDetails} />
          <ProtectedRoute path="/favourites" component={Favourites} />
          <Route path="/books/:genre" component={BooksData} />
          <Route path="/books" component={BooksData} />
          <Route path="/registration" component={RegisterForm} />
          <Route path="/logout" component={Logout} />
          <Route path="/login" component={LoginForm} />
        </Switch>
      </div>
    );
  }
}

export default MainComponent;
