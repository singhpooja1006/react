import React, { Component } from "react";
import { Link } from "react-router-dom";
import { NavDropdown } from "react-bootstrap";
class NavBar extends Component {
  state = {};
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <a className="navbar-brand" href="/books">
          BookSite
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <a href="/books?newarrival=yes" className="nav-link">
                New Arrival
              </a>
            </li>
            <li className="nav-item">
              <NavDropdown title="Book by Genre" id="basic-nav-dropdown">
                <NavDropdown.Item href="/books/fiction">
                  Fiction
                </NavDropdown.Item>
                <NavDropdown.Item href="/books/children">
                  Children
                </NavDropdown.Item>
                <NavDropdown.Item href="/books/mystery">
                  Mystery
                </NavDropdown.Item>
                <NavDropdown.Item href="/books/management">
                  Management
                </NavDropdown.Item>
                <NavDropdown.Item href="/books/self help">
                  Self Help
                </NavDropdown.Item>
              </NavDropdown>
            </li>
            <li className="nav-item">
              <li className="nav-item">
                <a href="/books" className="nav-link">
                  All Books
                </a>
              </li>
            </li>
          </ul>
          <ul className="navbar-nav ml-auto">
            {this.props.user ? (
              <li className="nav-item">
                <Link to="#" className="nav-link text-primary">
                  Welcome {this.props.user.name}
                </Link>
              </li>
            ) : (
              ""
            )}
            <li className="nav-item">
              <Link to="/favourites" className="nav-link">
                Favorites
              </Link>
            </li>
            <li className="nav-item">
              {this.props.user ? (
                <Link to="/logout" className="nav-link">
                  Logout
                </Link>
              ) : (
                <Link to="/login" className="nav-link">
                  Login
                </Link>
              )}
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
export default NavBar;
