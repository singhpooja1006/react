import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
class LoginForm extends Component {
  state = {
    data: { name: "", password: "" },
    errors: {},
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const apiEndpoint = config.apiEndPoint + "/loginuser";
      const { data: user } = await http.post(apiEndpoint, {
        name: this.state.data.name,
        password: this.state.data.password,
      });
      localStorage.setItem("user", JSON.stringify(user));
      window.location = "/books";
    } catch (ex) {
      if (ex.response && ex.response.status === 401) {
        const errors = { ...this.state.errors };
        if (ex.response.data.includes("Login Failed")) {
          errors.msg = "Login Failed. Check the username and password.";
        } else {
          errors.msg = ex.response.data;
        }

        this.setState({ errors });
      }
    }
  };
  validate = () => {
    let errs = {};

    if (!this.state.data.name.trim()) errs.name = "Username is required";

    if (!this.state.data.password.trim())
      errs.password = "Password is required";

    return errs;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "name":
        if (!e.currentTarget.value.trim()) return "Username is required";

        break;
      case "password":
        if (!e.currentTarget.value.trim()) return "Password is required";

        break;
      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const loginData = { ...this.state.data };
    loginData[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data: loginData, errors: errors });
  };
  viewRegisterForm = () => {
    window.location = "/registration";
  };
  render() {
    const { data, errors } = this.state;
    return (
      <div className="bg-light">
        <form onSubmit={this.handleSubmit}>
          <div className="ml-5 mr-5">
            <h1>Login</h1>
            {errors.msg ? <div className="text-danger">{errors.msg}</div> : ""}
            <div className="form-group">
              <label htmlFor="name">Username</label>
              <input
                value={data.name}
                onChange={this.handleChange}
                type="text"
                id="name"
                name="name"
                className="form-control"
              />
              {errors.name ? (
                <div className="alert alert-danger">{errors.name}</div>
              ) : (
                ""
              )}
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                value={data.password}
                onChange={this.handleChange}
                type="password"
                id="password"
                name="password"
                className="form-control"
              />
              {errors.password ? (
                <div className="alert alert-danger">{errors.password}</div>
              ) : (
                ""
              )}
            </div>
            <button className="btn btn-primary">Login</button>

            <div
              className="text-primary"
              onClick={() => this.viewRegisterForm()}
            >
              Register New User
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default LoginForm;
