import React, { Component } from "react";
class LeftPanelComponent extends Component {
  state = {};
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let { bestSellerOptions, languageOptions } = { ...this.props };
    bestSellerOptions = this.updateCBData(bestSellerOptions, input);
    languageOptions = this.updateCBData(languageOptions, input);

    this.props.onCheckboxEven(bestSellerOptions, languageOptions);
  };

  updateCBData(cbOptions, input) {
    let index = cbOptions.options.findIndex(
      (option) => option.refineValue === input.name
    );
    if (index !== -1) {
      cbOptions.options[index].isSelected = input.checked;
      if (input.checked) {
        cbOptions.selected = cbOptions.options[index].refineValue;
      } else {
        cbOptions.selected = "";
      }
    }
    return cbOptions;
  }

  render() {
    let { bestSellerOptions, languageOptions } = this.props;
    return (
      <React.Fragment>
        <div className="row border ">
          <div className="col-4 m-3 h6">Options</div>
        </div>
        <div className="row border ">
          <div className="col-4 m-3">
            <h6>{bestSellerOptions.display}</h6>
            {bestSellerOptions.options.map((option) => (
              <div className="form-check mt-1">
                <input
                  value={option.refineValue}
                  onChange={this.handleChange}
                  id={option.refineValue}
                  name={option.refineValue}
                  type="checkbox"
                  checked={option.isSelected}
                  className="form-check-input"
                />
                <label
                  className="form-check-label"
                  htmlFor={option.refineValue}
                >
                  {option.refineValue}({option.totalNum})
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border ">
          <div className="col-4 m-3">
            <h6>{languageOptions.display}</h6>
            {languageOptions.options.map((option) => (
              <div className="form-check mt-1">
                <input
                  value={option.refineValue}
                  onChange={this.handleChange}
                  id={option.refineValue}
                  name={option.refineValue}
                  type="checkbox"
                  checked={option.isSelected}
                  className="form-check-input"
                />
                <label
                  className="form-check-label"
                  htmlFor={option.refineValue}
                >
                  {option.refineValue}({option.totalNum})
                </label>
              </div>
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default LeftPanelComponent;
