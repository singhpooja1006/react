import React, { Component } from "react";
import axios from "axios";
import config from "./config.json";
import queryString from "query-string";
import LeftPanelComponent from "./leftPanel";
class BooksData extends Component {
  state = {
    booksData: [],
    pageInfo: {},
    bestSellerOptions: { options: [], selected: "", display: "BestSeller" },
    languageOptions: { options: [], selected: "", display: "Language" },
  };
  async componentDidMount() {
    const { data: bookDataList } = await axios.get(
      config.apiEndPoint +
        this.props.location.pathname +
        this.props.location.search
    );
    let { bestSellerOptions, languageOptions } = { ...this.state };
    bestSellerOptions.options = bookDataList.refineOptions.bestseller.filter(
      (option) => option.totalNum > 0
    );
    languageOptions.options = bookDataList.refineOptions.language.filter(
      (option) => option.totalNum > 0
    );
    this.setState({
      booksData: bookDataList.data,
      pageInfo: bookDataList.pageInfo,
      bestSellerOptions: bestSellerOptions,
      languageOptions: languageOptions,
    });
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.location.search !== this.props.location.search) {
      const { data: bookDataList } = await axios.get(
        config.apiEndPoint +
          this.props.location.pathname +
          this.props.location.search
      );
      let { bestSellerOptions, languageOptions } = { ...this.state };
      bestSellerOptions.options = bookDataList.refineOptions.bestseller.filter(
        (option) => option.totalNum > 0
      );
      languageOptions.options = bookDataList.refineOptions.language.filter(
        (option) => option.totalNum > 0
      );

      this.setState({
        booksData: bookDataList.data,
        pageInfo: bookDataList.pageInfo,
        bestSellerOptions: bestSellerOptions,
        languageOptions: languageOptions,
      });
    }
  }
  pageNavigate = (value) => {
    const { pageInfo } = { ...this.state };
    let { newarrival } = queryString.parse(this.props.location.search);
    const genre = this.props.match.params.genre;
    let currPage = pageInfo.pageNumber + value;
    this.calURL(
      "",
      genre,
      newarrival,
      currPage,
      this.state.bestSellerOptions.selected,
      this.state.languageOptions.selected
    );
  };

  calURL = (params, genre, newarrival, page, bestSeller, language) => {
    let path = "/books";
    if (genre) {
      path = path + "/" + genre;
    }
    params = this.addToParams(params, "newarrival", newarrival);
    params = this.addToParams(params, "page", page);
    params = this.addToParams(params, "bestseller", bestSeller);
    params = this.addToParams(params, "language", language);
    this.props.history.push({
      pathname: path,
      search: params,
    });
  };

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }

  handleOptionChange = (bestSellerOptions, languageOptions) => {
    const genre = this.props.match.params.genre;
    const { newarrival } = queryString.parse(this.props.location.search);
    this.calURL(
      "",
      genre,
      newarrival,
      "",
      bestSellerOptions.selected,
      languageOptions.selected
    );
  };
  handleBookDetails = (bookid) => {
    window.location = "/bookDetails/" + bookid;
  };
  handleShowOptions = () => {
    let x = { LeftPanelComponent };
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  };
  render() {
    const { booksData, pageInfo } = { ...this.state };
    let { page } = queryString.parse(this.props.location.search);
    page = page ? +page : 1;
    return (
      <div className="container-fluid">
        <div className="row float-right">
          <button
            type="button"
            className="col btn btn-sm btn-primary d-block d-lg-none d-md-none d-sm-block d-xs-block text-left"
            data-toggle="collapse"
            data-target="#leftPannel"
            //onClick={() => this.handleShowOptions()}
          >
            Options
          </button>
          <br />
        </div>
        <br />
        <br />
        <div className="row">
          <div
            className="col d-lg-block d-md-block d-sm-none d-xs-none mr-1 collapse"
            id="leftPannel"
          >
            <LeftPanelComponent
              bestSellerOptions={this.makeCheckBoxStructure(
                this.state.bestSellerOptions
              )}
              languageOptions={this.makeCheckBoxStructure(
                this.state.languageOptions
              )}
              onCheckboxEven={this.handleOptionChange}
            />
          </div>

          <div className="col-9 ml-2">
            <div className="row">
              {pageInfo.pageNumber === 1
                ? 1
                : pageInfo.numOfItems * pageInfo.pageNumber -
                  pageInfo.numOfItems +
                  1}{" "}
              to{" "}
              {pageInfo.numOfItems * pageInfo.pageNumber <
              pageInfo.totalItemCount
                ? pageInfo.numOfItems * pageInfo.pageNumber
                : pageInfo.totalItemCount}{" "}
              of {pageInfo.totalItemCount}
            </div>
            <div className="row mt-2 bg-info text-wrap text-center">
              <div className="col d-block text-center border">Title</div>
              <div className="col d-none d-md-block text-center border">
                Author
              </div>
              <div className="col d-none d-sm-block text-center border">
                Language
              </div>
              <div className="col d-none d-lg-block text-center border">
                Genre
              </div>
              <div className="col d-block border">Price</div>
              <div className="col d-none d-lg-block text-left border text-wrap">
                Bestseller
              </div>
            </div>
            {booksData.map((book) => (
              <div className="row border text-center text-wrap">
                <div
                  className="col d-block border text-primary"
                  onClick={() => this.handleBookDetails(book.bookid)}
                >
                  {book.name}
                </div>
                <div className="col d-none d-md-block border">
                  {book.author}
                </div>
                <div className="col d-none d-sm-block border">
                  {book.language}
                </div>
                <div className="col d-none d-lg-block border">{book.genre}</div>
                <div className="col d-block border">{book.price}</div>
                <div className="col d-none d-lg-block border">
                  {book.bestseller}
                </div>
              </div>
            ))}
            <div className="row m-1">
              <div>
                {page > 1 ? (
                  <button
                    className="btn btn-primary m-1"
                    onClick={() => this.pageNavigate(-1)}
                  >
                    Previous
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div>
                {page < pageInfo.numberOfPages ? (
                  <button
                    className="btn btn-primary m-1"
                    onClick={() => this.pageNavigate(1)}
                  >
                    Next
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  makeCheckBoxStructure(inputOptions) {
    let optionsCB = [];
    for (let i = 0; i < inputOptions.options.length; i++) {
      let optionJson = {};
      let obj = optionsCB.findIndex(
        (optionJ) => optionJ.refineValue === inputOptions.options[i].refineValue
      );
      if (obj === -1) {
        optionJson.refineValue = inputOptions.options[i].refineValue;
        optionJson.totalNum = inputOptions.options[i].totalNum;
        optionJson.isSelected = inputOptions.options[i].isSelected
          ? inputOptions.options[i].isSelected
          : false;
        optionsCB.push(optionJson);
      }
    }
    if (inputOptions.selected && inputOptions.selected.length > 0) {
      let cnames = inputOptions.selected;
      let obj = optionsCB.find((n1) => n1.refineValue === cnames);
      if (obj) obj.isSelected = true;
    }
    inputOptions.options = optionsCB;

    return inputOptions;
  }
}

export default BooksData;
