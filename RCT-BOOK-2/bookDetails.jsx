import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
class BookDetails extends Component {
  state = {
    detailsBookData: {},
    errors: {},
  };

  async componentDidMount() {
    const bookid = this.props.match.params.bookid;
    const apiEndpoint = config.apiEndPoint + "/book/" + bookid;
    const { data: detailsBookData } = await http.get(apiEndpoint);
    this.setState({ detailsBookData });
  }
  handleAddFavourites = async () => {
    const { detailsBookData } = this.state;
    const user = JSON.parse(localStorage.getItem("user"));
    try {
      const apiEndpoint = config.apiEndPoint + "/favourites/" + user.userid;
      const { data: favouriteBook } = await http.post(apiEndpoint, {
        bookid: detailsBookData.bookid,
      });
      window.location = "/books";
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        const errors = { ...this.state.errors };
        errors.msg = ex.response.data;
        this.setState({ errors });
      }
    }
  };
  render() {
    const { detailsBookData, errors } = this.state;
    const user = JSON.parse(localStorage.getItem("user"));
    return (
      <div className="container">
        {errors.msg ? <div className="text-danger">{errors.msg}</div> : ""}
        <h2>Book : {detailsBookData.name}</h2>
        <div className="row border">
          <div className="col-3">Author:</div>
          <div className="col-9">{detailsBookData.author}</div>
        </div>
        <div className="row border">
          <div className="col-3">Genre:</div>
          <div className="col-9">{detailsBookData.genre}</div>
        </div>
        <div className="row border">
          <div className="col-3">Publisher:</div>
          <div className="col-9">{detailsBookData.publisher}</div>
        </div>
        <div className="row border">
          <div className="col-3">Description:</div>
          <div className="col-9">{detailsBookData.description}</div>
        </div>
        <div className="row border">
          <div className="col-3">Blurb:</div>
          <div className="col-9">{detailsBookData.blurb}</div>
        </div>
        <div className="row border">
          <div className="col-3">Review:</div>
          <div className="col-9">{detailsBookData.review}</div>
        </div>
        <div className="row border">
          <div className="col-3">Price:</div>
          <div className="col-9">{detailsBookData.price}</div>
        </div>
        <div className="row border">
          <div className="col-3">Rating:</div>
          <div className="col-9">{detailsBookData.avgrating}</div>
        </div>
        <div className="row border">
          {user ? (
            <div
              className="col-3 text-primary"
              onClick={() => this.handleAddFavourites()}
            >
              Add to favourites
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}

export default BookDetails;
