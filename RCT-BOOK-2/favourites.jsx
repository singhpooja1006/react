import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
import queryString from "query-string";
class Favourites extends Component {
  state = {
    favouritesBookData: [],
    pageInfo: {},
  };
  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    const apiEndpoint = config.apiEndPoint + "/favourites/" + user.userid;
    const { data: favouritesBook } = await http.get(apiEndpoint);
    this.setState({
      favouritesBookData: favouritesBook.data,
      pageInfo: favouritesBook.pageInfo,
    });
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.location.search !== this.props.location.search) {
      const user = JSON.parse(localStorage.getItem("user"));
      const apiEndpoint =
        config.apiEndPoint +
        "/favourites/" +
        user.userid +
        this.props.location.search;
      const { data: favouritesBook } = await http.get(apiEndpoint);
      this.setState({
        favouritesBookData: favouritesBook.data,
        pageInfo: favouritesBook.pageInfo,
      });
    }
  }
  pageNavigate = (value) => {
    const { pageInfo } = { ...this.state };
    let currPage = pageInfo.pageNumber + value;
    this.calURL("", currPage);
  };

  calURL = (params, page) => {
    let path = "/favourites";

    params = this.addToParams(params, "page", page);
    this.props.history.push({
      pathname: path,
      search: params,
    });
  };

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  render() {
    return <div className="container">{this.renderFavouriteBooks()}</div>;
  }
  renderFavouriteBooks = () => {
    const { favouritesBookData, pageInfo } = this.state;
    if (favouritesBookData) {
      let { page } = queryString.parse(this.props.location.search);
      page = page ? +page : 1;
      return (
        <React.Fragment>
          <div className="row mt-2">
            FAVOURITES:{" "}
            {pageInfo.pageNumber === 1
              ? 1
              : pageInfo.numOfItems * pageInfo.pageNumber -
                pageInfo.numOfItems +
                1}{" "}
            to{" "}
            {pageInfo.numOfItems * pageInfo.pageNumber < pageInfo.totalItemCount
              ? pageInfo.numOfItems * pageInfo.pageNumber
              : pageInfo.totalItemCount}{" "}
            of {pageInfo.totalItemCount}
          </div>
          <div className="row mt-2 bg-info text-center">
            <div className="col-6 text-center border">Title</div>
            <div className="col-6 text-center border">Author</div>
          </div>
          {favouritesBookData.map((favourite) => (
            <div className="row border text-center">
              <div className="col-6 border text-primary">{favourite.name}</div>
              <div className="col-6 border">{favourite.author}</div>
            </div>
          ))}
          <div className="row m-1">
            <div>
              {page > 1 ? (
                <button
                  className="btn btn-primary m-1"
                  onClick={() => this.pageNavigate(-1)}
                >
                  Previous
                </button>
              ) : (
                ""
              )}
            </div>
            <div>
              {page < pageInfo.numberOfPages ? (
                <button
                  className="btn btn-primary m-1"
                  onClick={() => this.pageNavigate(1)}
                >
                  Next
                </button>
              ) : (
                ""
              )}
            </div>
          </div>
        </React.Fragment>
      );
    }
  };
}

export default Favourites;
