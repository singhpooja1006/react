import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
class AddProduct extends Component {
  state = {
    data: { name: "", description: "", imgLink: "", price: "", category: "" },
    categories: [
      "Sunglasses",
      "Watches",
      "Belts",
      "Handbags",
      "Wallets",
      "Formal Shoes",
      "Sport Shoes",
      "Floaters",
      "Sandals",
    ],
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const { data } = this.state;
      let apiEndPoint = config.apiEndPoint + "/product";
      await axios.post(apiEndPoint, {
        category: data.category,
        imgLink: data.imgLink,
        name: data.name,
        description: data.description,
        price: data.price,
      });
      window.location = "/products";
    } catch (ex) {}
  };
  handleChange = (e) => {
    const formdata = { ...this.state.data };
    formdata[e.currentTarget.id] = e.currentTarget.value;
    this.setState({ data: formdata });
  };
  render() {
    const { data, categories } = this.state;
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              value={data.name}
              onChange={this.handleChange}
              type="text"
              name="name"
              id="name"
              className="form-control"
              placeholder="Name.."
            />
          </div>
          <div className="form-group">
            <label htmlFor="description">Description</label>
            <input
              value={data.description}
              onChange={this.handleChange}
              type="text"
              name="description"
              id="description"
              className="form-control"
              placeholder="Description.."
            />
          </div>
          <div className="form-group">
            <label htmlFor="price">Price</label>
            <input
              value={data.price}
              onChange={this.handleChange}
              type="number"
              id="price"
              name="price"
              className="form-control"
              placeholder="Price.."
            />
          </div>
          <div className="form-group">
            <label htmlFor="imgLink">Image</label>
            <input
              value={data.imgLink}
              onChange={this.handleChange}
              type="text"
              id="imgLink"
              name="imgLink"
              className="form-control"
              placeholder="Image url.."
            />
          </div>
          <div className="form-group">
            <label htmlFor="category">Category</label>
            <select
              value={data.category}
              onChange={this.handleChange}
              id="category"
              name="category"
              className="form-control"
            >
              {categories.map((category) => (
                <option key={category}>{category}</option>
              ))}
            </select>
          </div>
          <button className="btn btn-primary" type="submit">
            Add
          </button>
        </form>
      </div>
    );
  }
}

export default AddProduct;
