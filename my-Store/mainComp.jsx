import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import axios from "axios";
import config from "./config.json";
import StoreData from "./showStoreData";
import Login from "./login";
import Logout from "./logout";
import Cart from "./cart";
import CheckOut from "./checkOut";
import OrderSuccess from "./orderSuccess";
import MyOrders from "./myOrders";
import ManageProducts from "./manageProducts";
import AddProduct from "./addNewProduct";
import Edit from "./editProduct";
class MainComp extends Component {
  state = {
    totalQty: 0,
    categoryData: {
      categories: [
        "Sunglasses",
        "Watches",
        "Belts",
        "Handbags",
        "Wallets",
        "Formal Shoes",
        "Sport Shoes",
        "Floaters",
        "Sandals",
      ],
      selected: "",
    },
  };
  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));

    const { data: products } = await axios.get(
      config.apiEndPoint + "/products"
    );
    let productsData = [];
    for (let index = 0; index < products.length; index++) {
      let product = products[index];
      product.qty = 0;
      productsData.push(product);
    }
    this.setState({
      productsData,
      user,
    });
  }
  handleTotalQty = (totalQty, productsData) => {
    this.setState({ totalQty, productsData });
  };

  handleCategory = (categoryData) => {
    this.setState({ categoryData });
  };
  render() {
    const { user } = this.state;
    return (
      <React.Fragment>
        <Switch>
          <Route
            path="/cart"
            component={() => (
              <Cart
                productsData={this.state.productsData}
                totalQty={this.state.totalQty}
                onTotalQtyChange={this.handleTotalQty}
              />
            )}
          />
          <Route path="/product/:id" component={Edit} />
          <Route path="/product" component={AddProduct} />
          <Route
            path="/orders"
            component={() => <MyOrders totalQty={this.state.totalQty} />}
          />

          <Route path="/checkout" component={CheckOut} />
          <Route path="/order-success" component={OrderSuccess} />
          <Route
            path="/products/:category"
            component={() => (
              <StoreData
                productsData={this.state.productsData}
                totalQty={this.state.totalQty}
                onTotalQtyChange={this.handleTotalQty}
                onCategoryChange={this.handleCategory}
                categoryData={this.state.categoryData}
              />
            )}
          />
          <Route path="/login" component={Login} />
          <Route path="/logout" component={Logout} />
          <Route
            path="/products"
            component={() => <ManageProducts totalQty={this.state.totalQty} />}
          />
          <Route
            path="/"
            component={() => (
              <StoreData
                productsData={this.state.productsData}
                totalQty={this.state.totalQty}
                onTotalQtyChange={this.handleTotalQty}
                onCategoryChange={this.handleCategory}
                categoryData={this.state.categoryData}
              />
            )}
          />
        </Switch>
      </React.Fragment>
    );
  }
}

export default MainComp;
