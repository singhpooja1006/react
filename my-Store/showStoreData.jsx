import React, { Component } from "react";
import LeftPanel from "./leftPanel";
import Fashion from "./fashionSale";
import { withRouter } from "react-router-dom";
import NavBar from "./navbar";
class StoreData extends Component {
  state = {
    productsData: [],
  };
  componentDidMount() {
    let { productsData, categoryData } = { ...this.props };
    let searchCategory = "";
    if (this.props.match) {
      searchCategory = this.props.match.params.category;
    }

    this.setState({ productsData, categoryData, searchCategory });
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.match &&
      prevProps.match.params.category !== this.props.match.params.category
    ) {
      let searchCategory = this.props.match.params.category;
      this.setState({ searchCategory });
    }
  }

  addProductToCart = (product) => {
    const productsData = [...this.state.productsData];
    let prdIndex = productsData.findIndex(
      (prod) => prod.prodId === product.prodId
    );
    if (prdIndex !== -1) {
      productsData[prdIndex].qty = productsData[prdIndex].qty + 1;
    }

    this.setState({
      productsData,
    });
    this.props.onTotalQtyChange(
      productsData.reduce((acc, curr) => acc + curr.qty, 0),
      productsData
    );
  };
  handleOptionChange = (categoryData) => {
    this.props.onCategoryChange(categoryData);
  };
  handleRemove = (product) => {
    const productsData = [...this.state.productsData];
    let prdIndex = productsData.findIndex(
      (prod) => prod.prodId === product.prodId
    );
    if (prdIndex !== -1) {
      productsData[prdIndex].qty = productsData[prdIndex].qty - 1;
    }

    this.setState({
      productsData,
    });
    this.props.onTotalQtyChange(
      productsData.reduce((acc, curr) => acc + curr.qty, 0),
      productsData
    );
  };
  render() {
    return (
      <React.Fragment>
        <NavBar totalQty={this.props.totalQty} />
        <Fashion />
        <div className="row">
          <div className="col-2 ml-5 mr-1">
            <LeftPanel
              categoryData={this.props.categoryData}
              onFilter={this.handleOptionChange}
            />
          </div>
          <div className="col-lg-9 col-12 ml-5">
            {this.renderProductsData()}
          </div>
        </div>
      </React.Fragment>
    );
  }
  renderProductsData = () => {
    let { productsData, searchCategory } = this.state;
    if (searchCategory && searchCategory !== "") {
      productsData = productsData.filter(
        (prd) => prd.category === searchCategory
      );
    } else if (
      this.props.categoryData &&
      this.props.categoryData.selected !== ""
    ) {
      productsData = productsData.filter(
        (prd) => prd.category === this.props.categoryData.selected
      );
    }
    return (
      <React.Fragment>
        <div className="row">
          {productsData
            ? productsData.map((product) => this.renderProductViewData(product))
            : ""}
        </div>
      </React.Fragment>
    );
  };

  renderProductViewData = (product) => {
    return (
      <React.Fragment>
        <div className="col-md-6 col-lg-3 ml-5 mt-2">
          <div className="card">
            <img
              className="card-img-top img-fluid"
              src={product.imgLink}
              style={{
                width: "100%",
                height: "150px",
              }}
            />
            <div className="card-block">
              <div className="ml-2 h5">{product.name}</div>
              <div className="ml-2">Rs.{product.price}</div>
              <div className="text-truncate ml-2">{product.description}</div>
              {product.qty === 0 ? (
                <button
                  className="btn btn-success btn-sm btn-block"
                  onClick={() => this.addProductToCart(product)}
                >
                  Add to cart
                </button>
              ) : (
                <button
                  className="btn  btn-warning btn-sm btn-block"
                  onClick={() => this.handleRemove(product)}
                >
                  Remove from cart
                </button>
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  };
}

export default withRouter(StoreData);
