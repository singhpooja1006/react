import React, { Component, Fragment } from "react";
import config from "./config.json";
import axios from "axios";
import Fashion from "./fashionSale";
import NavBar from "./navbar";
class MyOrders extends Component {
  state = {
    ordersData: [],
  };
  async componentDidMount() {
    let apiEndPoint = config.apiEndPoint + "/orders";
    const { data: orderData } = await axios.get(apiEndPoint);
    let ordersData = [];
    for (let index = 0; index < orderData.length; index++) {
      let order = orderData[index];
      ordersData.push(order);
    }
    this.setState({
      ordersData,
    });
  }
  render() {
    const { ordersData } = this.state;
    return (
      <Fragment>
        <NavBar totalQty={this.props.totalQty} />
        <div className="container">
          <Fashion />
          <h3>List of Orders</h3>
          <div className="row bg-dark text-white">
            <div className="col-2">Name</div>
            <div className="col-2">City</div>
            <div className="col-3">Address</div>
            <div className="col-2">Amount</div>
            <div className="col-3">Items</div>
          </div>
          {ordersData.map((orders) => (
            <div className="row border-bottom">
              <div className="col-2">{orders.name}</div>
              <div className="col-2">{orders.city}</div>
              <div className="col-3 text-truncate">{orders.address}</div>
              <div className="col-2">Rs.{orders.totalPrice}.00</div>
              <div className="col-3">{orders.items}</div>
            </div>
          ))}
        </div>
      </Fragment>
    );
  }
}

export default MyOrders;
