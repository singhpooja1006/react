import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./dropdown.css";
class NavBar extends Component {
  state = { user: JSON.parse(localStorage.getItem("user")) };
  render() {
    return (
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
        <Link className="navbar-brand" to="/">
          MyStore
        </Link>
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link to="/products/Watches" className="nav-link">
              Watches
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/products/Sunglasses" className="nav-link">
              Sunglasses
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/products/Belts" className="nav-link">
              Belts
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/products/Handbags" className="nav-link">
              Handbags
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/products/Wallets" className="nav-link">
              Wallets
            </Link>
          </li>
          <form className="form-inline my-2 my-lg-0">
            <li className="navbar-item">
              <div className="dropdown">
                <Link to="/" className="nav-link">
                  Footwear&nbsp;
                  <i
                    className="fa fa-chevron-down"
                    id="onhover"
                    style={{ fontSize: "10px" }}
                  ></i>
                </Link>
                <div className="dropdown-content">
                  <div>
                    <Link to="/products/FormalShoes">Formal Shoes</Link>
                  </div>
                  <div>
                    <Link to="/products/SportShoes">Sport Shoes</Link>
                  </div>
                  <div>
                    <Link to="/products/Floaters">Floaters</Link>
                  </div>
                  <div>
                    <Link to="/products/Sandals">Sandals</Link>
                  </div>
                </div>
              </div>
            </li>
          </form>
        </ul>
        <ul className="navbar-nav ml-auto">
          {this.state.user ? (
            <React.Fragment>
              <form className="form-inline my-2 my-lg-0">
                <a className="navbar-brand">
                  <div className="dropdown">
                    <div className="dropbtn">
                      {this.state.user.email}&nbsp;
                      <i
                        className="fa fa-chevron-down"
                        id="onhover"
                        style={{ fontSize: "10px" }}
                      ></i>
                    </div>
                    <div className="dropdown-content">
                      <div>
                        <Link to="/orders">My Orders</Link>
                      </div>
                      <div>
                        <Link to="/products">Manage Products</Link>
                      </div>
                      <div>
                        <Link to="/logout">Logout</Link>
                      </div>
                    </div>
                  </div>
                </a>
              </form>
            </React.Fragment>
          ) : (
            <li className="nav-item">
              <Link to="/login" className="nav-link">
                Login
              </Link>
            </li>
          )}
          <li className="nav-item">
            <Link to="/cart" className="nav-link">
              Cart
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/" className="nav-link">
              <span className="badge badge-pill badge-success">
                {this.props.totalQty ? this.props.totalQty : 0}
              </span>
            </Link>
          </li>
        </ul>
      </nav>
    );
  }
}

export default NavBar;
