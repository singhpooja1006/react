import React, { Component, Fragment } from "react";
import NavBar from "./navbar";
class OrderSuccess extends Component {
  state = {};
  render() {
    return (
      <Fragment>
        <NavBar />
        <div className="container">
          <div className="row">
            <h3 className="col-12 text-center">Thank You!</h3>
            <div className="col-12 text-center h6">
              We received your order and will process it within next 24 hours!
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default OrderSuccess;
