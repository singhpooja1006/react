import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
import NavBar from "./navbar";
class ManageProducts extends Component {
  state = {
    searchText: "",
    productsData: [],
  };
  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));

    const { data: products } = await axios.get(
      config.apiEndPoint + "/products"
    );
    let productsData = [];
    for (let index = 0; index < products.length; index++) {
      let product = products[index];
      product.qty = 0;
      productsData.push(product);
    }
    this.setState({
      productsData,
      user,
    });
  }
  handleChange = (ele) => {
    const data = { ...this.state.searchText };
    data[ele.currentTarget.name] = ele.currentTarget.value;
    this.setState({ searchText: data.searchText });
  };
  addNewProduct = () => {
    window.location = "/product";
  };
  editProducts = (prodId) => {
    window.location = "/product/" + prodId;
  };
  handleDeleteProduct = async () => {
    const { id } = this.props.match.params;
    let apiEndPoint = config.apiEndPoint + "/product/" + id;
    await axios.delete(apiEndPoint);
    alert("Product Deleted Successfullly");
    window.location = "/products";
  };
  render() {
    return (
      <React.Fragment>
        <NavBar totalQty={this.props.totalQty} />
        <div className="container">{this.renderAllProduct()}</div>
      </React.Fragment>
    );
  }
  renderAllProduct() {
    let productsData = [...this.state.productsData];
    if (this.state.searchText !== "") {
      productsData = productsData.filter((product) =>
        product.name.startsWith(this.state.searchText)
      );
    }

    return (
      <React.Fragment>
        <div className="row ml-2 mt-2">
          <button
            className="btn btn-success"
            onClick={() => this.addNewProduct()}
          >
            Add a Product
          </button>
        </div>
        <input
          value={this.state.searchText}
          onChange={this.handleChange}
          name="searchText"
          type="text"
          className="form-control col-12 mt-2"
          placeholder="Search..."
        />
        <h6 className="mt-3 ml-2">
          Showing products 1 - {productsData.length}
        </h6>
        <div className="row border ml-2 mr-2 bg-dark text-white">
          <div className="col-1 border">#</div>
          <div className="col-3 border">Title</div>
          <div className="col-3 border">Category</div>
          <div className="col-2 border">Price</div>
          <div className="col-3 border"></div>
        </div>
        {productsData.map((product, index) => (
          <div
            className="row ml-2 mr-2 border"
            style={
              index % 2 === 0
                ? { backgroundColor: "#ebebe0" }
                : { backgroundColor: "white" }
            }
          >
            <div className="col-1 border">{index + 1}</div>
            <div className="col-3 border">{product.name}</div>
            <div className="col-3 border">{product.category}</div>
            <div className="col-2 border">{product.price}</div>
            <div
              className="col-1 text-primary"
              style={{ cursor: "pointer" }}
              onClick={() => this.editProducts(product.prodId)}
            >
              Edit
            </div>
            <div
              className="col-1 text-primary"
              style={{ cursor: "pointer" }}
              onClick={() => this.handleDeleteProduct()}
            >
              Delete
            </div>
          </div>
        ))}
      </React.Fragment>
    );
  }
}

export default ManageProducts;
