import React, { Component } from "react";
import { apiEndPoint } from "./config.json";
import http from "./services/httpService";
import { Redirect } from "react-router-dom";
import Fashion from "./fashionSale";
import NavBar from "./navbar";
class Login extends Component {
  state = {
    data: { email: "", password: "" },
    errors: {},
    checkOutFlag: false,
    totalQty: this.props.location.state
      ? this.props.location.state.totalQty
      : 0,
  };
  validate = () => {
    let errs = {};

    if (!this.state.data.email.trim()) errs.email = "Email is required";
    else if (this.state.data.email.includes("@") === false)
      errs.email = "Must be a valid email";

    if (!this.state.data.password.trim())
      errs.password = "Password is required";
    else if (this.state.data.password.trim().length < 7)
      errs.password = "Password length must be at least 7 characters long";
    return errs;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "email":
        if (!e.currentTarget.value.trim()) return "Email is required";
        else if (e.currentTarget.value.includes("@") === false)
          return "Must be a valid email";
        break;
      case "password":
        if (!e.currentTarget.value.trim()) return "Password is required";
        else if (e.currentTarget.value.trim().length < 7)
          return "Password length must be at least 7 characters long";
        break;
      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const loginData = { ...this.state.data };
    loginData[e.currentTarget.name] = e.currentTarget.value;

    this.setState({ data: loginData, errors: errors });
    console.log(loginData);
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const apiBaseURL = apiEndPoint + "/login";
      const { data: user } = await http.post(apiBaseURL, {
        email: this.state.data.email,
        password: this.state.data.password,
      });
      localStorage.setItem("user", JSON.stringify(user));
      const { state } = this.props.location;
      if (!state) {
        window.location = "/";
      } else {
        this.setState({ checkOutFlag: true });
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 500) {
        const errors = { ...this.state.errors };
        if (ex.response.data.includes("Login Failed")) {
          errors.msg = "Login Unsuccessful.";
        } else {
          errors.msg = ex.response.data;
        }
        alert("Invalid login Credentials");
        this.setState({ errors });
      }
    }
  };

  render() {
    if (this.state.checkOutFlag) {
      return (
        <Redirect
          to={{
            pathname: this.props.location.state.from,
            state: {
              productsData: this.props.location.state.productsData,
              totalQty: this.props.location.state.totalQty,
            },
          }}
        />
      );
    }
    const { data, errors } = this.state;
    return (
      <React.Fragment>
        <NavBar totalQty={this.state.totalQty} />
        <div className="container">
          <Fashion />
          <div className="row">
            <div className="col-md-6 offset-md-3">
              <div>
                <form onSubmit={this.handleSubmit}>
                  <div className="form-group">
                    <h2 className="text-center">Login</h2>
                    <label htmlFor="email">Email address</label>
                    <input
                      name="email"
                      type="text"
                      placeholder="email"
                      className="form-control"
                      value={data.email}
                      onChange={this.handleChange}
                      id="email"
                    />
                    {errors.email ? (
                      <div className="text-danger text-center">
                        {errors.email}
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input
                      name="password"
                      type="password"
                      placeholder="Password"
                      className="form-control"
                      value={data.password}
                      onChange={this.handleChange}
                      id="password"
                    />
                    {errors.password ? (
                      <div className="text-danger text-center">
                        {errors.password}
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="form-group">
                    <button type="submit" className="btn btn-primary mr-2">
                      Login
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Login;
