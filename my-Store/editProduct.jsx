import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
class Edit extends Component {
  state = {
    data: [],
    categories: [
      "Sunglasses",
      "Watches",
      "Belts",
      "Handbags",
      "Wallets",
      "Formal Shoes",
      "Sport Shoes",
      "Floaters",
      "Sandals",
    ],
  };
  async componentDidMount() {
    const { id } = this.props.match.params;
    if (id) {
      const apiEndpoint = config.apiEndPoint + "/product/" + id;
      const { data } = await axios.get(apiEndpoint);
      this.setState({ data });
    }
  }
  handleChange = (e) => {
    const formdata = { ...this.state.data };
    formdata[e.currentTarget.id] = e.currentTarget.value;
    this.setState({ data: formdata });
  };
  handleEditProduct = async () => {
    const { id } = this.props.match.params;
    let apiEndPoint = config.apiEndPoint + "/product/" + id;
    try {
      const { data: product } = await axios.put(apiEndPoint, {
        category: this.state.data.category,
        imgLink: this.state.data.imgLink,
        name: this.state.data.name,
        price: this.state.data.price,
        description: this.state.data.description,
      });
      alert("Product Modified Successfully", product);
      window.location = "/products";
    } catch (ex) {
      console.log("Error", ex);
    }
  };
  handleDeleteProduct = async () => {
    const { id } = this.props.match.params;
    let apiEndPoint = config.apiEndPoint + "/product/" + id;
    await axios.delete(apiEndPoint);
    alert("Product Deleted Successfullly");
    window.location = "/products";
  };
  render() {
    const { data, categories } = this.state;
    return (
      <div className="container">
        <div className="row">
          {data.imgLink ? (
            <div className="col-lg-4 col-12 mt-5 ml-5">
              <div className="card bg-dark text-white">
                <img
                  className="card-img-top text-center"
                  alt=""
                  src={data.imgLink}
                  style={{
                    width: "100%",
                    height: "190px",
                    objectFit: "cover",
                  }}
                />
                <div className="card-block">
                  <div className="ml-2 h5">{data.name}</div>
                  <div className="ml-2">Category : {data.category}</div>
                  <div className="ml-2">Price : Rs.{data.price}</div>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
          <div className="col-md-6 ">
            <h2>Edit Product</h2>
            <div>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input
                  name="name"
                  id="name"
                  type="text"
                  className="form-control"
                  value={data.name}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="description">Description</label>
                <input
                  name="description"
                  id="description"
                  type="text"
                  className="form-control"
                  value={data.description}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="price">Price</label>
                <input
                  value={data.price}
                  onChange={this.handleChange}
                  type="number"
                  id="price"
                  name="price"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label htmlFor="imgLink">Image</label>
                <input
                  value={data.imgLink}
                  onChange={this.handleChange}
                  type="text"
                  id="imgLink"
                  name="imgLink"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label htmlFor="category">Category</label>
                <select
                  value={data.category}
                  onChange={this.handleChange}
                  id="category"
                  name="category"
                  className="form-control"
                >
                  {categories.map((category) => (
                    <option key={category}>{category}</option>
                  ))}
                </select>
              </div>
              <button
                className="btn btn-primary"
                onClick={() => this.handleEditProduct()}
              >
                Save
              </button>
              <button
                className="btn btn-secondary ml-2"
                onClick={() => this.handleDeleteProduct()}
              >
                Delete
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Edit;
