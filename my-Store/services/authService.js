import http from "./httpService";
import { apiEndPoint } from "./config.json";
const apiBaseURL = apiEndPoint + "/login";
const tokenKey = "user";
export async function login(email, password) {
  const { data: user } = await http.post(apiBaseURL, { email, password });
  localStorage.setItem(tokenKey, user);
}
export default {
  login,
};
