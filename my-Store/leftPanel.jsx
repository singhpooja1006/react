import React, { Component } from "react";
class LeftPanel extends Component {
  filterCategory = (category) => {
    let { categoryData } = { ...this.props };
    categoryData.selected = category;

    this.props.onFilter(categoryData);
  };
  render() {
    const { categoryData } = this.props;
    return (
      <div className="container ml-5">
        <div className="row border bg-light">
          <div className="col-12 m-2" onClick={() => this.filterCategory("")}>
            All
          </div>
        </div>
        {categoryData &&
          categoryData.categories.map((category) => (
            <div
              className="row border"
              onClick={() => this.filterCategory(category)}
              style={{ cursor: "pointer" }}
            >
              <div className="col-12 m-2">{category}</div>
            </div>
          ))}
      </div>
    );
  }
}

export default LeftPanel;
