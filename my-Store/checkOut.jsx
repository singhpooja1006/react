import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
import Fashion from "./fashionSale";
import NavBar from "./navbar";
class CheckOut extends Component {
  state = {
    data: { name: "", line1: "", line2: "", city: "" },
    productsData: [],
  };
  componentDidMount() {
    this.setState({
      productsData: this.props.location.state.productsData,
    });
  }
  handleSubmit = async (e) => {
    e.preventDefault();
    let orderData = {};
    const selectedProduct = [
      ...this.state.productsData.map((prod) => ({
        prodId: prod.prodId,
        quantity: prod.qty,
      })),
    ];
    const user = JSON.parse(localStorage.getItem("user"));
    orderData.name = this.state.data.name;
    orderData.address = this.state.data.line1 + ", " + this.state.data.line2;
    orderData.city = this.state.data.city;

    orderData.items = selectedProduct.filter((prod) => prod.quantity > 0);
    orderData.totalPrice = this.state.productsData.reduce(
      (acc, curr) => acc + curr.qty * curr.price,
      0
    );
    orderData.email = user.email;
    let apiEndPoint = config.apiEndPoint + "/order";
    const { data: orderOutput } = await axios.post(apiEndPoint, orderData);
    window.location = "/order-success";
  };
  handleChange = (e) => {
    const formdata = { ...this.state.data };
    formdata[e.currentTarget.id] = e.currentTarget.value;
    this.setState({ data: formdata });
  };
  render() {
    const { productsData, data } = this.state;
    return (
      <React.Fragment>
        <NavBar totalQty={this.props.location.state.totalQty} />
        <Fashion />
        <div className="container border">
          <div className="row">
            <div className="col-12 h4 text-center">Summary of your Order</div>
            <div className="col-12 text-center">
              You order has{" "}
              {productsData.reduce((acc, curr) => acc + curr.qty, 0)} items
            </div>
          </div>
          <div className="row border bg-dark ml-1 mr-1">
            <div className="col-2"></div>
            <div className="col-3">Name</div>
            <div className="col-1"></div>
            <div className="col-3">Quantity</div>
            <div className="col-3">Value</div>
          </div>

          {productsData.map((product) =>
            product.qty > 0 ? (
              <React.Fragment>
                <div className="row border-bottom ml-1 mr-1">
                  <div className="col-2"></div>
                  <div className="col-3">{product.name}</div>
                  <div className="col-1"></div>
                  <div className="col-3">{product.qty}</div>
                  <div className="col-3">Rs.{product.price}</div>
                </div>
              </React.Fragment>
            ) : (
              ""
            )
          )}
          <div className="row ml-1 mr-1">
            <div className="col-2"></div>
            <div className="col-3 mb-3">Total</div>
            <div className="col-1"></div>
            <div className="col-3"></div>
            <div className="col-3">
              Rs.
              {productsData.reduce(
                (acc, curr) => acc + curr.qty * curr.price,
                0
              )}
              .00
            </div>
          </div>
        </div>
        <div className="container">
          <h3 className="text-center mt-3">Delivery Details</h3>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input
                value={data.name}
                onChange={this.handleChange}
                type="text"
                id="name"
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label htmlFor="line1">Address</label>
              <input
                value={data.line1}
                onChange={this.handleChange}
                type="text"
                id="line1"
                placeholder="Line1"
                className="form-control"
              />
            </div>
            <div className="form-group">
              <input
                value={data.line2}
                onChange={this.handleChange}
                type="text"
                id="line2"
                placeholder="Line2"
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label htmlFor="city">City</label>
              <input
                value={data.city}
                onChange={this.handleChange}
                type="text"
                id="city"
                className="form-control"
              />
            </div>
            <button className="btn btn-success" type="submit">
              Submit
            </button>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default CheckOut;
