import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Fashion from "./fashionSale";
import NavBar from "./navbar";
class Cart extends Component {
  state = {
    productsData: this.props.productsData ? [...this.props.productsData] : [],
    cardCheckOut: -1,
  };
  handleDecrement = (product) => {
    const productsData = [...this.state.productsData];
    let prdIndex = productsData.findIndex(
      (prod) => prod.prodId === product.prodId
    );
    if (prdIndex !== -1) {
      productsData[prdIndex].qty = productsData[prdIndex].qty - 1;
    }
    this.setState({
      productsData,
    });
    this.props.onTotalQtyChange(
      productsData.reduce((acc, curr) => acc + curr.qty, 0),
      productsData
    );
  };
  handleIecrement = (product) => {
    const productsData = [...this.state.productsData];
    let prdIndex = productsData.findIndex(
      (prod) => prod.prodId === product.prodId
    );
    if (prdIndex !== -1) {
      productsData[prdIndex].qty = productsData[prdIndex].qty + 1;
    }
    this.setState({
      productsData,
    });
    this.props.onTotalQtyChange(
      productsData.reduce((acc, curr) => acc + curr.qty, 0),
      productsData
    );
  };
  checkOutCart = () => {
    const user = localStorage.getItem("user");
    if (!user) {
      this.setState({ cardCheckOut: 0 });
    } else {
      this.setState({ cardCheckOut: 1 });
    }
  };
  render() {
    const { productsData } = this.state;
    let cardProductsData = [...productsData.filter((prd) => prd.qty > 0)];
    if (this.state.cardCheckOut === 0) {
      return (
        <Redirect
          to={{
            pathname: "/login",
            state: {
              from: "/checkout",
              productsData: cardProductsData,
              totalQty: this.props.totalQty,
            },
          }}
        />
      );
    } else if (this.state.cardCheckOut === 1) {
      return (
        <Redirect
          to={{
            pathname: "/checkout",
            state: {
              productsData: cardProductsData,
              totalQty: this.props.totalQty,
            },
          }}
        />
      );
    } else {
      return (
        <React.Fragment>
          <NavBar totalQty={this.props.totalQty} />
          <div className="container">
            <Fashion />
            <h2 className="text-center">
              You Have {this.props.totalQty} Items In Your Cart
            </h2>
            {this.props.totalQty > 0 ? (
              <React.Fragment>
                <div className="row">
                  <div className="col-10">
                    Cart Value : Rs.
                    {productsData.reduce(
                      (acc, curr) => acc + curr.qty * curr.price,
                      0
                    )}
                    .00
                  </div>
                  <div className="col-2">
                    <button
                      className="btn btn-primary float-right"
                      onClick={() => this.checkOutCart()}
                    >
                      Check Out
                    </button>
                  </div>
                </div>

                <div className="row border bg-dark text-white">
                  <div className="col-3"></div>
                  <div className="col-2">Product Details</div>
                  <div className="col-3"></div>
                  <div className="col-2 ml-5">Quantity</div>
                  <div className="col-1 text-right">Price</div>
                </div>
              </React.Fragment>
            ) : (
              ""
            )}
            {productsData.map((product) =>
              product.qty > 0 ? (
                <React.Fragment>
                  <div className="row border">
                    <div className="col-3">
                      <img
                        className="card-img-top mb-2"
                        src={product.imgLink}
                        alt=""
                        style={{
                          height: "100px",
                          width: "45%",
                          borderRadius: "15%",
                        }}
                      />
                    </div>
                    <div className="col-5 text-truncate">
                      {product.name}
                      <br />
                      {product.category}
                      <br />
                      {product.description}
                    </div>
                    <div className="col-1">
                      <button
                        className="btn btn-success"
                        onClick={() => this.handleIecrement(product)}
                      >
                        +
                      </button>
                    </div>
                    <div>{product.qty}</div>
                    <div className="col-1 ml-3">
                      <button
                        className="btn btn-warning"
                        onClick={() => this.handleDecrement(product)}
                      >
                        -
                      </button>
                    </div>
                    <div className="col-1 text-right ml-5">
                      Rs.{product.price * product.qty}
                    </div>
                  </div>
                </React.Fragment>
              ) : (
                ""
              )
            )}
          </div>
        </React.Fragment>
      );
    }
  }
}

export default Cart;
