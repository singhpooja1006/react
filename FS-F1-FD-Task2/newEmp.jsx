import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
class AddNewEmp extends Component {
  state = {
    data: {
      name: "",
      city: "",
      technologies: "",
      experience: "",
      onProject: "",
    },
    cities: ["Delhi", "Noida", "Gurgaon"],
    skills: ["React", "Angular", "Python", "Android"],
    onProjects: ["Yes", "No"],
    errors: {},
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { data } = this.state;
    data[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data, errors });
  };
  async componentDidMount() {
    const { name } = this.props.match.params;
    if (name) {
      const apiEndpoint = config.apiEndPoint + "/employees/" + name;
      const { data } = await http.get(apiEndpoint);
      this.setState({ data });
    }
  }
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "name":
        if (!e.currentTarget.value.trim()) return "Name is required";
        break;
      case "city":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select City"
        )
          return "City is required";
        break;
      case "technologies":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select Technologies"
        )
          return "Technologies is required";
        break;
      case "experience":
        if (!e.currentTarget.value.trim()) return "Experience is required";
        else if (+e.currentTarget.value <= 0)
          return "Experience is greater than 0";
        break;
      case "onProject":
        if (!e.currentTarget.value.trim()) return "onProject is required";
        break;

      default:
        break;
    }
    return "";
  };
  validate = () => {
    let errs = {};
    if (!this.state.data.name.trim()) errs.name = "Name is required";
    if (!this.state.data.city.trim() || this.state.data.city === "Select City")
      errs.city = "City is required";
    if (
      !this.state.data.technologies.trim() ||
      this.state.data.technologies === "Select Technologies"
    )
      errs.technologies = "Technologies is required";
    if (+this.state.data.experience <= 0)
      errs.experience = "Experience is greater than 0";
    if (!this.state.data.onProject.trim())
      errs.onProject = "OnProject is required";
    return errs;
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const { data } = this.state;
      const { name } = this.props.match.params;
      if (name) {
        const apiEndpoint = config.apiEndPoint + "/employees/" + name;
        await http.put(apiEndpoint, {
          name: data.name,
          city: data.city,
          technologies: data.technologies,
          experience: data.experience,
          onProject: data.onProject,
        });
      } else {
        const apiEndpoint = config.apiEndPoint + "/employees";
        await http.post(apiEndpoint, data);
      }

      window.location = "/employees";
    } catch (ex) {}
  };
  render() {
    const { data, cities, skills, onProjects, errors } = this.state;
    const { name } = this.props.match.params;
    return (
      <div className="container">
        <h2>Developer Details</h2>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              value={data.name}
              onChange={this.handleChange}
              type="text"
              id="name"
              name="name"
              placeholder="Enter Name"
              className="form-control"
              disabled={name ? true : false}
            />
            {errors.name ? (
              <div className="text-danger">{errors.name}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="cities">City</label>
            <select
              value={data.city}
              onChange={this.handleChange}
              id="city"
              name="city"
              className="browser-default custom-select mb-1"
            >
              <option>Select City</option>
              {cities.map((city) => (
                <option key={city}>{city}</option>
              ))}
            </select>
            {errors.city ? (
              <div className="text-danger">{errors.city}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="technologies">Technologies</label>
            <select
              value={data.technologies}
              onChange={this.handleChange}
              id="technologies"
              name="technologies"
              className="browser-default custom-select mb-1"
            >
              <option>Select Technologies</option>
              {skills.map((technologies) => (
                <option key={technologies}>{technologies}</option>
              ))}
            </select>
            {errors.technologies ? (
              <div className="text-danger">{errors.technologies}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="experience">Experience</label>
            <input
              value={data.experience}
              onChange={this.handleChange}
              type="number"
              id="experience"
              name="experience"
              placeholder="Enter Experience"
              className="form-control"
            />
            {errors.experience ? (
              <div className="text-danger">{errors.experience}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="gender" className="control-label col-2">
              <b>OnProject</b>
            </label>
            {onProjects.map((onProject) => (
              <div className="form-check-inline col-2">
                <input
                  value={onProject}
                  onChange={this.handleChange}
                  id="onProject"
                  name="onProject"
                  type="radio"
                  checked={data.onProject === onProject}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={onProject}>
                  {onProject}
                </label>
              </div>
            ))}
            {errors.onProject ? (
              <div className="text-danger">{errors.onProject}</div>
            ) : (
              ""
            )}
          </div>
          <button className="btn btn-primary">Submit</button>
        </form>
      </div>
    );
  }
}

export default AddNewEmp;
