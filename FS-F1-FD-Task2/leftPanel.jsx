import React, { Component } from "react";
class LeftPanel extends Component {
  state = {};
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let { cities, skills, onProjectData } = {
      ...this.props,
    };
    if (input.type === "checkbox") {
      let city = cities.find((n1) => n1.name === input.name);
      if (city) city.isSelected = input.checked;

      let technologies = skills.find((n1) => n1.name === input.name);
      if (technologies) technologies.isSelected = input.checked;
    }
    if (input.name === "onProject") {
      onProjectData.selected = input.value;
    }

    this.props.onCheckButtonEvent(cities, skills, onProjectData);
  };
  render() {
    const { cities, skills, onProjectData } = this.props;
    return (
      <React.Fragment>
        <div className="row border ">
          <div className="col-1 m-3 h6">Options</div>
        </div>
        <div className="row border ">
          <div className="col-12 m-3">
            <h6>City</h6>
            {cities.map((city) => (
              <div className="form-check mt-1">
                <input
                  value={city.name}
                  onChange={this.handleChange}
                  id={city.name}
                  name={city.name}
                  type="checkbox"
                  checked={city.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={city.name}>
                  {city.name}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>Technologies</h6>
            {skills.map((technologies) => (
              <div className="form-check mt-1">
                <input
                  value={technologies.name}
                  onChange={this.handleChange}
                  name={technologies.name}
                  id={technologies.name}
                  type="checkbox"
                  checked={technologies.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={technologies.name}>
                  {technologies.name}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>OnProject</h6>
            {onProjectData.onProjects.map((onProject) => (
              <div className="form-check mt-1">
                <input
                  value={onProject}
                  onChange={this.handleChange}
                  id="onProject"
                  name="onProject"
                  type="radio"
                  checked={onProjectData.selected === onProject ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={onProject}>
                  {onProject}
                </label>
              </div>
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPanel;
