import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
import http from "./services/httpService";
import queryString from "query-string";
import LeftPanel from "./leftPanel";

class Employees extends Component {
  state = {
    employeesData: [],
    sortBy: "",
    cities: [
      { name: "Delhi", isSelected: false },
      { name: "Noida", isSelected: false },
      { name: "Jaipur", isSelected: false },
    ],
    skills: [
      { name: "React", isSelected: false },
      { name: "Angular", isSelected: false },
      { name: "Python", isSelected: false },
      { name: "Android", isSelected: false },
    ],
    onProjectData: { onProjects: ["Yes", "No"], selected: "" },
  };
  async componentDidMount() {
    const { data: employeesData } = await axios.get(
      config.apiEndPoint + "/employees"
    );

    this.setState({
      employeesData,
    });
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      console.log(this.props.location.search);
      let apiEndpoint =
        config.apiEndPoint + "/employees" + this.props.location.search;
      console.log("URL", apiEndpoint);
      const { data: employeesData } = await axios.get(apiEndpoint);
      this.setState({
        employeesData,
      });
    }
  }
  calURL = (params, city, technologies, onProject, sortBy) => {
    let path = "/employees";

    params = this.addToParams(params, "city", city);
    params = this.addToParams(params, "technologies", technologies);
    params = this.addToParams(params, "onProject", onProject);
    params = this.addToParams(params, "sortBy", sortBy);

    this.props.history.push({
      pathname: path,
      search: params,
    });
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }

  handleSortBy = (sortBy) => {
    this.setState({ sortBy });
    let { city, technologies, onProject } = queryString.parse(
      this.props.location.search
    );
    this.calURL("", city, technologies, onProject, sortBy);
  };
  handleEditDetails = (name) => {
    window.location = "/employees/" + name;
  };
  handleDeleteDetails = async (name) => {
    const originalEmployees = this.state.employeesData;
    try {
      const apiEndpoint = config.apiEndPoint + "/employees";
      await http.delete(apiEndpoint + "/" + name);
      const { data: employeesData } = await http.get(apiEndpoint);
      this.setState({
        employeesData,
      });
    } catch (ex) {
      if (ex.response && ex.response.status !== 404) {
        this.setState({ employeesData: originalEmployees });
      }
    }
  };
  handleCheckEvent = (cities, skills, onProjectData) => {
    this.setState({ cities, skills, onProjectData });
    let city = this.buildQueryString(cities);
    let technologies = this.buildQueryString(skills);
    this.calURL(
      "",
      city,
      technologies,
      onProjectData.selected,
      this.state.sortBy
    );
  };
  buildQueryString(optionsData) {
    let filterData = optionsData.filter((n1) => n1.isSelected);
    let arrayData = filterData.map((n1) => n1.name);
    return arrayData.join(",");
  }
  render() {
    return (
      <div className="container">
        <div className="row m-1">
          <div className="col-2 mr-1">
            <LeftPanel
              cities={this.state.cities}
              skills={this.state.skills}
              onProjectData={this.state.onProjectData}
              onCheckButtonEvent={this.handleCheckEvent}
            />
          </div>
          <div className="col-9 ml-2">{this.renderEmployeesData()}</div>
        </div>
      </div>
    );
  }
  renderEmployeesData = () => {
    const { employeesData } = this.state;

    if (employeesData) {
      return (
        <React.Fragment>
          <div className="row border bg-dark text-center text-white mt-2">
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("name")}
            >
              Name
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("city")}
            >
              City
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("technologies")}
            >
              Technologies
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("experience")}
            >
              Experience
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("onProject")}
            >
              OnProject
            </div>
            <div className="col-2 border"></div>
          </div>
          {employeesData.map((employee) => (
            <div
              className="row border text-center bg-light"
              key={employee.name}
            >
              <div className="col-2 border">{employee.name}</div>
              <div className="col-2 border">{employee.city}</div>
              <div className="col-2 border">{employee.technologies}</div>
              <div className="col-2 border">{employee.experience}</div>
              <div className="col-2 border">{employee.onProject}</div>
              <div className="col-1 border">
                <i
                  className="fa fa-pencil-square-o"
                  aria-hidden="true"
                  onClick={() => this.handleEditDetails(employee.name)}
                ></i>
              </div>
              <div className="col-1 border">
                <i
                  className="fa fa-trash"
                  aria-hidden="true"
                  onClick={() => this.handleDeleteDetails(employee.name)}
                ></i>
              </div>
            </div>
          ))}
        </React.Fragment>
      );
    }
  };
}

export default Employees;
