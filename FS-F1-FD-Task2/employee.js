var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
    next();
});
const port  = 2410;
app.listen(port,()=>console.log("Listening on port : ",port))
const MongoClient = require("mongodb").MongoClient;
let ObjectId = require("mongodb").ObjectId;
//const url = "mongodb://localhost:27017";
const dbName = "empdb";




var employeesData = [{name:"Vishal", city:"Delhi",technologies:"React",experience:2,onProject:"Yes"},{name:"Amit", city:"Noida",technologies:"Angular",experience:5,onProject:"No" },
{name:"Kishore", city:"Gurgaon",technologies:"Python",experience:3,onProject:"Yes" },
{name:"Rohit", city:"Delhi",technologies:"Android",experience:1,onProject:"No" },
{name:"Preeti", city:"Delhi",technologies:"React",experience:2,onProject:"Yes" },
{name:"Neha", city:"Noida", technologies:"Python",experience:3,onProject:"No"},
{name:"Swati", city:"Gurgaon",technologies:"Angular",experience:1,onProject:"Yes" },
{name:"Meghna", city:"Delhi", technologies:"Android",experience:6,onProject:"No"},
{name:"Irfan", city:"Delhi", technologies:"React",experience:4,onProject:"No"},
{name:"Gagan", city:"Noida", technologies:"Angular",experience:3,onProject:"Yes"},
{name:"John", city:"Gurgaon", technologies:"Python",experience:5,onProject:"No"},
{name:"Gurmeet", city:"Noida", technologies:"Android",experience:5,onProject:"Yes"},
{name:"Aarti", city:"Delhi",technologies:"React",experience:1,onProject:"Yes" },
{name:"Sandeep", city:"Noida",technologies:"Angular",experience:2,onProject:"No" },
{name:"Suman", city:"Gurgaon",technologies:"Python",experience:8,onProject:"Yes"},
{name:"Piyush", city:"Delhi", technologies:"Android",experience:4,onProject:"No"},
{name:"Payal", city:"Noida",technologies:"React",experience:3,onProject:"Yes" },
{name:"Richa", city:"Gurgaon", technologies:"Angular",experience:1,onProject:"Yes"},
{name:"Vivek", city:"Delhi",technologies:"React",experience:2,onProject:"Yes"},
{name:"Aayuh", city:"Noida",technologies:"Angular",experience:5,onProject:"No" },
{name:"Priya", city:"Gurgaon",technologies:"Python",experience:3,onProject:"Yes" },
{name:"Reema", city:"Delhi",technologies:"Android",experience:1,onProject:"No" },
{name:"Punita", city:"Delhi",technologies:"React",experience:2,onProject:"Yes" },
{name:"Nirmala", city:"Noida", technologies:"Python",experience:3,onProject:"No"},
{name:"Sushma", city:"Gurgaon",technologies:"Angular",experience:1,onProject:"Yes" },
{name:"Meena", city:"Delhi", technologies:"Android",experience:6,onProject:"No"},
{name:"Iran", city:"Delhi", technologies:"React",experience:4,onProject:"No"},
{name:"Gautam", city:"Noida", technologies:"Angular",experience:3,onProject:"Yes"},
{name:"Jack", city:"Gurgaon", technologies:"Python",experience:5,onProject:"No"},
{name:"Girdhar", city:"Noida", technologies:"Android",experience:5,onProject:"Yes"},
{name:"Anjali", city:"Delhi",technologies:"React",experience:1,onProject:"Yes" },
{name:"Seeva", city:"Noida",technologies:"Angular",experience:2,onProject:"No" },
{name:"Supriya", city:"Gurgaon",technologies:"Python",experience:8,onProject:"Yes"},
{name:"Parveen", city:"Delhi", technologies:"Android",experience:4,onProject:"No"},
{name:"Neeraj", city:"Noida",technologies:"React",experience:3,onProject:"Yes" }];
		
/*app.post("/employees",function(req,res) { 
    client.connect(function(err,client){
    console.log("Post : /employees");
    const db = client.db(dbName);
    db.collection("employees").insertMany(employeesData , function(err,result){
        console.log(result.insertedCount);
        res.send(result);
    });
  });
});*/

const client = new MongoClient("mongodb+srv://mongodb:mongodb@cluster0.l92tx.mongodb.net/empdb?retryWrites=true&w=majority");
app.post("/employees",function(req,res) { 
    let obj = {...req.body};
    client.connect(function(err,client){
        console.log("Post : /employees",obj);
    const db = client.db(dbName);
    db.collection("employees").insertOne(obj , function(err,result){
        console.log(result);
        res.send(result.ops[0]);
    });
  });
});

app.get("/employees",function(req,res) { 
	let {sortBy,city,technologies,onProject} = req.query;
	let employeeList = [];
   client.connect(function(err,client){
        console.log("Get: /employees");
		let filterCondition = {};
		let sorttingBy= {};
		if(city){
			let cityList = city.split(",");
			filterCondition.city = { $in : cityList};
		}
		if(technologies){
			let technologieList = technologies.split(",");
			filterCondition.technologies = { $in : technologieList};
		}
		if(onProject){
			
			filterCondition.onProject = { $in : [onProject]};
		}
		 if(sortBy === "name"){
			sorttingBy.name = 1;
		}
		else if(sortBy === "city"){
			sorttingBy.city = 1;
		}
		else if(sortBy === "technologies"){
			sorttingBy.technologies = 1;
		}
		else if(sortBy === "experience"){
			sorttingBy.experience = 1;
		}
		else if(sortBy === "onProject"){
			sorttingBy.onProject = 1;
		}
   const db = client.db(dbName);
		db.collection("employees").find(filterCondition).sort(sorttingBy).toArray(function(
			err,result
		){
        console.log(result);
        res.send(result);
    });
  });
});

app.delete("/employees/:name",function(req,res) { 
    let name = req.params.name;
    client.connect(function(err,client){
        console.log("Delete : /employees/:name",name);
    const db = client.db(dbName);
    db.collection("employees").removeOne({name: name}, function(
        err,result
    ){
        console.log(result);
        res.send(result);
    });
  });
});
app.get("/employees/:name",function(req,res) { 
    let name = req.params.name;
    client.connect(function(err,client){
        console.log("Get by name: /employees/:name",name);
    const db = client.db(dbName);
    db.collection("employees").findOne({name:name}, function(
        err,result
    ){
        console.log(result);
        res.send(result);
    });
  });
});
app.put("/employees/:name",function(req,res) { 
    let obj = {...req.body};
    let name = req.params.name;
    client.connect(function(err,client){
        console.log("Put : /employees/:name",name,obj);
    const db = client.db(dbName);
    db.collection("employees").updateOne({name: name},{$set:obj},function(
        err,result
    ){
        console.log(err);
        res.send(result);
    });
  });
});
