import React, { Component } from "react";
import { Switch, Route , Redirect} from "react-router-dom";
import "./App.css";
import NavBar from "./components/navBar";
import Employees from "./components/employees";
import AddNewEmp from "./components/newEmp";

class App extends Component {
  state = {};
  render() {
    return (
     <React.Fragment>
        <NavBar />
        <Switch>
          <Route path="/new" component={AddNewEmp} />
          <Route path="/employees/:name" component={AddNewEmp} />
          <Route path="/employees" component={Employees} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;

