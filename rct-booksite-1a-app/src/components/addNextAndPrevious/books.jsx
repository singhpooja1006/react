import React, { Component } from "react";
import axios from "axios";
import config from "./config.json";
import queryString from "query-string";
class BooksData extends Component {
  state = {
    booksData: [],
    pageInfo: {},
  };
  async componentDidMount() {
    const { data: bookDataList } = await axios.get(
      config.apiEndPoint + "/books"
    );
    this.setState({
      booksData: bookDataList.data,
      pageInfo: bookDataList.pageInfo,
    });
    const genre = this.props.match.params.genre;
    const { newarrival } = queryString.parse(this.props.location.search);

    if (genre) {
      const { data: bookDataList } = await axios.get(
        config.apiEndPoint + "/books/" + genre
      );
      this.setState({
        booksData: bookDataList.data,
        pageInfo: bookDataList.pageInfo,
      });
    } else if (newarrival) {
      const { data: bookDataList } = await axios.get(
        config.apiEndPoint + "/books?newarrival=" + newarrival
      );
      this.setState({
        booksData: bookDataList.data,
        pageInfo: bookDataList.pageInfo,
      });
    }
  }
  async getPageData(genre, newarrival, page) {
    const { data: bookDataList } = await axios.get(
      config.apiEndPoint + "/books?page=" + page
    );
    this.setState({
      booksData: bookDataList.data,
      pageInfo: bookDataList.pageInfo,
    });
    if (genre) {
      const { data: bookDataList } = await axios.get(
        config.apiEndPoint + "/books/" + genre + "?page=" + page
      );
      this.setState({
        booksData: bookDataList.data,
        pageInfo: bookDataList.pageInfo,
      });
    } else if (newarrival) {
      const { data: bookDataList } = await axios.get(
        config.apiEndPoint + "/books?newarrival=" + newarrival + "&page=" + page
      );
      this.setState({
        booksData: bookDataList.data,
        pageInfo: bookDataList.pageInfo,
      });
    }
  }
  pageNavigate = (value) => {
    let { newarrival, page } = queryString.parse(this.props.location.search);
    const genre = this.props.match.params.genre;
    let currPage = page ? +page : 1;
    currPage = currPage + value;
    this.calURL("", genre, newarrival, currPage);
    this.getPageData(genre, newarrival, currPage);
  };

  calURL = (params, genre, newarrival, page) => {
    let path = "/books";
    if (genre) {
      path = path + "/" + genre;
    }
    params = this.addToParams(params, "newarrival", newarrival);
    params = this.addToParams(params, "page", page);
    this.props.history.push({
      pathname: path,
      search: params,
    });
  };

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }

  render() {
    const { booksData, pageInfo } = { ...this.state };
    let { page } = queryString.parse(this.props.location.search);
    page = page ? +page : 1;
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-2">Left panel</div>
          <div className="col-10 mt-2">
            <div className="row">
              {pageInfo.pageNumber === 1
                ? 1
                : pageInfo.numOfItems * pageInfo.pageNumber -
                  pageInfo.numOfItems +
                  1}{" "}
              to{" "}
              {pageInfo.numOfItems * pageInfo.pageNumber <
              pageInfo.totalItemCount
                ? pageInfo.numOfItems * pageInfo.pageNumber
                : pageInfo.totalItemCount}{" "}
              of {pageInfo.totalItemCount}
            </div>
            <div className="row mt-2 bg-info">
              <div className="col-4 text-center border">Title</div>
              <div className="col-3 text-center border">Author</div>
              <div className="col-1 text-center border">Language</div>
              <div className="col-2 text-center border">Genre</div>
              <div className="col-1 border">Price</div>
              <div className="col-1 text-left border">Bestseller</div>
            </div>
            {booksData.map((book) => (
              <div className="row border text-center">
                <div className="col-4 border ">{book.name}</div>
                <div className="col-3 border">{book.author}</div>
                <div className="col-1 border">{book.language}</div>
                <div className="col-2 border">{book.genre}</div>
                <div className="col-1 border">{book.price}</div>
                <div className="col-1 border">{book.bestseller}</div>
              </div>
            ))}
            <div className="row m-1">
              <div>
                {page > 1 ? (
                  <button
                    className="btn btn-primary m-1"
                    onClick={() => this.pageNavigate(-1)}
                  >
                    Previous
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div>
                {page < pageInfo.numberOfPages ? (
                  <button
                    className="btn btn-primary m-1"
                    onClick={() => this.pageNavigate(1)}
                  >
                    Next
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BooksData;
