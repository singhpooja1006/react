import React, { Component } from "react";
import axios from "axios";
import config from "./config.json";
import queryString from "query-string";
class BooksData extends Component {
  state = {
    booksData: [],
  };
  async componentDidMount() {
    const { data: bookDataList } = await axios.get(
      config.apiEndPoint + "/books"
    );
    this.setState({ booksData: bookDataList.data });
    const genre = this.props.match.params.genre;
    const { newarrival } = queryString.parse(this.props.location.search);

    if (genre) {
      const { data: bookDataList } = await axios.get(
        config.apiEndPoint + "/books/" + genre
      );
      this.setState({ booksData: bookDataList.data });
    } else if (newarrival) {
      const { data: bookDataList } = await axios.get(
        config.apiEndPoint + "/books?newarrival=" + newarrival
      );
      this.setState({ booksData: bookDataList.data });
    }
  }
  render() {
    let booksData = [...this.state.booksData];

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-2">Left panel</div>
          <div className="col-10 mt-2">
            <div className="row  bg-info">
              <div className="col-4 text-center border">Title</div>
              <div className="col-3 text-center border">Author</div>
              <div className="col-1 text-center border">Language</div>
              <div className="col-2 text-center border">Genre</div>
              <div className="col-1 border">Price</div>
              <div className="col-1 text-left border">Bestseller</div>
            </div>
            {booksData.map((book) => (
              <div className="row border text-center">
                <div className="col-4 border ">{book.name}</div>
                <div className="col-3 border">{book.author}</div>
                <div className="col-1 border">{book.language}</div>
                <div className="col-2 border">{book.genre}</div>
                <div className="col-1 border">{book.price}</div>
                <div className="col-1 border">{book.bestseller}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default BooksData;
