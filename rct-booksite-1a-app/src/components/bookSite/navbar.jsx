import React, { Component } from "react";
import { Link } from "react-router-dom";
import { NavDropdown } from "react-bootstrap";
class NavBar extends Component {
  state = {};
  render() {
    return (
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
        <a className="navbar-brand" href="/books">
          BookSite
        </a>
        <div className="" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <a href="/books?newarrival=yes" className="nav-link">
                New Arrival
              </a>
            </li>
            <li className="nav-item">
              <NavDropdown title="Book by Genre" id="basic-nav-dropdown">
                <NavDropdown.Item href="/books/fiction">
                  Fiction
                </NavDropdown.Item>
                <NavDropdown.Item href="/books/children">
                  Children
                </NavDropdown.Item>
                <NavDropdown.Item href="/books/mystery">
                  Mystery
                </NavDropdown.Item>
                <NavDropdown.Item href="/books/management">
                  Management
                </NavDropdown.Item>
                <NavDropdown.Item href="/books/self help">
                  Self Help
                </NavDropdown.Item>
              </NavDropdown>
            </li>
            <li className="nav-item">
              <li className="nav-item">
                <a href="/books" className="nav-link">
                  All Books
                </a>
              </li>
            </li>
          </ul>
        </div>
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/">
              LOGIN
            </Link>
          </li>
        </ul>
      </nav>
    );
  }
}
export default NavBar;
