import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import NavBar from "./navbar";
import BooksData from "./books";
class MainComponent extends Component {
  state = {};
  render() {
    return (
      <div>
        <NavBar />
        <Switch>
          <Route path="/books/:genre" component={BooksData} />
          <Route path="/books" component={BooksData} />
          <Redirect to="/books" />
        </Switch>
      </div>
    );
  }
}

export default MainComponent;
