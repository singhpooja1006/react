import React, { Component } from "react";
import axios from "axios";
import config from "./config.json";
import queryString from "query-string";
import LeftPanelComponent from "./leftPanel";
class BooksData extends Component {
  state = {
    booksData: [],
    pageInfo: {},
    bestSellerOptions: { options: [], display: "Bestseller", selected: "" },
    languageOptions: { options: [], display: "Language", selected: "" },
  };
  async componentDidMount() {
    const { data: bookDataList } = await axios.get(
      config.apiEndPoint + "/books"
    );
    const { bestSellerOptions, languageOptions } = { ...this.state };
    let bestSeller = [...bookDataList.refineOptions.bestseller];
    bestSeller.forEach((element) => {
      element.isSelected = false;
    });
    let language = [...bookDataList.refineOptions.language];
    language.forEach((element) => {
      element.isSelected = false;
    });
    bestSellerOptions.options = bestSeller;
    languageOptions.options = language;
    this.setState({
      booksData: bookDataList.data,
      pageInfo: bookDataList.pageInfo,
    });
    const genre = this.props.match.params.genre;
    const { newarrival } = queryString.parse(this.props.location.search);

    if (genre) {
      const { data: bookDataList } = await axios.get(
        config.apiEndPoint + "/books/" + genre
      );
      this.setState({
        booksData: bookDataList.data,
        pageInfo: bookDataList.pageInfo,
      });
      bestSeller = [...bookDataList.refineOptions.bestseller];
      bestSeller.forEach((element) => {
        element.isSelected = false;
      });
      language = [...bookDataList.refineOptions.language];
      language.forEach((element) => {
        element.isSelected = false;
      });
      bestSellerOptions.options = bestSeller;
      languageOptions.options = language;
    } else if (newarrival) {
      const { data: bookDataList } = await axios.get(
        config.apiEndPoint + "/books?newarrival=" + newarrival
      );
      this.setState({
        booksData: bookDataList.data,
        pageInfo: bookDataList.pageInfo,
      });
      bestSeller = [...bookDataList.refineOptions.bestseller];
      bestSeller.forEach((element) => {
        element.isSelected = false;
      });
      language = [...bookDataList.refineOptions.language];
      language.forEach((element) => {
        element.isSelected = false;
      });
      bestSellerOptions.options = bestSeller;
      languageOptions.options = language;
    }
    this.setState({
      bestSellerOptions,
      languageOptions,
    });
  }
  async getPageData(
    selectedBestSeller,
    selectedLanguage,
    genre,
    newarrival,
    page
  ) {
    let url = config.apiEndPoint + "/books";
    if ((selectedBestSeller !== "") & (selectedLanguage !== "")) {
      url =
        url +
        "?language=" +
        selectedLanguage +
        "&bestseller=" +
        selectedBestSeller +
        "&page=" +
        page;
    } else if (selectedLanguage !== "") {
      url = url + "?language=" + selectedLanguage + "&page=" + page;
    } else if (selectedBestSeller !== "") {
      url = url + "?bestseller=" + selectedBestSeller + "&page=" + page;
    } else {
      url = url + "?page=" + page;
    }

    const { data: bookDataList } = await axios.get(url);
    const { bestSellerOptions, languageOptions } = { ...this.state };
    let bestSeller = [...bookDataList.refineOptions.bestseller];
    let language = [...bookDataList.refineOptions.language];
    if (selectedBestSeller) {
      bestSeller = bestSeller.filter(
        (element) => selectedBestSeller === element.refineValue
      );
      bestSeller.forEach((element) => {
        element.isSelected = true;
      });
    } else {
      bestSeller.forEach((element) => {
        element.isSelected = false;
      });
    }
    if (selectedLanguage) {
      language = language.filter(
        (element) => selectedLanguage === element.refineValue
      );
      language.forEach((element) => {
        element.isSelected = true;
      });
    } else {
      language.forEach((element) => {
        element.isSelected = false;
      });
    }
    bestSellerOptions.options = bestSeller;
    bestSellerOptions.selected = selectedBestSeller;
    languageOptions.options = language;
    languageOptions.selected = selectedLanguage;
    this.setState({
      booksData: bookDataList.data,
      pageInfo: bookDataList.pageInfo,
      bestSellerOptions,
      languageOptions,
    });

    if (genre) {
      const { data: bookDataList } = await axios.get(
        config.apiEndPoint + "/books/" + genre + "?page=" + page
      );
      this.setState({
        booksData: bookDataList.data,
        pageInfo: bookDataList.pageInfo,
      });
    } else if (newarrival) {
      const { data: bookDataList } = await axios.get(
        config.apiEndPoint + "/books?newarrival=" + newarrival + "&page=" + page
      );
      this.setState({
        booksData: bookDataList.data,
        pageInfo: bookDataList.pageInfo,
      });
    }
  }

  async getCheckBoxData(
    selectedBestSeller,
    selectedLanguage,
    genre,
    newarrival,
    page
  ) {
    let url = config.apiEndPoint + "/books";
    if ((selectedBestSeller !== "") & (selectedLanguage !== "")) {
      url =
        url +
        "?language=" +
        selectedLanguage +
        "&bestseller=" +
        selectedBestSeller;
    } else if (selectedLanguage !== "") {
      url = url + "?language=" + selectedLanguage;
    } else if (selectedBestSeller !== "") {
      url = url + "?bestseller=" + selectedBestSeller;
    }

    const { data: bookDataList } = await axios.get(url);
    const { bestSellerOptions, languageOptions } = { ...this.state };

    let bestSeller = [...bookDataList.refineOptions.bestseller];
    if (selectedBestSeller) {
      bestSeller = bestSeller.filter(
        (element) => selectedBestSeller === element.refineValue
      );
      bestSeller.forEach((element) => {
        element.isSelected = true;
      });
    } else {
      bestSeller.forEach((element) => {
        element.isSelected = false;
      });
    }
    let language = [...bookDataList.refineOptions.language];
    if (selectedLanguage) {
      language = language.filter(
        (element) => selectedLanguage === element.refineValue
      );
      language.forEach((element) => {
        element.isSelected = true;
      });
    } else {
      language.forEach((element) => {
        element.isSelected = false;
      });
    }
    bestSellerOptions.options = bestSeller;
    languageOptions.options = language;
    languageOptions.selected = selectedLanguage;
    this.setState({
      booksData: bookDataList.data,
      pageInfo: bookDataList.pageInfo,
      bestSellerOptions,
      languageOptions,
    });
  }

  handleOptionChange = (bestSellerOptions, languageOptions) => {
    let selectedBestSeller = bestSellerOptions.options
      .filter((option) => option.isSelected)
      .map((option) => option.refineValue)
      .join(",");
    let selectedLanguage = languageOptions.options
      .filter((option) => option.isSelected)
      .map((option) => option.refineValue)
      .join(",");
    if (!selectedLanguage) {
      languageOptions.selected = "";
    }
    if (!selectedBestSeller) {
      bestSellerOptions.selected = "";
    }

    const genre = this.props.match.params.genre;
    const { newarrival } = queryString.parse(this.props.location.search);
    this.calURL(
      "",
      bestSellerOptions.selected,
      languageOptions.selected,
      genre,
      newarrival,
      ""
    );
    this.getCheckBoxData(
      bestSellerOptions.selected,
      languageOptions.selected,
      genre,
      newarrival,
      ""
    );
  };

  pageNavigate = (value) => {
    const { pageInfo } = { ...this.state };
    let { newarrival, page } = queryString.parse(this.props.location.search);
    let selectedBestSeller = this.state.bestSellerOptions.selected;
    let selectedLanguage = this.state.languageOptions.selected;
    const genre = this.props.match.params.genre;
    let currPage = pageInfo.pageNumber + value;
    this.calURL(
      "",
      selectedBestSeller,
      selectedLanguage,
      genre,
      newarrival,
      currPage
    );
    this.getPageData(
      selectedBestSeller,
      selectedLanguage,
      genre,
      newarrival,
      currPage
    );
  };

  calURL = (
    params,
    selectedBestSeller,
    selectedLanguage,
    genre,
    newarrival,
    page
  ) => {
    let path = "/books";
    if (genre) {
      path = path + "/" + genre;
    }
    params = this.addToParams(params, "language", selectedLanguage);
    params = this.addToParams(params, "bestseller", selectedBestSeller);
    params = this.addToParams(params, "newarrival", newarrival);
    params = this.addToParams(params, "page", page);

    this.props.history.push({
      pathname: path,
      search: params,
    });
  };

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }

  render() {
    const { booksData, pageInfo } = { ...this.state };
    let { page } = queryString.parse(this.props.location.search);
    page = pageInfo.pageNumber;
    const { bestSellerOptions, languageOptions } = { ...this.state };

    return (
      <div className="container-fluid">
        <div className="row m-1">
          <div className="col-2 mr-1">
            <LeftPanelComponent
              bestSellerOptions={bestSellerOptions}
              languageOptions={languageOptions}
              onCheckboxEven={this.handleOptionChange}
            />
          </div>
          <div className="col-9 ml-2">
            <div className="row">
              {pageInfo.pageNumber === 1
                ? 1
                : pageInfo.numOfItems * pageInfo.pageNumber -
                  pageInfo.numOfItems +
                  1}{" "}
              to{" "}
              {pageInfo.numOfItems * pageInfo.pageNumber <
              pageInfo.totalItemCount
                ? pageInfo.numOfItems * pageInfo.pageNumber
                : pageInfo.totalItemCount}{" "}
              of {pageInfo.totalItemCount}
            </div>
            <div className="row mt-2 bg-info text-wrap text-center">
              <div className="col-3  border">Title</div>
              <div className="col-3 border">Author</div>
              <div className="col-2 border">Language</div>
              <div className="col-2 border">Genre</div>
              <div className="col-1 border">Price</div>
              <div className="col-1 border">Bestseller</div>
            </div>
            {booksData.map((book) => (
              <div className="row border text-center text-wrap">
                <div className="col-3 border ">{book.name}</div>
                <div className="col-3 border">{book.author}</div>
                <div className="col-2 border">{book.language}</div>
                <div className="col-2 border">{book.genre}</div>
                <div className="col-1 border">{book.price}</div>
                <div className="col-1 border">{book.bestseller}</div>
              </div>
            ))}
            <div className="row m-1">
              <div>
                {page > 1 ? (
                  <button
                    className="btn btn-primary m-1"
                    onClick={() => this.pageNavigate(-1)}
                  >
                    Previous
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div>
                {page < pageInfo.numberOfPages ? (
                  <button
                    className="btn btn-primary m-1"
                    onClick={() => this.pageNavigate(1)}
                  >
                    Next
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BooksData;
