import React, { Component } from "react";
class LeftPanelComponent extends Component {
  state = {};

  handleChange = (e) => {
    const { currentTarget: input } = e;
    const { bestSellerOptions, languageOptions } = { ...this.props };
    let bestSeller = bestSellerOptions.options.find(
      (option) => option.refineValue === input.name
    );
    if (bestSeller) {
      bestSeller.isSelected = input.checked;
      bestSellerOptions.selected = bestSeller.refineValue;
    }
    let language = languageOptions.options.find(
      (option) => option.refineValue === input.name
    );
    if (language) {
      language.isSelected = input.checked;
      languageOptions.selected = language.refineValue;
    }
    this.props.onCheckboxEven(bestSellerOptions, languageOptions);
  };

  render() {
    const { bestSellerOptions, languageOptions } = { ...this.props };
    return (
      <React.Fragment>
        <div className="row border ">
          <div class="col-4 m-3 h6">Options</div>
        </div>
        <div className="row border ">
          <div class="col-4 m-3">
            <h6>{bestSellerOptions.display}</h6>
            {bestSellerOptions.options.map((option) => (
              <div className="form-check mt-1">
                <input
                  value={option.refineValue}
                  onChange={this.handleChange}
                  id={option.refineValue}
                  name={option.refineValue}
                  type="checkbox"
                  checked={option.isSelected}
                  className="form-check-input"
                />
                <label
                  className="form-check-label"
                  htmlFor={option.refineValue}
                >
                  {option.refineValue}({option.totalNum})
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border ">
          <div class="col-4 m-3">
            <h6>{languageOptions.display}</h6>
            {languageOptions.options.map((option) => (
              <div className="form-check mt-1">
                <input
                  value={option.refineValue}
                  onChange={this.handleChange}
                  id={option.refineValue}
                  name={option.refineValue}
                  type="checkbox"
                  checked={option.isSelected}
                  className="form-check-input"
                />
                <label
                  className="form-check-label"
                  htmlFor={option.refineValue}
                >
                  {option.refineValue}({option.totalNum})
                </label>
              </div>
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPanelComponent;
