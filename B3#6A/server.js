var express = require("express");
var app = express();
var jwt = require('jsonwebtoken');
var cookieParser = require('cookie-parser')
const jwtKey = "my_secret_key";
const jwtExpirySeconds = 300;
app.use(express.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
  res.header("")
  res.header("Access-Control-Allow-Credentials",true);
  next();
});

const port  = 2410;
app.use(cookieParser());
app.listen(port,()=>console.log("Listening on port : ",port))

userDetails = [{ email: "marry@gmail.com",fname:"Marry",lname:"Sikhawat",gender :"Female",status:"No",mobile:"9000563213"},
              { email: "test@test.com",fname:"Test",lname:"Dev",gender :"Female",status:"No",mobile:"9034567898"},
              { email: "jack@gmail.com",fname:"Jack",lname:"Singh",gender :"Male",status:"Yes",mobile:"9834597898"},
              { email: "steave@gmail.com",fname:"Steave",lname:"Singh",gender :"Male",status:"No",mobile:"9098677898"}];

app.post("/user", function (req, res) {
    var email = req.body.email;
          
    var user = userDetails.find(function (item) {
        return item.email === email;
    });
    if(user){
        const token = jwt.sign({user},jwtKey,{
        algorithm : "HS256",
        expiresIn : jwtExpirySeconds
    });
        res.cookie("token",token,{maxAge: jwtExpirySeconds * 1000});
        res.json({token: token});
    }else{
        return res.status(404).send("Invalide Email");
    }
});
app.get("/user",(req,res)=> {
    console.log(req.headers)
    var token = req.headers.authorization;
    console.log(token);
    if(token === undefined || token === null || token === 'null') res.status(401).end();
    else {
      const decrypt = jwt.verify(token,jwtKey);
    console.log(decrypt);
    res.send(decrypt)
    }
    
  });

  app.get("/userDetails",function(req,res){
    var token = req.headers.authorization;
    if(token === undefined || token === null || token === 'null') res.status(401).end();
    else {
      const decrypt = jwt.verify(token,jwtKey);
      console.log(decrypt.user.email)
      let user = userDetails.find((obj)=> obj.email === decrypt.user.email);
      res.send(user); 
    }
  })
  
  app.put("/userEditDetails",function(req,res) { 
    console.log("in get request for /User: ");
    var token = req.headers.authorization;
    if(token === undefined || token === null || token === 'null') res.status(401).end();
    else {
      const decrypt = jwt.verify(token,jwtKey);
      let  index = userDetails.findIndex((obj)=> obj.email === decrypt.user.email);
      if(index !== -1){
        userDetails[index] = req.body;
        res.send("User Updated Successfully");
      }else{
        res.status(401).end()
      }
      
    }
  })