import React, { Component } from "react";
import "./dropDown.css";
import config from "./config.json";
import axios from "axios";
import { connect } from "react-redux";
import ConstProfile from "./constProfile";
import ConstPage from "./constPage";
class MyProfile extends Component {
  state = {};

  async componentDidMount() {
    let token = localStorage.getItem("token");
    let apiEndpoint = config.userApi + "userDetails";
    let user;
    await axios
      .get(apiEndpoint, {
        headers: { Authorization: token },
      })
      .then(function (result) {
        user = result.data;
      });
    this.setState({
      user,
    });
  }

  handleChange = (e) => {
    const data = { ...this.state.user };
    data[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ user: data });
  };

  handleSubmit = async () => {
    let token = localStorage.getItem("token");
    let apiEndPoint = config.userApi + "userEditDetails";
    try {
      const { data: user } = await axios.put(
        apiEndPoint,
        {
          fname: this.state.user.fname,
          lname: this.state.user.lname,
          email: this.state.user.email,
          gender: this.state.user.gender,
          status: this.state.user.status,
          mobile: this.state.user.mobile,
        },
        {
          headers: { Authorization: token },
        }
      );
    } catch (ex) {
      console.log("Error", ex);
    }
  };

  render() {
    return (
      <React.Fragment>
        <ConstProfile user={this.state.user} />

        <div className="row" style={{ backgroundColor: "lightgrey" }}>
          <div className="container mt-5" style={{ backgroundColor: "white" }}>
            <div className="row ml-3 mt-2">
              <h5>Edit Profile</h5>
            </div>
            <form>
              <div className="row mb-1">
                <label className="col-4 ml-3">First Name</label>
                <div className="col-6">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="First name"
                    name="fname"
                    id="fname"
                    value={this.state.user ? this.state.user.fname : ""}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div className="row mb-1">
                <label className="col-4 ml-3">Last Name</label>
                <div className="col-6">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Last name"
                    name="lname"
                    id="lname"
                    value={this.state.user ? this.state.user.lname : ""}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div className="row mb-1">
                <label className="col-4 ml-3">Email</label>
                <div className="col-6">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Email"
                    name="email"
                    id="email"
                    value={this.state.user ? this.state.user.email : ""}
                  />
                </div>
              </div>
              <div className="row mt-5">
                <label className="col-4 ml-3">Married?</label>
              </div>
              <div className="form-check form-check-inline col-4 ml-3">
                <input
                  className="form-check-input"
                  type="radio"
                  value="Yes"
                  onChange={this.handleChange}
                  id="yes"
                  name="status"
                  checked={this.state.user && this.state.user.status === "Yes"}
                />
                <label className="form-check-label" for="Yes">
                  Yes
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  value="No"
                  id="no"
                  name="status"
                  checked={this.state.user && this.state.user.status === "No"}
                  onChange={this.handleChange}
                />
                <label className="form-check-label" for="No">
                  No
                </label>
              </div>
              <div className="row mt-5">
                <label className="col-4 ml-3">Gender?</label>
              </div>
              <div className="form-check form-check-inline col-4 ml-3">
                <input
                  className="form-check-input"
                  type="radio"
                  value="Female"
                  onChange={this.handleChange}
                  id="female"
                  name="gender"
                  checked={
                    this.state.user && this.state.user.gender === "Female"
                  }
                />
                <label className="form-check-label" for="Female">
                  Female
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  value="Male"
                  id="male"
                  name="gender"
                  checked={this.state.user && this.state.user.gender === "Male"}
                  onChange={this.handleChange}
                />
                <label className="form-check-label" for="Male">
                  Male
                </label>
              </div>

              <div className="row ml-3 mt-2 mb-2">
                <button className="btn btn-primary" onClick={this.handleSubmit}>
                  UPDATE
                </button>
              </div>
            </form>
          </div>
        </div>
        <ConstPage />
      </React.Fragment>
    );
  }
}

export default connect(null, null)(MyProfile);
