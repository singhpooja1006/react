import React, { Component } from "react";
class SlideImage extends Component {
  state = {};
  render() {
    return (
      <div className="row">
        <div className="col-2"></div>
        <div className="col-8">
          <div
            id="carouselExampleIndicators"
            className="carousel slide"
            data-ride="carousel"
          >
            <ol className="carousel-indicators">
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="0"
                className="active"
              ></li>
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="1"
              ></li>
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="2"
              ></li>
            </ol>
            <div className="row text-center m-1 carousel-inner">
              <div className="carousel-item active col-12 text-center">
                <img
                  src="https://i.ibb.co/ZGsJ3dh/jio-mami-21st-mumbai-film-festival-with-star-2019-02-09-2019-10-58-45-992.png"
                  className="d-block w-100"
                  alt="jio-mami-21st-mumbai-film-festival-with-star-2019-02-09-2019-10-58-45-992"
                />
              </div>
              <div className="carousel-item">
                <img
                  src="https://i.ibb.co/wRr7W1P/hustlers-01-10-2019-05-09-55-486.png"
                  className="d-block w-100"
                  alt="hustlers-01-10-2019-05-09-55-486"
                />
              </div>
              <div className="carousel-item">
                <img
                  src="https://i.ibb.co/qFWPRpF/laal-kaptaan-16-10-2019-12-48-06-721.jpg"
                  className="d-block w-100"
                  alt="laal-kaptaan-16-10-2019-12-48-06-721"
                />
              </div>
            </div>
            <a
              className="carousel-control-prev"
              href="#carouselExampleIndicators"
              role="button"
              data-slide="prev"
            >
              <span
                className="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span className="sr-only">Previous</span>
            </a>
            <a
              className="carousel-control-next"
              href="#carouselExampleIndicators"
              role="button"
              data-slide="next"
            >
              <span
                className="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span className="sr-only">Next</span>
            </a>
          </div>
        </div>
        <div className="col-2"></div>
      </div>
    );
  }
}

export default SlideImage;
