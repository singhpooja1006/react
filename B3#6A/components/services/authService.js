
const tokenKey = "token";
const logout = () => {
  localStorage.removeItem(tokenKey);
};

export default {
  logout
};
