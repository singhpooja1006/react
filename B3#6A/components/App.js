import React, { Component } from "react";
import './App.css';
import { BrowserRouter,Route, Switch, Redirect, Router } from "react-router-dom";
import { Provider } from 'react-redux'
import reducer from './components/reducers/index';
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import NavBar from "./components/bookMyShowNavbar";
import Movies from "./components/movies";
import history from "./components/history";
import BookMovie from "./components/bookMovie";
import BookMovieTicket from "./components/bookMovieTicket";
import Payment from "./components/payment";
import MyProfile from './components/myProfile';
import BookingHistory from './components/bookingHistory';
import BookASmile from './components/bookASmile';
import ProtectedRoute from './components/protectedRoute';
export const  store = createStore(reducer,applyMiddleware(thunk));
window.store = store

class App extends Component {
  state = {
    user:null
  }
  render() {
    let user = this.state.user;
    return (
      <Provider store={store}>
        <BrowserRouter>
        <div>
            {!history.location.pathname.includes("buyTicket") &&
            !history.location.pathname.includes("payment") ? (
              <Route
              path="/"
              render={(props) => (
              <NavBar {...props} user={user} />)}/>
            ) : (
              ""
            )}
            <Router history={history}>
              <Switch>
                <Route path='/myprofile/settings' component={MyProfile}/>
                <Route path='/myprofile/booking-history' component={BookingHistory}/>
                <Route path="/donation" component={BookASmile} />
                <Route
                  path="/bookMovie/:city/:id/buyTicket/:rowIndex/:colIndex/:day"
                  component={BookMovieTicket}
                />
                <Route path="/payment" component={Payment} />
                <Route path="/bookMovie/:city/:id" component={BookMovie} />
                <Route path="/home/:city" render={(history) => (<Movies {...history}/>)}/>
                <ProtectedRoute path="/home/NCR" component={Movies} />

                
                <Redirect from="/" to="/home/NCR" />
              </Switch>
            </Router>
          </div>
         </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
