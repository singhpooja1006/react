import React from "react";
const Like = (props) => {
  let classes = "fa fa-heart-o";
  return (
    <i
      onClick={props.onClick}
      style={{ cursor: "pointer", color: props.liked ? "red" : "black" }}
      className={classes}
      aria-hidden="true"
    ></i>
  );
};

export default Like;
