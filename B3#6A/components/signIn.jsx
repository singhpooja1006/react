import React, { Component } from "react";
import "./signIn.css";
class SignIn extends Component {
  state = {};
  render() {
    const { data } = this.props;
    return (
      <div className={this.props.show === true ? "modal-block" : "modal-none"}>
        <section className="modal-main">
          <div className="row">
            <div className="col text-right">
              <button onClick={this.props.handleClose}>X</button>
            </div>
          </div>
          <div className="row">
            <div
              className="col text-center pt-2"
              style={{
                backgroundColor: "rgb(59, 89, 152)",
                width: "100px",
                marginLeft: "30px",
                marginRight: "30px",
                boxSizing: "inherit",
                color: "rgb(255, 255, 255)",
                fontFamily: "Roboto, sans-serif",
                height: "48px",
                borderRadius: "3px",
              }}
            >
              <i
                className="fa fa-facebook-f"
                style={{ fontSize: "20px", paddingRight: "15px" }}
              ></i>
              Continue via facebook
            </div>
          </div>
          <div className="row">
            <div className="col text-center">
              <h6>OR</h6>
            </div>
          </div>
          <div className="row">
            <div
              className="col text-center pt-2"
              style={{
                width: "100px",
                marginLeft: "30px",
                marginRight: "30px",
                boxSizing: "inherit",
                color: "rgb(255, 255, 255)",
                fontFamily: "Roboto, sans-serif",
                height: "48px",
                borderRadius: "3px",
              }}
            >
              <form>
                <div className="form-group">
                  <input
                    className="form-control"
                    type="text"
                    id="email"
                    name="email"
                    placeholder=" Continue via mail"
                    value={data.email}
                    onChange={this.props.handleInputField}
                    onKeyDown={this.props.handleInputKeyDown}
                  />
                </div>
              </form>
            </div>
          </div>
          <br />
          <br />
          <br />
          <div className="row">
            <div
              className="col text-center"
              style={{ color: "rgb(102, 102, 102)", fontSize: "12px" }}
            >
              <p>I agree to the Terms and Conditions and Privacy Policy.</p>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default SignIn;
