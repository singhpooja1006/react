import React, { Component } from "react";
import ConstPage from "./constPage";
class BookASmile extends Component {
  state = {};
  render() {
    return (
      <div className="continer-fluid">
        <div
          style={{
            background:
              "url(https://in.bmscdn.com/webin/static/book-a-smile/showcase/banner11.jpg)",
            paddingBottom: "50px",
          }}
        >
          <div
            className="row"
            style={{
              color: "rgb(255, 255, 255)",
              fontSize: "40px",
              paddingLeft: "180px",
              paddingTop: "150px",
              paddingBottom: "150px",
              fontWeight: "100",
            }}
          >
            <div className="col">
              <h3>#BookMyShowCares.</h3>
            </div>
          </div>
          <br />
          <div
            className="row"
            style={{ backgroundColor: "rgb(242, 242, 242)", padding: "50px" }}
          >
            <div className="col">
              <div
                className="row"
                style={{
                  backgroundColor: "rgb(168, 18, 30)",
                  padding: "50px 0px",
                }}
              >
                <div className="col">
                  <h3
                    style={{
                      float: "left",
                      width: "100%",
                      textAlign: "center",
                      fontSize: "42px",
                      color: "rgb(255, 255, 255)",
                      fontFamily: "Jenna Sue&quot, cursive",
                      marginBottom: "30px",
                    }}
                  >
                    Where is it going
                  </h3>
                  <div className="row">
                    <div className="col">
                      <img
                        src="//in.bmscdn.com/webin/static/book-a-smile/afail.jpg"
                        style={{
                          opacity: "1",
                          width: "177px",
                          marginRight: "20px",
                          height: "75px",
                        }}
                      />
                      <img
                        src="//in.bmscdn.com/webin/static/book-a-smile/artreach.jpg"
                        style={{
                          opacity: "1",
                          width: "177px",
                          marginRight: "20px",
                          height: "75px",
                        }}
                      />
                      <img
                        src="//in.bmscdn.com/webin/static/book-a-smile/antarang.jpg"
                        style={{
                          opacity: "1",
                          width: "177px",
                          marginRight: "20px",
                          height: "75px",
                        }}
                      />
                      <img
                        src="//in.bmscdn.com/webin/static/book-a-smile/astha.jpg"
                        style={{
                          opacity: "1",
                          width: "177px",
                          marginRight: "20px",
                          height: "75px",
                        }}
                      />
                      <img
                        src="//in.bmscdn.com/webin/static/book-a-smile/advitya.png"
                        style={{
                          opacity: "1",
                          width: "177px",
                          marginRight: "20px",
                          height: "75px",
                        }}
                      />
                      <img
                        src="//in.bmscdn.com/webin/static/book-a-smile/akansha.png"
                        style={{
                          opacity: "1",
                          width: "177px",
                          marginRight: "20px",
                          height: "75px",
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="row" style={{ paddingBottom: "35px" }}>
                <div className="col text-center">
                  <h2
                    style={{
                      color: "rgb(44, 57, 70)",
                      paddingTop: "20px",
                      marginTop: "40px",
                      fontSize: "18px",
                    }}
                  >
                    IN THE NEWS
                  </h2>
                  <div
                    style={{
                      width: "30px",
                      height: "2px",
                      background: "rgb(200, 9, 16)",
                      margin: "20px auto 0px",
                    }}
                  ></div>
                  <div className="row">
                    <div className="col">
                      <img
                        src="//in.bmscdn.com/webin/static/bms-news/adgully.png"
                        style={{
                          opacity: "1",
                          width: "177px",
                          marginRight: "20px",
                          height: "75px",
                        }}
                      />
                      <img
                        src="//in.bmscdn.com/webin/static/bms-news/afternoon.png"
                        style={{
                          opacity: "1",
                          width: "177px",
                          marginRight: "20px",
                          height: "75px",
                        }}
                      />
                      <img
                        src="//in.bmscdn.com/webin/static/bms-news/business-standard.png"
                        style={{
                          opacity: "1",
                          width: "177px",
                          marginRight: "20px",
                          height: "75px",
                        }}
                      />
                      <img
                        src="//in.bmscdn.com/webin/static/bms-news/business-line.png"
                        style={{
                          opacity: "1",
                          width: "177px",
                          marginRight: "20px",
                          height: "75px",
                        }}
                      />
                      <img
                        src="//in.bmscdn.com/webin/static/bms-news/bollyspice.png"
                        style={{
                          opacity: "1",
                          width: "177px",
                          marginRight: "20px",
                          height: "75px",
                        }}
                      />
                      <img
                        src="//in.bmscdn.com/webin/static/bms-news/deccan-herald.png"
                        style={{
                          opacity: "1",
                          width: "177px",
                          marginRight: "20px",
                          height: "75px",
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <h3
                    style={{
                      fontSize: "18px",
                      color: "rgb(102, 102, 102)",
                      marginBottom: "20px",
                      paddingTop: "35px",
                    }}
                  >
                    Disclaimer
                  </h3>
                  <ul>
                    <li
                      style={{
                        marginRight: "10px",
                        fontSize: "12px",
                        color: "rgb(102, 102, 102)",
                        lineHeight: "24px",
                        listStyleType: "decimal",
                      }}
                    >
                      Contributions once made cannot be refunded or cancelled.
                    </li>
                    <li
                      style={{
                        marginRight: "10px",
                        fontSize: "12px",
                        color: "rgb(102, 102, 102)",
                        lineHeight: "24px",
                        listStyleType: "decimal",
                        overflowWrap: "normal",
                      }}
                    >
                      Big Tree Entertainment Pvt Ltd (BEPL) is facilitating the
                      transactions on the platform
                      https://in.bookmyshow.com/donation. The proceeds of the
                      same will be used for social initiatives for the
                      underprivileged sections of society.
                    </li>
                    <li
                      style={{
                        marginRight: "10px",
                        fontSize: "12px",
                        color: "rgb(102, 102, 102)",
                        lineHeight: "24px",
                        listStyleType: "decimal",
                      }}
                    >
                      Apart from this, BEPL is not engaged in any partnership,
                      association or tie-up with the NGOs.
                    </li>
                    <li
                      style={{
                        marginRight: "10px",
                        fontSize: "12px",
                        color: "rgb(102, 102, 102)",
                        lineHeight: "24px",
                        listStyleType: "decimal",
                      }}
                    >
                      BEPL will not be responsible for End use of the funds
                      donated.
                    </li>
                    <li
                      style={{
                        marginRight: "10px",
                        fontSize: "12px",
                        color: "rgb(102, 102, 102)",
                        lineHeight: "24px",
                        listStyleType: "decimal",
                      }}
                    >
                      BEPL expressly disclaims any and all liability and assumes
                      no responsibility whatsoever for consequences resulting
                      from any actions or inactions of the NGO.
                    </li>
                    <li
                      style={{
                        marginRight: "10px",
                        fontSize: "12px",
                        color: "rgb(102, 102, 102)",
                        lineHeight: "24px",
                        listStyleType: "decimal",
                      }}
                    >
                      By proceeding to donate the money, you do so at your own
                      risk and expressly waive any and all claims, rights of
                      action and/or remedies (under law or otherwise) that you
                      may have against BEPL arising out of or in connection with
                      the aforesaid transaction.
                    </li>
                    <li
                      style={{
                        marginRight: "10px",
                        fontSize: "12px",
                        color: "rgb(102, 102, 102)",
                        lineHeight: "24px",
                        listStyleType: "decimal",
                        width: "92%",
                      }}
                    >
                      BEPL will not be held responsible for the issuance of 80G
                      certificate.
                    </li>
                    <li
                      style={{
                        marginRight: "10px",
                        fontSize: "12px",
                        color: "rgb(102, 102, 102)",
                        lineHeight: "24px",
                        listStyleType: "decimal",
                      }}
                    >
                      For any queries, kindly{" "}
                      <a
                        target="_blank"
                        href="https://support.bookmyshow.com/support/tickets/new"
                      >
                        email us
                      </a>
                      .
                    </li>
                  </ul>
                </div>
              </div>
              <br />
              <br />
            </div>
            <ConstPage />
          </div>
        </div>
      </div>
    );
  }
}

export default BookASmile;
