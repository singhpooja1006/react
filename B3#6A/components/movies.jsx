import React, { Component } from "react";
import axios from "axios";
import config from "./config.json";
import SlideImage from "./slideImage";
import MovieNavBar from "./movieNavBar";
import LeftPanel from "./leftPanel";
import history from "./history";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
class Movies extends Component {
  state = {
    moviesData: [],
    totalMovieList: [],
    formats: [],
    languages: [
      { name: "Hindi", check: false },
      { name: "English", check: false },
      { name: "Punjabi", check: false },
      { name: "Tamil", check: false },
    ],
    genres: [],
  };

  async componentDidMount() {
    let city = this.props.match.params.city;

    city = city ? city : "NCR";
    const { data: totalMovieList } = await axios.get(config.apiEndPoint + city);
    const { data: moviesData } = await axios.get(
      config.apiEndPoint + city + this.props.location.search
    );
    const { data: formats } = await axios.get(
      config.apiEndPoint1 + "formatList"
    );
    const { data: genres } = await axios.get(config.apiEndPoint1 + "genreList");
    this.setState({ moviesData, totalMovieList, formats, genres });
  }
  findSelectedMovieID = (movie) => {
    return this.state.totalMovieList.findIndex((m) => m.title === movie.title);
  };
  async componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.match.params !== this.props.match.params ||
      prevProps.location.search !== this.props.location.search
    ) {
      const { data: moviesData } = await axios.get(
        config.apiEndPoint +
          this.props.match.params.city +
          this.props.location.search
      );

      this.setState({ moviesData });
    }
  }
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  handleOptionChange = (languages, formats, genres) => {
    let language = this.buildQueryString(languages);
    let format = this.buildQueryString(formats);
    let genre = this.buildQueryString(genres);
    this.setState({ languages, formats, genres });
    this.calURL("", language, format, genre);
  };
  buildQueryString(optionsData) {
    let filterData = optionsData.filter((n1) => n1.check);
    let arrayData = filterData.map((n1) => n1.name);
    return arrayData.join(",");
  }
  calURL = (params, language, format, genre) => {
    let path = "/home";
    const city = this.props.match.params.city;
    if (city) path = path + "/" + city;

    params = this.addToParams(params, "lang", language);
    params = this.addToParams(params, "format", format);
    params = this.addToParams(params, "genre", genre);
    this.props.history.push({
      pathname: path,
      search: params,
    });
  };

  render() {
    return (
      <React.Fragment>
        <br />
        <div className="container-fluid">
          <SlideImage />
          <div class="d-none d-lg-block">
            <MovieNavBar />
          </div>
          <div className="row bg-light">
            <div className="col-3 d-none d-lg-block">
              <LeftPanel
                formats={this.state.formats}
                languages={this.state.languages}
                genres={this.state.genres}
                onOptionChange={this.handleOptionChange}
              />
            </div>
            <div className="col-lg-9 col-12">{this.renderMovieView()}</div>
          </div>
        </div>
      </React.Fragment>
    );
  }
  renderMovieView = () => {
    if (this.state.moviesData) {
      return (
        <React.Fragment>
          <div className="row ">
            {this.renderMoviesData(0)}
            {this.renderMoviesData(1)}
            {this.renderMoviesData(2)}
            {this.renderMoviesData(3)}
            {this.renderMoviesData(4)}
            {this.renderMoviesData(5)}
            {this.renderMoviesData(6)}
            {this.renderMoviesData(7)}
            {this.renderMoviesData(8)}
            {this.renderMoviesData(9)}
            {this.renderMoviesData(10)}
            {this.renderMoviesData(11)}
          </div>
        </React.Fragment>
      );
    }
  };
  renderMoviesData(index) {
    const movieData = this.state.moviesData[index];
    if (movieData) {
      const city = this.props.match.params.city;
      let id = this.findSelectedMovieID(movieData);
      const url = "/bookMovie/" + city + "/" + id;
      return (
        <div
          className="col-lg-3 col-md-3 col-5  ml-4 ml-md-5 mr-md-1 ml-lg-1 bg-white"
          onClick={() => history.push(url)}
        >
          <div className="row">
            <div className="col-lg-12">
              <img
                className="card-img-top text-center"
                alt=""
                src={movieData.img}
                style={{
                  width: "100%",
                  height: "100%",
                  objectFit: "cover",
                }}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-lg-5 col-6">
              <div className="row">
                <div className="col-lg-12" style={{ fontSize: "14px" }}>
                  <FontAwesomeIcon
                    icon={faHeart}
                    style={{ color: "#ff0000" }}
                  />{" "}
                  {movieData.rating}
                </div>
              </div>
              <div className="row d-none d-lg-block">
                <div className="col text-muted" style={{ fontSize: "12px" }}>
                  {" "}
                  {movieData.votes} votes{" "}
                </div>
              </div>
            </div>
            <div className="col-lg-7 col-12">
              <div className="row ml-2" style={{ fontSize: "14px" }}>
                {movieData.title}
              </div>
              <div
                className="row text-muted d-none d-lg-block ml-2"
                style={{ fontSize: "13px" }}
              >
                {" "}
                {movieData.desc}{" "}
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}
export default Movies;
