import {
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT,
} from '../action';

const token_data = localStorage.getItem("token")

const initialState = {
    isLoggedIn : token_data ? true : false,
    token : token_data,
    showLogin: false,
    email : null
};

const reducer = (state = initialState, action ) => {
    

    switch (action.type) {
        case LOGIN_SUCCESS:
                return {
                  ...state,
                  isLoggedIn: true,
                  showLogin: false,
                  token: action.payload.token,
                  email : action.payload.email
                };
        case LOGIN_FAIL:
              return {
              ...state,
              isLoggedIn: false,
              token: null,
              showLogin : true,
              email : null
      };
        case LOGOUT:
            return {
                ...state,
                isLoggedIn: false,
                showLogin : false,
                token: null,
                email : null
            };
        default:
            return state;

    }
};

export default reducer