import React, { Component } from "react";
import axios from "axios";
import "./dropDown.css";
import config from "./config.json";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Like from "./like";
class BookMovie extends Component {
  state = {
    moviesData: {},
    months: [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ],
    priceList: [
      { value: "0-100", isSelected: false },
      { value: "101-200", isSelected: false },
      { value: "201-300", isSelected: false },
      { value: "More than 300", isSelected: false },
    ],
    showTime: [
      { value: "Morning", isSelected: false },
      { value: "Afternoon", isSelected: false },
      { value: "Evening", isSelected: false },
      { value: "Night", isSelected: false },
    ],
    currentMovieIndex: 0,
    liked: [],
  };
  async componentDidMount() {
    let { city, id } = this.props.match.params;
    city = city ? city : "NCR";
    const { data: moviesData } = await axios.get(
      config.apiEndPoint + city + "/" + id
    );
    /*let length = moviesData.showTiming.reduce(
      (max, col1) => (col1.length > max ? col1.length : max),
      moviesData.showTiming[0].length
    );*/
    let liked = [];

    for (let index = 0; index < moviesData.showTiming.length; index++) {
      let rowLiked = [];
      for (let i = 0; i < moviesData.showTiming[index].length; i++) {
        rowLiked.push(false);
      }
      liked.push(rowLiked);
    }
    this.setState({ moviesData, liked });
  }

  handleMovieIndex = (currentMovieIndex) => {
    this.setState({ currentMovieIndex });
  };
  handleMovieTime = (rowIndex, columnIndex) => {
    let { city, id } = this.props.match.params;
    let url =
      "/bookMovie/" +
      city +
      "/" +
      id +
      "/buyTicket/" +
      rowIndex +
      "/" +
      columnIndex +
      "/" +
      this.state.currentMovieIndex +
      "?time=" +
      this.getDate(this.state.currentMovieIndex);
    window.location = url;
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    const { priceList, showTime } = this.state;
    let price = priceList.find((n1) => n1.value === input.name);
    if (price) price.isSelected = input.checked;

    let time = showTime.find((n1) => n1.value === input.name);
    if (time) time.isSelected = input.checked;
    this.setState({ priceList, showTime });
  };
  getDate = (index) => {
    const date = new Date();
    switch (index) {
      case 0:
        return new Date().getDate() + " TODAY";
      case 1:
        const tomorrowDate = new Date(date.setDate(date.getDate() + 1));
        return (
          tomorrowDate.getDate() +
          " " +
          this.state.months[tomorrowDate.getMonth()]
        );
      case 2:
        const dayAftertomorrowDate = new Date(date.setDate(date.getDate() + 2));
        return (
          dayAftertomorrowDate.getDate() +
          " " +
          this.state.months[dayAftertomorrowDate.getMonth()]
        );
      default:
        break;
    }
  };
  render() {
    const { moviesData } = this.state;
    return (
      <React.Fragment>
        <div className="container-fluid">
          <div className="row bg-secondary text-white pt-4">
            <div className="col">
              <h3>{moviesData.title}</h3>
            </div>
          </div>
          <div className="row bg-secondary text-white">
            <div className="col">
              <FontAwesomeIcon icon={faHeart} style={{ color: "#ff0000" }} />{" "}
              &nbsp;{" "}
              <span style={{ fontSize: "20px" }}>
                <strong>{moviesData.rating}</strong>
              </span>
              &nbsp;&nbsp; <span className="circle">Action</span>
              &nbsp;
              <span className="circle">Thriller</span>
            </div>
          </div>
          <div className="row bg-secondary text-white">
            <div className="col">
              <span style={{ fontSize: "10px" }}>{moviesData.votes} votes</span>
            </div>
          </div>
          <div className="row bg-light pt-2 pb-2">
            <div className="col-lg-5">
              <button
                className="btn btn-sm btn-light m-1"
                id={
                  this.state.currentMovieIndex === 0
                    ? "buttoncss-true"
                    : "buttoncss-false"
                }
                onClick={() => this.handleMovieIndex(0)}
              >
                <b>{this.getDate(0)}</b>
              </button>
              <button
                className="btn btn-sm btn-light m-1"
                id={
                  this.state.currentMovieIndex === 1
                    ? "buttoncss-true"
                    : "buttoncss-false"
                }
                onClick={() => this.handleMovieIndex(1)}
              >
                <b>{this.getDate(1)}</b>
              </button>
              <button
                className="btn btn-sm btn-light  m-1"
                id={
                  this.state.currentMovieIndex === 2
                    ? "buttoncss-true"
                    : "buttoncss-false"
                }
                onClick={() => this.handleMovieIndex(2)}
              >
                <b>{this.getDate(2)}</b>
              </button>
            </div>
            <div className="col-lg-2 border-right d-none d-lg-block">
              {this.renderPriceList()}
            </div>
            <div className="col-lg-2 border-right d-none d-lg-block">
              {this.renderShowTime()}
            </div>
          </div>
          <div className="row">
            <div className="col-lg-9 col-12">
              <div
                className="row"
                style={{ backgroundColor: "rgb(245, 191, 169)" }}
              >
                <div className="col-lg-6 col-6 border-right">
                  <div className="row">
                    <span className="logo nav-item">
                      <svg
                        version="1.1"
                        xmlns="http://www.w3.org/2000/svg"
                        xlinkHref="http://www.w3.org/1999/xlink"
                        width="40"
                        height="45"
                        viewBox="0 0 130 45"
                        xspace="preserve"
                        style={{ fill: "green" }}
                      >
                        <path d="M73.5 95.2H26.8c-1.3 0-2.3-1-2.3-2.3V7.6c0-1.3 1-2.3 2.3-2.3h46.7c1.3 0 2.3 1 2.3 2.3V93c0 1.2-1.1 2.2-2.3 2.2zM26.8 6.4c-.6 0-1.1.5-1.1 1.1v85.4c0 .6.5 1.1 1.1 1.1h46.7c.6 0 1.1-.5 1.1-1.1V7.6c0-.6-.5-1.1-1.1-1.1l-46.7-.1z" />
                        <path d="M68.8 78.9H31.3c-.6 0-1.2-.5-1.2-1.2V16c0-.6.5-1.2 1.2-1.2h37.5c.6 0 1.2.5 1.2 1.2v61.7c0 .6-.5 1.2-1.2 1.2zm0-62.9H31.3v61.6h37.4l.1-61.6zm-19 74.4c-2.3 0-4.1-1.9-4.1-4.1 0-2.3 1.9-4.1 4.1-4.1 2.3 0 4.1 1.9 4.1 4.1a4 4 0 0 1-4.1 4.1zm0-7.1a2.9 2.9 0 1 0 2.9 2.9c.1-1.5-1.2-2.9-2.9-2.9zm-5.5-72.1h-1.6c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h1.6c.3 0 .6.3.6.6s-.3.6-.6.6zm11 0h-6.5c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h6.5c.3 0 .6.3.6.6s-.2.6-.6.6z" />
                      </svg>
                    </span>
                  </div>
                  <div
                    className="row"
                    style={{ size: "2px", fontSize: "10px" }}
                  >
                    <span>M-Ticket Available</span>
                  </div>
                </div>
                <div className="col-lg-6 col-6 border-right">
                  <div className="row">
                    <span className="logo nav-item">
                      <svg
                        version="1.1"
                        xmlns="http://www.w3.org/2000/svg"
                        xlinkHref="http://www.w3.org/1999/xlink"
                        width="40"
                        height="45"
                        viewBox="0 0 102 102"
                        xspace="preserve"
                        style={{ fill: " green" }}
                      >
                        <path
                          fill="#F90"
                          d="M58.7 85.9H8.5c-4.1 0-7.5-3.4-7.5-7.5v-1.1c0-4.1 3.4-7.5 7.5-7.5h50.2c4.1 0 7.5 3.4 7.5 7.5v1.1c0 4.1-3.4 7.5-7.5 7.5zM8.5 74.3a3 3 0 0 0-3 3v1.1a3 3 0 0 0 3 3h50.2a3 3 0 0 0 3-3v-1.1a3 3 0 0 0-3-3H8.5zM54.8 99H12.4c-3.7 0-6.8-3-6.8-6.8v-4c0-1.2 1-2.2 2.2-2.2s2.2 1 2.2 2.2v4c0 1.3 1.1 2.3 2.4 2.3h42.4c1.3 0 2.3-1.1 2.3-2.3v-4c0-1.2 1-2.2 2.2-2.2s2.2 1 2.2 2.2v4c.1 3.8-3 6.8-6.7 6.8zm42-57.9c-1.2 0-2.2-1-2.2-2.2v-2.8c0-.6-.5-1-1-1h-54a1 1 0 0 0-1 1v2.8c0 1.2-1 2.2-2.2 2.2-1.2 0-2.2-1-2.2-2.2v-2.8c0-3 2.5-5.5 5.5-5.5h54c3 0 5.5 2.4 5.5 5.5v2.8c-.2 1.2-1.2 2.2-2.4 2.2z"
                        />
                        <path
                          fill="#F90"
                          d="M63.9 31.4c-1 0-1.9-.7-2.2-1.7L56.6 6.2c-.1-.4-.3-.5-.4-.6-.1-.1-.4-.2-.8-.1L41 8.6c-1.2.3-2.4-.5-2.6-1.7-.3-1.2.5-2.4 1.7-2.6l14.4-3.1c1.4-.3 2.9 0 4.1.7 1.2.8 2.1 2 2.4 3.4l5.1 23.5c.3 1.2-.5 2.4-1.7 2.6h-.5zm-3.7 38.7c-.8 0-1.5-.4-1.9-1.1-3.9-6.7-12.4-14.5-23.8-14.5s-20 7.8-23.8 14.5c-.6 1.1-2 1.4-3 .8-1.1-.6-1.4-2-.8-3 5.9-10.1 16.7-16.7 27.7-16.7s21.8 6.5 27.7 16.7c.6 1.1.3 2.4-.8 3-.5.2-.9.3-1.3.3z"
                        />
                        <ellipse
                          fill="#F90"
                          cx="24.3"
                          cy="63.9"
                          rx="1.7"
                          ry="1.7"
                        ></ellipse>
                        <ellipse
                          fill="#F90"
                          cx="40.7"
                          cy="65.6"
                          rx="1.7"
                          ry="1.7"
                        ></ellipse>
                        <ellipse
                          fill="#F90"
                          cx="33.6"
                          cy="58.8"
                          rx="1.7"
                          ry="1.7"
                        ></ellipse>
                        <path
                          fill="#F90"
                          d="M47 49.6c-1.2 0-2.2-.9-2.2-2.1l-.5-10.3c-.1-1.2.9-2.3 2.1-2.3 1.3-.1 2.3.9 2.3 2.1l.5 10.3c.1 1.2-.9 2.3-2.1 2.3H47zM82.1 93H71.4c-1.2 0-2.2-1-2.2-2.2s1-2.2 2.2-2.2h10.7c.6 0 1-.5 1-1L85.5 37a2.3 2.3 0 0 1 2.3-2.1c1.2.1 2.2 1.1 2.1 2.3l-2.4 50.5c.1 2.9-2.4 5.3-5.4 5.3z"
                        />
                      </svg>
                    </span>
                  </div>
                  <div
                    className="row"
                    style={{ size: "2px", fontSize: "10px" }}
                  >
                    <span>Food Available</span>
                  </div>
                </div>
              </div>
              <div>{this.renderMovie()}</div>
            </div>
            <div className="col-2 mt-1 d-none d-lg-block">
              <img
                className="img-fluid"
                src="https://i.ibb.co/JqbbCJz/1331654202504679967.jpg"
                alt=""
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
  handleLike = (currentMovieIndex, index) => {
    const liked = [...this.state.liked];
    liked[currentMovieIndex][index] = liked[currentMovieIndex][index]
      ? false
      : true;
    this.setState({ liked });
  };
  renderMovieRow(show, rowIndex) {
    let classes = "fa fa-heart";
    let liked = this.state.liked[this.state.currentMovieIndex][rowIndex];
    return (
      <div className="row border-bottom-1 pt-1">
        <div className="col-lg-1 col-2">
          <Like
            liked={liked}
            onClick={() =>
              this.handleLike(this.state.currentMovieIndex, rowIndex)
            }
          />
        </div>
        <div className="col-lg-3 col-9" style={{ fontSize: "12px" }}>
          <div className="row">
            <strong>{show.name}</strong>
          </div>
          <div className="row">
            <span
              className="logo nav-item"
              style={{ size: "2px", fontSize: "10px" }}
            >
              <svg
                xspace="preserve"
                xlinkHref="http://www.w3.org/1999/xlink"
                height="25"
                style={{ fill: "green" }}
                version="1.1"
                viewBox="0 0 102 130"
                width="20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M73.5 95.2H26.8c-1.3 0-2.3-1-2.3-2.3V7.6c0-1.3 1-2.3 2.3-2.3h46.7c1.3 0 2.3 1 2.3 2.3V93c0 1.2-1.1 2.2-2.3 2.2zM26.8 6.4c-.6 0-1.1.5-1.1 1.1v85.4c0 .6.5 1.1 1.1 1.1h46.7c.6 0 1.1-.5 1.1-1.1V7.6c0-.6-.5-1.1-1.1-1.1l-46.7-.1z"></path>
                <path d="M68.8 78.9H31.3c-.6 0-1.2-.5-1.2-1.2V16c0-.6.5-1.2 1.2-1.2h37.5c.6 0 1.2.5 1.2 1.2v61.7c0 .6-.5 1.2-1.2 1.2zm0-62.9H31.3v61.6h37.4l.1-61.6zm-19 74.4c-2.3 0-4.1-1.9-4.1-4.1 0-2.3 1.9-4.1 4.1-4.1 2.3 0 4.1 1.9 4.1 4.1a4 4 0 0 1-4.1 4.1zm0-7.1a2.9 2.9 0 1 0 2.9 2.9c.1-1.5-1.2-2.9-2.9-2.9zm-5.5-72.1h-1.6c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h1.6c.3 0 .6.3.6.6s-.3.6-.6.6zm11 0h-6.5c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h6.5c.3 0 .6.3.6.6s-.2.6-.6.6z"></path>
              </svg>
              M-Ticket
            </span>
            &nbsp;&nbsp;
            <span
              className="logo nav-item"
              style={{ size: "2px", fontSize: "10px" }}
            >
              <svg
                xspace="preserve"
                xlinkHref="http://www.w3.org/1999/xlink"
                height="25"
                style={{ fill: "green" }}
                version="1.1"
                viewBox="0 0 102 102"
                width="40"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M58.7 85.9H8.5c-4.1 0-7.5-3.4-7.5-7.5v-1.1c0-4.1 3.4-7.5 7.5-7.5h50.2c4.1 0 7.5 3.4 7.5 7.5v1.1c0 4.1-3.4 7.5-7.5 7.5zM8.5 74.3a3 3 0 0 0-3 3v1.1a3 3 0 0 0 3 3h50.2a3 3 0 0 0 3-3v-1.1a3 3 0 0 0-3-3H8.5zM54.8 99H12.4c-3.7 0-6.8-3-6.8-6.8v-4c0-1.2 1-2.2 2.2-2.2s2.2 1 2.2 2.2v4c0 1.3 1.1 2.3 2.4 2.3h42.4c1.3 0 2.3-1.1 2.3-2.3v-4c0-1.2 1-2.2 2.2-2.2s2.2 1 2.2 2.2v4c.1 3.8-3 6.8-6.7 6.8zm42-57.9c-1.2 0-2.2-1-2.2-2.2v-2.8c0-.6-.5-1-1-1h-54a1 1 0 0 0-1 1v2.8c0 1.2-1 2.2-2.2 2.2-1.2 0-2.2-1-2.2-2.2v-2.8c0-3 2.5-5.5 5.5-5.5h54c3 0 5.5 2.4 5.5 5.5v2.8c-.2 1.2-1.2 2.2-2.4 2.2z"
                  fill="#F90"
                ></path>
                <path
                  d="M63.9 31.4c-1 0-1.9-.7-2.2-1.7L56.6 6.2c-.1-.4-.3-.5-.4-.6-.1-.1-.4-.2-.8-.1L41 8.6c-1.2.3-2.4-.5-2.6-1.7-.3-1.2.5-2.4 1.7-2.6l14.4-3.1c1.4-.3 2.9 0 4.1.7 1.2.8 2.1 2 2.4 3.4l5.1 23.5c.3 1.2-.5 2.4-1.7 2.6h-.5zm-3.7 38.7c-.8 0-1.5-.4-1.9-1.1-3.9-6.7-12.4-14.5-23.8-14.5s-20 7.8-23.8 14.5c-.6 1.1-2 1.4-3 .8-1.1-.6-1.4-2-.8-3 5.9-10.1 16.7-16.7 27.7-16.7s21.8 6.5 27.7 16.7c.6 1.1.3 2.4-.8 3-.5.2-.9.3-1.3.3z"
                  fill="#F90"
                ></path>
                <ellipse
                  cx="24.3"
                  cy="63.9"
                  fill="#F90"
                  rx="1.7"
                  ry="1.7"
                ></ellipse>
                <ellipse
                  cx="40.7"
                  cy="65.6"
                  fill="#F90"
                  rx="1.7"
                  ry="1.7"
                ></ellipse>
                <ellipse
                  cx="33.6"
                  cy="58.8"
                  fill="#F90"
                  rx="1.7"
                  ry="1.7"
                ></ellipse>
                <path
                  d="M47 49.6c-1.2 0-2.2-.9-2.2-2.1l-.5-10.3c-.1-1.2.9-2.3 2.1-2.3 1.3-.1 2.3.9 2.3 2.1l.5 10.3c.1 1.2-.9 2.3-2.1 2.3H47zM82.1 93H71.4c-1.2 0-2.2-1-2.2-2.2s1-2.2 2.2-2.2h10.7c.6 0 1-.5 1-1L85.5 37a2.3 2.3 0 0 1 2.3-2.1c1.2.1 2.2 1.1 2.1 2.3l-2.4 50.5c.1 2.9-2.4 5.3-5.4 5.3z"
                  fill="#F90"
                ></path>
              </svg>
              F&amp;B
            </span>
          </div>
        </div>
        <div className="col-lg-6">
          {show.timings.map((time, columnIndex) => (
            <button
              className="btn btn-outline-secondary text-primary btn-sm border-muted m-1"
              routerlinkactive="router-link-active"
              style={{
                marginBottom: "12px",
                maxHeight: "40px",
                fontSize: "12px",
              }}
              tabindex="0"
              onClick={() => this.handleMovieTime(rowIndex, columnIndex)}
            >
              <span data-toggle="tooltip" title={time.price}>
                {time.name}
              </span>
            </button>
          ))}

          <ul style={{ fontSize: "12px" }}>
            <li>Cancellation available</li>
          </ul>
        </div>
      </div>
    );
  }
  renderMovie() {
    if (this.state.moviesData.showTiming) {
      const showTime = this.state.moviesData.showTiming[
        this.state.currentMovieIndex
      ];
      return (
        <React.Fragment>
          {showTime.map((show, rowIndex) =>
            this.renderMovieRow(show, rowIndex)
          )}
        </React.Fragment>
      );
    }
  }
  renderShowTime() {
    return (
      <div className="dropdown">
        <div className="dropbtn1">
          {" "}
          Filter Showtime &nbsp;
          <span>
            <i
              className="fa fa-chevron-down"
              id="onhover"
              style={{ fontsize: "10px", color: "lightgrey" }}
            ></i>
          </span>
        </div>
        <div className="dropdown-content">
          {this.state.showTime.map((time) => (
            <div className="checkbox ml-1">
              <label>
                <input
                  type="checkbox"
                  value={time.value}
                  name={time.value}
                  className="ng-untouched ng-pristine ng-valid"
                  onChange={this.handleChange}
                  checked={time.isSelected}
                />
                &nbsp;&nbsp;{time.value}
              </label>
            </div>
          ))}
        </div>
      </div>
    );
  }
  renderPriceList() {
    return (
      <div className="dropdown">
        <div className="dropbtn1">
          {" "}
          Filter Price &nbsp;
          <span>
            <i
              className="fa fa-chevron-down"
              id="onhover"
              style={{ fontsize: "10px", color: "lightgrey" }}
            ></i>
          </span>
        </div>
        <div className="dropdown-content">
          {this.state.priceList.map((price) => (
            <div className="checkbox ml-1">
              <label>
                <input
                  type="checkbox"
                  value={price.value}
                  name={price.value}
                  className="ng-untouched ng-pristine ng-valid"
                  onChange={this.handleChange}
                  checked={price.isSelected}
                />
                &nbsp;&nbsp;{price.value}
              </label>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default BookMovie;
