import React, { Component } from "react";
import history from "./history";
class ConstProfile extends Component {
  state = {};

  handleBookingHistory = () => {
    history.push({
      pathname: "/myprofile/booking-history",
    });
  };
  handleSetting = () => {
    history.push({
      pathname: "/myprofile/settings",
    });
  };
  render() {
    return (
      <div className="container-fluid" style={{ backgroundColor: "#204060" }}>
        <div className="row">
          <div className="mt-4 ml-5" style={{ color: "white" }}>
            Online Tickets Profile Settings
          </div>
        </div>
        <div className="mt-4 row" style={{ color: "white" }}>
          <div id="test">
            <i
              className="fa fa-user-circle-o"
              style={{ fontSize: "120px" }}
              aria-hidden="true"
            ></i>
          </div>
          <div className="col-2">
            <div className="row">
              <h1 className="ml-1">
                {this.props.user ? this.props.user.fname : ""}
              </h1>
            </div>
            <div className="row">
              <h5 className="ml-1">
                {this.props.user ? this.props.user.mobile : ""}
              </h5>
            </div>
          </div>
        </div>
        <div className="row" id="booking">
          <div
            className="ml-4"
            style={{ color: "white" }}
            onClick={() => this.handleBookingHistory()}
          >
            {" "}
            Booking History
          </div>
          <div id="setting" onClick={() => this.handleSetting()}>
            Settings
          </div>
        </div>
      </div>
    );
  }
}

export default ConstProfile;
