import React, { Component } from "react";
import ConstProfile from "./constProfile";
import config from "./config.json";
import axios from "axios";
import ConstPage from "./constPage";
class BookingHistory extends Component {
  state = {
    isShow: false,
    bookingHistory: {},
  };
  handleViewAllBooking = () => {
    this.setState({ isShow: this.state.isShow ? false : true });
  };
  async componentDidMount() {
    let token = localStorage.getItem("token");
    let apiEndpoint = config.userApi + "userDetails";
    const { data: bookingHistory } = await axios.get(
      "https://us-central1-bkyow-22da6.cloudfunctions.net/app/details"
    );
    let user;
    await axios
      .get(apiEndpoint, {
        headers: { Authorization: token },
      })
      .then(function (result) {
        user = result.data;
      });
    this.setState({
      user,
      bookingHistory,
    });
  }

  render() {
    return (
      <React.Fragment>
        <ConstProfile user={this.state.user} />
        <div className="row" style={{ backgroundColor: "lightgrey" }}>
          <div
            className="mt-4 ml-5 col-12"
            style={{
              color: "black",
              fontSize: "24px",
              padding: "0px 0px 15px",
              fontWeight: "100",
              fontFamily: "Brush Script MT, cursive",
            }}
          >
            You don't seem to have any recent bookings.
          </div>

          <div
            className="ml-5 col-12"
            style={{
              color: "rgb(192, 44, 57)",
              fontSize: "14px",
              padding: "0px 0px 15px",
              cursor: "pointer",
              fontFamily: "Roboto, sans-serif",
            }}
            onClick={() => this.handleViewAllBooking()}
          >
            View all bookings
          </div>
          <br />
          {this.state.isShow === true ? (
            <div className="container" style={{ backgroundColor: "white" }}>
              <div
                style={{
                  backgroundColor: "rgb(255, 255, 255)",
                  fontSize: "16px",
                  padding: "15px",
                  fontFamily: "Roboto, sans-serif",
                }}
              >
                <div className="col">
                  <div className="row" style={{ color: "rgb(192, 44, 57)" }}>
                    <div className="col-2">
                      <h5>Movie</h5>
                    </div>
                    <div className="col-4">
                      <h5>Hall</h5>
                    </div>
                    <div className="col-2">
                      <h5>Amount</h5>
                    </div>
                    <div className="col-2">
                      <h5>Seats</h5>
                    </div>
                    <div className="col-2">
                      <h5>Date</h5>
                    </div>
                  </div>
                  {this.renderAllBookingHistory()}
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
        </div>
        <ConstPage />
      </React.Fragment>
    );
  }
  renderAllBookingHistory = () => {
    const { bookingHistory } = this.state;

    if (bookingHistory) {
      return (
        <React.Fragment>
          <div className="row">
            <div className="col-2">
              <h6>{bookingHistory.title}</h6>
            </div>
            <div className="col-4">
              <h6>{bookingHistory.movieHall}</h6>
            </div>
            <div className="col-2">
              <h6>{bookingHistory.amount}</h6>
            </div>
            <div className="col-2">
              <h6>
                {bookingHistory.tickets ? bookingHistory.tickets.join(" ") : ""}
              </h6>
            </div>
            <div className="col-2">
              <h6>{bookingHistory.date}</h6>
            </div>
          </div>
        </React.Fragment>
      );
    }
  };
}

export default BookingHistory;
