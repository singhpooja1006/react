import AuthService from "../services/authService";
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGOUT = 'LOGOUT';
export const login  = (token, email)  => {
  if(token){
      return {
          type: LOGIN_SUCCESS,
          payload: { token: token, email : email },
      }
  }else{
      return {
          type: LOGIN_FAIL,
      }
     // return Promise.reject();
  }
  
};


export const logout = ()  => {
    AuthService.logout();
    return{
        type: LOGOUT,
    }
  };






