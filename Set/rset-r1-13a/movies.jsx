import React, { Component } from "react";
import axios from "axios";
import config from "./config.json";
import SlideImage from "./slideImage";
import MovieNavBar from "./movieNavBar";
import LeftPanel from "./leftPanel";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
class Movies extends Component {
  state = {
    moviesData: [],
    formats: [
      { value: "2D", isSelected: false },
      { value: "3D", isSelected: false },
      { value: "4DX", isSelected: false },
    ],
    languages: [
      { value: "Hindi", isSelected: false },
      { value: "English", isSelected: false },
      { value: "Punjabi", isSelected: false },
      { value: "Tamil", isSelected: false },
    ],
    genres: [
      { value: "Action", isSelected: false },
      { value: "Adventure", isSelected: false },
      { value: "Biography", isSelected: false },
      { value: "Comedy", isSelected: false },
    ],
  };

  async componentDidMount() {
    let city = this.props.match.params.city;

    city = city ? city : "NCR";
    const { data: moviesData } = await axios.get(
      config.apiEndPoint + city + this.props.location.search
    );

    this.setState({ moviesData });
  }
  async componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.match.params !== this.props.match.params ||
      prevProps.location.search !== this.props.location.search
    ) {
      const { data: moviesData } = await axios.get(
        config.apiEndPoint +
          this.props.match.params.city +
          this.props.location.search
      );

      this.setState({ moviesData });
    }
  }
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  handleOptionChange = (languages, formats, genres) => {
    let language = this.buildQueryString(languages);
    let format = this.buildQueryString(formats);
    let genre = this.buildQueryString(genres);
    this.setState({ languages, formats, genres });
    this.calURL("", language, format, genre);
  };
  buildQueryString(optionsData) {
    let filterData = optionsData.filter((n1) => n1.isSelected);
    let arrayData = filterData.map((n1) => n1.value);
    return arrayData.join(",");
  }
  calURL = (params, language, format, genre) => {
    let path = "/movies";
    const city = this.props.match.params.city;
    if (city) path = path + "/" + city;

    params = this.addToParams(params, "lang", language);
    params = this.addToParams(params, "format", format);
    params = this.addToParams(params, "genre", genre);

    this.props.history.push({
      pathname: path,
      search: params,
    });
  };

  render() {
    return (
      <div className="mt-3 container-fluid">
        <SlideImage />
        <MovieNavBar />

        <div className="row bg-light">
          <div className="col-3 d-none d-lg-block">
            <LeftPanel
              formats={this.state.formats}
              languages={this.state.languages}
              genres={this.state.genres}
              onOptionChange={this.handleOptionChange}
            />
          </div>
          <div className="col-lg-9 col-12">{this.renderMovieView()}</div>
        </div>
      </div>
    );
  }
  renderMovieView = () => {
    if (this.state.moviesData) {
      return (
        <React.Fragment>
          <div className="row ">
            {this.renderMoviesData(this.state.moviesData[0])}
            {this.renderMoviesData(this.state.moviesData[1])}
            {this.renderMoviesData(this.state.moviesData[2])}
          </div>
          <div className="row">
            {this.renderMoviesData(this.state.moviesData[3])}
            {this.renderMoviesData(this.state.moviesData[4])}
            {this.renderMoviesData(this.state.moviesData[5])}
          </div>
          <div className="row">
            {this.renderMoviesData(this.state.moviesData[6])}
            {this.renderMoviesData(this.state.moviesData[7])}
            {this.renderMoviesData(this.state.moviesData[8])}
          </div>
          <div className="row">
            {this.renderMoviesData(this.state.moviesData[9])}
            {this.renderMoviesData(this.state.moviesData[10])}
            {this.renderMoviesData(this.state.moviesData[11])}
          </div>
        </React.Fragment>
      );
    }
  };
  renderMoviesData(movieData) {
    if (movieData) {
      return (
        <div className="col-lg-3 col-md-3 col-5  ml-4 ml-md-5 mr-md-1 ml-lg-1 bg-white">
          <div className="card">
            <img
              className="card-img-top text-center"
              alt=""
              src={movieData.img}
              style={{
                width: "100%",
                height: "100%",
                objectFit: "cover",
              }}
            />
          </div>
          <div className="row m-2">
            <div className="col-5">
              {" "}
              <FontAwesomeIcon icon={faHeart} style={{ color: "#ff0000" }} />
              {movieData.rating}
            </div>
            <div className="col-7">{movieData.title}</div>
          </div>
          <div className="row">
            <div className="col-5">{movieData.votes}</div>
            <div className="col-7">{movieData.desc}</div>
          </div>
        </div>
      );
    }
  }
}
export default Movies;
