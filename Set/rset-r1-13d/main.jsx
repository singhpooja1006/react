import React, { Component } from "react";
import { Route, Switch, Redirect, Router } from "react-router-dom";
import NavBar from "./bookMyShowNavbar";
import Movies from "./movies";
import history from "./history";
import BookMovie from "./bookMovie";
import BookMovieTicket from "./bookMovieTicket";
import Payment from "./payment";
class MainComp extends Component {
  render() {
    return (
      <div>
        {!history.location.pathname.includes("buyTicket") &&
        !history.location.pathname.includes("payment") ? (
          <NavBar />
        ) : (
          ""
        )}
        <Router history={history}>
          <Switch>
            <Route path="/movies/:city" component={Movies} />
            <Route path="/movies/NCR" component={Movies} />
            <Route
              path="/bookMovie/:city/:id/buyTicket/:rowIndex/:colIndex/:day"
              component={BookMovieTicket}
            />
            <Route path="/payment" component={Payment} />
            <Route path="/bookMovie/:city/:id" component={BookMovie} />
            <Redirect to="/movies/NCR" />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default MainComp;
