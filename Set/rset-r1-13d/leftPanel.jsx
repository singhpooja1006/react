import React, { Component } from "react";
class LeftPanel extends Component {
  state = {
    languageClass: { css: "fa fa-chevron-up", isShow: true },
    formatClass: { css: "fa fa-chevron-down", isShow: false },
    genreClass: { css: "fa fa-chevron-down", isShow: false },
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    const { languages, formats, genres } = this.props;
    let lang = languages.find((n1) => n1.value === input.name);
    if (lang) lang.isSelected = input.checked;

    let format = formats.find((n1) => n1.value === input.name);
    if (format) format.isSelected = input.checked;
    let genre = genres.find((n1) => n1.value === input.name);
    if (genre) genre.isSelected = input.checked;
    this.props.onOptionChange(languages, formats, genres);
  };
  handleLanguage = () => {
    let { languageClass } = this.state;
    if (languageClass.css.includes("up")) {
      languageClass.css = "fa fa-chevron-down";
      languageClass.isShow = false;
    } else {
      languageClass.css = "fa fa-chevron-up";
      languageClass.isShow = true;
    }

    this.setState({ languageClass });
  };

  handleGenre = () => {
    let { genreClass } = this.state;
    if (genreClass.css.includes("up")) {
      genreClass.css = "fa fa-chevron-down";
      genreClass.isShow = false;
    } else {
      genreClass.css = "fa fa-chevron-up";
      genreClass.isShow = true;
    }

    this.setState({ genreClass });
  };

  handleFormat = () => {
    let { formatClass } = this.state;
    if (formatClass.css.includes("up")) {
      formatClass.css = "fa fa-chevron-down";
      formatClass.isShow = false;
    } else {
      formatClass.css = "fa fa-chevron-up";
      formatClass.isShow = true;
    }
    this.setState({ formatClass });
  };

  render() {
    const { languages, formats, genres } = this.props;
    return (
      <React.Fragment>
        <div className="row d-none d-lg-block">
          <div
            className="col-10 text-center ml-4 bg-white"
            style={{ padding: "5px" }}
          >
            <img
              src="https://i.ibb.co/Hry1kDH/17443322900502723126.jpg"
              className="img-fluid"
              alt=""
              style={{ borderRadius: "3px" }}
            />
          </div>
        </div>
        <br />
        <br />
        <div
          className="row ml-3 mr-2 pt-2 pb-2 d-none d-lg-block bg-white"
          style={{ borderRadius: "3px" }}
        >
          <div
            className="col"
            style={
              this.state.languageClass.isShow
                ? { color: "blue" }
                : { color: "black" }
            }
          >
            <i
              _ngcontent-wii-c3=""
              className={this.state.languageClass.css}
              onClick={() => this.handleLanguage()}
            ></i>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select Language{" "}
          </div>
        </div>
        {this.state.languageClass.isShow ? (
          <div>
            {languages.map((language) => (
              <div
                className="form-check ml-3 mr-2 pb-2 d-none d-lg-block bg-white"
                style={{ borderRadius: "3px" }}
              >
                <label className="form-check-label ml-3" for={language.value}>
                  <input
                    value={language.value}
                    onChange={this.handleChange}
                    className="form-check-input ng-untouched ng-pristine ng-valid"
                    name={language.value}
                    type="checkbox"
                    id={language.value}
                    checked={language.isSelected}
                  />
                  &nbsp;
                  {language.value}&nbsp;
                </label>
              </div>
            ))}
          </div>
        ) : (
          ""
        )}

        <br />
        <div
          className="row ml-3 mr-2 pt-2 pb-2 d-none d-lg-block bg-white"
          style={{ borderRadius: "3px" }}
        >
          <div
            className="col"
            style={
              this.state.formatClass.isShow
                ? { color: "blue" }
                : { color: "black" }
            }
          >
            <i
              className={this.state.formatClass.css}
              onClick={() => this.handleFormat()}
            ></i>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Format{" "}
          </div>
        </div>
        {this.state.formatClass.isShow ? (
          <div>
            {formats.map((format) => (
              <div
                className="form-check ml-3 mr-2 pb-2 d-none d-lg-block bg-white"
                style={{ borderRadius: "3px" }}
              >
                <label className="form-check-label ml-3" for={format.value}>
                  <input
                    value={format.value}
                    onChange={this.handleChange}
                    className="form-check-input ng-untouched ng-pristine ng-valid"
                    name={format.value}
                    type="checkbox"
                    id={format.value}
                    checked={format.isSelected}
                  />
                  &nbsp;
                  {format.value}&nbsp;
                </label>
              </div>
            ))}
          </div>
        ) : (
          ""
        )}
        <br />
        <div
          className="row ml-3 mr-2 pt-2 pb-2 d-none d-lg-block bg-white"
          style={{ borderRadius: "3px" }}
        >
          <div
            className="col"
            style={
              this.state.genreClass.isShow
                ? { color: "blue" }
                : { color: "black" }
            }
          >
            <i
              className={this.state.genreClass.css}
              onClick={() => this.handleGenre()}
            ></i>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Genre{" "}
          </div>
        </div>
        {this.state.genreClass.isShow ? (
          <div>
            {genres.map((genre) => (
              <div
                className="form-check ml-3 mr-2 pb-2 d-none d-lg-block bg-white"
                style={{ borderRadius: "3px" }}
              >
                <label className="form-check-label ml-3" for={genre.value}>
                  <input
                    value={genre.value}
                    onChange={this.handleChange}
                    className="form-check-input ng-untouched ng-pristine ng-valid"
                    name={genre.value}
                    type="checkbox"
                    id={genre.value}
                    checked={genre.isSelected}
                  />
                  &nbsp;
                  {genre.value}&nbsp;
                </label>
              </div>
            ))}
          </div>
        ) : (
          ""
        )}
      </React.Fragment>
    );
  }
}

export default LeftPanel;
