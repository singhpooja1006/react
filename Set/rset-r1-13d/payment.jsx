import React, { Component } from "react";
import { faChevronLeft, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
class Payment extends Component {
  state = {
    paymentData: {},
  };
  handleCloseMovieTicket = () => {
    this.props.history.go(-1);
  };

  async componentDidMount() {
    const { data: paymentData } = await axios.get(
      "https://us-central1-bkyow-22da6.cloudfunctions.net/app/details"
    );
    this.setState({ paymentData });
  }
  render() {
    const { paymentData } = this.state;
    return (
      <React.Fragment>
        <div className="container-fluid">
          <div className="row bg-dark pt-1 pb-3">
            <div
              className="col-lg-6  col-md-12 col-sm-12  text-left text-white"
              style={{ fontSize: "25px" }}
            >
              <div className="row ">
                <div className="col-lg-1 col-md-1 col-2  pt-2">
                  <FontAwesomeIcon
                    icon={faChevronLeft}
                    onClick={() => this.handleCloseMovieTicket()}
                  />
                </div>
                <div className="col-lg-11 col-md-4 col-7">
                  <div className="row">
                    <span>{paymentData.title}</span>
                  </div>
                  <div className="row" style={{ fontSize: "15px" }}>
                    {paymentData.movieHall}
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6 text-right text-white d-none d-lg-block">
              <div className="row pt-3 ">
                <div className="col" style={{ fontSize: "12px" }}>
                  <FontAwesomeIcon
                    icon={faTimes}
                    onClick={() => this.handleCloseMovieTicket()}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="row">
            <div
              className="col-lg-8 col-md-4 p-2 m-2"
              style={{ backgroundColor: "white" }}
            >
              <img
                alt="bookasmile-03"
                className="img-fluid"
                src="https://i.ibb.co/SK0HfNT/bookasmile-03.png"
              />
            </div>
            <div
              className="col-lg-3 col-md-6"
              style={{ backgroundColor: "white" }}
            >
              <div className="row">
                <div className="col">
                  <div className="row text-danger mt-1 ml-1">
                    {" "}
                    BOOKING SUMMARY{" "}
                  </div>
                  <br />
                  <div className="row ml-2">
                    <div className="col-6 text-left">Movie Name</div>
                    <div className="col-6 text-right">{paymentData.title}</div>
                  </div>
                  <div className="row ml-2">
                    <div class="col-5 text-left">Movie Hall</div>
                    <div className="col-7 text-right">
                      {paymentData.movieHall}
                    </div>
                  </div>
                  <div className="row ml-2">
                    <div className="col-6 text-left">Total Tickets</div>
                    <div className="col-6 text-right">
                      {paymentData.tickets ? paymentData.tickets.length : ""}
                    </div>
                  </div>
                  <div className="row ml-2">
                    <div className="col-6 text-left">Tickets</div>
                    <div className="col-6 text-right">
                      {paymentData.tickets ? paymentData.tickets.join(" ") : ""}
                    </div>
                  </div>
                  <div className="row ml-2">
                    <div className="col-4 text-left">Date</div>
                    <div className="col-8 text-right">{paymentData.date}</div>
                  </div>
                  <div className="row ml-2">
                    <div className="col-6 text-left">Time</div>
                    <div className="col-6 text-right">{paymentData.time}</div>
                  </div>
                  <div
                    className="row ml-2 pt-2 pb-2"
                    style={{ backgroundColor: "#fffcdc" }}
                  >
                    <div className="col-6 text-left">Amount Paid</div>
                    <div className="col-6 text-right">
                      Rs {paymentData.amount}
                    </div>
                  </div>
                  <div className="row ml-4 text-center">
                    <img
                      className="img-fluid"
                      src="https://i.ibb.co/CVHYxVK/images-q-tbn-ANd9-Gc-Qq-PT1-GB7-Cpvo3-WDDCi-Wt-Vto-Q-SLqp-Z9-B1x-D3-D69-WTj-MPyl-Chnd.png"
                      style={{ height: "300px", width: "300px" }}
                    />
                  </div>
                  <div className="row ml-2" style={{ fontSize: "10px" }}>
                    {" "}
                    You can cancel the tickets 4 hour(s) before the show.
                    Refunds will be done according to Cancellation Policy.{" "}
                  </div>
                </div>
                <br />
              </div>
            </div>
            <br />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Payment;
