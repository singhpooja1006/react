import React, { Component } from "react";
import axios from "axios";
import config from "./config.json";
import queryString from "query-string";
import "./dropDown.css";
import { faChevronLeft, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { post } from "jquery";
class BookMovieTicket extends Component {
  state = {
    moviesData: {},
    movieShowSeatsData: [],
    totalTicketPrice: 0,
    ticketCounts: 0,
  };
  async componentDidMount() {
    console.log("");
    let { city, id, rowIndex, colIndex, day } = this.props.match.params;
    city = city ? city : "NCR";
    const { data: moviesData } = await axios.get(
      config.apiEndPoint + city + "/" + id
    );
    const { data: seatsData } = await axios.get(
      "https://us-central1-bkyow-22da6.cloudfunctions.net/app/seats"
    );
    let movieShowSeatsData = [];

    for (
      let index = 0;
      index < moviesData.showTiming[day][rowIndex].timings.length;
      index++
    ) {
      movieShowSeatsData.push(seatsData[index % 3]);
    }

    this.setState({
      moviesData,
      rowIndex,
      colIndex,
      day,
      movieShowSeatsData,
    });
  }
  handleSelectTime = (colIndex) => {
    this.setState({ colIndex, totalTicketPrice: 0, ticketCounts: 0 });
  };
  handleCloseMovieTicket = () => {
    let { city, id } = this.props.match.params;
    let url = "/bookMovie/" + city + "/" + id;
    window.location = url;
  };
  handlePayment = async () => {
    const {
      movieShowSeatsData,
      rowIndex,
      moviesData,
      day,
      colIndex,
      totalTicketPrice,
    } = this.state;
    let tickets = [];
    movieShowSeatsData[colIndex].seats.map((seatRow) => {
      let selectedSeats = seatRow.seatList.filter(
        (seat) => seat.booked === true
      );
      selectedSeats.map((selectedSeat) =>
        tickets.push(seatRow.rowName + "" + selectedSeat.seatNo)
      );
    });
    //const apiEndPoint = config.apiEndPoint + "/payment";
    let { time } = queryString.parse(this.props.location.search);
    await post("https://us-central1-bkyow-22da6.cloudfunctions.net/app/seat", {
      title: moviesData.title,
      movieHall: moviesData.showTiming[day][rowIndex].name,
      tickets: tickets,
      amount: totalTicketPrice,
      time: moviesData.showTiming[day][rowIndex].timings[colIndex].name,
      date: time,
    });

    /* const {
      movieShowSeatsData,
      moviesData,
      rowIndex,
      day,
      ticketCounts,
      colIndex,
      totalTicketPrice,
    } = this.state;

     let { time } = queryString.parse(this.props.location.search);
    console.log(
      moviesData.title,
      moviesData.showTiming[day][rowIndex].name,
      ticketCounts,
      time,
      moviesData.showTiming[day][rowIndex].timings[colIndex].name,
      totalTicketPrice
    );

    
    console.log("Total Seats", tickets);*/
    window.location = "/payment";
  };

  render() {
    const { moviesData, rowIndex, day, colIndex } = this.state;
    let { time } = queryString.parse(this.props.location.search);

    return (
      <div className="container-fluid">
        <div className="row bg-dark pt-1 pb-3">
          <div
            className="col-lg-6  col-md-12 col-sm-12  text-left text-white"
            style={{ fontSize: "25px" }}
          >
            <div className="row ">
              <div className="col-lg-1 col-md-1 col-2  pt-2">
                <FontAwesomeIcon
                  icon={faChevronLeft}
                  onClick={() => this.handleCloseMovieTicket()}
                />
              </div>
              <div className="col-lg-11 col-md-4 col-7">
                <div className="row">
                  <span>{moviesData.title}</span>
                </div>
                <div className="row" style={{ fontSize: "15px" }}>
                  {moviesData.showTiming
                    ? moviesData.showTiming[day][rowIndex].name
                    : ""}
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 text-right text-white d-none d-lg-block">
            <div className="row pt-3 ">
              <div className="col" style={{ fontSize: "12px" }}>
                {this.state.ticketCounts} Tickets
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <FontAwesomeIcon
                  icon={faTimes}
                  onClick={() => this.handleCloseMovieTicket()}
                />
              </div>
            </div>
          </div>
        </div>
        <div
          className="row  pt-3 pb-3"
          style={{ backgroundColor: "#f5f5fa", fontSize: "12px" }}
        >
          <div className="col">
            <div className="row">
              <div className="col-lg-6 col-12 ml-1 text-center text-lg-left text-md-left">
                {time}{" "}
                {moviesData.showTiming
                  ? moviesData.showTiming[day][rowIndex].timings[colIndex].name
                  : ""}
              </div>
            </div>
            <div className="row">
              <div className="col">
                {moviesData.showTiming
                  ? this.renderShowTiminig(
                      moviesData.showTiming[day][rowIndex].timings
                    )
                  : ""}
              </div>
            </div>
          </div>
        </div>
        <div>{this.renderMovieSeats()}</div>

        <br />
        <br />
        <div className="row">
          <div className="col text-center">
            <span>
              <svg
                xspace="preserve"
                xlinkHref="http://www.w3.org/1999/xlink"
                enable-background="new 0 0 100 100"
                height="20px"
                style={{ fill: "rgba(0, 0, 0, 0.6)" }}
                version="1.1"
                viewBox="0 0 260 20"
                width="260px"
                x="0px"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
              >
                <g fill="none" fill-rule="evenodd" opacity=".3">
                  <g fill="#E1E8F1">
                    <path d="M27.1 0h205.8L260 14.02H0z" id="da"></path>
                  </g>
                  <path
                    d="M27.19.33L1.34 13.7h257.32L232.81.32H27.2z"
                    stroke="#4F91FF"
                    stroke-width=".65"
                  ></path>
                  <path
                    d="M28.16 2.97h203.86l17.95 9.14H10.35z"
                    fill="#8FB9FF"
                  ></path>
                  <g fill="#E3ECFA">
                    <path d="M0 13.88h260l-3.44 6.06H3.44z" id="db"></path>
                  </g>
                  <path
                    d="M.56 14.2l3.07 5.41h252.74l3.07-5.4H.56z"
                    stroke="#4F91FF"
                    stroke-width=".65"
                  ></path>
                </g>
              </svg>
            </span>
            <br />
            <span style={{ fontSize: "10 px" }}>
              <p>All Eyes this way please!</p>
            </span>
          </div>
        </div>
        {this.state.totalTicketPrice > 0 ? (
          <div className="row fixed-bottom">
            <div className="col-4"></div>
            <div className="col-lg-4 col-6 ml-3 mr-2 text-center">
              <button
                class="btn btn-primary btn-block btn-md"
                onClick={() => this.handlePayment(moviesData)}
              >
                Pay Rs. {this.state.totalTicketPrice}
              </button>
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }

  renderShowTiminig = (movieTimeList) => {
    let id = movieTimeList.filter(
      (time, index) => this.state.colIndex === index // ? "timingcss-true" : "timingcss-false"
    );
    return (
      <React.Fragment>
        {movieTimeList.map((time, index) => (
          <button
            className="btn btn-md btn btn-outline-success m-1"
            //routerlinkactive="router-link-active"
            style={{
              fontSize: "12px",
            }}
            id={
              this.state.colIndex === index
                ? "timingcss-true"
                : "timingcss-false"
            }
            //tabindex="0"
            onClick={() => this.handleSelectTime(index)}
          >
            {time.name}
          </button>
        ))}
      </React.Fragment>
    );
  };
  renderMovieSeats = () => {
    let { movieShowSeatsData, colIndex } = this.state;

    if (movieShowSeatsData.length > 0) {
      let reclinerSeatsData = movieShowSeatsData[colIndex % 3].seats.filter(
        (seat) => seat.price === 420
      );

      let goldSeatsData = movieShowSeatsData[colIndex % 3].seats.filter(
        (seat) => seat.price === 250
      );

      return (
        <React.Fragment>
          <div className="row pb-2 ml-4 mr-4 no-gutters">
            <div className="col text-secondary text-left border-bottom ml-5">
              RECLINER - Rs 420.00
            </div>
          </div>
          <div>
            {movieShowSeatsData[colIndex].seats
              .filter((seat) => seat.price === 420)
              .map((seats) => (
                <div className="row ml-4 mr-4 no-gutters">
                  <div className="col-1 text-right mr-1">{seats.rowName}</div>
                  <div className="col-10 text-left">
                    <div
                      style={{
                        marginLeft: "1",
                        marginRight: "1",
                        marginTop: "1",
                        marginBottom: "1",
                        float: "left",
                        width: "19",
                        height: "19",
                      }}
                    >
                      {seats.seatList.map((seat) =>
                        this.generateSeatButton(seat, seats.rowName)
                      )}
                    </div>
                  </div>
                </div>
              ))}
          </div>
          <br />
          <div className="row pb-2 ml-4 mr-4">
            <div className="col text-secondary text-left border-bottom ml-5">
              GOLD - Rs 250.00
            </div>
          </div>
          <div>
            {movieShowSeatsData[colIndex].seats
              .filter((seat) => seat.price === 250)
              .map((seats) => (
                <div className="row ml-4 mr-4 no-gutters">
                  <div className="col-1 text-right mr-1">{seats.rowName}</div>
                  <div className="col-3">
                    <div
                      style={{
                        marginLeft: "2",
                        marginRight: "2",
                        marginTop: "4",
                        marginBottom: "4",
                        float: "left",
                        width: "21",
                        height: "21",
                      }}
                    >
                      {seats.seatList.map((seat, index) =>
                        index < 7
                          ? this.generateSeatButton(seat, seats.rowName)
                          : ""
                      )}
                    </div>
                  </div>
                  <div class="col-4">
                    <div
                      style={{
                        marginLeft: "2",
                        marginRight: "2",
                        marginTop: "4",
                        marginBottom: "4",
                        float: "left",
                        width: "21",
                        height: "21",
                      }}
                    >
                      {seats.seatList.map((seat, index) =>
                        index >= 7 && index <= 17
                          ? this.generateSeatButton(seat, seats.rowName)
                          : ""
                      )}
                    </div>
                  </div>
                  <div className="col-3">
                    <div
                      style={{
                        marginLeft: "2",
                        marginRight: "2",
                        marginTop: "4",
                        marginBottom: "4",
                        float: "left",
                        width: "21",
                        height: "21",
                      }}
                    >
                      {seats.seatList.map((seat, index) =>
                        index >= 18
                          ? this.generateSeatButton(seat, seats.rowName)
                          : ""
                      )}
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </React.Fragment>
      );
    }
  };
  handleMovieTicketBookSeat = (seatNo, rowName) => {
    const { movieShowSeatsData, colIndex } = this.state;
    let { totalTicketPrice, ticketCounts } = this.state;

    const index = movieShowSeatsData[colIndex].seats.findIndex(
      (seat) => seat.rowName === rowName
    );
    let selectedSeatList = movieShowSeatsData[colIndex].seats[index].seatList;
    const seatIndex = selectedSeatList.findIndex(
      (seat) => seat.seatNo === seatNo
    );
    if (selectedSeatList[seatIndex].booked) {
      selectedSeatList[seatIndex].booked = false;
      totalTicketPrice -= +movieShowSeatsData[colIndex].seats[index].price;
      ticketCounts -= 1;
    } else {
      selectedSeatList[seatIndex].booked = true;
      totalTicketPrice += +movieShowSeatsData[colIndex].seats[index].price;
      ticketCounts += 1;
    }
    movieShowSeatsData[colIndex].seats[index].seatList = selectedSeatList;

    this.setState({ movieShowSeatsData, totalTicketPrice, ticketCounts });
  };
  generateSeatButton = (seat, rowName) => {
    let style = {
      fontSize: "10px",
      width: "21px",
      height: "21px",
      borderRadius: "5px",
      marginTop: "4px",
      marginRight: "2px",
      marginBottom: "8px",
      marginLeft: "2px",
    };
    if (seat.booked) {
      style.backgroundColor = "#2dc492";
    } else if (seat.available === false) {
      style.backgroundColor = "lightgray";
    }

    return (
      <button
        style={style}
        className="btn btn-sm  btn-outline-secondary mr-1 p-1  d-inline-block text-center"
        onClick={() => this.handleMovieTicketBookSeat(seat.seatNo, rowName)}
      >
        <span>{seat.seatNo}</span>
      </button>
    );
  };
}

export default BookMovieTicket;
