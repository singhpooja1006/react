import React, { Component } from "react";
import { Route, Switch, Redirect, Router } from "react-router-dom";
import NavBar from "./bookMyShowNavbar";
import Movies from "./movies";
import history from "./history";
import BookMovie from "./bookMovie";
class MainComp extends Component {
  state = {};
  render() {
    return (
      <div>
        <NavBar />
        <Router history={history}>
          <Switch>
            <Route path="/movies/NCR" component={Movies} />
            <Route path="/movies/:city" component={Movies} />
            <Route path="/bookMovie/:city/:id" component={BookMovie} />
            <Redirect to="/movies/NCR" />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default MainComp;
