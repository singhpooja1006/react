import React, { Component } from "react";
import axios from "axios";
import config from "./config.json";
import SlideImage from "./slideImage";
import MovieNavBar from "./movieNavBar";
import LeftPanel from "./leftPanel";
import BookMovie from "./bookMovie";
import history from "./history";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
class Movies extends Component {
  state = {
    moviesData: [],
    totalMovieList: [],
    formats: [
      { value: "2D", isSelected: false },
      { value: "3D", isSelected: false },
      { value: "4DX", isSelected: false },
    ],
    languages: [
      { value: "Hindi", isSelected: false },
      { value: "English", isSelected: false },
      { value: "Punjabi", isSelected: false },
      { value: "Tamil", isSelected: false },
    ],
    genres: [
      { value: "Action", isSelected: false },
      { value: "Adventure", isSelected: false },
      { value: "Biography", isSelected: false },
      { value: "Comedy", isSelected: false },
    ],
  };

  async componentDidMount() {
    let city = this.props.match.params.city;

    city = city ? city : "NCR";
    const { data: totalMovieList } = await axios.get(config.apiEndPoint + city);
    const { data: moviesData } = await axios.get(
      config.apiEndPoint + city + this.props.location.search
    );

    this.setState({ moviesData, totalMovieList });
  }
  findSelectedMovieID = (movie) => {
    console.log("Index", this.state.totalMovieList);
    return this.state.totalMovieList.findIndex((m) => m.title === movie.title);
  };
  async componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.match.params !== this.props.match.params ||
      prevProps.location.search !== this.props.location.search
    ) {
      const { data: moviesData } = await axios.get(
        config.apiEndPoint +
          this.props.match.params.city +
          this.props.location.search
      );

      this.setState({ moviesData });
    }
  }
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  handleOptionChange = (languages, formats, genres) => {
    let language = this.buildQueryString(languages);
    let format = this.buildQueryString(formats);
    let genre = this.buildQueryString(genres);
    this.setState({ languages, formats, genres });
    this.calURL("", language, format, genre);
  };
  buildQueryString(optionsData) {
    let filterData = optionsData.filter((n1) => n1.isSelected);
    let arrayData = filterData.map((n1) => n1.value);
    return arrayData.join(",");
  }
  calURL = (params, language, format, genre) => {
    let path = "/movies";
    const city = this.props.match.params.city;
    if (city) path = path + "/" + city;

    params = this.addToParams(params, "lang", language);
    params = this.addToParams(params, "format", format);
    params = this.addToParams(params, "genre", genre);

    this.props.history.push({
      pathname: path,
      search: params,
    });
  };

  render() {
    return (
      <div className="mt-3 container-fluid">
        <SlideImage />
        <MovieNavBar />

        <div className="row bg-light">
          <div className="col-3 d-none d-lg-block">
            <LeftPanel
              formats={this.state.formats}
              languages={this.state.languages}
              genres={this.state.genres}
              onOptionChange={this.handleOptionChange}
            />
          </div>
          <div className="col-lg-9 col-12">{this.renderMovieView()}</div>
        </div>
      </div>
    );
  }
  renderMovieView = () => {
    if (this.state.moviesData) {
      return (
        <React.Fragment>
          <div className="row ">
            {this.renderMoviesData(0)}
            {this.renderMoviesData(1)}
            {this.renderMoviesData(2)}
          </div>
          <div className="row">
            {this.renderMoviesData(3)}
            {this.renderMoviesData(4)}
            {this.renderMoviesData(5)}
          </div>
          <div className="row">
            {this.renderMoviesData(6)}
            {this.renderMoviesData(7)}
            {this.renderMoviesData(8)}
          </div>
          <div className="row">
            {this.renderMoviesData(9)}
            {this.renderMoviesData(10)}
            {this.renderMoviesData(11)}
          </div>
        </React.Fragment>
      );
    }
  };
  renderMoviesData(index) {
    const movieData = this.state.moviesData[index];
    if (movieData) {
      console.log("Data", movieData);
      console.log("Selected City", this.props.match.params);
      const city = this.props.match.params.city;
      let id = this.findSelectedMovieID(movieData);
      const url = "/bookMovie/" + city + "/" + id;
      return (
        <div
          className="col-lg-3 col-md-3 col-5  ml-4 ml-md-5 mr-md-1 ml-lg-1 bg-white"
          onClick={() => history.push(url)}
        >
          <div className="card">
            <img
              className="card-img-top text-center"
              alt=""
              src={movieData.img}
              style={{
                width: "100%",
                height: "100%",
                objectFit: "cover",
              }}
            />
          </div>
          <div className="row m-2">
            <div className="col-5">
              {" "}
              <FontAwesomeIcon icon={faHeart} style={{ color: "#ff0000" }} />
              {movieData.rating}
            </div>
            <div className="col-7">{movieData.title}</div>
          </div>
          <div className="row">
            <div className="col-5">{movieData.votes}</div>
            <div className="col-7">{movieData.desc}</div>
          </div>
        </div>
      );
    }
  }
}
export default Movies;
