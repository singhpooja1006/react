import React, { Component } from "react";
import { Link } from "react-router-dom";
class MovieNavBar extends Component {
  state = {};
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <ul className="navbar-nav">
          <Link className="navbar-brand" to="#">
            Movies
          </Link>

          <li className="nav-item">
            <Link className="nav-link" to="#">
              Now Showing
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="#">
              Coming Soon
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="#">
              Exclusive
            </Link>
          </li>
        </ul>
      </nav>
    );
  }
}

export default MovieNavBar;
