import React, { Component } from "react";
import { Route, Switch, Redirect, Router } from "react-router-dom";
import NavBar from "./bookMyShowNavbar";
import Movies from "./movies";
import history from "./history";
import BookMovie from "./bookMovie";
import BookMovieTicket from "./bookMovieTicket";
class MainComp extends Component {
  render() {
    return (
      <div>
        {!history.location.pathname.includes("buyTicket") ? <NavBar /> : ""}
        <Router history={history}>
          <Switch>
            <Route path="/movies/:city" component={Movies} />
            <Route path="/movies/NCR" component={Movies} />
            <Route
              path="/bookMovie/:city/:id/buyTicket/:rowIndex/:colIndex/:day"
              component={BookMovieTicket}
            />
            <Route path="/bookMovie/:city/:id" component={BookMovie} />
            <Redirect to="/movies/NCR" />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default MainComp;
