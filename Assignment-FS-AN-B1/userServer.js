var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
    next();
});
//const port  = 2410;
var port = process.env.PORT || 2410;
app.listen(port,()=>console.log("Listening on port : ",port))



let userDetails= [{ email : "jack@gmail.com",password:"jack123",fname:"Jack",lname:"Singh",title:"Mr.",mobileNo:"9876543465",country: "India"},
{ email : "test@test.com",password:"test123",fname:"Test",lname:"Dev",title:"Mr.",mobileNo:"9655456465",country:"India"}]

app.get("/user/:email",function (req, res) {
  var email = req.params.email;
  var user = userDetails.find(obj=> obj.email === email);
  if(user){
    res.send({status : true})
  }else{
    res.send({status : false})
  }
});
app.post("/login", function (req, res) {
  const user = {
  email:req.body.email,
  password: req.body.password,
  fname: req.body.fname,
  lname: req.body.lname,
  title: req.body.title,
  mobileNo: req.body.mobileNo,
  };
  userDetails.push(user);
  res.send(user);
});

 app.post("/user", function (req, res) {
 var email = req.body.email
 var password = req.body.password
 var user = userDetails.find(obj=> obj.email === email && 
   obj.password === password);
 res.send(user);
});
app.get("/userDetails/:email",function (req, res) {
  var email = req.params.email;
  var user = userDetails.find(obj=> obj.email === email);
  res.send(user)
});

app.put("/edit/:email", function (req, res) {
  var email = req.params.email;
  let  index = userDetails.findIndex((obj)=> obj.email === email);
    if(index !== -1){
      userDetails[index] = req.body;
      res.send("User Updated Successfully");
    }else{
      res.status(401).end()
    }
});