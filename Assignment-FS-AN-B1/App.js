import React, { Component } from 'react';
import './App.css';
import {Switch, Route, Redirect} from 'react-router-dom';
import RightPanel from './components/rightPanel';
import YatrairSearch from './components/airSearch';
import BookFlight from './components/bookFlight';
import Payment from './components/payment';
import HotelSearch from './components/hotelSearch';
import ChooseRoom from './components/chooseRoom';
import HotelBooking from './components/hotelBooking';
import HotelPayment from './components/hotelPayment';
import SignUp from './components/signUp';
import Navbar from './components/navbar';
import Register from './components/register';
import Dashboard from './components/dashboard';
import Login from './components/login';
import Logout from './components/logout';
import Ecash from './components/eCashPage';
import ProfilePage from './components/profilePage';
class App extends Component {
  state = { departure : "", arival : ""}
  handleSerachFlight = (departure, arival) => {
    this.setState({departure, arival})
  }
  componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    this.setState({ user });
  }
  render() { 
    const { user } = this.state;
    return (
      <React.Fragment>
      <Navbar user={user}/>
      <Switch>
        <Route path="/yatra/common/register" component={Register}/>
        <Route path="/yatra/login" component={Login}/>
        <Route path="/yatra/logout" component={Logout} />
        <Route path="/yatra/common" component={SignUp}/>
        <Route path="/yatra/allBookings" component={Dashboard}/>
        <Route path="/yatra/eCash" component={Ecash}/>
        <Route path="/yatra/profile" component={ProfilePage}/>
        <Route path="/checkOut" component={HotelPayment}/>
        <Route path="/hotelBooking" component={HotelBooking}/>
        <Route path="/yatra/hotelSearch" component={HotelSearch}/>
        <Route path="/roomDetails" component={ChooseRoom}/>
        <Route path="/payment" component={Payment}/>
        <Route path="/booking" component={BookFlight}/>
        <Route path="/yatra/airSearch" component={YatrairSearch}/>
       
        <Route
          exact path='/yatra'
         component={RightPanel}
        />
            <Route exact path={'/'} render={() => {
              return <Redirect to={'/yatra'}/>
              }}/>
      </Switch>
      </React.Fragment>
      
    );
  }
}
 
export default App;
  