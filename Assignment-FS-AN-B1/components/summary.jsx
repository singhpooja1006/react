import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
class Summary extends Component {
  state = {
    eCashData: [],
    months: [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ],
  };
  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    let apiEndPoint =
      config.hotelApiEndPoint + "/getBookingDetails/" + user.email;
    const { data: eCashData } = await axios.get(apiEndPoint);
    this.setState({ eCashData });
  }
  render() {
    return (
      <React.Fragment>
        <div className="row bg-light">
          <div className="col-4 mt-2 mb-2">VIEW ECASH SUMMARY</div>
          <div className="col-4"></div>
          <div className="mt-2 mb-2 col-4">CURRENT BALANCE ₹.0</div>
        </div>
        <div className="row mt-3 mb-3 border ml-1 mr-1">
          <div className="col-3 bg-light text-center">
            <div
              className="row mt-4 mb-4 ml-1 mr-1 border"
              style={{ backgroundColor: "white" }}
            >
              <div className="text-danger mt-2 ml-4">REDEEMABLE</div>
              <br />
              <div className="ml-5">
                <b>₹0</b>
              </div>
              <hr className="col-7" />
            </div>
            <div
              className="row mt-4 mb-4 ml-1 mr-1 border"
              style={{ backgroundColor: "white" }}
            >
              <div className="text-danger mt-2 ml-4">PENDING</div>
              <br />
              <div className="ml-5">
                <b>₹0</b>
              </div>
              <hr className="col-7" />
            </div>
          </div>
          <div className="col-9">
            <div className="row mt-3">
              <div className="col-3">
                Transferable&nbsp;<i className="fa fa-info-circle"></i>
              </div>
              <div className="col-3">
                <button className="btn btn-danger">Transfer</button>
              </div>

              <div className="col-3">
                Convertible&nbsp;<i className="fa fa-info-circle"></i>
              </div>
              <div className="col-3">
                <button className="btn mr-1">Convert</button>
              </div>
            </div>
            <div className="row">
              <div className="col-3">₹0</div>
              <div className="col-3"></div>
              <div className="col-3">₹0</div>
            </div>
            <hr />
            <div className="row mt-2 mb-2 mr-1">
              <div className="col-2">Date</div>
              <div className="col-3">Description</div>
              <div className="col-4">Reference Number</div>
              <div className="col-2">Credit</div>
              <div className="col-1">Debit</div>
            </div>

            {this.state.eCashData.map((data) => (
              <React.Fragment>
                <hr />
                <div className="row mt-2 mb-2 mr-1 text-secondary">
                  <div className="col-2">
                    {this.state.months[new Date(data.checkInDate).getMonth()] +
                      " " +
                      new Date(data.checkInDate).getDate() +
                      " " +
                      new Date(data.checkInDate).getFullYear()}
                  </div>
                  <div className="col-3">
                    {this.renderDiscription(data.checkInDate)}
                  </div>
                  <div className="col-4 text-truncate">{data.bookingRef}</div>
                  <div className="col-2">
                    ₹
                    {data.selectedRoom.roomtype.reduce(
                      (acc, curr) => acc + curr.eCash,
                      0
                    )}
                  </div>
                  <div className="col-1">₹0</div>
                </div>
                <hr />
              </React.Fragment>
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
  renderDiscription(checkIn) {
    let checkinDate = new Date(checkIn);
    let expDate = new Date();
    let currentDate = new Date();
    expDate.setDate(checkinDate.getDate() + 45);
    if (expDate.getTime() === currentDate.getTime()) {
      return "Ecash expired";
    } else {
      return (
        "Ecash using promocode (Expired on " +
        this.state.months[expDate.getMonth()] +
        " " +
        expDate.getDay() +
        "," +
        expDate.getFullYear() +
        " )"
      );
    }
  }
}

export default Summary;
