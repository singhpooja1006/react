import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
class MyBookings extends Component {
  state = {
    data: [
      "ALL",
      "FLIGHTS",
      "HOTELS",
      "HOMESTAYS",
      "FLIGHTS + HOTELS",
      "BUSES",
      "TRAINS",
      "ACTIVITIES",
      "HOLIDAYS",
    ],
    sortData: ["Upcoming", "Completed", "Cancelled", "Booking Date"],
    selected: "",
    bookingDetails: [],
    months: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
    weeks: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    view: 0,
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    if (input.name === "selected") this.setState({ selected: input.value });
  };
  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    let apiEndPoint =
      config.hotelApiEndPoint + "/getBookingDetails/" + user.email;
    const { data: bookingDetails } = await axios.get(apiEndPoint);
    console.log("HotelName", bookingDetails);
    this.setState({ bookingDetails });
  }
  handleFareDetails = (index, lable) => {
    this.setState({ view: 1, selectedDetailIndex: index });
    this.props.handleFareDetails(lable);
  };
  render() {
    return this.state.view === 1 ? (
      this.renderFareDetails()
    ) : (
      <React.Fragment>
        <div className="row bg-light">
          <div className="text-left mt-2">
            <i
              className="fa fa-toggle-left"
              id="agd"
              style={{ fontSize: "28px", color: "lightgrey" }}
            ></i>
          </div>{" "}
          {this.state.data.map((item) => (
            <div className="m-2 text-secondary" style={{ fontSize: "14px" }}>
              {item}
            </div>
          ))}
          <div className="text-right mt-2">
            <i
              className="fa fa-toggle-right"
              style={{ fontSize: "28px", color: "lightgrey" }}
            ></i>
          </div>
        </div>
        <div className="row mt-2">
          <div className="col-9"></div>
          <div className="col-3">Filter/ Sort By</div>
        </div>
        <div className="row">
          <div className="col-9"></div>
          <div className="col-3">
            <select
              className="browser-default custom-select"
              value={this.state.selected}
              onChange={this.handleChange}
              name="selected"
            >
              <option>Relevance</option>
              {this.state.sortData.map((item) => (
                <option key={item}>{item}</option>
              ))}
            </select>
          </div>
        </div>

        {this.state.bookingDetails.length > 0 ? (
          this.renderBookingHotel()
        ) : (
          <div className="row mt-2">
            <div className="col-4"></div>
            <div className="col-8 text-secondary">No Records Found.</div>
          </div>
        )}
      </React.Fragment>
    );
  }
  renderBookingHotel = () => {
    const { bookingDetails } = this.state;
    if (bookingDetails.length > 0) {
      return (
        <React.Fragment>
          {bookingDetails.map((hotel, index) => (
            <React.Fragment>
              <div
                className="row mt-2 pt-1 pb-1 ml-1 mr-1"
                style={{
                  backgroundColor: "lightgrey",
                }}
              >
                <div className="col-1 text-center mt-1 d-none d-lg-block">
                  <i
                    className="fa fa-hotel"
                    style={{ fontSize: "26px", color: "gray" }}
                  ></i>
                </div>
                <div className="col-8">
                  <div className="row">
                    <b>{hotel.name}</b>
                  </div>

                  <div className="row" id="abc10">
                    <div className="h6 text-secondary">
                      {hotel.rooms.length}
                      ROOM(S)|{hotel.noOfNights} NIGHT(S)
                    </div>
                  </div>
                </div>
                <div className="mt-1 ml-1 text-secondary">
                  Booked On:
                  {new Date(hotel.checkInDate).getDate() +
                    " " +
                    this.state.months[new Date(hotel.checkInDate).getMonth()] +
                    " " +
                    new Date(hotel.checkInDate).getFullYear()}
                  | Booking Ref No. {hotel.bookingRef}
                </div>
              </div>
              <div className="row border ml-1 mr-1">
                <div className="col-4 mt-4 mb-3">
                  <b>Check In</b>
                  <div className="text-secondary">
                    {new Date(hotel.checkInDate).getDate() +
                      " " +
                      this.state.months[
                        new Date(hotel.checkInDate).getMonth()
                      ] +
                      " " +
                      new Date(hotel.checkInDate).getFullYear()}
                  </div>
                </div>
                <div className="col-3 mt-4 mb-3">
                  <b>Check Out</b>
                  <div className="text-secondary">
                    {new Date(hotel.checkOutDate).getDate() +
                      " " +
                      this.state.months[
                        new Date(hotel.checkOutDate).getMonth()
                      ] +
                      " " +
                      new Date(hotel.checkOutDate).getFullYear()}
                  </div>
                </div>
                <div
                  className="col-1 mt-4 mb-3"
                  style={{ borderLeft: "1px inset" }}
                ></div>
                <div className="col-4 mt-5 mb-5">
                  <button
                    className="btn btn-danger"
                    onClick={() =>
                      this.handleFareDetails(index, "Fare Details")
                    }
                  >
                    Fare Details
                  </button>
                  <button
                    className="btn btn-danger ml-2"
                    onClick={() =>
                      this.handleFareDetails(index, "Itinerary Details")
                    }
                  >
                    Itinerary
                  </button>
                </div>
                <hr className="col-10" />
                <div className="col-1"></div>
                <div className="col-3 text-center ml-1 mt-4 mb-4">
                  <i
                    className="fa fa-check-square-o"
                    style={{ fontSize: "30px" }}
                  ></i>
                  <div>Print Invoice</div>
                </div>
                <div
                  className="mt-3 mb-3"
                  style={{ borderLeft: "1px inset" }}
                ></div>
                <div className="col-3 mt-4 mb-4 text-center">
                  <i
                    className="fa fa-location-arrow"
                    style={{ fontSize: "30px" }}
                  ></i>
                  <div>Get Directions</div>
                </div>
                <div
                  className="mt-3 mb-3"
                  style={{ borderLeft: "1px inset" }}
                ></div>
                <div className="col-2 mt-4 mb-4 text-center">
                  <i className="fa fa-refresh" style={{ fontSize: "30px" }}></i>
                  <div>Re-Book</div>
                </div>
              </div>
            </React.Fragment>
          ))}
        </React.Fragment>
      );
    }
  };
  renderFareDetails = () => {
    const { bookingDetails, selectedDetailIndex } = this.state;
    if (selectedDetailIndex >= 0) {
      let hotel = bookingDetails[selectedDetailIndex];
      return (
        <React.Fragment>
          <div
            className="row mt-2 pt-1 pb-1 ml-1 mr-1"
            style={{
              backgroundColor: "lightgrey",
            }}
          >
            <div className="col-1 text-center mt-1 d-none d-lg-block">
              <i
                className="fa fa-hotel"
                style={{ fontSize: "26px", color: "gray" }}
              ></i>
            </div>
            <div className="col-7 mt-1">
              <div className="row">
                <b>{hotel.name}</b>
              </div>
            </div>
            <div className="mt-1 ml-3 text-secondary">
              Booked On:&nbsp;
              {this.state.weeks[new Date(hotel.checkInDate).getDay()]}
              ,&nbsp;
              {new Date(hotel.checkInDate).getDate() +
                " " +
                this.state.months[new Date(hotel.checkInDate).getMonth()] +
                " " +
                new Date(hotel.checkInDate).getFullYear()}{" "}
              | Booking Ref No. {hotel.bookingRef}
            </div>
          </div>
          <div className="row border ml-1 mr-1">
            <div className="col-4 mt-4 mb-3">
              <b>Check In</b>
              <div style={{ fontSize: "12px" }} className="text-secondary">
                {hotel.CHECKIN}&nbsp;
                {this.state.months[new Date(hotel.checkInDate).getMonth()] +
                  " " +
                  new Date(hotel.checkInDate).getDate() +
                  " "}
                ,{this.state.weeks[new Date(hotel.checkInDate).getDay()]}
              </div>
            </div>
            <div className="col-4 mt-4 mb-3">
              <b>Check Out</b>
              <div style={{ fontSize: "12px" }} className="text-secondary">
                {hotel.CHECKOUT}&nbsp;
                {this.state.months[new Date(hotel.checkInDate).getMonth()] +
                  " " +
                  new Date(hotel.checkInDate).getDate() +
                  " "}
                ,{this.state.weeks[new Date(hotel.checkInDate).getDay()]}
              </div>
            </div>
            <div className="col-3 col-lg-2 mt-4 mb-3">
              <b>{hotel.rooms.length} Room</b>
              <div style={{ fontSize: "10px" }} className="text-secondary">
                {hotel.noOfNights} Night(s)/{hotel.noOfNights + 1} Day(s)
              </div>
            </div>
            <div className="col-2 mt-4 mb-3">
              <b>Status</b>
              <div style={{ fontSize: "12px" }} className="text-secondary">
                Booking
              </div>
            </div>
            <hr className="col-11" />
            <div className="col-2 text-center mt-4 mb-4">
              <i
                className="fa fa-check-square-o"
                style={{ fontSize: "30px", color: "red" }}
              ></i>
              <div>Print Invoice</div>
            </div>
            <div
              className="mt-3 mb-3"
              style={{ borderLeft: "1px inset" }}
            ></div>
            <div className="col-3 mt-4 mb-4 text-center">
              <i
                className="fa fa-location-arrow"
                style={{ fontSize: "30px", color: "red" }}
              ></i>
              <div>Get Directions</div>
            </div>
            <div
              className="mt-3 mb-3"
              style={{ borderLeft: "1px inset" }}
            ></div>

            <div className="col-2 mt-4 mb-4 text-center">
              <i
                className="fa fa-refresh"
                style={{ fontSize: "30px", color: "red" }}
              ></i>
              <div>Re-Book</div>
            </div>
            <div
              className="mt-3 mb-3"
              style={{ borderLeft: "1px inset" }}
            ></div>

            <div className="col-2 mt-4 mb-4 text-center">
              <i
                className="fa fa-file-text-o"
                style={{ fontSize: "30px", color: "red" }}
              ></i>
              <div>Fare Details</div>
            </div>
            <div
              className="mt-3 mb-3"
              style={{ borderLeft: "1px inset" }}
            ></div>

            <div className="col-2 mt-4 mb-4 text-center">
              <i
                className="fa fa-pencil"
                style={{ fontSize: "30px", color: "red" }}
              ></i>
              <div>Write To Us</div>
            </div>
          </div>
          <div className="row border ml-1 mr-1 mt-4">
            <b className="ml-3 mt-2">Your Booking Details</b>
            <hr className="col-11" />
            <div className="col-3 col-lg-4 mt-1 mb-3">
              <i className="fa fa-hotel" style={{ color: "lightgrey" }}></i>
              &nbsp;
              <b style={{ fontSize: "14px" }}>{hotel.name}</b>
            </div>
            <div className="col-2 col-lg-2 mt-1 mb-3">
              <b>Duration</b>
              <div style={{ fontSize: "10px" }} className="text-secondary">
                {hotel.noOfNights} Night(s)/{hotel.noOfNights + 1} Day(s)
              </div>
            </div>
            <div className="col-4 col-lg-3 mt-1 mb-3">
              <b>Check In</b>
              <div style={{ fontSize: "12px" }} className="text-secondary">
                {hotel.CHECKIN}&nbsp;
                {this.state.months[new Date(hotel.checkInDate).getMonth()] +
                  " " +
                  new Date(hotel.checkInDate).getDate() +
                  " "}
                ,{this.state.weeks[new Date(hotel.checkInDate).getDay()]}
              </div>
            </div>
            <div className="col-4 mt-1 col-lg-3 mb-3">
              <b>Check Out</b>
              <div style={{ fontSize: "12px" }} className="text-secondary">
                {hotel.CHECKOUT}
                {this.state.months[new Date(hotel.checkInDate).getMonth()] +
                  " " +
                  new Date(hotel.checkInDate).getDate() +
                  " "}
                ,{this.state.weeks[new Date(hotel.checkInDate).getDay()]}
              </div>
            </div>
            <div
              className="row ml-4 col-lg-11 col-12 mr-2 mt-2 mb-2"
              style={{ backgroundColor: "lightgrey" }}
            >
              <i className="fa fa-map-marker" style={{ fontSize: "35px" }}></i>
              <div className="mt-1 mb-1">{hotel.fullLoc}</div>
            </div>
            <div className="row ml-4 col-lg-11 border col-12 mr-2 mt-3 mb-3">
              <div className="col-3 mt-3 mb-3 ml-2">
                <b>Inclusions</b>
              </div>
              <div className="col-6"></div>
              <div className="col-9 mb-4 text-secondary">
                <i className="fa fa-check" style={{ color: "grey" }}></i>
                &nbsp; Breakfast, Complimentary WiFi Internet
              </div>
            </div>
            <div className="row ml-1 mt-3 mb-3 mr-2 col-lg-11 col-12">
              <div className="col-3 border">
                <b>Lead Guest</b>
              </div>
              <div className="col-4 border">
                <b>Room Type</b>
              </div>
              <div className="col-3 border">
                <b>Guest Details</b>
              </div>
              <div className="col-2 border">
                <b>Status</b>
              </div>
              <div className="col-3 border text-secondary">
                {hotel.rooms[0].fname}&nbsp;
                {hotel.rooms[0].lname}
              </div>
              <div className="col-4 border text-secondary">
                {hotel.selectedRoom.type}
              </div>
              {hotel.rooms.map((room) => (
                <div className="col-3 border text-secondary">
                  {room.adult} {room.adult > 1 ? "Adults" : "Adult"}{" "}
                  {room.child > 0
                    ? "& " +
                      room.child +
                      (room.child > 1 ? " Childs" : " Child")
                    : ""}
                </div>
              ))}
              <div className="col-2 border text-secondary">Booking</div>
            </div>
            <div className="mb-5 mt-3"></div>
          </div>
          <div className="mb-5"></div>
        </React.Fragment>
      );
    }
  };
}

export default MyBookings;
