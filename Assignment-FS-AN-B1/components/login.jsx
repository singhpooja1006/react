import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
class Login extends Component {
  state = {
    data: { email: "", password: "" },
    errors: {},
  };
  async componentDidMount() {
    let data = {
      email: "",
      password: "",
    };
    if (this.props.location.state) {
      data.email = this.props.location.state.email;
    }
    this.setState({ data });
  }
  handleChange = (e) => {
    const data = { ...this.state.data };
    data[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data: data });
  };

  handleLogin = async () => {
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    console.log(errors, errCount);
    if (errCount > 0) return;
    let apiEndPoint = config.userApi + "/user";
    try {
      const { data: user } = await axios.post(apiEndPoint, {
        email: this.state.data.email,
        password: this.state.data.password,
      });
      delete user.password;
      localStorage.setItem("user", JSON.stringify(user));
      window.location = "/yatra";
    } catch (ex) {
      console.log(ex);
    }
  };

  validate = () => {
    let errs = {};
    if (!this.state.data.password.trim())
      errs.password = "Please enter your password";
    return errs;
  };
  handleUser = () => {
    window.location = "/yatra/common";
  };

  render() {
    const { data, errors } = this.state;
    return (
      <div className="container-fluid">
        <div className="row mt-4">
          <div className="col-12 text-center">
            <h2>Welcome to Yatra!</h2>
            <div>Please enter your password to login</div>
          </div>
        </div>

        <div className="row mt-3">
          <div className="col-3"></div>

          <div className="col-6">
            <div className="row bg-light">
              <div className="col-6 text-center mt-4">
                <i
                  className="fa fa-user-circle"
                  style={{ fontSize: "85px", color: "lightgrey" }}
                ></i>
                <div className="mt-2">{data.email}</div>
                <div className="mt-2">
                  <input
                    className="form-control"
                    placeholder="Enter your Password"
                    name="password"
                    id="password"
                    type="password"
                    value={data.password}
                    onChange={this.handleChange}
                  />
                  {errors.password ? (
                    <div className="text-danger">{errors.password}</div>
                  ) : (
                    ""
                  )}
                </div>
                <div
                  className="text-center mt-3 text-primary"
                  style={{ cursor: "pointer" }}
                >
                  OR Login using OTP
                </div>
                <div className="mt-3">
                  <button
                    className="form-control btn btn-danger"
                    onClick={() => this.handleLogin()}
                    type="submit"
                  >
                    Login
                  </button>
                </div>
                <div
                  className="text-center mt-3 text-primary"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.handleUser()}
                >
                  Login as a different user
                </div>
              </div>
              <div className="col-6 text-center mt-4">
                <div
                  style={{
                    backgroundColor: "#feeab8",
                    padding: "9px",
                  }}
                >
                  Logged In/Registered users get MORE!
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    class="fa fa-calendar"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>{" "}
                  View/ Cancel/ Reschedule bookings
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    className="fa fa-ticket"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>{" "}
                  Check booking history, manage cancellations & print eTickets
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    className="fa fa-edit"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>{" "}
                  Book faster with Pre-Filled Forms, saved Travellers & Saved
                  Cards
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    className="fa fa-money-bill"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>{" "}
                  Use Yatra eCash to get discounts
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    class="fa fa-barcode"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>{" "}
                  Transfer eCash to your Family/Friends
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    className="fa fa-desktop"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>
                  Convert eCash to Shopping Coupons from Amazon, BookMyShow,
                  etc.
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    className="fa fa-briefcase"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>{" "}
                  Do you have GST number?Additional Benefits of Free Meals, Low
                  Cancellation Fee, Free Rescheduling for SME business customers
                </div>
                <br /> <br />
              </div>
            </div>
          </div>
          <div className="col-3"></div>
        </div>
      </div>
    );
  }
}

export default Login;
