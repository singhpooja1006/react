import http from "./httpService";
import { userApi } from "./config.json";
const apiEndpoint = userApi + "/login";
const tokenKey = "user";
export async function login(email, password) {
  const { data: user } = await http.post(apiEndpoint, { email, password });
  localStorage.setItem(tokenKey, user);
}
export default {
  login,
};
