import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
class SignUp extends Component {
  state = {
    data: { email: "" },
    errors: {},
  };
  handleChange = (ele) => {
    const { currentTarget: input } = ele;
    let data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data: data });
  };
  handleContinue = async () => {
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    console.log(errors, errCount);
    if (errCount > 0) return;
    let apiEndPoint = config.userApi + "/user/" + this.state.data.email;
    try {
      const { data: result } = await axios.get(apiEndPoint);
      if (result.status === false) {
        this.props.history.push({
          pathname: "/yatra/common/register",
          state: {
            email: this.state.data.email,
          },
        });
      } else {
        this.props.history.push({
          pathname: "/yatra/login",
          state: {
            email: this.state.data.email,
          },
        });
      }
    } catch (ex) {
      console.log(ex);
    }
  };
  validate = () => {
    let errs = {};
    if (!this.state.data.email.trim())
      errs.email = "Please enter your Email Id / Mobile Number";
    return errs;
  };
  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-4">
          <div className="col-12 text-center">
            <h2>Welcome to Yatra!</h2>
            <div>Please Login/Register using your Email/Mobile to continue</div>
          </div>
        </div>
        <div className="row mt-3">
          <div className="col-3"></div>
          <div className="col-6">
            <div className="row bg-light">
              <div className="col-6 text-center mt-4">
                <i
                  className="fa fa-user-circle"
                  style={{ fontSize: "85px", color: "lightgrey" }}
                ></i>
                <div className="mt-2">EMAIL ID / MOBILE NUMBER</div>
                <div className="mt-2">
                  <input
                    className="form-control"
                    placeholder="EMAIL ID / MOBILE NUMBER"
                    name="email"
                    id="email"
                    type="text"
                    value={this.state.data.email}
                    onChange={this.handleChange}
                  />
                  {this.state.errors.email ? (
                    <div className="text-danger">{this.state.errors.email}</div>
                  ) : (
                    ""
                  )}
                </div>
                <div className="mt-4">
                  <button
                    className="form-control btn btn-danger"
                    onClick={() => this.handleContinue()}
                    type="submit"
                  >
                    Continue
                  </button>
                </div>
                <div className="text-center mt-3">
                  By proceeding, you agree with our Terms of Service, Privacy
                  Policy & Master User Agreement.
                </div>
                <hr className="mb-2" />
              </div>
              <div className="col-6 text-center mt-4">
                <div
                  style={{
                    backgroundColor: "#feeab8",
                    padding: "9px",
                  }}
                >
                  Logged In/Registered users get MORE!
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    class="fa fa-calendar"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>{" "}
                  View/ Cancel/ Reschedule bookings
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    className="fa fa-ticket"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>{" "}
                  Check booking history, manage cancellations & print eTickets
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    className="fa fa-edit"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>{" "}
                  Book faster with Pre-Filled Forms, saved Travellers & Saved
                  Cards
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    className="fa fa-money-bill"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>{" "}
                  Use Yatra eCash to get discounts
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    class="fa fa-barcode"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>{" "}
                  Transfer eCash to your Family/Friends
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    className="fa fa-desktop"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>
                  Convert eCash to Shopping Coupons from Amazon, BookMyShow,
                  etc.
                </div>
                <div
                  style={{
                    backgroundColor: "#fef2d8",
                    padding: "9px",
                  }}
                >
                  <i
                    className="fa fa-briefcase"
                    aria-hidden="true"
                    style={{ color: "#f76b38" }}
                  ></i>{" "}
                  Do you have GST number?Additional Benefits of Free Meals, Low
                  Cancellation Fee, Free Rescheduling for SME business customers
                </div>
                <br /> <br />
              </div>
            </div>
          </div>
          <div className="col-3"></div>
        </div>
      </div>
    );
  }
}

export default SignUp;
