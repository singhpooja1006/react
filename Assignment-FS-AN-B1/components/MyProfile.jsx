import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
import "./requireFields.css";
class MyProfile extends Component {
  state = {
    view: 0,
    title: ["Mr.", "Ms.", "Mrs.", "Dr."],
    data: {
      email: "",
      password: "",
      mobileNo: "",
      fname: "",
      lname: "",
      title: "",
      city: "",
      pincode: "",
      address: "",
      country: "India",
      state: "",
    },
    state: [
      "Andhra Pradesh",
      "Arunachal Pradesh",
      "Assam",
      "Bihar",
      "Chhattisgarh",
      "Goa",
      "Gujarat",
      "Haryana",
      "Himachal Pradesh",
      "Jharkhand",
      "Karnataka",
      "Kerala",
      "Madhya Pradesh",
      "Maharashtra",
      "Manipur",
      "Meghalaya",
      "Mizoram",
      "Nagaland",
      "Odisha",
      "Punjab",
      "Rajasthan",
      "Sikkim",
      "Tamil Nadu",
      "Telangana",
      "Tripura",
      "Uttar Pradesh",
      "Uttarakhand",
      "West Bengal",
    ],
  };

  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    let apiEndPoint = config.userApi + "/userDetails/" + user.email;
    const { data: data } = await axios.get(apiEndPoint);
    this.setState({ data });
  }
  handlEditDeatil = () => {
    this.setState({ view: 1 });
  };
  handleCancel = () => {
    this.setState({ view: 0 });
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let apiEndPoint = config.userApi + "/edit/" + this.state.data.email;
    try {
      const { data: user } = await axios.put(apiEndPoint, {
        email: this.state.data.email,
        password: this.state.data.password,
        fname: this.state.data.fname,
        lname: this.state.data.lname,
        title: this.state.data.title,
        mobileNo: this.state.data.mobileNo,
        city: this.state.data.city,
        pincode: this.state.data.pincode,
        address: this.state.data.address,
        country: this.state.data.country,
        state: this.state.data.state,
      });
      alert("Your profile has been updated successfully.");
      this.setState({ view: 0 });
    } catch (ex) {
      console.log(ex);
    }
  };
  handleChange = (e) => {
    const data = { ...this.state.data };
    data[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data: data });
    console.log("data", data);
  };
  render() {
    const { data } = this.state;
    return (
      <React.Fragment>
        <div className="row bg-light">
          <div className="ml-4 mt-2 mb-2">MY ACCOUNT</div>
        </div>
        {this.state.view === 1 ? (
          <React.Fragment>
            <div className="row">
              <div className="ml-3 mt-1 mb-1">Edit PROFILE</div>
            </div>
            <div className="ml-1 mt-1 mr-1 row border mb-4">
              <form onSubmit={this.handleSubmit}>
                <div className="form-group required">
                  <div className="row mb-3 ml-4 mt-3">
                    <label className="control-label">User Name:</label>
                    <div className="row ml-2">
                      <div className="col-3">
                        <select
                          className="browser-default custom-select"
                          value={data.title}
                          onChange={this.handleChange}
                          name="title"
                        >
                          <option>Title</option>
                          {this.state.title.map((item) => (
                            <option key={item}>{item}</option>
                          ))}
                        </select>
                      </div>
                      <div className="col-4">
                        <input
                          className="form-control"
                          placeholder="First Name"
                          name="fname"
                          id="fname"
                          type="text"
                          value={data.fname}
                          onChange={this.handleChange}
                        />
                      </div>
                      <div className="col-4">
                        <input
                          className="form-control"
                          placeholder="Last Name"
                          name="lname"
                          id="lname"
                          type="text"
                          value={data.lname}
                          onChange={this.handleChange}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="form-group row ml-3">
                  <label for="email" className="col-sm-2 col-form-label">
                    Email:
                  </label>
                  <div className="col-sm-10">
                    <input
                      className="form-control"
                      placeholder="Enter your Email"
                      name="email"
                      id="email"
                      type="text"
                      value={data.email}
                      disabled
                    />
                  </div>
                </div>
                <div className="form-group row ml-3">
                  <label for="password" className="col-sm-2 col-form-label">
                    Password:
                  </label>
                  <div className="col-sm-10">
                    <input
                      className="form-control"
                      placeholder="Enter your Password"
                      name="password"
                      id="password"
                      type="password"
                      value={data.password}
                      disabled
                    />
                  </div>
                </div>
                <div className="form-group required">
                  <div className="row ml-4">
                    <label for="mobileNo" className="control-label">
                      Phone:
                    </label>
                    <div className="col-sm-10 ml-5">
                      <input
                        className="form-control"
                        placeholder="Enter your MobileNo"
                        name="mobileNo"
                        id="mobileNo"
                        type="number"
                        value={data.mobileNo}
                        onChange={this.handleChange}
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group required">
                  <div className="row mb-3 ml-4 mt-3">
                    <label className="control-label">Address:</label>
                    <div className="row ml-4">
                      <div className="col ml-1">
                        <input
                          className="form-control"
                          placeholder="Enter Country"
                          name="country"
                          id="country"
                          type="text"
                          value={data.country}
                          disabled
                        />
                      </div>
                      <div className="col">
                        <select
                          className="browser-default custom-select"
                          value={data.state}
                          onChange={this.handleChange}
                          name="state"
                        >
                          <option>Select State</option>
                          {this.state.state.map((item) => (
                            <option key={item}>{item}</option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row ml-5">
                  <div className="col-1"></div>
                  <div className="col ml-4">
                    <input
                      className="form-control"
                      placeholder="Enter City"
                      name="city"
                      id="city"
                      type="text"
                      value={data.city}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-5">
                    <input
                      className="form-control"
                      placeholder="Enter Pincode"
                      name="pincode"
                      id="pincode"
                      type="text"
                      value={data.pincode}
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                <div className="row mt-3">
                  <div className="col-2"></div>
                  <div className="col ml-4">
                    <textarea
                      name="address"
                      id="address"
                      type="text"
                      value={data.address}
                      onChange={this.handleChange}
                      rows="3"
                      cols="70"
                      placeholder="Enter address"
                    ></textarea>
                  </div>
                </div>
                <div className="row mt-3 mb-5">
                  <div className="col-3"></div>
                  <div className="col-6">
                    <button
                      type="submit"
                      className="btn btn-outline-danger m-2"
                    >
                      Submit
                    </button>
                    <button
                      type="button"
                      className="btn btn-outline-danger m-2"
                      onClick={() => this.handleCancel()}
                    >
                      Cancel
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <div className="row">
              <div className="ml-3 mt-1 mb-1">MY PROFILE</div>
            </div>
            <div className="ml-1 mt-1 mr-1 row border mb-4">
              <div className="mt-4 ml-3">
                <i
                  className="fa fa-user-circle"
                  style={{ fontSize: "35px", color: "red" }}
                ></i>
              </div>
              <div className="mt-4 ml-2">
                <b>
                  {data ? data.title : ""}&nbsp;
                  {data ? data.fname : ""}&nbsp;
                  {data ? data.lname : ""}
                </b>
              </div>
              <div
                className="col-9 text-right mt-4"
                onClick={() => this.handlEditDeatil()}
              >
                <i
                  className="fa fa-pen-square"
                  style={{ fontSize: "30px", color: "red" }}
                ></i>
              </div>
              <div style={{ marginLeft: "50px" }}>
                <i className="fa fa-envelope"></i>
                &nbsp;&nbsp;{data ? data.email : ""}
              </div>
              <div className="col-12"></div>{" "}
              <div style={{ marginLeft: "50px" }}>
                <i className="fa fa-phone"></i>
                &nbsp;&nbsp;
                {data ? data.mobileNo : ""}
              </div>{" "}
              <div className="col-12"></div>{" "}
              <div style={{ marginLeft: "50px" }}>
                <i className="fa fa-map-marker"></i>
                &nbsp;&nbsp;
                {data ? data.address : ""}&nbsp;
                {data ? data.city : ""}&nbsp;
                {data ? data.state : ""}&nbsp;{data ? data.pincode : ""}&nbsp;
                {data ? data.country : ""}
              </div>{" "}
            </div>

            <div className="row">
              <div className="ml-3 mt-1 mb-1">GST DETAILS</div>
            </div>
            <div className="ml-1 mt-1 mr-1 row border mb-4">
              <div className="mt-4 ml-3">
                <i
                  className="fa fa-bank"
                  style={{ fontSize: "35px", color: "red" }}
                ></i>
              </div>
              <div className="mt-4 ml-2">
                <b>GST Number</b>
              </div>
              <div className="col-9 text-right mt-4">
                <i
                  className="fa fa-plus-square"
                  style={{ fontSize: "30px", color: "red" }}
                ></i>
              </div>
              <div style={{ marginLeft: "50px" }}>Add GST Details</div>
            </div>
            <div className="row">
              <div className="ml-3 mt-1 mb-1">SAVED TRAVELLERS</div>
            </div>
            <div className="ml-1 mt-1 mr-1 row border mb-4">
              <div className="mt-4 ml-3">
                <i
                  className="fa fa-users"
                  style={{ fontSize: "35px", color: "red" }}
                ></i>
              </div>
              <div className="mt-4 ml-2">
                <b>No Travellers added</b>
              </div>
              <div className="col-8 ml-1 text-right mt-4">
                <i
                  className="fa fa-plus-square"
                  style={{ fontSize: "30px", color: "red" }}
                ></i>
              </div>
              <div style={{ marginLeft: "50px" }}>
                Add Traveller for a faster booking experience.
              </div>
            </div>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default MyProfile;
