import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./dropdown.css";
class Navbar extends Component {
  state = {};

  handleSignUp = () => {
    window.location = "/yatra/common";
  };
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link className="navbar-brand" to="/yatra">
          <img
            src="https://www.yatra.com/content/fresco/beetle/images/newIcons/yatra_logo.svg"
            alt=""
            style={{ height: "40px" }}
          />
        </Link>
        <div className="col-6"></div>
        {this.props.user ? (
          <div className="mt-1">
            <div className="dropdown">
              <div className="dropbtn1">
                {" "}
                Hi {this.props.user.fname}
                <i
                  className="fa fa-chevron-down"
                  id="onhover"
                  style={{ fontSize: "10px" }}
                ></i>
              </div>
              <div className="dropdown-content">
                <div>
                  <Link to="/yatra/allBookings">
                    <i
                      className="fa fa-chevron-left"
                      id="onhover"
                      style={{ fontSize: "10px", color: "lightgrey" }}
                    ></i>
                    &nbsp; My Bookings
                  </Link>
                </div>
                <div>
                  <Link to="/yatra/eCash">
                    <i
                      className="fa fa-chevron-left"
                      id="onhover"
                      style={{ fontSize: "10px", color: "lightgrey" }}
                    ></i>
                    &nbsp; My eCash
                  </Link>
                </div>
                <div>
                  <Link to="/yatra/profile">
                    <i
                      className="fa fa-chevron-left"
                      id="onhover"
                      style={{ fontSize: "10px", color: "lightgrey" }}
                    ></i>
                    &nbsp; My Profile
                  </Link>
                </div>
                <div>
                  <Link to="/yatra/logout" className="ml-2">
                    Logout
                  </Link>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="mt-1">
            <div className="dropdown">
              <div className="dropbtn1">
                {" "}
                My Account
                <i
                  className="fa fa-chevron-down"
                  id="onhover"
                  style={{ fontSize: "10px" }}
                ></i>
              </div>

              <div className="dropdown-content">
                <div className="row mt-2">
                  <div className="col-3 ml-2">
                    <i
                      className="fa fa-user-circle"
                      style={{ fontSize: "35px", color: "lightgrey" }}
                    ></i>
                  </div>
                  <div className="ml-2" onClick={() => this.handleSignUp()}>
                    My Bookings
                    <div>My eCash</div>
                  </div>
                </div>
                <div className="row mt-2 mb-4">
                  <div className="col-1"></div>
                  <button
                    type="button"
                    className="btn btn-danger ml-2"
                    onClick={() => this.handleSignUp()}
                  >
                    &nbsp;Login &nbsp;
                  </button>{" "}
                  <button
                    type="button"
                    className="btn btn-outline-danger ml-2"
                    onClick={() => this.handleSignUp()}
                  >
                    Sign Up
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}
        <div className="mt-1 ml-5">
          <div className="dropdown">
            <div className="dropbtn1">
              {" "}
              Support
              <i
                className="fa fa-chevron-down"
                id="onhover"
                style={{ fontSize: "10px" }}
              ></i>
            </div>
          </div>
        </div>
        <div className="mt-1 ml-5">
          <div className="dropdown">
            <div className="dropbtn1">
              {" "}
              Recent Search
              <i
                className="fa fa-chevron-down"
                id="onhover"
                style={{ fontSize: "10px" }}
              ></i>
            </div>
            <div className="dropdown-content">
              <div className="col-12 mt-2">
                Things you view while searching are saved here.
              </div>
            </div>
          </div>
        </div>
        <div className="mt-1 ml-5">Offers</div>
        <button
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
          className="navbar-toggler"
          data-target="#navbarNav"
          data-toggle="collapse"
          type="button"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
      </nav>
    );
  }
}

export default Navbar;
