import React, { Component } from "react";
import { faUserFriends } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import config from "./config.json";
import axios from "axios";
import "./icon.css";
import queryString from "query-string";
import HotelsLeftPanel from "./hotelsLeftPanel";
class HotelSearch extends Component {
  state = {
    priceData: {
      prices: [
        { name: "Less than Rs.1,000", value: "<1000" },
        { name: "Rs.1,001 to Rs.2,000", value: "1001-2000" },
        { name: "Rs.2,001 to Rs.4,000", value: "2001-4000" },
        { name: "Rs.4,001 to Rs.7,000", value: "4001-7000" },
        { name: "Rs.7,001 to Rs.10,000 ", value: "7001-10000" },
        { name: "More than 10,001", value: ">10001" },
      ],
      selected: "",
    },
    starRatings: {
      ratings: [
        { name: "1 ★", value: "1" },
        { name: "2 ★", value: "2" },
        { name: "3 ★", value: "3" },
        { name: "4 ★", value: "4" },
        { name: "5 ★", value: "5" },
      ],
      selected: "",
    },
    paymentMode: {
      payments: ["UPI", "Credit Card", "Debit Card", "Net Banking", "PayPals"],
      selected: "",
    },
    selectedLoc: "",
    checkInDate: new Date(),
    checkOutDate: this.getReturnDate(),
    travellersCount: "",
    rooms: [],
    months: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
  };
  getReturnDate() {
    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);
    return tomorrow;
  }

  async componentDidMount() {
    let selectedLoc = "";
    let checkInDate = "";
    let checkOutDate = "";
    let travellersCount = "";
    let rooms = [];
    if (this.props.location.state) {
      selectedLoc = this.props.location.state.selectedLoc;
      checkInDate = new Date(this.props.location.state.checkInDate);
      checkOutDate = new Date(this.props.location.state.checkOutDate);
      travellersCount = this.props.location.state.travellersCount;
      rooms = this.props.location.state.rooms;
    }
    let apiEndPoint = config.hotelApiEndPoint + "/hotels?city=" + selectedLoc;
    const { data: searchHotels } = await axios.get(apiEndPoint);
    console.log("rooms", checkInDate);
    this.setState({
      searchHotels: searchHotels,
      selectedLoc: selectedLoc,
      checkInDate: checkInDate,
      checkOutDate: checkOutDate,
      travellersCount: travellersCount,
      rooms: rooms,
    });
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      let apiEndPoint = config.hotelApiEndPoint + "/hotels";
      apiEndPoint =
        apiEndPoint +
        this.props.location.search +
        "&city=" +
        this.state.selectedLoc;
      const { data: searchHotels } = await axios.get(apiEndPoint);
      this.setState({ searchHotels });
    }
  }
  getNoOfDays(checkInDate, checkOutDate) {
    let one_day = 1000 * 60 * 60 * 24;
    return (
      Math.round(
        new Date(checkOutDate).getTime() - new Date(checkInDate).getTime()
      ) / one_day
    ).toFixed(0);
  }
  handleSearchAgain = () => {
    window.location = "/yatra";
  };
  handleOptionChange = (priceData, starRatings, paymentMode) => {
    this.setState({
      priceData,
      starRatings,
      paymentMode,
    });
    let { sort } = queryString.parse(this.props.location.search);
    this.calURL(
      "",
      sort,
      priceData.selected,
      starRatings.selected,
      paymentMode.selected
    );
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  calURL = (params, sort, price, rating, payment) => {
    let path = "/yatra/hotelSearch";
    params = this.addToParams(params, "price", price);
    params = this.addToParams(params, "rating", rating);
    params = this.addToParams(params, "payment", payment);
    params = this.addToParams(params, "sort", sort);
    this.props.history.push({
      pathname: path,
      search: params,
    });
  };
  handleSortBy = (sortBy) => {
    let { price, rating, payment, sort } = queryString.parse(
      this.props.location.search
    );
    if (sort && sort === sortBy) {
      sort = "";
    } else {
      sort = sortBy;
    }

    this.calURL("", sort, price, rating, payment);
  };
  handleChooseRoom = (hotel) => {
    hotel.selectedLoc = this.state.selectedLoc;
    hotel.checkInDate =
      this.state.checkInDate.getDate() +
      " " +
      this.state.months[this.state.checkInDate.getMonth()] +
      " " +
      this.state.checkInDate.getFullYear();
    hotel.checkOutDate =
      this.state.checkOutDate.getDate() +
      " " +
      this.state.months[this.state.checkOutDate.getMonth()] +
      " " +
      this.state.checkOutDate.getFullYear();
    hotel.travellersCount = this.state.travellersCount;
    hotel.rooms = this.state.rooms;
    this.props.history.push({
      pathname: "/roomDetails",
      state: {
        hotel: hotel,
      },
    });
  };

  render() {
    const {
      selectedLoc,
      checkInDate,
      checkOutDate,
      rooms,
      travellersCount,
    } = this.state;
    return (
      <div className="container-fluid" style={{ backgroundColor: "lightgray" }}>
        <div
          className="row mt-4 pt-2 pb-2 "
          style={{
            background: "linear-gradient(to right, #b7243a,#132522)",
            color: "white",
          }}
        >
          <div className="col-1 text-center mt-1 d-none d-lg-block">
            <i className="fa fa-hotel" style={{ fontSize: "26px" }}></i>
          </div>
          <div className="col-lg-4 col-8">
            <div className="row" id="abc">
              <div className="col">
                <b>{selectedLoc}</b>
              </div>
            </div>
            {checkOutDate && checkInDate ? (
              <div className="row" id="abc10">
                <div className="col h6">
                  {new Date(checkInDate).getDate() +
                    " " +
                    this.state.months[new Date(checkInDate).getMonth()] +
                    ", " +
                    new Date(checkInDate).getFullYear()}
                  ,{" "}
                  {new Date(checkOutDate).getDate() +
                    " " +
                    this.state.months[new Date(checkOutDate).getMonth()] +
                    ", " +
                    new Date(checkOutDate).getFullYear()}
                  | {travellersCount}&nbsp; Travellers , {rooms.length}{" "}
                  {rooms.length > 1 ? "Rooms" : "Room"}
                </div>
              </div>
            ) : (
              ""
            )}
          </div>

          <div className="col-1 mt-1 d-none d-lg-block">
            <button
              className="btn btn-danger text-white router-link-active"
              routerlinkactive="router-link-active"
              tabindex="0"
              onClick={() => this.handleSearchAgain()}
            >
              Search Again
            </button>
          </div>
        </div>
        <div className="row">
          <div className="col-3">
            <HotelsLeftPanel
              priceData={this.state.priceData}
              starRatings={this.state.starRatings}
              paymentMode={this.state.paymentMode}
              onOptionChange={this.handleOptionChange}
            />
          </div>
          <div className="col-9">{this.renderHotels()}</div>
        </div>
      </div>
    );
  }
  renderHotels = () => {
    const { searchHotels } = this.state;
    let sort = "";
    if (this.props.location) {
      sort = queryString.parse(this.props.location.search).sort;
    }
    let noOfNights = this.getNoOfDays(
      this.state.checkInDate,
      this.state.checkOutDate
    );
    let sortByCss = "col-3 text-right";
    if (searchHotels && searchHotels.length > 0) {
      return (
        <React.Fragment>
          <div className="row mt-3">
            <div className="col-lg-11 col-12">
              <div
                className="row pl-4 pt-1 pb-1 mb-4"
                style={{
                  backgroundColor: "white",
                }}
              >
                <div className="col-2">
                  <strong>Sort By:</strong>
                </div>
                <div
                  className={
                    sort === "rating" ? sortByCss + " text-primary" : sortByCss
                  }
                  onClick={() => this.handleSortBy("rating")}
                  style={{ cursor: "pointer", fontSize: ".78571rem" }}
                >
                  STAR RATING
                  {sort === "rating" ? <i class="fa fa-arrow-up"></i> : ""}
                </div>

                <div
                  style={{ cursor: "pointer", fontSize: ".78571rem" }}
                  className={
                    sort === "price" ? sortByCss + " text-primary" : sortByCss
                  }
                  onClick={() => this.handleSortBy("price")}
                >
                  {" "}
                  PRICE {sort === "price" ? <i class="fa fa-arrow-up"></i> : ""}
                </div>
                <div
                  style={{ cursor: "pointer", fontSize: ".78571rem" }}
                  className={
                    sort === "taipadvisorRating"
                      ? sortByCss + " text-primary"
                      : sortByCss
                  }
                  onClick={() => this.handleSortBy("taipadvisorRating")}
                >
                  TRIPADVISOR RATING
                  {sort === "taipadvisorRating" ? (
                    <i class="fa fa-arrow-up"></i>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          </div>
          {searchHotels.map((hotel) => (
            <div className="row">
              <div className="col-lg-11 col-12">
                <div className="row bg-white" id="box2">
                  <div className="col-12">
                    <div className="row bg-white">
                      <div className="col-lg-4 col-12 text-center d-none d-lg-block">
                        <div className="row pl-1">
                          <div className="col-lg-12 col-12">
                            <div
                              id={"carouselExampleControls" + hotel.id}
                              className="carousel slide"
                              data-ride="carousel"
                            >
                              <div className="carousel-inner">
                                {hotel.imgList.length > 0
                                  ? hotel.imgList.map((img, index) => (
                                      <div
                                        className={
                                          index === 0
                                            ? "carousel-item active"
                                            : "carousel-item"
                                        }
                                      >
                                        <img
                                          style={{
                                            width: "100%",
                                            height: "100%",
                                          }}
                                          src={img}
                                        />
                                      </div>
                                    ))
                                  : ""}
                              </div>
                              <a
                                className="carousel-control-prev"
                                href={"#carouselExampleControls" + hotel.id}
                                role="button"
                                data-slide="prev"
                              >
                                <span
                                  className="carousel-control-prev-icon"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Previous</span>
                              </a>
                              <a
                                className="carousel-control-next"
                                href={"#carouselExampleControls" + hotel.id}
                                role="button"
                                data-slide="next"
                              >
                                <span
                                  className="carousel-control-next-icon"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Next</span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-8 h6">
                        <div className="row">
                          <div id="fs12">{hotel.name}</div>
                        </div>
                        <div className="row">
                          <div id="sh1">{hotel.rating}</div>
                          <div className="ml-1">
                            <i
                              className="fa fa-map-marker"
                              style={{ color: "gray" }}
                              aria-hidden="true"
                            ></i>
                          </div>
                          <div className="text-primary ml-1">
                            {hotel.location}
                          </div>
                        </div>
                        <div className="row">
                          <div>
                            <i
                              className="fa fa-check-circle-o"
                              style={{ color: "gray" }}
                              aria-hidden="true"
                            ></i>
                            &nbsp;Free Cancellation
                          </div>
                          <div>
                            <i
                              className="fa fa-wifi"
                              style={{ color: "gray" }}
                              aria-hidden="true"
                            ></i>
                            &nbsp;Free WiFi
                          </div>
                        </div>

                        <div className="row">
                          <div>
                            <FontAwesomeIcon
                              icon={faUserFriends}
                              style={{ color: "red", cursor: "pointer" }}
                              data-toggle="tooltip"
                              data-placement="bottom"
                              title="Smooth check-In,Local ID accepted except PAN card,Couples are welcome"
                            />
                            &nbsp;Couple Friendly
                          </div>
                        </div>

                        <div className="row">
                          <div className="text-success">
                            {hotel.tripadvisorRating}
                          </div>
                        </div>
                        <div className="row">
                          <div>
                            <span
                              style={{
                                textDecoration: "line-through",
                                fontSize: "14px",
                                color: "rgb(135, 135, 135)",
                              }}
                            >
                              ₹ {hotel.prevPrice * noOfNights}
                            </span>
                            <span> ₹ {hotel.price * noOfNights}</span>
                            <span>
                              {" For " +
                                noOfNights +
                                " " +
                                (noOfNights > 1 ? "nights" : "night")}
                            </span>
                          </div>
                        </div>
                        <div className="text-right">
                          <button
                            className="btn btn-danger"
                            onClick={() => this.handleChooseRoom(hotel)}
                          >
                            Choose Room
                          </button>
                        </div>

                        <br />
                        <div className="row mt-4">
                          <div id="sh2">You Save </div>
                          <span
                            className="text-success"
                            style={{ fontSize: "1rem" }}
                          >
                            ₹ {hotel.yatraOffer}
                          </span>
                          <span
                            className="text-primary"
                            style={{ fontSize: "1rem" }}
                          >
                            &nbsp;| Login
                          </span>
                          <div id="sh2">& get additional Rs.200 off using</div>
                          <span
                            className="text-warning"
                            style={{ fontSize: "1rem" }}
                          >
                            &nbsp; eCash
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </React.Fragment>
      );
    }
  };
}

export default HotelSearch;
