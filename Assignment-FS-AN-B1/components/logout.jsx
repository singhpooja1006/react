import React, { Component } from "react";
class Logout extends Component {
  componentDidMount() {
    localStorage.removeItem("user");
    window.location = "/yatra";
  }
  render() {
    return null;
  }
}

export default Logout;
