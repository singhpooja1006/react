import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
class Register extends Component {
  state = {
    title: ["Mr.", "Ms.", "Mrs.", "Dr."],
    data: {
      email: "",
      password: "",
      mobileNo: "",
      fname: "",
      lname: "",
      title: "",
    },
    errors: {},
  };
  async componentDidMount() {
    let data = {
      email: "",
      password: "",
      mobileNo: "",
      fname: "",
      lname: "",
      title: "",
    };
    if (this.props.location.state) {
      data.email = this.props.location.state.email;
    }
    this.setState({ data });
  }
  handleChange = (e) => {
    const data = { ...this.state.data };
    data[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data: data });
    console.log("data", data);
  };
  handleArrowLeft = () => {
    /*history.push({
      pathname: "/yatra/common",
    });*/
    window.location = "/yatra/common";
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    console.log(errors, errCount);
    if (errCount > 0) return;
    let apiEndPoint = config.userApi + "/login";
    try {
      const { data: user } = await axios.post(apiEndPoint, {
        email: this.state.data.email,
        password: this.state.data.password,
        fname: this.state.data.fname,
        lname: this.state.data.lname,
        title: this.state.data.title,
        mobileNo: this.state.data.mobileNo,
      });
      localStorage.setItem("user", JSON.stringify(user));
      window.location = "/yatra";
    } catch (ex) {
      console.log(ex);
    }
  };

  validate = () => {
    let errs = {};
    if (!this.state.data.mobileNo.trim())
      errs.mobileNo = "Mobile Number is mandatory.";
    else if (this.state.data.mobileNo.trim().length < 10)
      errs.mobileNo = "Mobile Number atleast 10 Digits.";
    if (!this.state.data.password.trim())
      errs.password = "Password is required";
    if (!this.state.data.fname.trim())
      errs.fname =
        "Title is required, First name should be at least 1 letter long, Last name should be at least 2 letters long";
    return errs;
  };
  render() {
    const { data, errors } = this.state;
    return (
      <div className="container-fluid">
        <div className="row mt-4">
          <div className="col-12 text-center">
            <h2>Welcome to Yatra!</h2>
            <div>
              We just need a few more details to create your Yatra account
            </div>
          </div>
        </div>
        <div className="row mt-3 ">
          <div className="col-4"></div>
          <div className="col-4 bg-light">
            <div className="mt-4">
              <div className="mt-2">
                <i
                  className="fa fa-arrow-left"
                  style={{ fontSize: "25px" }}
                  onClick={() => this.handleArrowLeft()}
                ></i>
              </div>
              <form onSubmit={this.handleSubmit}>
                <div className="mt-1">EMAIL ID</div>
                <div className="mt-1">
                  <input
                    className="form-control"
                    placeholder="EMAIL ID / MOBILE NUMBER"
                    name="email"
                    id="email"
                    type="text"
                    value={data.email}
                    disabled
                  />
                </div>
                <div className="mt-1">MOBILE NUMBER</div>
                <div className="mt-1">
                  <input
                    className="form-control"
                    placeholder="Enter Mobile Number"
                    name="mobileNo"
                    id="mobileNo"
                    type="text"
                    value={data.mobileNo}
                    onChange={this.handleChange}
                  />
                  {errors.mobileNo ? (
                    <div className="text-danger">{errors.mobileNo}</div>
                  ) : (
                    ""
                  )}
                </div>
                <div className="mt-1">CREATE PASSWORD</div>
                <div className="mt-1">
                  <input
                    className="form-control"
                    placeholder="Enter your Password"
                    name="password"
                    id="password"
                    type="password"
                    value={data.password}
                    onChange={this.handleChange}
                  />
                  {errors.password ? (
                    <div className="text-danger">{errors.password}</div>
                  ) : (
                    ""
                  )}
                </div>
                <div className="mt-1">FULL NAME</div>
                <div className="mt-1">
                  <div className="row">
                    <div className="col-3">
                      <select
                        className="browser-default custom-select"
                        value={data.title}
                        onChange={this.handleChange}
                        name="title"
                      >
                        <option>Title</option>
                        {this.state.title.map((item) => (
                          <option key={item}>{item}</option>
                        ))}
                      </select>
                    </div>
                    <div className="col-4">
                      <input
                        className="form-control"
                        placeholder="First Name"
                        name="fname"
                        id="fname"
                        type="text"
                        value={data.fname}
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="col-4">
                      <input
                        className="form-control"
                        placeholder="Last Name"
                        name="lname"
                        id="lname"
                        type="text"
                        value={data.lname}
                        onChange={this.handleChange}
                      />
                    </div>
                  </div>
                  {errors.fname ? (
                    <div className="text-danger">{errors.fname}</div>
                  ) : (
                    ""
                  )}
                </div>
                <div className="mt-4">
                  <button className="form-control btn btn-danger" type="submit">
                    Create Account
                  </button>
                </div>
                <div className="mt-2">
                  By proceeding, you agree with our Terms of Service, Privacy
                  Policy & Master User Agreement.
                </div>
                <div className="mb-3"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
