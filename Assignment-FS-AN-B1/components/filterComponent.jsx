import React, { Component } from "react";
import "./icon.css";
class FilterComponent extends Component {
  state = {};
  handleChange = (e) => {
    const { currentTarget: input } = e;
    const { priceData, timesData, airLines, airCrafts } = this.props;
    if (input.name === "price") {
      priceData.selected = input.value;
    }
    if (input.name === "time") {
      timesData.selected = input.value;
    }
    if (input.type === "checkbox") {
      let flight = airLines.find((n1) => n1.name === input.name);
      if (flight) flight.check = input.checked;
      let airbus = airCrafts.find((n1) => n1.name === input.name);
      if (airbus) airbus.check = input.checked;
    }
    this.props.onOptionChange(priceData, timesData, airLines, airCrafts);
  };

  render() {
    const { priceData, timesData, airLines, airCrafts } = this.props;
    return (
      <div className="expand">
        <div className="row">
          <div className="col-lg-3 ml-1 col-3">
            <div className="row ml-1 " id="id1">
              <strong>Price</strong>
            </div>
            {priceData.prices.map((price) => (
              <div className="form-check" id="id1">
                <input
                  checked={priceData.selected === price ? true : false}
                  value={price}
                  className="form-check-input ng-untouched ng-pristine ng-valid"
                  name="price"
                  type="radio"
                  onChange={this.handleChange}
                  id="price"
                />
                <label className="form-check-label" for={price}>
                  {price}
                </label>
              </div>
            ))}
          </div>
          <div className="col-lg-3 col-3">
            <div className="row ml-1">
              <strong>Time</strong>
            </div>
            {timesData.times.map((time) => (
              <div className="form-check">
                <input
                  checked={timesData.selected === time ? true : false}
                  className="form-check-input ng-untouched ng-pristine ng-valid"
                  name="time"
                  value={time}
                  type="radio"
                  onChange={this.handleChange}
                  id="time"
                />
                <label className="form-check-label" for={time}>
                  {time}
                </label>
              </div>
            ))}
          </div>
          <div className="col-lg-3 col-3">
            <div className="row ml-1">
              <strong>AirLine</strong>
            </div>
            {airLines.map((airline) => (
              <div className="form-check">
                <input
                  className="form-check-input ng-untouched ng-pristine ng-valid"
                  name={airline.name}
                  type="checkbox"
                  id={airline.name}
                  value={airline.name}
                  onChange={this.handleChange}
                  checked={airline.check}
                />
                <label className="form-check-label" for={airline.name}>
                  {airline.name}
                </label>
              </div>
            ))}
          </div>
          <div className="col-lg-2 col-3">
            <div className="row ml-1">
              <strong>AirCraft</strong>
            </div>
            {airCrafts.map((airCraft) => (
              <div className="form-check">
                <input
                  className="form-check-input ng-untouched ng-pristine ng-valid"
                  name={airCraft.name}
                  type="checkbox"
                  id={airCraft.name}
                  value={airCraft.name}
                  onChange={this.handleChange}
                  checked={airCraft.check}
                />
                <label className="form-check-label" for={airCraft.name}>
                  {airCraft.name}
                </label>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default FilterComponent;
