import React, { Component } from "react";
import "./icon.css";
import config from "./config.json";
import axios from "axios";
import { Link } from "react-router-dom";
import Summary from "./summary";
class Ecash extends Component {
  state = {
    selectedLeftOption: "Your eCash",
    selectedCss: "border bg-danger",
    eCash: { css: "fa fa-chevron-up", isShow: true },
  };
  handleLeftMenu = (selectedLeftOption) => {
    if (selectedLeftOption === "Your eCash") {
      let { eCash } = this.state;
      if (eCash.css.includes("up")) {
        eCash.css = "fa fa-chevron-down";
        eCash.isShow = false;
      } else {
        eCash.css = "fa fa-chevron-up";
        eCash.isShow = true;
      }
      this.setState({ eCash });
    }
    if (selectedLeftOption === "My Bookings")
      window.location = "/yatra/allBookings";
    if (selectedLeftOption === "Your Profile")
      window.location = "/yatra/profile";
    //if (selectedLeftOption === "Your eCash") window.location = "/yatra/eCash";
  };

  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    let apiEndPoint = config.userApi + "/userDetails/" + user.email;
    const { data: result } = await axios.get(apiEndPoint);
    this.setState({ result });
  }
  render() {
    return (
      <div className="container-fluid" style={{ backgroundColor: "lightgrey" }}>
        <div className="row">
          <div className="col-lg-9 col-12">
            <div className="row mt-3 mb-2">
              <div className="col-9 ml-1">
                Dashboard /{" "}
                <Link
                  to="/yatra/eCash"
                  onClick={() => this.handleLeftMenu("Your eCash")}
                >
                  {this.state.selectedLeftOption}
                </Link>{" "}
              </div>
            </div>
            <div className="row ml-1" id="payment">
              <div className="col-lg-3 col-3" id="fs20">
                <div
                  style={{ cursor: "pointer", color: "white" }}
                  className={
                    this.state.selectedLeftOption === "My Bookings"
                      ? this.state.selectedCss
                      : "border bg-dark"
                  }
                  onClick={() => this.handleLeftMenu("My Bookings")}
                >
                  <div className="mt-3 mb-3 ml-3">
                    {" "}
                    <i className="fa fa-plane" style={{ fontSize: "15px" }}></i>
                    &nbsp;&nbsp; ALL BOOKINGS
                  </div>
                </div>

                <div
                  style={{ cursor: "pointer", color: "white" }}
                  className={
                    this.state.selectedLeftOption === "Your eCash"
                      ? this.state.selectedCss
                      : "border bg-dark"
                  }
                  onClick={() => this.handleLeftMenu("Your eCash")}
                >
                  <div className="mt-3 mb-3 ml-3">
                    {" "}
                    <i className="fa fa-money" style={{ fontSize: "15px" }}></i>
                    &nbsp;&nbsp;ECASH
                    <i
                      className={this.state.eCash.css}
                      id="onhover"
                      style={{ fontSize: "10px", marginLeft: "150px" }}
                    ></i>
                  </div>
                </div>
                {this.state.eCash.isShow ? (
                  <React.Fragment>
                    <div
                      style={{ cursor: "pointer" }}
                      className="border bg-light"
                    >
                      <div
                        className="mt-3 mb-3 ml-3"
                        //onClick={() => this.handleLeftMenu("Your eCash")}
                        id="abc"
                      >
                        {" "}
                        <i
                          className="fa fa-chevron-left"
                          id="onhover"
                          style={{ fontSize: "10px" }}
                        ></i>
                        &nbsp;&nbsp; Summary
                      </div>
                    </div>
                    <div
                      style={{ cursor: "pointer" }}
                      className="border bg-light"
                    >
                      <div
                        className="mt-3 mb-3 ml-3"
                        //onClick={() => this.handleLeftMenu("Transfer eCash")}
                        id="abc"
                      >
                        {" "}
                        <i
                          className="fa fa-chevron-left"
                          id="onhover"
                          style={{ fontSize: "10px" }}
                        ></i>
                        &nbsp;&nbsp; Transfer
                      </div>
                    </div>
                    <div
                      style={{ cursor: "pointer" }}
                      className="border bg-light"
                    >
                      <div
                        className="mt-3 mb-3 ml-3"
                        id="abc"
                        //onClick={() =>this.handleLeftMenu("Convert eCash To Coupons")}
                      >
                        {" "}
                        <i
                          className="fa fa-chevron-left"
                          id="onhover"
                          style={{ fontSize: "10px" }}
                        ></i>
                        &nbsp;&nbsp;Convert To Coupons
                      </div>
                    </div>
                    <div
                      style={{ cursor: "pointer" }}
                      className="border bg-light"
                    >
                      <div
                        className="mt-3 mb-3 ml-3"
                        id="abc"
                        //onClick={() => this.handleLeftMenu("My Coupons")}
                      >
                        {" "}
                        <i
                          className="fa fa-chevron-left"
                          id="onhover"
                          style={{ fontSize: "10px" }}
                        ></i>
                        &nbsp;&nbsp; My Coupons
                      </div>
                    </div>
                  </React.Fragment>
                ) : (
                  ""
                )}
                <div
                  style={{ cursor: "pointer", color: "white" }}
                  className={
                    this.state.selectedLeftOption === "Your Profile"
                      ? this.state.selectedCss
                      : "border bg-dark"
                  }
                  onClick={() => this.handleLeftMenu("Your Profile")}
                >
                  <div className="mt-3 mb-3 ml-3">
                    {" "}
                    <i className="fa fa-user" style={{ fontSize: "15px" }}></i>
                    &nbsp;&nbsp; YOUR PROFILE
                  </div>
                </div>
              </div>
              <div className="col-9">{this.renderMainPage()}</div>
            </div>
          </div>

          <div className="col-lg-2 col-12">
            <div className="row bg-white ml-1 mr-1 mt-5" id="flight">
              <div className="col">
                <div className="row">
                  <div
                    className="col-12 text-center"
                    style={{ fontSize: "35px", color: "red" }}
                  >
                    <i className="fa fa-user-circle"></i>
                  </div>
                </div>
                <div className="row">
                  <div
                    className="col-12 text-center"
                    style={{ fontSize: "1rem" }}
                  >
                    {this.state.result ? this.state.result.title : ""}
                    {this.state.result ? this.state.result.fname : ""}&nbsp;
                    {this.state.result ? this.state.result.lname : ""}
                  </div>
                </div>
                <div className="row">
                  <hr className="col-9 text-center" />
                </div>
                <div className="row">
                  <div
                    className="col-12 text-center"
                    style={{ fontSize: "1rem" }}
                  >
                    {this.state.result ? this.state.result.email : ""}
                  </div>
                </div>
                <div className="row">
                  <div
                    className="col-12 text-center"
                    style={{ fontSize: "1rem" }}
                  >
                    Phone: {this.state.result ? this.state.result.mobileNo : ""}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  renderMainPage() {
    return <Summary />;
  }
}

export default Ecash;
