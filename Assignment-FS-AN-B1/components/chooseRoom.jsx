import React, { Component } from "react";
import "./icon.css";
import { faMugHot } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
class ChooseRoom extends Component {
  state = {
    selectedImg: "",
  };
  componentDidMount() {
    let hotel = {};
    if (this.props.location.state) {
      hotel = this.props.location.state.hotel;
      hotel.checkInDate = new Date(hotel.checkInDate);
      hotel.checkOutDate = new Date(hotel.checkOutDate);
      this.setState({ hotel });
    }

    console.log("OneHotelDetail", hotel);
  }
  handleSearchAgain = () => {
    window.location = "/yatra";
  };
  handleBookHotel = (selectedRoom, index) => {
    let hotel = this.state.hotel;
    hotel.selectedRoom = selectedRoom;
    hotel.selectedRoomIndex = index;
    hotel.noOfNights = +this.getNoOfDays(hotel.checkInDate, hotel.checkOutDate);
    this.props.history.push({
      pathname: "/hotelBooking",
      state: {
        hotel: hotel,
      },
    });
  };
  getNoOfDays(checkInDate, checkOutDate) {
    let one_day = 1000 * 60 * 60 * 24;
    return (
      Math.round(
        new Date(checkOutDate).getTime() - new Date(checkInDate).getTime()
      ) / one_day
    ).toFixed(0);
  }
  render() {
    const { hotel } = this.state;
    let noOfNights = this.getNoOfDays(
      hotel ? hotel.checkInDate : new Date(),
      hotel ? hotel.checkOutDate : new Date()
    );
    return (
      <React.Fragment>
        <div
          className="container-fluid"
          style={{ backgroundColor: "lightgray" }}
        >
          <div className="row mt-4 pt-2 pb-2 ml-2">
            <div className="col-lg-4 col-5">
              <div className="row">
                <div className="col-12 d-none d-lg-block" id="abc">
                  {hotel ? hotel.name : ""}
                </div>
                <div className="col-12" id="sh1">
                  {hotel ? hotel.rating : ""}
                </div>
              </div>
              <div className="row" id="abc10">
                <div className="col-12">
                  <b>{hotel ? hotel.fullLoc : ""}</b>
                </div>
              </div>
            </div>

            <div className="col-2 d-none d-lg-block">
              <div
                className="row"
                style={{ fontSize: ".71429rem", fontFamily: "Rubik-Medium" }}
              ></div>
              <div
                className="row"
                style={{ fontSize: "1.14286rem", fontFamily: "Rubik-Medium" }}
              >
                <b></b>
              </div>
            </div>
            <div className="col-1 d-none d-lg-block">
              <div className="row">
                <div className="text-primary"></div>
                <div className="ml-1">
                  <span
                    style={{
                      textDecoration: "line-through",
                      fontSize: "14px",
                      color: "rgb(135, 135, 135)",
                    }}
                  ></span>
                </div>
              </div>
              <div
                className="row ml-1"
                style={{ fontSize: "1.14286rem", fontFamily: "Rubik-Medium" }}
              ></div>
            </div>

            <div className="col-3 d-none d-lg-block">
              <div className="row">
                <div className="text-primary">
                  Save ₹ {hotel ? hotel.yatraOffer : ""}
                </div>
                <div className="ml-1">
                  <span
                    style={{
                      textDecoration: "line-through",
                      fontSize: "14px",
                      color: "rgb(135, 135, 135)",
                    }}
                  >
                    ₹ {hotel ? hotel.prevPrice * noOfNights : ""}
                  </span>
                </div>
              </div>
              <div
                className="row ml-1"
                style={{ fontSize: "1.14286rem", fontFamily: "Rubik-Medium" }}
              >
                <b>₹ {hotel ? hotel.price * noOfNights : ""}</b>
              </div>
            </div>
            <div className="col-2">
              <button
                className="btn btn-danger text-white"
                onClick={() => this.handleSearchAgain()}
              >
                Search Again
              </button>
            </div>
          </div>
          <div className="row mt-2 bg-white" id="box2">
            <div className="col-9">
              <div
                id="hotelRoomSlider"
                className="carousel slide"
                data-ride="carousel"
              >
                <div className="carousel-inner">
                  {hotel
                    ? hotel.imgList.map((img, index) => (
                        <div
                          className={
                            index === 0
                              ? "carousel-item active"
                              : "carousel-item"
                          }
                        >
                          <img
                            className="d-block w-100 border border-light"
                            style={{ width: "452px", height: "452px" }}
                            src={img}
                          />
                        </div>
                      ))
                    : ""}
                </div>
                <a
                  className="carousel-control-prev"
                  href="#hotelRoomSlider"
                  role="button"
                  data-slide="prev"
                >
                  <span
                    className="carousel-control-prev-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="sr-only">Previous</span>
                </a>
                <a
                  className="carousel-control-next"
                  href="#hotelRoomSlider"
                  role="button"
                  data-slide="next"
                >
                  <span
                    className="carousel-control-next-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="sr-only">Next</span>
                </a>
              </div>
            </div>
            <div className="col text-right mr-5">
              <h3 className="text-success">
                {hotel ? hotel.tripadvisorRating : ""}
              </h3>
              <div>Based on Overall Traveller Rating</div>
              <hr />
              <div className="row h6">
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                {hotel ? hotel.service : ""}
              </div>
              <div className="row h6">
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                {hotel ? hotel.service : ""}
              </div>
              <div className="row h6">
                <b>CHECKIN :&nbsp;</b> {hotel ? hotel.CHECKIN : ""}
              </div>
              <div className="row h6">
                <b>CHECKOUT :&nbsp;</b> {hotel ? hotel.CHECKOUT : ""}
              </div>
            </div>
            <div className="col-12">
              <br />
              <p
                style={{
                  fontSize: "14px",
                  marginBottom: "15px",
                  fontFamily: "Rubik-Medium",
                  width: "100%",
                  float: "left",
                }}
              >
                OVERVIEW
              </p>
            </div>
            <div className="col-12">
              <p
                style={{
                  color: "#333",
                  width: "100%",
                  display: "inline !important",
                  lineHeight: "22px !important",
                  boxSizing: "border-box",
                }}
              >
                Internet access is provided to its patrons at Goveia Holiday
                Resorts, which is a budget accommodation in Goa. It is 1 km away
                from Monteiro Bus Stop and 2 km from Candolim Beach.This Goa
                property houses a total of 60 well-kept rooms, spread over 5
                floors. Amenities in-room include air conditioner, make-up
                mirror, reading lamp, premium bedding and attached bathroom with
                hot/cold running water. Goveia Holiday Resorts offers laundry,
                airport transportation and room service to its guests. This
                accommodation also features a garden, swimming pool, restaurant,
                Jacuzzi and parking area within the premises. Some of the local
                tourist spots include Chapora Fort (12 km), Vagator Beach (12
                km) and Calangute Beach (5 km). This resort in Goa is reachable
                via travel hubs such as Thivim Railway Station (21 km) and
                Panjim Bus Stand (11 km)
              </p>
            </div>
          </div>
          <div className="row mt-1">
            <div className="col-12 h3 ml-1">CHOOSE ROOM</div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-9 col-12">
            <div className="row ml-4 mt-2">
              <div className="col-3">CHECK-IN</div>
              <div className="col-3">CHECK-OUT</div>
              <div className="col-3">GUESTS - ROOMS</div>
            </div>
            <div className="row ml-4">
              <div className="col-3">
                <label>
                  <b>
                    {hotel
                      ? hotel.checkInDate.getDate() +
                        "/" +
                        (hotel.checkInDate.getMonth() + 1) +
                        "/" +
                        hotel.checkInDate.getFullYear()
                      : ""}
                  </b>
                </label>
              </div>
              <div className="col-3">
                <label>
                  <b>
                    {hotel
                      ? hotel.checkOutDate.getDate() +
                        "/" +
                        (hotel.checkOutDate.getMonth() + 1) +
                        "/" +
                        hotel.checkOutDate.getFullYear()
                      : ""}
                  </b>
                </label>
              </div>
              <div className="col-3">
                {hotel
                  ? hotel.rooms.map((room, index) => (
                      <label>
                        <b>
                          {room.adult + " Adults - " + (index + 1) + " Room"}
                        </b>
                      </label>
                    ))
                  : ""}
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-2 mr-4 ml-4 border">
          <div className="col-3">
            <h5>{hotel ? hotel.roomlist[0].type : ""}</h5>
            <div
              id="carouselExampleControls"
              className="carousel slide"
              data-ride="carousel"
            >
              <div className="carousel-inner">
                {hotel
                  ? hotel.roomlist[0].imgDetail.map((img, index) => (
                      <div
                        className={
                          index === 0 ? "carousel-item active" : "carousel-item"
                        }
                      >
                        <img
                          className="d-block w-100 border border-light"
                          style={{ width: "100%", height: "100%" }}
                          src={img}
                        />
                      </div>
                    ))
                  : ""}
              </div>
              <a
                className="carousel-control-prev"
                href={"#carouselExampleControls"}
                role="button"
                data-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Previous</span>
              </a>
              <a
                className="carousel-control-next"
                href="#carouselExampleControls"
                role="button"
                data-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Next</span>
              </a>
            </div>
            <div className="bg-light">
              <h6 className="mt-1">Amenities</h6>
              <div>
                {" "}
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Wake up service/Alarm clock
              </div>
              <div>
                {" "}
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Internet Access
              </div>
              <div>
                {" "}
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Free toiletries
              </div>
              <div>
                {" "}
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Shower
              </div>
              <div>
                {" "}
                <i className="fa fa-hotel" style={{ fontSize: "26px" }}></i>
                &nbsp;{hotel ? hotel.roomlist[0].bedSize : ""}
              </div>
            </div>
          </div>
          <div className="col-8 mt-5">
            <b>{hotel ? hotel.roomlist[0].roomtype[0].type : ""}</b>
            <div className="row mt-3">
              <div className="ml-1 col-2">Max Guests</div>
              <div className="col-4">Inclusions</div>
              <div className="col-2">Highlights</div>
              <div className="col-2">Price for stay</div>
            </div>
            <div className="row mt-1">
              <div className="ml-1 col-2">
                {" "}
                <i className="fa fa-male mr-1" style={{ color: "gray" }}></i>
                <i className="fa fa-male mr-1" style={{ color: "gray" }}></i>
                <i className="fa fa-male mr-1" style={{ color: "gray" }}></i>
                <i className="fa fa-child mr-1" style={{ color: "gray" }}></i>
              </div>
              <div className="col-4">
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Complimentary Wi-Fi In
              </div>
              <div className="col-2">
                <i
                  className="fa fa-wifi"
                  style={{ color: "gray" }}
                  aria-hidden="true"
                ></i>
                &nbsp;Free WiFi
              </div>
              <div className="col-2">
                <span
                  style={{
                    textDecoration: "line-through",
                    fontSize: "14px",
                    color: "rgb(135, 135, 135)",
                  }}
                >
                  ₹{" "}
                  {hotel
                    ? hotel.roomlist[0].roomtype[0].prevPrice * noOfNights
                    : ""}
                </span>
                <b>
                  &nbsp;₹
                  {hotel
                    ? hotel.roomlist[0].roomtype[0].price * noOfNights
                    : ""}
                </b>
              </div>

              <button
                className="btn btn-danger ml-1"
                onClick={() => this.handleBookHotel(hotel.roomlist[0], 0)}
              >
                Book Now
              </button>
            </div>
            <div className="row">
              <div className="col-6"></div>
              <div className="col-5">
                <i className="fa fa-undo mr-1" style={{ color: "gray" }}></i>Non
                Refundable
              </div>
            </div>
            <div className="row">
              <div className="col-10"></div>
              <div className="col-2">
                <span id="rect">Get eCash</span>
                <span id="rect1">
                  {" "}
                  &nbsp;₹{hotel ? hotel.roomlist[0].roomtype[0].eCash : ""}
                </span>
              </div>
            </div>
            <div className="row mt-5">
              <div id="sh2">You Save </div>
              <span className="text-success" style={{ fontSize: "1rem" }}>
                ₹{" "}
                {hotel
                  ? hotel.roomlist[0].roomtype[0].yatraOffer * noOfNights
                  : ""}
              </span>
              <span className="text-primary" style={{ fontSize: "1rem" }}>
                &nbsp; | Login
              </span>
              <div id="sh2">&nbsp; & save more using</div>
              <span className="text-warning" style={{ fontSize: "1rem" }}>
                &nbsp; eCash
              </span>
            </div>

            <hr />
            <div className="row">
              <div className="col-12">
                <b>{hotel ? hotel.roomlist[0].roomtype[1].type : ""}</b>
                <div className="row mt-3">
                  <div className="ml-1 col-2">Max Guests</div>
                  <div className="col-4">Inclusions</div>
                  <div className="col-2">Highlights</div>
                  <div className="col-2">Price for stay</div>
                </div>
                <div className="row mt-1">
                  <div className="ml-1 col-2">
                    {" "}
                    <i
                      className="fa fa-male mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    <i
                      className="fa fa-male mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    <i
                      className="fa fa-male mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    <i
                      className="fa fa-child mr-1"
                      style={{ color: "gray" }}
                    ></i>
                  </div>
                  <div className="col-4">
                    <i
                      className="fa fa-check mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    Complimentary Wi-Fi In
                  </div>
                  <div className="col-2">
                    <i
                      className="fa fa-wifi"
                      style={{ color: "gray" }}
                      aria-hidden="true"
                    ></i>
                    &nbsp;Free WiFi
                  </div>
                  <div className="col-2">
                    <span
                      style={{
                        textDecoration: "line-through",
                        fontSize: "14px",
                        color: "rgb(135, 135, 135)",
                      }}
                    >
                      ₹
                      {hotel
                        ? hotel.roomlist[0].roomtype[1].prevPrice * noOfNights
                        : ""}
                    </span>
                    <b>
                      &nbsp;₹
                      {hotel
                        ? hotel.roomlist[0].roomtype[1].price * noOfNights
                        : ""}
                    </b>
                  </div>

                  <button
                    className="btn btn-danger ml-1"
                    onClick={() => this.handleBookHotel(hotel.roomlist[0], 1)}
                  >
                    Book Now
                  </button>
                </div>
                <div className="row">
                  <div className="col-2"></div>
                  <div className="col-4 mb-1">
                    {" "}
                    <i
                      className="fa fa-check mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    Breakfast
                  </div>
                  <div className="col-5">
                    <i
                      className="fa fa-undo mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    Non Refundable
                  </div>
                </div>
                <div className="row">
                  <div className="col-6"></div>
                  <div className="col-5">
                    <FontAwesomeIcon
                      icon={faMugHot}
                      className="mr-1"
                      style={{ color: "gray" }}
                    />
                    Free Breakfast
                  </div>
                </div>
                <div className="row">
                  <div className="col-10"></div>
                  <div className="col-2">
                    <span id="rect">Get eCash</span>
                    <span id="rect1">
                      {" "}
                      &nbsp;₹{hotel ? hotel.roomlist[0].roomtype[1].eCash : ""}
                    </span>
                  </div>
                </div>
                <div className="row mt-1">
                  <div id="sh2">You Save </div>
                  <span className="text-success" style={{ fontSize: "1rem" }}>
                    ₹{" "}
                    {hotel
                      ? hotel.roomlist[0].roomtype[1].yatraOffer * noOfNights
                      : ""}
                  </span>
                  <span className="text-primary" style={{ fontSize: "1rem" }}>
                    &nbsp; | Login
                  </span>
                  <div id="sh2">&nbsp; & save more using</div>
                  <span className="text-warning" style={{ fontSize: "1rem" }}>
                    &nbsp; eCash
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-3 mr-4 ml-4 border">
          <div className="col-3">
            <h5>{hotel ? hotel.roomlist[1].type : ""}</h5>
            <div id="roomPics" className="carousel slide" data-ride="carousel">
              <div className="carousel-inner">
                {hotel
                  ? hotel.roomlist[1].imgDetail.map((img, index) => (
                      <div
                        className={
                          index === 0 ? "carousel-item active" : "carousel-item"
                        }
                      >
                        <img
                          className="d-block w-100 border border-light"
                          style={{ width: "100%", height: "100%" }}
                          src={img}
                        />
                      </div>
                    ))
                  : ""}
              </div>

              <a
                className="carousel-control-prev"
                href="#roomPics"
                role="button"
                data-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Previous</span>
              </a>
              <a
                className="carousel-control-next"
                href="#roomPics"
                role="button"
                data-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Next</span>
              </a>
            </div>
            <div className="bg-light">
              <h6 className="mt-1">Amenities</h6>
              <div>
                {" "}
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Wake up service/Alarm clock
              </div>
              <div>
                {" "}
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Internet Access
              </div>
              <div>
                {" "}
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Free toiletries
              </div>
              <div>
                {" "}
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Shower
              </div>
              <div>
                {" "}
                <i className="fa fa-hotel" style={{ fontSize: "26px" }}></i>
                &nbsp;{hotel ? hotel.roomlist[1].bedSize : ""}
              </div>
            </div>
          </div>
          <div className="col-8 mt-5">
            <b>{hotel ? hotel.roomlist[1].roomtype[0].type : ""}</b>
            <div className="row mt-3">
              <div className="ml-1 col-2">Max Guests</div>
              <div className="col-4">Inclusions</div>
              <div className="col-2">Highlights</div>
              <div className="col-2">Price for stay</div>
            </div>
            <div className="row mt-1">
              <div className="ml-1 col-2">
                {" "}
                <i className="fa fa-male mr-1" style={{ color: "gray" }}></i>
                <i className="fa fa-male mr-1" style={{ color: "gray" }}></i>
                <i className="fa fa-child mr-1" style={{ color: "gray" }}></i>
              </div>
              <div className="col-4">
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Complimentary Wi-Fi In
              </div>
              <div className="col-2">
                <i
                  className="fa fa-wifi"
                  style={{ color: "gray" }}
                  aria-hidden="true"
                ></i>
                &nbsp;Free WiFi
              </div>
              <div className="col-2">
                <span
                  style={{
                    textDecoration: "line-through",
                    fontSize: "14px",
                    color: "rgb(135, 135, 135)",
                  }}
                >
                  ₹
                  {hotel
                    ? hotel.roomlist[1].roomtype[0].prevPrice * noOfNights
                    : ""}
                </span>
                <b>
                  &nbsp;₹
                  {hotel
                    ? hotel.roomlist[1].roomtype[0].price * noOfNights
                    : ""}
                </b>
              </div>

              <button
                className="btn btn-danger ml-1"
                onClick={() => this.handleBookHotel(hotel.roomlist[1], 0)}
              >
                Book Now
              </button>
            </div>
            <div className="row">
              <div className="col-6"></div>
              <div className="col-5">
                <i className="fa fa-undo mr-1" style={{ color: "gray" }}></i>Non
                Refundable
              </div>
            </div>
            <div className="row">
              <div className="col-10"></div>
              <div className="col-2">
                <span id="rect">Get eCash</span>
                <span id="rect1">
                  {" "}
                  &nbsp;₹{hotel ? hotel.roomlist[1].roomtype[0].eCash : ""}
                </span>
              </div>
            </div>
            <div className="row mt-5">
              <div id="sh2">You Save </div>
              <span className="text-success" style={{ fontSize: "1rem" }}>
                ₹{" "}
                {hotel
                  ? hotel.roomlist[1].roomtype[0].yatraOffer * noOfNights
                  : ""}
              </span>
              <span className="text-primary" style={{ fontSize: "1rem" }}>
                &nbsp; | Login
              </span>
              <div id="sh2">&nbsp; & save more using</div>
              <span className="text-warning" style={{ fontSize: "1rem" }}>
                &nbsp; eCash
              </span>
            </div>
            <hr />
            <div className="row">
              <div className="col-12">
                <b>{hotel ? hotel.roomlist[1].roomtype[1].type : ""}</b>
                <div className="row mt-3">
                  <div className="ml-1 col-2">Max Guests</div>
                  <div className="col-4">Inclusions</div>
                  <div className="col-2">Highlights</div>
                  <div className="col-2">Price for stay</div>
                </div>
                <div className="row mt-1">
                  <div className="ml-1 col-2">
                    {" "}
                    <i
                      className="fa fa-male mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    <i
                      className="fa fa-male mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    <i
                      className="fa fa-child mr-1"
                      style={{ color: "gray" }}
                    ></i>
                  </div>
                  <div className="col-4">
                    <i
                      className="fa fa-check mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    Complimentary Wi-Fi In
                  </div>
                  <div className="col-2">
                    <i
                      className="fa fa-wifi"
                      style={{ color: "gray" }}
                      aria-hidden="true"
                    ></i>
                    &nbsp;Free WiFi
                  </div>
                  <div className="col-2">
                    <span
                      style={{
                        textDecoration: "line-through",
                        fontSize: "14px",
                        color: "rgb(135, 135, 135)",
                      }}
                    >
                      ₹{" "}
                      {hotel
                        ? hotel.roomlist[1].roomtype[1].prevPrice * noOfNights
                        : ""}
                    </span>
                    <b>
                      &nbsp; ₹{" "}
                      {hotel
                        ? hotel.roomlist[1].roomtype[1].price * noOfNights
                        : ""}
                    </b>
                  </div>

                  <button
                    className="btn btn-danger ml-1"
                    onClick={() => this.handleBookHotel(hotel.roomlist[1], 1)}
                  >
                    Book Now
                  </button>
                </div>
                <div className="row">
                  <div className="col-2"></div>
                  <div className="col-4 mb-1">
                    {" "}
                    <i
                      className="fa fa-check mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    Breakfast
                  </div>
                  <div className="col-5">
                    <i
                      className="fa fa-undo mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    Non Refundable
                  </div>
                </div>
                <div className="row">
                  <div className="col-6"></div>
                  <div className="col-5">
                    <FontAwesomeIcon
                      icon={faMugHot}
                      className="mr-1"
                      style={{ color: "gray" }}
                    />
                    Free Breakfast
                  </div>
                </div>
                <div className="row">
                  <div className="col-10"></div>
                  <div className="col-2">
                    <span id="rect">Get eCash</span>
                    <span id="rect1">
                      {" "}
                      &nbsp;₹{hotel ? hotel.roomlist[1].roomtype[1].eCash : ""}
                    </span>
                  </div>
                </div>
                <div className="row mt-1">
                  <div id="sh2">You Save </div>
                  <span className="text-success" style={{ fontSize: "1rem" }}>
                    ₹{" "}
                    {hotel
                      ? hotel.roomlist[1].roomtype[1].yatraOffer * noOfNights
                      : ""}
                  </span>
                  <span className="text-primary" style={{ fontSize: "1rem" }}>
                    &nbsp; | Login
                  </span>
                  <div id="sh2">&nbsp; & save more using</div>
                  <span className="text-warning" style={{ fontSize: "1rem" }}>
                    &nbsp; eCash
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row mt-3 mr-4 ml-4 border">
          <div className="col-3">
            <h5>{hotel ? hotel.roomlist[2].type : ""}</h5>
            <div
              id="deluxRoomPics"
              className="carousel slide"
              data-ride="carousel"
            >
              <div className="carousel-inner">
                {hotel
                  ? hotel.roomlist[2].imgDetail.map((img, index) => (
                      <div
                        className={
                          index === 0 ? "carousel-item active" : "carousel-item"
                        }
                      >
                        <img
                          className="d-block w-100 border border-light"
                          style={{ width: "100%", height: "100%" }}
                          src={img}
                        />
                      </div>
                    ))
                  : ""}
              </div>

              <a
                className="carousel-control-prev"
                href="#deluxRoomPics"
                role="button"
                data-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Previous</span>
              </a>
              <a
                className="carousel-control-next"
                href="#deluxRoomPics"
                role="button"
                data-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Next</span>
              </a>
            </div>
            <div className="bg-light">
              <h6 className="mt-1">Amenities</h6>
              <div>
                {" "}
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Wake up service/Alarm clock
              </div>
              <div>
                {" "}
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Internet Access
              </div>
              <div>
                {" "}
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Free toiletries
              </div>
              <div>
                {" "}
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Shower
              </div>
              <div>
                {" "}
                <i className="fa fa-hotel" style={{ fontSize: "26px" }}></i>
                &nbsp;{hotel ? hotel.roomlist[2].bedSize : ""}
              </div>
            </div>
          </div>
          <div className="col-8 mt-5">
            <b>{hotel ? hotel.roomlist[2].roomtype[0].type : ""}</b>
            <div className="row mt-3">
              <div className="ml-1 col-2">Max Guests</div>
              <div className="col-4">Inclusions</div>
              <div className="col-2">Highlights</div>
              <div className="col-2">Price for stay</div>
            </div>
            <div className="row mt-1">
              <div className="ml-1 col-2">
                {" "}
                <i className="fa fa-male mr-1" style={{ color: "gray" }}></i>
                <i className="fa fa-male mr-1" style={{ color: "gray" }}></i>
                <i className="fa fa-male mr-1" style={{ color: "gray" }}></i>
                <i className="fa fa-child mr-1" style={{ color: "gray" }}></i>
              </div>
              <div className="col-4">
                <i className="fa fa-check mr-1" style={{ color: "gray" }}></i>
                Complimentary Wi-Fi In
              </div>
              <div className="col-2">
                <i
                  className="fa fa-wifi"
                  style={{ color: "gray" }}
                  aria-hidden="true"
                ></i>
                &nbsp;Free WiFi
              </div>
              <div className="col-2">
                <span
                  style={{
                    textDecoration: "line-through",
                    fontSize: "14px",
                    color: "rgb(135, 135, 135)",
                  }}
                >
                  ₹
                  {hotel
                    ? hotel.roomlist[2].roomtype[0].prevPrice * noOfNights
                    : ""}
                </span>
                <b>
                  &nbsp;₹
                  {hotel
                    ? hotel.roomlist[2].roomtype[0].price * noOfNights
                    : ""}
                </b>
              </div>

              <button
                className="btn btn-danger ml-1"
                onClick={() => this.handleBookHotel(hotel.roomlist[2], 0)}
              >
                Book Now
              </button>
            </div>
            <div className="row">
              <div className="col-6"></div>
              <div className="col-5">
                <i className="fa fa-undo mr-1" style={{ color: "gray" }}></i>Non
                Refundable
              </div>
            </div>
            <div className="row">
              <div className="col-10"></div>
              <div className="col-2">
                <span id="rect">Get eCash</span>
                <span id="rect1">
                  {" "}
                  &nbsp;₹{hotel ? hotel.roomlist[2].roomtype[0].eCash : ""}
                </span>
              </div>
            </div>
            <div className="row mt-5">
              <div id="sh2">You Save </div>
              <span className="text-success" style={{ fontSize: "1rem" }}>
                ₹{" "}
                {hotel
                  ? hotel.roomlist[2].roomtype[0].yatraOffer * noOfNights
                  : ""}
              </span>
              <span className="text-primary" style={{ fontSize: "1rem" }}>
                &nbsp; | Login
              </span>
              <div id="sh2">&nbsp; & save more using</div>
              <span className="text-warning" style={{ fontSize: "1rem" }}>
                &nbsp; eCash
              </span>
            </div>
            <hr />
            <div className="row">
              <div className="col-12">
                <b>{hotel ? hotel.roomlist[2].roomtype[1].type : ""}</b>
                <div className="row mt-3">
                  <div className="ml-1 col-2">Max Guests</div>
                  <div className="col-4">Inclusions</div>
                  <div className="col-2">Highlights</div>
                  <div className="col-2">Price for stay</div>
                </div>
                <div className="row mt-1">
                  <div className="ml-1 col-2">
                    {" "}
                    <i
                      className="fa fa-male mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    <i
                      className="fa fa-male mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    <i
                      className="fa fa-male mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    <i
                      className="fa fa-child mr-1"
                      style={{ color: "gray" }}
                    ></i>
                  </div>
                  <div className="col-4">
                    <i
                      className="fa fa-check mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    Complimentary Wi-Fi In
                  </div>
                  <div className="col-2">
                    <i
                      className="fa fa-wifi"
                      style={{ color: "gray" }}
                      aria-hidden="true"
                    ></i>
                    &nbsp;Free WiFi
                  </div>
                  <div className="col-2">
                    <span
                      style={{
                        textDecoration: "line-through",
                        fontSize: "14px",
                        color: "rgb(135, 135, 135)",
                      }}
                    >
                      ₹{" "}
                      {hotel
                        ? hotel.roomlist[2].roomtype[1].prevPrice * noOfNights
                        : ""}
                    </span>
                    <b>
                      &nbsp;₹
                      {hotel
                        ? hotel.roomlist[2].roomtype[1].price * noOfNights
                        : ""}
                    </b>
                  </div>

                  <button
                    className="btn btn-danger ml-1"
                    onClick={() => this.handleBookHotel(hotel.roomlist[2], 1)}
                  >
                    Book Now
                  </button>
                </div>
                <div className="row">
                  <div className="col-2"></div>
                  <div className="col-4 mb-1">
                    {" "}
                    <i
                      className="fa fa-check mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    Breakfast
                  </div>
                  <div className="col-5">
                    <i
                      className="fa fa-undo mr-1"
                      style={{ color: "gray" }}
                    ></i>
                    Non Refundable
                  </div>
                </div>
                <div className="row">
                  <div className="col-6"></div>
                  <div className="col-5">
                    <FontAwesomeIcon
                      icon={faMugHot}
                      className="mr-1"
                      style={{ color: "gray" }}
                    />
                    Free Breakfast
                  </div>
                </div>
                <div className="row">
                  <div className="col-10"></div>
                  <div className="col-2">
                    <span id="rect">Get eCash</span>
                    <span id="rect1">
                      {" "}
                      &nbsp;₹{hotel ? hotel.roomlist[2].roomtype[1].eCash : ""}
                    </span>
                  </div>
                </div>
                <div className="row mt-1">
                  <div id="sh2">You Save </div>
                  <span className="text-success" style={{ fontSize: "1rem" }}>
                    ₹{" "}
                    {hotel
                      ? hotel.roomlist[2].roomtype[1].yatraOffer * noOfNights
                      : ""}
                  </span>
                  <span className="text-primary" style={{ fontSize: "1rem" }}>
                    &nbsp; | Login
                  </span>
                  <div id="sh2">&nbsp; & save more using</div>
                  <span className="text-warning" style={{ fontSize: "1rem" }}>
                    &nbsp; eCash
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ChooseRoom;
