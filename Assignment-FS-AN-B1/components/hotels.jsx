import React, { Component } from "react";
import Calendar from "react-calendar";
import config from "./config.json";
import axios from "axios";
class ShowHotels extends Component {
  state = {
    locationData: [],
    selectedLoc: "",
    checkInFlag: false,
    checkoutFlag: false,
    travellers: { css: "fa fa-chevron-down", isShow: false },
    months: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
    weeks: [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ],
    rooms: [{ roomNo: 1, adult: 2, child: 0, fname: "", lname: "" }],
    checkInDate: new Date(),
    checkOutDate: this.getCheckOutDate(),
  };
  getCheckOutDate() {
    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);
    return tomorrow;
  }
  handleCheckInDate = () => {
    this.setState({
      checkInFlag: this.state.checkInFlag === true ? false : true,
    });
  };
  getCheckInDate = (value) => {
    this.setState({ checkInDate: value, checkInFlag: false });
  };
  handleCheckOutDate = () => {
    this.setState({
      checkoutFlag: this.state.checkoutFlag === true ? false : true,
    });
  };
  getCheckOutDate = (value) => {
    if (value < this.state.checkInDate) {
      alert("Select Check Out Date after Check In Date");
    } else {
      this.setState({ checkOutDate: value, checkoutFlag: false });
    }
  };
  handleIecrement = (roomNo, generation) => {
    const { rooms } = this.state;
    let roomIndex = rooms.findIndex((room) => room.roomNo === roomNo);
    if (roomIndex !== -1) {
      let room = rooms[roomIndex];
      if (generation === "Adult") {
        room.adult++;
      }
      if (generation === "Child") {
        room.child++;
      }
      rooms[roomIndex] = room;
    }
    this.setState({ rooms });
  };
  handleDecrement = (roomNo, generation) => {
    const { rooms } = this.state;
    let roomIndex = rooms.findIndex((room) => room.roomNo === roomNo);
    if (roomIndex !== -1) {
      let room = rooms[roomIndex];
      if (generation === "Adult") {
        room.adult--;
      }
      if (generation === "Child") {
        room.child--;
      }
      rooms[roomIndex] = room;
    }
    this.setState({ rooms });
  };
  handletravellers() {
    let { travellers } = this.state;
    if (travellers.css.includes("up")) {
      travellers.css = "fa fa-chevron-down";
      travellers.isShow = false;
    } else {
      travellers.css = "fa fa-chevron-up";
      travellers.isShow = true;
    }

    this.setState({ travellers });
  }
  handleSearchRooms = () => {
    const {
      checkInDate,
      checkOutDate,
      months,
      selectedLoc,
      rooms,
    } = this.state;
    let travellersCount = rooms.reduce(
      (acc, curr) => acc + curr.adult + curr.child,
      0
    );
    let checkIn =
      checkInDate.getDate() +
      " " +
      months[checkInDate.getMonth()] +
      " " +
      checkInDate.getFullYear();
    let checkOut =
      checkOutDate.getDate() +
      " " +
      months[checkOutDate.getMonth()] +
      " " +
      checkOutDate.getFullYear();
    this.props.onSerachHotel(
      selectedLoc,
      checkIn,
      checkOut,
      travellersCount,
      rooms
    );
  };
  handleLocation = (ele) => {
    const { currentTarget: input } = ele;
    let selectedLoc = "";
    if (input.name === "selectedLoc") {
      selectedLoc = input.value;
      this.setState({ selectedLoc });
    }
  };
  handleAddRoom = () => {
    let rooms = this.state.rooms;
    if (rooms) {
      let room = {
        roomNo: rooms.length + 1,
        adult: 2,
        child: 0,
        fname: "",
        lname: "",
      };
      rooms.push(room);
      this.setState(rooms);
    }
  };
  handleRemoveRoom = () => {
    let rooms = this.state.rooms;
    if (rooms) {
      rooms.splice(rooms.length - 1, 1);
      this.setState(rooms);
    }
  };
  async componentDidMount() {
    let apiEndPoint = config.hotelApiEndPoint + "/getCityHotelCount";
    const { data: cityData } = await axios.get(apiEndPoint);
    let locationData = [];
    for (let index = 0; index < cityData.length; index++) {
      let cityDetail = {
        display:
          cityData[index].city +
          ", India (" +
          cityData[index].count +
          (cityData[index].count > 1 ? " Hotels)" : " Hotel)"),
        value: cityData[index].city,
      };
      locationData.push(cityDetail);
    }
    this.setState({
      locationData,
      selectedLoc: locationData[0].value,
    });
  }
  render() {
    const { locationData, checkInDate, checkOutDate, rooms } = this.state;
    return (
      <div className="container">
        <div className="row">
          <div className="h6 ml-4">Select City, Location, Hotel name</div>
          <div className="col-12">
            <select
              className="browser-default custom-select"
              value={this.state.selectedLoc}
              onChange={this.handleLocation}
              id="selectedLoc"
              name="selectedLoc"
            >
              {locationData.map((location) => (
                <option key={location} value={location.value}>
                  {location.display}
                </option>
              ))}
            </select>
          </div>
        </div>
        <hr />
        <div className="row mt-2">
          <div className="col-6">
            <h6 onClick={() => this.handleCheckInDate()}>Check-in Date</h6>
            <div className="h6">
              {checkInDate.getDate() +
                " " +
                this.state.months[checkInDate.getMonth()] +
                ", " +
                checkInDate.getFullYear()}
            </div>
            <div>{this.state.weeks[checkInDate.getDay()]}</div>
            {this.state.checkInFlag === true ? (
              <div>
                <Calendar
                  height={280}
                  width={280}
                  tileHeight={35}
                  style={{ alignSelf: "center" }}
                  topbarVisible={true}
                  showOtherDates="false"
                  datesSelection={"single"}
                  onClickDay={this.getCheckInDate}
                />
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="col-6">
            <h6 onClick={() => this.handleCheckOutDate()}>Check-out Date</h6>
            <div className="h6">
              {checkOutDate.getDate() +
                " " +
                this.state.months[checkOutDate.getMonth()] +
                ", " +
                checkOutDate.getFullYear()}
            </div>
            <div>{this.state.weeks[checkOutDate.getDay()]}</div>
            {this.state.checkoutFlag === true ? (
              <div>
                <Calendar
                  height={280}
                  width={280}
                  tileHeight={35}
                  style={{ alignSelf: "center" }}
                  topbarVisible={true}
                  showOtherDates="false"
                  datesSelection={"single"}
                  onClickDay={this.getCheckOutDate}
                />
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
        <hr />
        <div className="row mt-2">
          <div className="col-5">
            <h6>Traveller and Hotel</h6>
            <div className="h5">
              {rooms.reduce((acc, curr) => acc + curr.adult + curr.child, 0)}
              Travellers ,{rooms.length} {rooms.length > 1 ? "Rooms" : "Room"}
            </div>
          </div>
          <div className="col-6"></div>
          <div className="col-1">
            <i
              className={this.state.travellers.css}
              onClick={() => this.handletravellers()}
            ></i>
          </div>
        </div>
        <hr />
        {this.state.travellers.isShow
          ? this.state.rooms.map((room) => (
              <React.Fragment>
                <div>
                  <div className="row mt-2">
                    <div className="col-2 h6">Room {room.roomNo}:</div>
                  </div>
                  <div className="row mt-2">
                    <div className="col-2"></div>
                    <div className="col-5">
                      <h6>Adult Above 12 years</h6>
                    </div>
                    <div className="col-5">
                      <h6>Child Below 12 years</h6>
                    </div>
                  </div>

                  <div className="row ml-1">
                    <div className="col-2">
                      <i className="fa fa-user"></i>
                    </div>
                    <h6 className="col-1 border">
                      <small
                        className="text-muted"
                        style={{ cursor: "pointer" }}
                        onClick={() =>
                          this.handleDecrement(room.roomNo, "Adult")
                        }
                      >
                        -
                      </small>
                    </h6>
                    <h6 className="col-1 border bg-light">
                      <small className="text-muted">{room.adult}</small>
                    </h6>
                    <h6 className="col-1 border">
                      <small
                        className="text-muted"
                        style={{ cursor: "pointer" }}
                        onClick={() =>
                          this.handleIecrement(room.roomNo, "Adult")
                        }
                      >
                        +
                      </small>
                    </h6>
                    <div className="ml-5">
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                    <h6 className="col-1 border">
                      <small
                        className="text-muted"
                        style={{ cursor: "pointer" }}
                        onClick={() =>
                          this.handleDecrement(room.roomNo, "Child")
                        }
                      >
                        -
                      </small>
                    </h6>
                    <h6 className="col-1 border bg-light">
                      <small className="text-muted">{room.child}</small>
                    </h6>
                    <h6 className="col-1 border">
                      <small
                        className="text-muted"
                        style={{ cursor: "pointer" }}
                        onClick={() =>
                          this.handleIecrement(room.roomNo, "Child")
                        }
                      >
                        +
                      </small>
                    </h6>
                  </div>
                </div>
              </React.Fragment>
            ))
          : ""}
        {this.state.travellers.isShow ? (
          <div className="row">
            <button
              className="btn btn-outline-danger btn-sm"
              onClick={() => this.handleAddRoom()}
            >
              Add Room
            </button>
            {this.state.rooms.length === 1 ? (
              ""
            ) : (
              <button
                className="btn btn-outline-danger btn-sm ml-2"
                onClick={() => this.handleRemoveRoom()}
              >
                Remove Room
              </button>
            )}
          </div>
        ) : (
          ""
        )}

        <div className="row mt-2">
          <div className="col-8"></div>
          <div className="col-3">
            <button
              className="btn btn-danger"
              onClick={() => this.handleSearchRooms()}
            >
              Search Hotels
              <i className="fa fa-arrow-right ml-1" aria-hidden="true"></i>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default ShowHotels;
