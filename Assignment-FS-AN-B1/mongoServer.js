var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
    next();
});
const port  = 2410;
app.listen(port,()=>console.log("Listening on port : ",port))
const MongoClient = require("mongodb").MongoClient;
let ObjectId = require("mongodb").ObjectId;
const url = "mongodb://localhost:27017";
const dbName = "userdb";
const client = new MongoClient(url);

app.get("/user/:email",function(req,res) { 
    let email = req.params.email;
    client.connect(function(err,client){
    console.log("Get : /user/:email",email);
    const db = client.db(dbName);
    db.collection("userRecord").findOne({email:email}, function(
    err,result
    ){
        console.log(result);
        if(result){
            res.send({status : true})
        }
        else{
            res.send({status : false})
        }
    });
  });
});


app.post("/login",function(req,res) { 
    let obj = {...req.body};
    client.connect(function(err,client){
    console.log("Post : /login",obj);
    const db = client.db(dbName);
    let userObj = {email : obj.email, password: obj.password,fname:obj.fname,
        lname:obj.lname,title:obj.title,mobileNo:obj.mobileNo,country:"",city:"",
        pincode:"",address:"",state:""}
    
    db.collection("userRecord").insertOne(userObj , function(err,result){
        console.log(result);
    });
    res.send(obj)
  });
});



app.post("/user", function (req, res) {
    let email = req.body.email
    let password = req.body.password
    client.connect(function(err,client){
        console.log("Post : /user");
    const db = client.db(dbName);
    db.collection("userRecord").findOne({email:email, password:password}, function(
        err,result
        ){
        console.log(result);
        res.send("Successfull");
        });

    });
   
});


app.get("/userDetails/:email",function (req, res) {
    let email = req.params.email;
    client.connect(function(err,client){
        console.log("GET : /userDetails/:email",email);
    const db = client.db(dbName);
    db.collection("userRecord").findOne({email:email}, function(
        err,result
        ){
            console.log(result);
            res.send(result);
        });
    });
    
});

app.put("/edit/:email",function(req,res) { 
    let obj = {...req.body};
    let email = req.params.email;
    client.connect(function(err,client){
        console.log("Put : /edit/:email",email,obj);
    const db = client.db(dbName);
    let userObj = {email : obj.email, password: obj.password,fname:obj.fname,
        lname:obj.lname,title:obj.title,mobileNo:obj.mobileNo,country:obj.country,
		city:obj.city,pincode:obj.pincode,address:obj.address,state:obj.state }
    db.collection("userRecord").updateOne({email: email},{$set:userObj});
    res.send(obj);
  });
});