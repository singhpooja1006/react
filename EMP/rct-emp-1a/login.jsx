import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import http from "./services/httpService";
class LoginForm extends Component {
  state = {
    data: { email: "", password: "" },
    errors: {},
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const apiEndpoint = apiBaseURL + "/loginuser";
      const { data: user } = await http.post(apiEndpoint, {
        email: this.state.data.email,
        password: this.state.data.password,
      });
      localStorage.setItem("user", JSON.stringify(user));
      if (user.role === "ADMIN") {
        window.location = "/admin";
      } else {
        window.location = "/emp";
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 401) {
        const errors = { ...this.state.errors };
        if (ex.response.data.includes("Login Failed")) {
          errors.msg = "Login Failed. Check the username and password.";
        } else {
          errors.msg = ex.response.data;
        }

        this.setState({ errors });
      }
    }
  };
  validate = () => {
    let errs = {};

    if (!this.state.data.email.trim()) errs.email = "Email is required";
    else if (this.state.data.email.includes("@") === false)
      errs.email = "Must be a valid email";

    if (!this.state.data.password.trim())
      errs.password = "Password is required";
    else if (this.state.data.password.trim().length < 5)
      errs.password = "Password length must be at least 5 characters long";
    return errs;
  };
  isFormvalid = () => {
    let errs = this.validate();

    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "email":
        if (!e.currentTarget.value.trim()) return "Email is required";
        else if (e.currentTarget.value.includes("@") === false)
          return "Must be a valid email";
        break;
      case "password":
        if (!e.currentTarget.value.trim()) return "Password is required";
        else if (e.currentTarget.value.trim().length < 5)
          return "Password length must be at least 5 characters long";
        break;
      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const loginData = { ...this.state.data };
    loginData[e.currentTarget.name] = e.currentTarget.value;

    this.setState({ data: loginData, errors: errors });
  };
  render() {
    const { data } = this.state;
    return (
      <React.Fragment>
        <h2 className="text-center">Welcome to Employee Management Portal</h2>

        <div className=" mr-5 bg-light ml-5">
          <h4 className="text-center">Login</h4>
          {this.state.errors.msg ? (
            <div className="text-danger col-12 text-center">
              {this.state.errors.msg}
            </div>
          ) : (
            ""
          )}
          <form onSubmit={this.handleSubmit}>
            <label htmlFor="email" className="col-6 text-right">
              Email ID:
            </label>
            <input
              value={data.email}
              type="email"
              onChange={this.handleChange}
              className="col-6"
              id="email"
              placeholder="Enter your Email ID"
              name="email"
            />
            {this.state.errors.email ? (
              <div className="text-danger text-center">
                {this.state.errors.email}
              </div>
            ) : (
              ""
            )}

            <label htmlFor="password" className="col-6 mt-2 text-right">
              Password:
            </label>
            <input
              value={data.password}
              type="password"
              onChange={this.handleChange}
              className="col-6 mt-2"
              id="password"
              placeholder="Enter your Password"
              name="password"
            />
            {this.state.errors.password ? (
              <div className="text-danger text-center">
                {this.state.errors.password}
              </div>
            ) : (
              ""
            )}
            <div className="col-12 text-center">
              <button className="btn btn-primary" disabled={this.isFormvalid()}>
                Submit
              </button>
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default LoginForm;
