import React, { Component } from "react";

class Admin extends Component {
  state = {};
  componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    this.setState({ user });
  }
  render() {
    return <div className="container h3">Welcome, Admin</div>;
  }
}

export default Admin;
