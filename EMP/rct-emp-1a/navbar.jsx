import { times } from "lodash";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./dropdown.css";
class NavBar extends Component {
  state = {};
  render() {
    return (
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
        <Link className="navbar-brand" to="/">
          Employee Portal
        </Link>
        {this.renderMenuBar()}
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            {this.props.user ? (
              <Link to="/logout" className="nav-link">
                Logout
              </Link>
            ) : (
              <Link to="/login" className="nav-link">
                Login
              </Link>
            )}
          </li>
        </ul>
      </nav>
    );
  }
  renderMenuBar() {
    const { user } = this.props;
    if (user) {
      if (user.role === "ADMIN") {
        return (
          <div>
            <form className="form-inline my-2 my-lg-0">
              <a className="navbar-brand">
                <div className="dropdown">
                  <div className="dropbtn" style={{ color: "white" }}>
                    Admin
                    <i
                      className="fa fa-chevron-down"
                      id="onhover"
                      style={{ fontSize: "10px", color: "white" }}
                    ></i>
                  </div>
                  <div className="dropdown-content">
                    <div>
                      <Link to="/admin/addemp">Add Employee</Link>
                    </div>
                    <div>
                      <Link to="/admin/viewemp">View Employees</Link>
                    </div>
                  </div>
                </div>
              </a>
            </form>
          </div>
        );
      } else {
        return (
          <div>
            <form className="form-inline my-2 my-lg-0">
              <a className="navbar-brand">
                <div className="dropdown">
                  <div className="dropbtn" style={{ color: "white" }}>
                    My Portal
                    <i
                      className="fa fa-chevron-down"
                      id="onhover"
                      style={{ fontSize: "10px", color: "white" }}
                    ></i>
                  </div>
                  <div className="dropdown-content">
                    <div>
                      <Link to="/emp/ContactDetails">Contact Details</Link>
                    </div>
                    <div>
                      <Link to="/Bills">Bills</Link>
                    </div>
                  </div>
                </div>
              </a>
            </form>
          </div>
        );
      }
    }
    return;
  }
}
export default NavBar;
