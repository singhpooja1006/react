import React, { Component } from "react";
class Employee extends Component {
  state = {};
  componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    this.setState({ user });
  }
  render() {
    return (
      <div className="container h3">
        Welcome {this.state.user ? this.state.user.name : ""} to the Employee
        Management Portal
      </div>
    );
  }
}

export default Employee;
