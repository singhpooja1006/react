import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import http from "./services/httpService";
import axios from "axios";
class TravelBill extends Component {
  state = {
    flightBillsData: {
      empuserid: "",
      amount: "",
      billid: "",
      description: "",
      expensetype: "",
      goflightDate: "",
      goflightOrigin: "",
      goflightDest: "",
      goflightNum: "",
      backflightDate: "",
      backflightOrigin: "",
      backflightDest: "",
      backflightNum: "",
      corpbooking: "",
      goflightday: "",
      goflightmonth: "",
      goflightyear: "",
      backflightday: "",
      backflightmonth: "",
      backflightyear: "",
    },
    days: [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,
      31,
    ],
    months: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
    years: [2018, 2019, 2020],
    errors: {},
  };
  async componentDidMount() {
    const { billid } = this.props.match.params;
    const empuserid = JSON.parse(localStorage.getItem("user")).empuserid;
    const { data: flightBillsData } = await axios.get(
      apiBaseURL + "/travelbill/" + empuserid + "/" + billid
    );
    let isDataFetched = false;
    if (flightBillsData.corpbooking) {
      let goflightDate = flightBillsData.goflightDate;
      let backflightDate = flightBillsData.backflightDate;
      flightBillsData.goflightday = goflightDate.substring(
        0,
        goflightDate.indexOf("-")
      );
      flightBillsData.goflightmonth = goflightDate.substring(
        goflightDate.indexOf("-") + 1,
        goflightDate.lastIndexOf("-")
      );
      flightBillsData.goflightyear = goflightDate.substring(
        goflightDate.lastIndexOf("-") + 1,
        goflightDate.length
      );
      flightBillsData.backflightday = backflightDate.substring(
        0,
        backflightDate.indexOf("-")
      );
      flightBillsData.backflightmonth = backflightDate.substring(
        backflightDate.indexOf("-") + 1,
        backflightDate.lastIndexOf("-")
      );
      flightBillsData.backflightyear = backflightDate.substring(
        backflightDate.lastIndexOf("-") + 1,
        backflightDate.length
      );
      isDataFetched = true;
    } else {
      flightBillsData.corpbooking = "No";
      flightBillsData.goflightOrigin = "";
      flightBillsData.goflightNum = "";
      flightBillsData.goflightDest = "";
      flightBillsData.backflightOrigin = "";
      flightBillsData.backflightNum = "";
      flightBillsData.backflightDest = "";
      flightBillsData.backflightday = "";
      flightBillsData.backflightmonth = "";
      flightBillsData.backflightyear = "";
      flightBillsData.goflightday = "";
      flightBillsData.goflightmonth = "";
      flightBillsData.goflightyear = "";
    }
    this.setState({
      flightBillsData,
      isDataFetched,
    });
  }
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    const { flightBillsData } = this.state;
    const empuserid = JSON.parse(localStorage.getItem("user")).empuserid;
    const apiEndpoint = apiBaseURL + "/travelbill";
    try {
      await http.post(apiEndpoint, {
        billid: flightBillsData.billid,
        empuserid: flightBillsData.empuserid,
        goflightDate:
          flightBillsData.goflightday +
          "-" +
          flightBillsData.goflightmonth +
          "-" +
          flightBillsData.goflightyear,
        backflightDate:
          flightBillsData.backflightday +
          "-" +
          flightBillsData.backflightmonth +
          "-" +
          flightBillsData.backflightyear,

        goflightOrigin: flightBillsData.goflightOrigin,
        goflightDest: flightBillsData.goflightDest,
        goflightNum: flightBillsData.goflightNum,
        backflightOrigin: flightBillsData.backflightOrigin,
        backflightDest: flightBillsData.backflightDest,
        backflightNum: flightBillsData.backflightNum,
        corpbooking: flightBillsData.corpbooking,
      });
      errors.msg = "Flight Details have been successfully created";
      this.setState({ errors });
    } catch (ex) {}
  };
  validate = () => {
    let errs = {};
    if (!this.state.flightBillsData.goflightday)
      errs.goflightday = "Flight Day is required";
    if (!this.state.flightBillsData.goflightmonth)
      errs.goflightmonth = "Flight Month is required";
    if (!this.state.flightBillsData.goflightyear)
      errs.goflightyear = "Flight Year is required";
    if (!this.state.flightBillsData.backflightday)
      errs.backflightday = "Flight Day is required";
    if (!this.state.flightBillsData.backflightmonth)
      errs.backflightmonth = "Flight Month is required";
    if (!this.state.flightBillsData.backflightyear)
      errs.backflightyear = "Flight Year is required";
    if (!this.state.flightBillsData.goflightOrigin.trim())
      errs.goflightOrigin = "Origin City is required";
    if (!this.state.flightBillsData.goflightDest.trim())
      errs.goflightDest = "Destination is required";
    if (!this.state.flightBillsData.goflightNum.trim())
      errs.goflightNum = "Flight Number is required";

    if (!this.state.flightBillsData.backflightOrigin.trim())
      errs.backflightOrigin = "Origin City is required";
    if (!this.state.flightBillsData.backflightDest.trim())
      errs.backflightDest = "Destination is required";
    if (!this.state.flightBillsData.backflightNum.trim())
      errs.backflightNum = "Flight Number is required";
    return errs;
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const flightBillsData = { ...this.state.flightBillsData };
    if (e.currentTarget.type === "checkbox") {
      if (flightBillsData[e.currentTarget.name] === "No") {
        flightBillsData.corpbooking = "Yes";
      } else {
        flightBillsData.corpbooking = "No";
      }
    } else {
      flightBillsData[e.currentTarget.name] = e.currentTarget.value;
    }

    this.setState({ flightBillsData, errors });
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "goflightOrigin":
        if (!e.currentTarget.value.trim()) return "Origin City is required";
        break;
      case "goflightDest":
        if (!e.currentTarget.value.trim()) return "Destination is required";
        break;
      case "goflightNum":
        if (!e.currentTarget.value.trim()) return "Flight Number is required";
        break;
      case "backflightOrigin":
        if (!e.currentTarget.value.trim()) return "Origin City is required";
        break;
      case "backflightDest":
        if (!e.currentTarget.value.trim()) return "Destination is required";
        break;
      case "backflightNum":
        if (!e.currentTarget.value.trim()) return "Flight Number is required";
        break;
      default:
        break;
    }
    return "";
  };
  render() {
    const { flightBillsData, isDataFetched, errors } = this.state;
    const { billid } = this.props.match.params;
    return (
      <React.Fragment>
        <h2 className="text-center">Welcome to Employee Management Portal</h2>
        <div className=" mr-5 bg-light ml-5 mt-2">
          <h4 className="text-center">Flight Details</h4>
          <div className="text-center">Bill Id : {billid}</div>
          {isDataFetched === true ? (
            <div className="text-success text-center">
              Displaying Flight Details
            </div>
          ) : errors.msg ? (
            <div className="text-primary text-center">{errors.msg}</div>
          ) : (
            <div className="text-primary text-center">
              No Flight Details Found. Please Enter Them.
            </div>
          )}

          <div className="col text-secondary mt-2 text-center border-bottom"></div>
          <h4 className="text-center">Departure Flight Details</h4>
          <form onSubmit={this.handleSubmit}>
            <label htmlFor="goflight" className="col-6 text-right">
              Flight date:
            </label>

            {this.renderDropDown("goflight")}
            <label htmlFor="goflightOrigin" className="col-6 text-right">
              Origin City:
            </label>
            <input
              value={flightBillsData.goflightOrigin}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="goflightOrigin"
              name="goflightOrigin"
            />
            <label htmlFor="goflightDest" className="col-6 text-right">
              Destination City:
            </label>
            <input
              value={flightBillsData.goflightDest}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="goflightDest"
              name="goflightDest"
            />
            <label htmlFor="goflightNum" className="col-6 text-right">
              Flight Number:
            </label>
            <input
              value={flightBillsData.goflightNum}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="goflightNum"
              name="goflightNum"
            />
            <div className="col text-secondary mt-2 text-center border-bottom"></div>
            <h4 className="text-center">Return Flight Details</h4>
            <label htmlFor="backflight" className="col-6 text-right">
              Flight date:
            </label>

            {this.renderDropDown("backflight")}
            <label htmlFor="backflightOrigin" className="col-6 text-right">
              Origin City:
            </label>
            <input
              value={flightBillsData.backflightOrigin}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="backflightOrigin"
              name="backflightOrigin"
            />
            <label htmlFor="backflightDest" className="col-6 text-right">
              Destination City:
            </label>
            <input
              value={flightBillsData.backflightDest}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="backflightDest"
              name="backflightDest"
            />
            <label htmlFor="backflightNum" className="col-6 text-right">
              Flight Number:
            </label>
            <input
              value={flightBillsData.backflightNum}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="backflightNum"
              name="backflightNum"
            />
            <div class="form-group col-7 ml-5 text-right">
              <div class="form-check ">
                <input
                  value={flightBillsData.corpbooking}
                  class="form-check-input"
                  type="checkbox"
                  id="corpbooking"
                  name="corpbooking"
                  onChange={this.handleChange}
                  checked={flightBillsData.corpbooking === "Yes" ? true : false}
                />
                <label htmlFor="corpbooking" className="form-check-label">
                  Corporate Booking
                </label>
              </div>
            </div>
            <div className="col-12 text-center">
              <button className="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
  renderDropDown = (param) => {
    const { flightBillsData, days, months, years } = this.state;
    return (
      <React.Fragment>
        <select
          value={
            param === "goflight"
              ? flightBillsData.goflightday
              : flightBillsData.backflightday
          }
          onChange={this.handleChange}
          id={param ? param + "day" : "SelectedDay"}
          name={param ? param + "day" : "SelectedDay"}
          className="browser-default custom-select col-2 mb-1"
        >
          <option disabled>Day</option>
          {days.map((day) => (
            <option key={day}>{day}</option>
          ))}
        </select>
        <select
          value={
            param === "goflight"
              ? flightBillsData.goflightmonth
              : flightBillsData.backflightmonth
          }
          onChange={this.handleChange}
          id={param ? param + "month" : "SelectedMonth"}
          name={param ? param + "month" : "SelectedMonth"}
          className="browser-default custom-select col-2 mb-1"
        >
          <option disabled>Month</option>
          {months.map((month) => (
            <option key={month}>{month}</option>
          ))}
        </select>
        <select
          value={
            param === "goflightDate"
              ? flightBillsData.goflightyear
              : flightBillsData.backflightyear
          }
          onChange={this.handleChange}
          id={param ? param + "year" : "SelectedYear"}
          name={param ? param + "year" : "SelectedYear"}
          className="browser-default custom-select col-2 mb-1"
        >
          <option disabled>Year</option>
          {years.map((year) => (
            <option key={year}>{year}</option>
          ))}
        </select>
      </React.Fragment>
    );
  };
}

export default TravelBill;
