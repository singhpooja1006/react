import http from "./httpService";
import { apiBaseURL } from "../config/config.json";
const apiEndpoint = apiBaseURL + "/loginuser";
const tokenKey = "user";
export async function login(email, password) {
  const { data: user } = await http.post(apiEndpoint, { email, password });
  localStorage.setItem(tokenKey, user);
  //return user;
}
export default {
  login,
};
