import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import http from "./services/httpService";
import axios from "axios";
class ContactDetails extends Component {
  state = {
    empcontact: {
      mobile: "",
      address: "",
      city: "",
      country: "",
      pincode: "",
      empuserid: "",
    },
    errors: {},
  };
  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    const { data: empcontact } = await axios.get(
      apiBaseURL + "/empcontact/" + user.empuserid
    );
    let dataFound = false;
    if (empcontact.pincode) {
      dataFound = true;
    }
    this.setState({ empcontact, dataFound });
  }
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    const { empcontact } = this.state;
    const user = JSON.parse(localStorage.getItem("user"));
    const apiEndpoint = apiBaseURL + "/empcontact/" + user.empuserid;
    try {
      await http.post(apiEndpoint, {
        mobile: empcontact.mobile,
        address: empcontact.address,
        city: empcontact.city,
        country: empcontact.country,
        pincode: empcontact.pincode,
        empuserid: empcontact.empuserid,
      });
      errors.msg = "Details have been successfully added";
      this.setState({ dataSaved: true, errors });
    } catch (ex) {}
  };
  validate = () => {
    let errs = {};
    const { empcontact } = this.state;
    if (empcontact.mobile && !empcontact.mobile.trim())
      errs.mobile = "Mobile Number is required";
    else if (empcontact.mobile && empcontact.mobile.length < 10)
      errs.mobile =
        "Mobile Number has at least 10 characters. Allowed are 0-9,+,-and space";
    if (empcontact.country && !empcontact.country.trim())
      errs.country = "Country  is required";
    if (empcontact.pincode && !empcontact.pincode.trim())
      errs.pincode = "Pincode is required";
    return errs;
  };
  isFormvalid = () => {
    let errs = this.validate();

    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "mobile":
        if (!e.currentTarget.value.trim()) return "Mobile Number is required";
        else if (e.currentTarget.value.trim().length < 10)
          return "Mobile Number has at least 10 characters. Allowed are 0-9,+,-and space";
        break;
      case "country":
        if (!e.currentTarget.value.trim()) return "Country is required";

        break;
      case "pincode":
        if (!e.currentTarget.value.trim()) return "Pincode is required";

        break;

      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const empContactData = { ...this.state.empcontact };
    empContactData[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ empcontact: empContactData, errors: errors });
  };
  render() {
    const { empcontact, dataFound, errors } = this.state;
    return (
      <React.Fragment>
        <h2 className="text-center">Welcome to Employee Management Portal</h2>
        <div className=" mr-5 bg-light ml-5">
          <h4 className="text-center">Your Contact Details</h4>
          {dataFound ? (
            <div className="text-success text-center">
              Displaying Contact Details
            </div>
          ) : errors.msg ? (
            <div className="text-success text-center">{errors.msg}</div>
          ) : (
            <div className="text-primary text-center">
              No Contact Details Found. Please Enter Them.
            </div>
          )}

          <form onSubmit={this.handleSubmit}>
            <label htmlFor="mobile" className="col-6 text-right">
              Mobile:
            </label>
            <input
              value={empcontact.mobile}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="mobile"
              placeholder="Mobile"
              name="mobile"
              disabled={dataFound ? true : errors.msg ? true : false}
            />
            {errors.mobile ? (
              <div className="text-danger text-center">{errors.mobile}</div>
            ) : (
              ""
            )}
            <label htmlFor="address" className="col-6 text-right">
              Address:
            </label>
            <input
              value={empcontact.address}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="address"
              placeholder="Address"
              name="address"
              disabled={dataFound ? true : errors.msg ? true : false}
            />
            <label htmlFor="city" className="col-6 text-right">
              City:
            </label>
            <input
              value={empcontact.city}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="city"
              placeholder="City"
              name="city"
              disabled={dataFound ? true : errors.msg ? true : false}
            />
            <label htmlFor="country" className="col-6 text-right">
              Country:
            </label>
            <input
              value={empcontact.country}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="country"
              placeholder="Country"
              name="country"
              disabled={dataFound ? true : errors.msg ? true : false}
            />
            {errors.country ? (
              <div className="text-danger text-center">{errors.country}</div>
            ) : (
              ""
            )}
            <label htmlFor="pincode" className="col-6 text-right">
              Pincode:
            </label>
            <input
              value={empcontact.pincode}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="pincode"
              placeholder="Pincode"
              name="pincode"
              disabled={dataFound ? true : errors.msg ? true : false}
            />
            {errors.pincode ? (
              <div className="text-danger text-center">{errors.pincode}</div>
            ) : (
              ""
            )}
            <div className="col-12 text-center">
              <button
                className="btn btn-primary"
                disabled={dataFound ? true : errors.msg ? true : false}
              >
                Submit
              </button>
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default ContactDetails;
