import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import http from "./services/httpService";
import axios from "axios";
class DisplayDetails extends Component {
  state = {
    employeeDeptData: {
      department: "",
      designation: "",
      manager: "",
      empuserid: "",
    },
    errors: {},
    isSubmit: false,
  };
  async componentDidMount() {
    const { id } = this.props.match.params;
    const { data: employeeDeptData } = await axios.get(
      apiBaseURL + "/empdept/" + id
    );
    let dataFound = false;
    if (employeeDeptData.department) {
      dataFound = true;
    }
    this.setState({
      employeeDeptData,
      dataFound,
    });
  }
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    const { employeeDeptData } = this.state;
    const { id } = this.props.match.params;
    const apiEndpoint = apiBaseURL + "/empdept/" + id;
    try {
      await http.post(apiEndpoint, {
        department: employeeDeptData.department,
        designation: employeeDeptData.designation,
        manager: employeeDeptData.manager,
        empuserid: employeeDeptData.empuserid,
      });
      errors.msg = "Details have been successfully added";
      this.setState({ errors, isSubmit: true });
    } catch (ex) {}
  };
  validate = () => {
    let errs = {};
    const { employeeDeptData } = this.state;
    if (employeeDeptData.department && !employeeDeptData.department.trim())
      errs.department = "Department is required";
    if (employeeDeptData.designation && !employeeDeptData.designation.trim())
      errs.designation = "Designation is required";
    if (employeeDeptData.manager && !employeeDeptData.manager.trim())
      errs.manager = "Manager is required";

    return errs;
  };
  isFormvalid = () => {
    let errs = this.validate();

    let errCount = Object.keys(errs).length;

    return errCount <= 0;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "department":
        if (!e.currentTarget.value.trim()) return "Department is required";
        break;
      case "designation":
        if (!e.currentTarget.value.trim()) return "Designation is required";
        break;
      case "manager":
        if (!e.currentTarget.value.trim()) return "Manager is required";
        break;
      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const departmentData = { ...this.state.employeeDeptData };
    departmentData[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ employeeDeptData: departmentData, errors: errors });
  };
  render() {
    const { employeeDeptData, dataFound, errors } = this.state;
    return (
      <React.Fragment>
        <h2 className="text-center">Welcome to Employee Management Portal</h2>

        <div className=" mr-5 bg-light ml-5">
          <h4 className="text-center">Department Details of New Employee</h4>
          {errors.msg ? (
            <div className="text-success text-center">{errors.msg}</div>
          ) : dataFound ? (
            <div className="text-success text-center">
              Displaying Department Details
            </div>
          ) : (
            <div className="text-danger text-center">
              No Department Details Found. Please Enter Them.
            </div>
          )}

          <form onSubmit={this.handleSubmit}>
            <label htmlFor="dpt" className="col-6 text-right">
              Department:
            </label>
            <input
              value={employeeDeptData.department}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="department"
              placeholder="Enter the Employee's Department"
              name="department"
              disabled={dataFound ? true : errors.msg ? true : false}
            />
            {errors.department ? (
              <div className="text-danger text-center">{errors.department}</div>
            ) : (
              ""
            )}
            <label htmlFor="designation" className="col-6 text-right">
              Designation:
            </label>
            <input
              value={employeeDeptData.designation}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="designation"
              placeholder="Enter the Employee's Designation"
              name="designation"
              disabled={dataFound ? true : errors.msg ? true : false}
            />{" "}
            {errors.designation ? (
              <div className="text-danger text-center">
                {errors.designation}
              </div>
            ) : (
              ""
            )}
            <label htmlFor="manager" className="col-6 text-right">
              Manager's Name:
            </label>
            <input
              value={employeeDeptData.manager}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="manager"
              placeholder="Enter the Manager's Name"
              name="manager"
              disabled={dataFound ? true : errors.msg ? true : false}
            />
            {errors.manager ? (
              <div className="text-danger text-center">{errors.manager}</div>
            ) : (
              ""
            )}
            <div className="col-12 text-center">
              <button
                className="btn btn-primary"
                disabled={
                  dataFound && dataFound === true ? true : this.isFormvalid()
                }
              >
                Submit
              </button>
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default DisplayDetails;
