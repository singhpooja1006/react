import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import LoginForm from "./components/rct-emp-1a/login";
import NavBar from "./components/rct-emp-1a/navbar";
import Admin from "./components/rct-emp-1a/admin";
import Employee from "./components/rct-emp-1a/employee";
import Logout from "./components/rct-emp-1a/logout";
import ViewEmployee from "./components/rct-emp-1a/viewEmp";
import AddEmployee from "./components/rct-emp-1a/addEmp";
import ContactDetails from "./components/rct-emp-1a/contact";
import TableViewDisplay from "./components/rct-emp-1a/tableView";
import DisplayDetails from "./components/rct-emp-1a/details";
import HotelBill from "./components/rct-emp-1a/hotelBill";
import TravelBill from "./components/rct-emp-1a/travelBill";
import "./App.css";

class App extends Component {
  state = {};
  componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    this.setState({ user });
  }
  render() {
    const { user } = this.state;
    return (
      <React.Fragment>
        <NavBar user={user} />
        <Switch>
          <Route path="/admin/viewemp/:id" component={DisplayDetails} />
          <Route exact path="/admin/viewemp" component={ViewEmployee} />
          <Route path="/admin/addemp" component={AddEmployee} />
          <Route path="/admin" component={Admin} />
          <Route path="/emp/contact" component={ContactDetails} />
          <Route path="/emp/bills" component={TableViewDisplay} />
          <Route path="/emp/hotelbill/:billid" component={HotelBill} />
          <Route path="/emp/travelbill/:billid" component={TravelBill} />
          <Route path="/emp" component={Employee} />
          <Route path="/logout" component={Logout} />
          <Route path="/login" component={LoginForm} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;
