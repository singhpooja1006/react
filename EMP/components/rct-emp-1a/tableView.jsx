import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import axios from "axios";
import BillDetails from "./billDetails";
class TableViewDisplay extends Component {
  state = {
    billsData: {},
    view: -1,
    billsDetails: {
      description: "",
      amount: "",
      empuserid: "",
      expensetype: "",
    },
    error: {},
  };
  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));

    try {
      const { data: billsData } = await axios.get(
        apiBaseURL + "/empbills/" + user.empuserid
      );
      this.setState({ billsData });
    } catch (ex) {}
  }
  handleAddNewBill = () => {
    this.setState({ view: 0 });
  };
  handlePlusSquare = (billid, type) => {
    if (type === "Hotel") {
      window.location = "/emp/hotelbill/" + billid;
    }
    if (type === "Travel") {
      window.location = "/emp/travelbill/" + billid;
    }
  };
  render() {
    return <div className="container">{this.rendertableView()}</div>;
  }
  rendertableView = () => {
    const { billsData } = { ...this.state };

    return (
      <React.Fragment>
        <h2 className="text-center">Welcome to Employee Management Portal</h2>
        <div className="row mt-2">
          <b>Details of Bills Submitted</b>
        </div>
        <div className="row mt-2 bg-info text-center">
          <div className="col-2 text-center border">Id</div>
          <div className="col-4 text-center border">Description</div>
          <div className="col-3 text-center border text-wrap">Expense Head</div>
          <div className="col-2 text-center border">Amount</div>
          <div className="col-1 text-center border"></div>
        </div>
        {billsData.data
          ? billsData.data.map((bill) => (
              <div className="row border text-center">
                <div className="col-2 border ">{bill.billid}</div>
                <div className="col-4 border ">{bill.description}</div>
                <div className="col-3 border ">{bill.expensetype}</div>
                <div className="col-2 border ">{bill.amount}</div>
                <div className="col-1 border ">
                  {bill.expensetype === "Travel" ? (
                    <i
                      class="fa fa-plus-square"
                      aria-hidden="true"
                      onClick={() =>
                        this.handlePlusSquare(bill.billid, "Travel")
                      }
                    ></i>
                  ) : bill.expensetype === "Hotel" ? (
                    <i
                      class="fa fa-plus-square"
                      aria-hidden="true"
                      onClick={() =>
                        this.handlePlusSquare(bill.billid, "Hotel")
                      }
                    ></i>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            ))
          : ""}

        <div className="row" style={{ textDecoration: "underline" }}>
          <h4 className="ml-3" onClick={() => this.handleAddNewBill()}>
            Submit a New Bill
          </h4>
        </div>

        <div className="row">
          {this.state.view === 0 ? (
            <BillDetails
              billsDetails={this.state.billsData}
              error={this.state.error}
            />
          ) : (
            ""
          )}
        </div>
      </React.Fragment>
    );
    //   }
  };
}

export default TableViewDisplay;
