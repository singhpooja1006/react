import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import axios from "axios";
class ViewEmployee extends Component {
  state = {
    employeesData: {},
  };
  async componentDidMount() {
    const { data: employeesData } = await axios.get(apiBaseURL + "/emps");
    this.setState({
      employeesData,
    });
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.location !== this.props.location) {
      const { data: employeesData } = await axios.get(apiBaseURL + "/emps");
      this.setState({
        employeesData,
      });
    }
  }
  handleDeptDetails = (empid) => {
    window.location = "/admin/viewemp/" + empid;
  };
  render() {
    return <div className="container">{this.renderViewEmp()}</div>;
  }
  renderViewEmp = () => {
    const { employeesData } = { ...this.state };
    if (employeesData.data) {
      return (
        <React.Fragment>
          <div className="row mt-2">
            {employeesData.pageInfo.pageNumber} to
            {employeesData.pageInfo.numOfItems} of
            {employeesData.pageInfo.totalItemCount}
          </div>
          <div className="row mt-2 bg-info text-center">
            <div className="col-4 text-center border">Name</div>
            <div className="col-5 text-center border">EmailID</div>
            <div className="col-3 text-center border"></div>
          </div>
          {employeesData.data.map((employee) => (
            <div className="row border text-center">
              <div className="col-4 border ">{employee.name}</div>
              <div className="col-5 border">{employee.email}</div>
              <div className="col-3 border">
                {employee.role !== "ADMIN" ? (
                  <button
                    className="btn btn-secondary"
                    onClick={() => this.handleDeptDetails(employee.empuserid)}
                  >
                    Details
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          ))}
        </React.Fragment>
      );
    }
  };
}

export default ViewEmployee;
