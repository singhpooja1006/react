import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import http from "./services/httpService";
import axios from "axios";
class HotelBill extends Component {
  state = {
    hotelsBillData: {
      empuserid: "",
      billid: "",
      staystartdate: "",
      stayenddate: "",
      city: "",
      hotel: "",
      corpbooking: "",
      staystartday: "",
      staystartmonth: "",
      staystartyear: "",
      stayendday: "",
      stayendmonth: "",
      stayendyear: "",
    },
    isDataFetched: false,

    days: [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,
      31,
    ],
    months: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
    years: [2018, 2019, 2020],
    errors: {},
  };
  async componentDidMount() {
    const { billid } = this.props.match.params;
    const empuserid = JSON.parse(localStorage.getItem("user")).empuserid;
    const { data: hotelsBillData } = await axios.get(
      apiBaseURL + "/hotelbill/" + empuserid + "/" + billid
    );
    let isDataFetched = false;
    if (hotelsBillData.corpbooking) {
      let stayInDate = hotelsBillData.staystartdate;
      let stayOutDate = hotelsBillData.staystartdate;
      hotelsBillData.staystartday = stayInDate.substring(
        0,
        stayInDate.indexOf("-")
      );
      hotelsBillData.staystartmonth = stayInDate.substring(
        stayInDate.indexOf("-") + 1,
        stayInDate.lastIndexOf("-")
      );
      hotelsBillData.staystartyear = stayInDate.substring(
        stayInDate.lastIndexOf("-") + 1,
        stayInDate.length
      );
      hotelsBillData.stayendday = stayOutDate.substring(
        0,
        stayOutDate.indexOf("-")
      );
      hotelsBillData.stayendmonth = stayOutDate.substring(
        stayOutDate.indexOf("-") + 1,
        stayOutDate.lastIndexOf("-")
      );
      hotelsBillData.stayendyear = stayOutDate.substring(
        stayOutDate.lastIndexOf("-") + 1,
        stayOutDate.length
      );
      isDataFetched = true;
    } else {
      hotelsBillData.corpbooking = "No";
      hotelsBillData.hotel = "";
      hotelsBillData.city = "";
      hotelsBillData.staystartday = "";
      hotelsBillData.staystartmonth = "";
      hotelsBillData.staystartyear = "";
      hotelsBillData.stayendday = "";
      hotelsBillData.stayendmonth = "";
      hotelsBillData.stayendyear = "";
    }
    this.setState({
      hotelsBillData,
      isDataFetched,
    });
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    const { hotelsBillData } = this.state;
    const empuserid = JSON.parse(localStorage.getItem("user")).empuserid;
    const apiEndpoint = apiBaseURL + "/hotelbill";
    try {
      await http.post(apiEndpoint, {
        billid: hotelsBillData.billid,
        empuserid: hotelsBillData.empuserid,
        staystartdate:
          hotelsBillData.staystartday +
          "-" +
          hotelsBillData.staystartmonth +
          "-" +
          hotelsBillData.staystartyear,
        stayenddate:
          hotelsBillData.stayendday +
          "-" +
          hotelsBillData.stayendmonth +
          "-" +
          hotelsBillData.stayendyear,
        hotel: hotelsBillData.hotel,
        city: hotelsBillData.city,
        corpbooking: hotelsBillData.corpbooking,
      });
      errors.msg = "Hotel Stay Details have been successfully created";
      this.setState({ errors, isDataFetched: true });
    } catch (ex) {}
  };
  validate = () => {
    let errs = {};
    if (!this.state.hotelsBillData.staystartday)
      errs.staystartday = "Checked In Day is required";
    if (!this.state.hotelsBillData.staystartmonth)
      errs.staystartmonth = "Checked In Month is required";
    if (!this.state.hotelsBillData.staystartyear)
      errs.staystartyear = "Checked In Year is required";
    if (!this.state.hotelsBillData.stayendday)
      errs.stayendday = "Checked Out Day is required";
    if (!this.state.hotelsBillData.stayendmonth)
      errs.stayendmonth = "Checked Out Month is required";
    if (!this.state.hotelsBillData.stayendyear)
      errs.stayendyear = "Checked Out Year is required";
    if (!this.state.hotelsBillData.hotel.trim())
      errs.hotel = "Hotel Name is required";

    if (!this.state.hotelsBillData.city.trim()) errs.city = "City is required";
    return errs;
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const hotelsBillData = { ...this.state.hotelsBillData };
    if (e.currentTarget.type === "checkbox") {
      if (hotelsBillData[e.currentTarget.name] === "No") {
        hotelsBillData.corpbooking = "Yes";
      } else {
        hotelsBillData.corpbooking = "No";
      }
    } else {
      hotelsBillData[e.currentTarget.name] = e.currentTarget.value;
    }

    this.setState({ hotelsBillData, errors });
  };

  isFormvalid = () => {
    let errs = this.validate();

    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "hotel":
        if (!e.currentTarget.value.trim()) return "Hotel Name is required";
        break;
      case "city":
        if (!e.currentTarget.value.trim()) return "City is required";
        break;
      default:
        break;
    }
    return "";
  };
  render() {
    const { hotelsBillData, isDataFetched, errors } = this.state;
    const { billid } = this.props.match.params;
    return (
      <React.Fragment>
        <h2 className="text-center">Welcome to Employee Management Portal</h2>
        <div className=" mr-5 bg-light ml-5 mt-2">
          <h4 className="text-center">Hotel Stay Details</h4>
          <div className="text-center">Bill Id : {billid}</div>
          {!errors.msg && isDataFetched === true ? (
            <div className="text-success text-center">
              Displaying Hotel Bill Details
            </div>
          ) : errors.msg ? (
            <div className="text-primary text-center">{errors.msg}</div>
          ) : (
            <div className="text-primary text-center">
              No Hotel Stay Details Found. Please Enter Them.
            </div>
          )}
          <form onSubmit={this.handleSubmit}>
            <fieldset disabled={isDataFetched}>
              <label htmlFor="dpt" className="col-6 text-right">
                Check In date:
              </label>
              {this.renderDropDown("staystart")}
              <label htmlFor="dpt" className="col-6 text-right">
                Check Out date:
              </label>
              {this.renderDropDown("stayend")}
              <label htmlFor="hotel" className="col-6 text-right">
                Hotel:
              </label>
              <input
                value={hotelsBillData.hotel}
                type="text"
                onChange={this.handleChange}
                className="col-6"
                id="hotel"
                name="hotel"
                placeholder="Name of the Hotel"
              />
              {errors.hotel ? (
                <div className="text-danger text-center">{errors.hotel}</div>
              ) : (
                ""
              )}
              <label htmlFor="city" className="col-6 text-right">
                City:
              </label>
              <input
                value={hotelsBillData.city}
                type="text"
                onChange={this.handleChange}
                className="col-6"
                id="city"
                name="city"
                placeholder="City"
              />
              {errors.city ? (
                <div className="text-danger text-center">{errors.city}</div>
              ) : (
                ""
              )}
              <div class="form-group col-7 ml-5 text-right">
                <div class="form-check ">
                  <input
                    value={hotelsBillData.corpbooking}
                    class="form-check-input"
                    type="checkbox"
                    id="corpbooking"
                    name="corpbooking"
                    onChange={this.handleChange}
                    checked={
                      hotelsBillData.corpbooking === "Yes" ? true : false
                    }
                  />
                  <label htmlFor="corpbooking" className="form-check-label">
                    Corporate Booking
                  </label>
                </div>
              </div>

              <div className="col-12 text-center">
                <button
                  className="btn btn-primary"
                  disabled={this.isFormvalid()}
                >
                  Submit
                </button>
              </div>
            </fieldset>
          </form>
        </div>
      </React.Fragment>
    );
  }
  renderDropDown = (param) => {
    const { hotelsBillData, days, months, years, errors } = this.state;

    return (
      <React.Fragment>
        <select
          value={
            param === "staystart"
              ? hotelsBillData.staystartday
              : hotelsBillData.stayendday
          }
          onChange={this.handleChange}
          id={param ? param + "day" : "SelectedDay"}
          name={param ? param + "day" : "SelectedDay"}
          className="browser-default custom-select col-2 mb-1"
        >
          <option disabled>Day</option>
          {days.map((day) => (
            <option key={day}>{day}</option>
          ))}
        </select>
        {param === "staystart" && errors.staystartday ? (
          <div className="text-danger text-center">{errors.staystartday}</div>
        ) : (
          ""
        )}
        {param === "stayend" && errors.stayendday ? (
          <div className="text-danger text-center">{errors.stayendday}</div>
        ) : (
          ""
        )}

        <select
          value={
            param === "staystart"
              ? hotelsBillData.staystartmonth
              : hotelsBillData.stayendmonth
          }
          onChange={this.handleChange}
          id={param ? param + "month" : "SelectedMonth"}
          name={param ? param + "month" : "SelectedMonth"}
          className="browser-default custom-select col-2 mb-1"
        >
          <option disabled>Month</option>
          {months.map((month) => (
            <option key={month}>{month}</option>
          ))}
        </select>
        {param === "staystart" && errors.staystartmonth ? (
          <div className="text-danger text-center">{errors.staystartmonth}</div>
        ) : (
          ""
        )}
        {param === "stayend" && errors.stayendmonth ? (
          <div className="text-danger text-center">{errors.stayendmonth}</div>
        ) : (
          ""
        )}
        <select
          value={
            param === "staystart"
              ? hotelsBillData.staystartyear
              : hotelsBillData.stayendyear
          }
          onChange={this.handleChange}
          id={param ? param + "year" : "SelectedYear"}
          name={param ? param + "year" : "SelectedYear"}
          className="browser-default custom-select col-2 mb-1"
        >
          <option disabled>Year</option>
          {years.map((year) => (
            <option key={year}>{year}</option>
          ))}
        </select>
        {param === "staystart" && errors.staystartyear ? (
          <div className="text-danger text-center">{errors.staystartyear}</div>
        ) : (
          ""
        )}
        {param === "stayend" && errors.stayendyear ? (
          <div className="text-danger text-center">{errors.stayendyear}</div>
        ) : (
          ""
        )}
      </React.Fragment>
    );
  };
}

export default HotelBill;
