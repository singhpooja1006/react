import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import http from "./services/httpService";
class AddEmployee extends Component {
  state = {
    data: { name: "", email: "", password: "", reEnter: "" },
    errors: {},
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const apiEndpoint = apiBaseURL + "/emps";
      await http.post(apiEndpoint, {
        email: this.state.data.email,
        password: this.state.data.password,
        name: this.state.data.name,
        role: "EMPLOYEE",
      });
      const errors = { ...this.state.errors };
      errors.msg = "Employee successfully added";
      this.setState({ errors });
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.msg = "Database Error.";
        this.setState({ errors });
      }
    }
  };
  validate = () => {
    var pattern = new RegExp("^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])");
    let errs = {};
    if (!this.state.data.name.trim()) errs.name = "Name is required";
    else if (this.state.data.name.trim().length < 8)
      errs.name = "Name should have at least 8 characters";

    if (!this.state.data.email.trim()) errs.email = "Email is required";
    else if (this.state.data.email.includes("@") === false)
      errs.email = "Not a Valid Email";

    if (!this.state.data.password.trim())
      errs.password = "Password is required";
    else if (!pattern.test(this.state.data.password.trim()))
      errs.password =
        "Password should be min, 8 char with a lowercase,uppercase,digit";
    if (!this.state.data.reEnter.trim())
      errs.reEnter = "Re-Enter password is required";
    else if (this.state.data.password !== this.state.data.reEnter)
      errs.reEnter = "Password do not Match";
    return errs;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "name":
        if (!e.currentTarget.value.trim()) return "Name is required";
        else if (e.currentTarget.value.trim().length < 8)
          return "Name should have at least 8 characters";
        break;
      case "email":
        if (!e.currentTarget.value.trim()) return "Email is required";
        else if (e.currentTarget.value.includes("@") === false)
          return "Not a Valid Email";
        break;
      case "password":
        var pattern = new RegExp(
          "^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])"
        );
        if (!e.currentTarget.value.trim()) return "Password is required";
        else if (!pattern.test(e.currentTarget.value.trim()))
          return "Password should be min, 8 char with a lowercase,uppercase,digit";
        break;
      case "reEnter":
        if (!e.currentTarget.value.trim())
          return "Re-Enter password is required";
        else if (e.currentTarget.value !== this.state.data.password)
          return "Password do not Match";
        break;

      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const addEmpData = { ...this.state.data };
    addEmpData[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data: addEmpData, errors: errors });
  };
  render() {
    const { data } = this.state;
    return (
      <React.Fragment>
        <h2 className="text-center">Welcome to Employee Management Portal</h2>
        <div className=" mr-5 bg-light ml-5">
          <h4 className="text-center">Add New Employee</h4>
          {this.state.errors.msg ? (
            this.state.errors.msg === "Database Error." ? (
              <div className="text-danger col-12 text-center">
                {this.state.errors.msg}
              </div>
            ) : (
              <div className="text-success col-12 text-center">
                {this.state.errors.msg}
              </div>
            )
          ) : (
            ""
          )}
          <form onSubmit={this.handleSubmit}>
            <label htmlFor="name" className="col-6 text-right">
              Name:
            </label>
            <input
              value={data.name}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="name"
              placeholder="Enter the Employee Name"
              name="name"
            />
            {this.state.errors.name ? (
              <div className="text-danger text-center">
                {this.state.errors.name}
              </div>
            ) : (
              ""
            )}
            <label htmlFor="email" className="col-6 text-right">
              Email:
            </label>
            <input
              value={data.email}
              type="text"
              onChange={this.handleChange}
              className="col-6"
              id="email"
              placeholder="Enter the Employee's email"
              name="email"
            />
            {this.state.errors.email ? (
              <div className="text-danger text-center">
                {this.state.errors.email}
              </div>
            ) : (
              ""
            )}

            <label htmlFor="password" className="col-6 mt-2 text-right">
              Password:
            </label>
            <input
              value={data.password}
              type="password"
              onChange={this.handleChange}
              className="col-6 mt-2"
              id="password"
              placeholder="Enter the Password"
              name="password"
            />
            {this.state.errors.password ? (
              <div className="text-danger text-center">
                {this.state.errors.password}
              </div>
            ) : (
              ""
            )}
            <label htmlFor="reEnter" className="col-6 mt-2 text-right"></label>
            <input
              value={data.reEnter}
              type="password"
              onChange={this.handleChange}
              className="col-6 mt-2"
              id="reEnter"
              placeholder="Re-Enter the Password"
              name="reEnter"
            />
            {this.state.errors.reEnter ? (
              <div className="text-danger text-center">
                {this.state.errors.reEnter}
              </div>
            ) : (
              ""
            )}
            <div className="col-12 text-center">
              <button className="btn btn-primary mt-2">Add</button>
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default AddEmployee;
