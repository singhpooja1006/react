import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import http from "./services/httpService";
import axios from "axios";
class BillDetails extends Component {
  state = {
    billsDetails: {
      description: "",
      amount: "",
      empuserid: "",
      expensetype: "",
    },
    errors: {},
    isFormSaved: false,
  };

  getDecimalValidation = (value) => {
    let pattern = new RegExp("^[0-9]+(?:.[0-9]{1,2})?$");
    return pattern.test(value);
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    const { billsDetails } = this.state;
    const empuserid = JSON.parse(localStorage.getItem("user")).empuserid;
    const apiEndpoint = apiBaseURL + "/empbills/" + empuserid;
    try {
      await http.post(apiEndpoint, {
        description: billsDetails.description,
        amount: billsDetails.amount,
        expensetype: billsDetails.expensetype,
        empuserid: empuserid,
      });
      errors.msg = "New Bill has been successfully created";
      this.setState({ errors, isFormSaved: true });
    } catch (ex) {}
  };
  validate = () => {
    let errs = {};
    const { billsDetails } = this.state;
    if (!billsDetails.description.trim())
      errs.description = "Description is required";
    if (!billsDetails.expensetype)
      errs.expensetype = "Expense Type is required";
    if (!billsDetails.amount.trim()) errs.amount = "Amount is Required";
    else if (!this.getDecimalValidation(billsDetails.amount))
      errs.amount = "Not a valid Amount";
    return errs;
  };
  isFormvalid = () => {
    let errs = this.validate();
    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "description":
        if (!e.currentTarget.value) return "Description is required";

        break;
      case "expensetype":
        if (!e.currentTarget.value) return "Expense Type required";

        break;
      case "amount":
        if (!e.currentTarget.value) return "Amount is required";
        else if (!this.getDecimalValidation(e.currentTarget.value))
          return "Not a valid Amount";
        break;
      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const empBillData = { ...this.state.billsDetails };
    empBillData[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ billsDetails: empBillData, errors: errors });
  };
  render() {
    const { billsDetails, errors, isFormSaved } = this.state;

    return (
      <React.Fragment>
        <div className=" mr-5 bg-light ml-5">
          <h4 className="text-center">Enter Details of the new Bill</h4>
          {errors.msg ? (
            <div className="text-success text-center">{errors.msg}</div>
          ) : (
            ""
          )}
          <form onSubmit={this.handleSubmit}>
            <fieldset disabled={isFormSaved}>
              <label htmlFor="description" className="col-6 text-right">
                Description:
              </label>
              <input
                value={billsDetails.description}
                type="text"
                onChange={this.handleChange}
                className="col-6"
                id="description"
                name="description"
              />
              {errors.description ? (
                <div className="text-danger text-center">
                  {errors.description}
                </div>
              ) : (
                ""
              )}
              <label htmlFor="expensetype" className="col-6 text-right">
                Expense Type:
              </label>
              <select
                value={billsDetails.expensetype}
                onChange={this.handleChange}
                id="expensetype"
                name="expensetype"
                className="col-6"
              >
                <option></option>
                <option>Travel</option>
                <option>Hotel</option>
                <option>Software</option>
                <option>Communication</option>
                <option>Others</option>
              </select>
              {errors.expensetype ? (
                <div className="text-danger text-center">
                  {errors.expensetype}
                </div>
              ) : (
                ""
              )}
              <label htmlFor="amount" className="col-6 text-right">
                Amount:
              </label>
              <input
                value={billsDetails.amount}
                type="text"
                onChange={this.handleChange}
                className="col-6"
                id="amount"
                name="amount"
              />
              {errors.amount ? (
                <div className="text-danger text-center">{errors.amount}</div>
              ) : (
                ""
              )}
              <div className="col-12 text-center">
                <button
                  className="btn btn-primary"
                  disabled={this.isFormvalid() === true ? true : false}
                >
                  Submit
                </button>
              </div>
            </fieldset>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default BillDetails;
