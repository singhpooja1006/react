import React, { Component } from "react";
import { getMovies, deleteMovie } from "../services/movieService";
import { getGenres } from "../services/genreService";
import _ from "lodash";
import MoviesTable from "./moviesTable";
import { paginate } from "./../utils/paginate";
import ListGroup from "./common/listGroup";
import Pagination from "./common/pagination";
import { Link } from "react-router-dom";

class Movies extends Component {
  state = {
    movies: [],
    genres: [],
    currentPage: 1,
    pageSize: 4,
    sortColumn: { path: "title", order: "asc" },
    view: 0,
    searchText: "",
  };
  async componentDidMount() {
    const { data } = await getGenres();
    const genres = [{ _id: "", name: "All" }, ...data];
    const { data: movies } = await getMovies();
    this.setState({
      movies,
      genres,
      selectedGenre: genres[0],
    });
  }
  handleDelete = async (movie) => {
    const originalMovies = this.state.movies;
    const movies = originalMovies.filter((m) => m.id !== movie.id);
    this.setState({ movies });
    try {
      await deleteMovie(movie);
    } catch (ex) {
      if (ex.response && ex.response.status !== 404) {
        this.setState({ movies: originalMovies });
      }
    }
    this.handlePageChange(this.state.currentPage);
  };

  handleChange = (ele) => {
    const data = { ...this.state.searchText };
    data[ele.currentTarget.name] = ele.currentTarget.value;
    this.setState({ searchText: data.searchText });
  };

  handleLike = (movie) => {
    const movies = [...this.state.movies];
    const index = movies.indexOf(movie);
    movies[index] = { ...movies[index] };
    movies[index].liked = !movies[index].liked;
    this.setState({ movies });
  };
  handlePageChange = (page) => {
    //this.setState({ currentPage: page });
    const pagesCount = Math.ceil(
      this.state.movies.length / this.state.pageSize
    );
    let currentPage = Math.min(Math.max(page, 1), pagesCount);
    this.setState({ currentPage });
  };
  handleGenreSelect = (genre) => {
    console.log("Selected Gen", genre);
    this.setState({ selectedGenre: genre, currentPage: 1, searchText: "" });
  };
  handleSort = (sortColumn) => {
    this.setState({ sortColumn });
  };

  handleSubmit = (newMovie) => {
    let movies = [...this.state.movies];
    movies.push(newMovie);
    this.setState({ movies: movies, view: 0 });
  };
  getPagedData = () => {
    const {
      pageSize,
      currentPage,
      selectedGenre,
      movies,
      sortColumn,
    } = this.state;

    let filtered = movies;

    if (this.state.searchText) {
      filtered = filtered.filter(
        (m) =>
          m.title.toUpperCase().indexOf(this.state.searchText.toUpperCase()) >=
          0
      );
    }

    if (selectedGenre && selectedGenre.id) {
      filtered = filtered.filter((m) => m.genre.id === selectedGenre.id);
    }

    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
    const paginatedMovies = paginate(sorted, currentPage, pageSize);
    return { totalCount: filtered.length, data: paginatedMovies };
  };

  render() {
    const { pageSize, currentPage, sortColumn } = this.state;
    const { totalCount, data: movies } = this.getPagedData();
    const { user } = this.props;
    if (totalCount === 0) return <p>There are no movies in the database.</p>;
    console.log("rendring movies", movies);
    return (
      <div className="row">
        <div className="col-3">
          <ListGroup
            items={this.state.genres}
            selectedItem={this.state.selectedGenre}
            onItemSelect={this.handleGenreSelect}
          />
        </div>
        <div className="col">
          <div className="mt-2">
            {user && (
              <Link className="btn btn-primary mb-2" to="/movies/new">
                {" "}
                New Movie
              </Link>
            )}
          </div>
          <p className="mt-3">Showing {totalCount} movies in the database.</p>
          <input
            type="text"
            value={this.state.searchText}
            name="searchText"
            className="form-control"
            placeholder="Search..."
            onChange={this.handleChange}
          />
          <MoviesTable
            movies={movies}
            sortColumn={sortColumn}
            onLike={this.handleLike}
            onDelete={this.handleDelete}
            onSort={this.handleSort}
          />
          <Pagination
            itemsCount={totalCount}
            pageSize={pageSize}
            currentPage={currentPage}
            onPageChange={this.handlePageChange}
          />
        </div>
      </div>
    );
  }
}

export default Movies;
