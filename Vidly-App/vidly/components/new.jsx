import React, { Component } from "react";
import { saveMovie, getMovie } from "../services/movieService";
import { getGenres } from "../services/genreService";
class NewForm extends Component {
  state = {
    movieId: "",
    movieData: { title: "", numberInStock: 0, dailyRentalRate: 0, genreId: "" },
    genres: [],
    errors: {},
  };
  async componentDidMount() {
    await this.fetchGenres();
    const movieId = this.props.match.params.id;
    const { movieData } = this.state;
    if (movieId === "new") return;
    const { data: movie } = await getMovie(movieId);
    movieData._id = movie.id;
    movieData.title = movie.title;
    movieData.genreId = movie.genre.id;
    movieData.numberInStock = movie.numberInStock;
    movieData.dailyRentalRate = movie.dailyRentalRate;
    this.setState({ movieData });
  }
  async fetchGenres() {
    const { data: genres } = await getGenres();
    genres.splice(0, 0, { id: "", name: "Select a Genre" });
    this.setState({ genres });
  }
  isFormvalid = () => {
    let errs = this.validate();
    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "title":
        if (!e.currentTarget.value) return "Title is required";
        break;
      case "genreId":
        if (!e.currentTarget.value) return "Dropdown, Required";
        break;
      case "numberInStock":
        if (parseInt(e.currentTarget.value) < 0)
          return "Number in Stock & must be a larger than or equal to 0";
        break;
      case "dailyRentalRate":
        if (parseInt(e.currentTarget.value) > 10)
          return "Daily Rental Rate must be less than or equal to 10";
        break;
      default:
        break;
    }
    return "";
  };
  validate = () => {
    let errs = {};
    if (!this.state.movieData.title) errs.title = "Title is required";
    if (!this.state.movieData.genreId) errs.genre = "Dropdown, Required";

    if (parseInt(this.state.movieData.numberInStock) < 0)
      errs.numberInStock =
        "Number in Stock & must be a larger than or equal to 0";
    if (parseInt(this.state.movieData.dailyRentalRate) > 10)
      errs.dailyRentalRate =
        "Daily Rental Rate & must be less than or equal to 10";
    return errs;
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const data = { ...this.state.movieData };
    if (e.currentTarget.name === "genreId") {
      data[e.currentTarget.name] = this.state.genres.find(
        (genre) => genre.name === e.currentTarget.value
      ).id;
    } else {
      data[e.currentTarget.name] = e.currentTarget.value;
    }

    this.setState({ movieData: data, errors: errors });
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      await saveMovie(this.state.movieData);
      this.props.history.push("/movies");
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.msg = ex.response.data;
        this.setState({ errors });
      }
    }
  };

  render() {
    const { movieData, genres } = this.state;
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          <h2>Movie Form</h2>
          <div className="form-group">
            <lable htmlFor="title">Title</lable>
            <input
              value={movieData.title}
              onChange={this.handleChange}
              id="title"
              name="title"
              type="text"
              className="form-control"
            />
            {this.state.errors.title ? (
              <div className="alert alert-danger">
                {this.state.errors.title}
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <lable htmlFor="genreId">Genre</lable>
            <select
              value={
                genres && genres.length > 0
                  ? genres.find((genre) => genre.id === movieData.genreId).name
                  : ""
              }
              onChange={this.handleChange}
              id="genreId"
              name="genreId"
              className="form-control"
            >
              {genres.map((genre) => (
                <option>{genre.name}</option>
              ))}
            </select>
            {this.state.errors.genre ? (
              <div className="alert alert-danger">
                {this.state.errors.genre}
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <lable htmlFor="numberInStock">Number in Stock</lable>
            <input
              value={movieData.numberInStock}
              onChange={this.handleChange}
              id="numberInStock"
              name="numberInStock"
              type="number"
              className="form-control"
            />
            {this.state.errors.numberInStock ? (
              <div className="alert alert-danger">
                {this.state.errors.numberInStock}
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <lable htmlFor="dailyRentalRate">Rate</lable>
            <input
              value={movieData.dailyRentalRate}
              onChange={this.handleChange}
              id="dailyRentalRate"
              name="dailyRentalRate"
              type="number"
              className="form-control"
            />
            {this.state.errors.dailyRentalRate ? (
              <div className="alert alert-danger">
                {this.state.errors.dailyRentalRate}
              </div>
            ) : (
              ""
            )}
          </div>
          <button className="btn btn-primary" disabled={this.isFormvalid()}>
            Save
          </button>
        </form>
      </div>
    );
  }
}

export default NewForm;
