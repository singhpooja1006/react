import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import auth from "../services/authService";
class LoginForm extends Component {
  state = {
    data: { username: "", password: "" },
    errors: {},
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const { data } = this.state;
      await auth.login(data.username, data.password);
      const { state } = this.props.location;
      window.location = state ? state.from.pathname : "/";
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.username = ex.response.data;
        this.setState({ errors });
      }
    }
  };
  validate = () => {
    let errs = {};

    if (!this.state.data.username.trim())
      errs.username = "Username is required";
    else if (this.state.data.username.includes("@") === false)
      errs.email = "Username must be a valid email";

    if (!this.state.data.password.trim())
      errs.password = "Password is required";
    else if (this.state.data.password.trim().length < 5)
      errs.password = "Password length must be at least 5 characters long";
    return errs;
  };
  isFormvalid = () => {
    let errs = this.validate();

    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "username":
        if (!e.currentTarget.value.trim()) return "Username is required";
        else if (e.currentTarget.value.includes("@") === false)
          return "Username must be a valid email";
        break;
      case "password":
        if (!e.currentTarget.value.trim()) return "Password is required";
        else if (e.currentTarget.value.trim().length < 5)
          return "Password length must be at least 5 characters long";
        break;
      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const loginData = { ...this.state.data };
    loginData[e.currentTarget.name] = e.currentTarget.value;

    this.setState({ data: loginData, errors: errors });
  };
  render() {
    if (auth.getCurrentUser()) return <Redirect to="/" />;
    const { data } = this.state;
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          <h1>Login</h1>
          <div className="form-group">
            <label htmlFor="username">Username</label>
            <input
              value={data.username}
              onChange={this.handleChange}
              type="text"
              id="username"
              name="username"
              className="form-control"
            />
            {this.state.errors.username ? (
              <div className="alert alert-danger">
                {this.state.errors.username}
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="emailid">Password</label>
            <input
              value={data.password}
              onChange={this.handleChange}
              type="password"
              id="password"
              name="password"
              className="form-control"
            />
            {this.state.errors.password ? (
              <div className="alert alert-danger">
                {this.state.errors.password}
              </div>
            ) : (
              ""
            )}
          </div>

          <button className="btn btn-primary" disabled={this.isFormvalid()}>
            Login
          </button>
        </form>
      </div>
    );
  }
}

export default LoginForm;
