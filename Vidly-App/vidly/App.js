import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import NavBar from "./components/vidly/components/navBar";
import "./App.css";

import RegisterForm from "./components/vidly/components/registerForm";
import LoginForm from "./components/vidly/components/login";
import Logout from "./components/vidly/components/logout";
import Movies from "./components/vidly/components/movies";
import auth from "./components/vidly/services/authService";
import NewForm from "./components/vidly/components/new";
import ProtectedRoute from "./components/vidly/components/common/protectedRoute";
class App extends Component {
  state = {};

  componentDidMount() {
    const user = auth.getCurrentUser();
    this.setState({ user });
  }

  render() {
    const { user } = this.state;
    console.log("User ", user);
    return (
      <React.Fragment>
        <main className="container">
          <NavBar user={user} />
          <Switch>
            <Route path="/login" component={LoginForm} />
            <Route path="/register" component={RegisterForm} />
            <ProtectedRoute path="/movies/:id" component={NewForm} />
            <Route
              path="/movies"
              render={(props) => <Movies {...props} user={user} />}
            />
            <Route path="/logout" component={Logout} />
            <Redirect exact from="/" to="/movies" />
          </Switch>
        </main>
      </React.Fragment>
    );
  }
}

export default App;
