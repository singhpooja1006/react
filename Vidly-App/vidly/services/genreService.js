import http from "./httpService";
import { apiBaseURL } from "../config/config.json";
const apiEndpoint = apiBaseURL + "/genres";

export function getGenres() {
  return http.get(apiEndpoint);
}
