import http from "./httpService";
import { apiBaseURL } from "../config/config.json";
const apiEndpoint = apiBaseURL + "/users";
export function register(user) {
  console.log("URL", apiEndpoint);
  return http.post(apiEndpoint, {
    email: user.username,
    password: user.password,
    name: user.name,
  });
}
