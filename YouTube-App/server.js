var express = require("express");
var fs = require('fs');
var mysql = require('mysql');
//Install these packages in your node
var {google} = require("googleapis");
var readline = require('readline');
var OAuth2 = google.auth.OAuth2;
var SCOPES = ["https://www.googleapis.com/auth/youtube.readonly"];
var TOKEN_DIR = 
"C:/Users/Ashish Singh/credentials/";
var TOKEN_PATH = TOKEN_DIR + "youtube-nodejs-quickstart.json";
var app = express();
console.log(`Token path :: ${TOKEN_PATH}`);
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
    next();
});
80586593

64068676
const port  = 2410;
app.listen(port,()=>console.log("Listening on port : ",port))
const connectionDetails = {
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'mysqldb',
    port: "3306",
    connectionLimit: 50
}
//const connection = mysql.createConnection();
///Make a function authenticate

function authorize(credentials,callback){
    var clientSecret = credentials.installed.client_secret;
    var clientId = credentials.installed.client_id;
    var redirectUrl = credentials.installed.redirect_uris[0];
    var oauth2Client = new OAuth2(clientId,clientSecret,redirectUrl);
    fs.readFile(TOKEN_PATH,function(err,token){
        console.log(err)
        if(err){
            getNewToken(oauth2Client,callback);
        }else{
            oauth2Client.credentials = JSON.parse(token);
            callback(oauth2Client);
        }
    });
}

//Make a function which will generate token

function getNewToken(oauth2Client,callback){
    var authUrl = oauth2Client.generateAuthUrl({
        access_type:"offline",
        scope: SCOPES
    });
    console.log("Authorise this app by visiting this url: ", authUrl);
    var r1 = readline.createInterface({
        input: process.stdin,
        output:process.stdout
    });
    r1.question("Enter the code from that page here: ", function(code) {
        r1.close();
        oauth2Client.getToken(code,function (err,token) {
           if(err) {
               console.log("Error while trying to retrieve access token", err);
               return;
           }
           oauth2Client.credentials = token;
           storeToken(token);
           callback(oauth2Client);
        });
    });
}

//Make function storeToken which will store the token generated

function storeToken(token) {
    try{
        fs.mkdirSync(TOKEN_DIR);
    }catch(err){
        if(err.code != "EEXIST"){
            throw err;
        }
    }
    fs.writeFile(TOKEN_PATH,JSON.stringify(token),err=>{
        if(err) throw err;
        console.log("Token Stored to "+ TOKEN_PATH);
    });
}

//Now make a request which will have fetch channelId as query  named q. 

app.get("/playlists",function(req,res) {
    channelId = req.query.q;
    console.log(channelId);
    fs.readFile("client_secret.json",function processClientSecrets(err,content) {
        if(err){
            console.log("Error loading client secret file: "+ err);
            return;
        }
        authorize(JSON.parse(content),searchByKeyword);
        function searchByKeyword(auth) {
            let list;
            const youtube = google.youtube({
             version: "v3",
             auth: auth  
            });
          var results = youtube.playlists.list({
                part:"snippet,contentDetails",
                channelId:channelId,
                maxResults:25
          }); 
          results.then(function(res1) {
              list = res1.data.items;
              playlistList = list;
              res.send(list);
          });
        }
    });
});
app.get("/playlistVideos",function(req,res) {
    playlistId = req.query.id;
    console.log(playlistId);
    fs.readFile("client_secret.json",function processClientSecrets(err,content) {
        if(err){
            console.log("Error loading client secret file: "+ err);
            return;
        }
        authorize(JSON.parse(content),searchByKeyword);
        function searchByKeyword(auth) {
            let list;
            const youtube = google.youtube({
             version: "v3",
             auth: auth  
            });
          var results = youtube.playlistItems.list({
                part:"snippet,contentDetails",
                playlistId:playlistId,
          }); 
          results.then(function(res1) {
              list = res1.data.items;
              playlistItemList = list;
              res.send(list);
          });
        }
    });
});

app.get("/getPlayListDetails",function(req,res) {
        channelId = req.query.q;
        console.log(channelId);
        fs.readFile("client_secret.json",function processClientSecrets(err,content) {
            if(err){
                console.log("Error loading client secret file: "+ err);
                return;
            }
            authorize(JSON.parse(content),searchByKeyword);
            function searchByKeyword(auth) {
                let list;
                const youtube = google.youtube({
                 version: "v3",
                 auth: auth  
                });
              var results = youtube.playlists.list({
                    part:"snippet,contentDetails",
                    id:channelId,
              }); 
              results.then(function(res1) {
                  list = res1.data.items;
                  playlistList = list;
                  res.send(list);
            });
         }
    });
});
app.get("/getChannelDetails",function(req,res) {
    channelId = req.query.q;
    console.log(channelId);
    fs.readFile("client_secret.json",function processClientSecrets(err,content) {
        if(err){
            console.log("Error loading client secret file: "+ err);
            return;
        }
        authorize(JSON.parse(content),searchByKeyword);
        function searchByKeyword(auth) {
            let list;
            const youtube = google.youtube({
             version: "v3",
             auth: auth  
            });
          var results = youtube.channels.list({
                part:"snippet,contentDetails,statistics",
                id:channelId,
          }); 


          results.then(function(res1) {
              list = res1.data.items;
              playlistList = list;
              res.send(list);
        });
     }
});
});

app.post("/login", function (req, res) {
    const connection = mysql.createConnection(connectionDetails);
    connection.connect((error) => {
         if (error) res.status(500).send("unable to connnect mysql");
        var sql = "select * from users where email = ? and password = ?";
        connection.query(sql, [req.body.email, req.body.password],function (error, result) {
            if (error) res.status(500).send(error);
            if(result.length === 0) res.status(404).send("Details no found");
            console.log(result[0]);
            res.send(result[0]);
        });
    });
});
app.post("/createUser", function (req, res) {
    const connection = mysql.createConnection(connectionDetails);
    let sql = "";
    connection.connect((error) => {
        if (error) res.status(500).send(error);
        sql = "select * from users where email = ?";
        connection.query(sql, [req.body.email],function (error, result) {
            if (error) {
                res.status(500).send(error);
                return;
            }
            if(result.length > 0) {
                console.log("email id already exist");
                res.status(404).send("email id already exist");
                return;
            }else{
                sql = "INSERT INTO users (`name`,`email`,`password`,`role`) VALUES ('" + req.body.name + "', '" + req.body.email + "','" + req.body.password + "','" + req.body.role + "')";
                connection.query(sql, function (error, result) {
                    if (error) {
                        res.status(500).send(error);
                        return;
                    }
                    console.log("Successfully Inserted");
                    res.send("Successfully Inserted");
                });
            }
        });
        
    });
});

app.get("/allUsers", function (req, res) {
    const connection = mysql.createConnection(connectionDetails);
    connection.connect(function(err) {
        if (err) throw err;
        connection.query("SELECT * FROM users where role = 'user'", function (err, result, fields) {
          if (err) throw err;
          console.log(result);
          res.send(result)
        });
    }); 
});

app.post("/addfavorite", function (req, res) {
    const connection = mysql.createConnection(connectionDetails);
    connection.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
        var sql = "INSERT INTO favorite (`email`,`playlistId`) VALUES ('" + req.body.email + "','" + req.body.playlistId + "')";
        connection.query(sql, function (err, result) {
          if (err) throw err;
          res.send("Successfully added in favoriteList");
        });
    });
});


app.get("/getFavorite", function (req, res) {
    const email = req.query.email
    const connection = mysql.createConnection(connectionDetails);
    var sql = "select * from favorite where email = ?";
    connection.connect(function(err) {
        if (err) throw err;
        connection.query(sql,[email], function (err, result) {
          if (err) throw err;
          console.log(result);
          res.send(result)
        });
    }); 
});
 

app.post("/addHistory", function (req, res) {
    const connection = mysql.createConnection(connectionDetails);
    connection.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
        let title = req.body.title;
        title = title.replace(/'/g,"")
        var sql = "INSERT INTO History (`email`,`title`) VALUES ('" + req.body.email + "','" + title + "')";
        connection.query(sql, function (err, result) {
          if (err) throw err;
          res.send("Successfully added in History");
        });
    });
});

app.get("/getHistory", function (req, res) {
    const email = req.query.email
    const connection = mysql.createConnection(connectionDetails);
    var sql = "select * from History where email = ?";
    connection.connect(function(err) {
        if (err) throw err;
        connection.query(sql,[email], function (err, result) {
          if (err) throw err;
          console.log(result);
          res.send(result)
        });
    }); 
});