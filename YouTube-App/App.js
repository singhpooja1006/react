import React, { Component } from 'react';
import './App.css';
import { Switch, Route, Redirect } from "react-router-dom";
import Navbar from './components/navbar';
import SignIn from './components/signIn';
import AllPlaylist from './components/showPlaylist';
import SignOut from './components/signOut';
import ViewFullPlaylist from './components/viewFullPlaylist';
import MyAccount from './components/myAccount';
import User from './components/user';
import ProtectedRoute from './components/protectedRoute';
class App extends Component {
  state = {  }
  componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    this.setState({ user });
  }
  render() { 
    const { user } = this.state;
    return ( 
      <React.Fragment>
      <Navbar  user={user}/>
      <Switch>
      <Route path="/signOut" component={SignOut} />
      <Route path="/sign-in" component={SignIn} />
      <ProtectedRoute path="/myAccount" component={MyAccount} />
      <Route path="/user" component={User} />
      <Route path="/home/viewPlaylist" component={ViewFullPlaylist} />
      <Route path="/home" component={AllPlaylist} />
     
        </Switch>
        </React.Fragment>
     );
  }
}
 
export default App;
