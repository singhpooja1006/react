import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
class SignIn extends Component {
  state = {
    data: { email: "", password: "" },
    errors: {},
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    console.log("Props", this.props.location);
    try {
      let apiEndpoint = config.apiEndPoint + "/login";
      const { data: user } = await axios.post(apiEndpoint, {
        email: this.state.data.email,
        password: this.state.data.password,
      });
      console.log(user);
      localStorage.setItem("user", JSON.stringify(user));
      if (user.Role === "admin") {
        window.location = "/myAccount";
      } else if (user.Role === "user") {
        if (this.props.location.state) {
          window.location = this.props.location.state.redirectUrl;
        } else {
          window.location = "/user";
        }
        console.log("Test");
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        const errors = { ...this.state.errors };
        if (ex.response.data.includes("Login Failed")) {
          errors.msg = "Login Failed. Check the username and password.";
        } else {
          errors.msg = ex.response.data;
        }

        this.setState({ errors });
      }
    }
  };
  validate = () => {
    let errs = {};

    if (!this.state.data.email.trim())
      errs.email = "Email address is required.";
    else if (this.state.data.email.includes("@") === false)
      errs.email = "Provide valid email address";

    if (!this.state.data.password.trim())
      errs.password = "Password is required";
    else if (this.state.data.password.trim().length < 7)
      errs.password = "Password  must be of 7 characters";
    return errs;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "email":
        if (!e.currentTarget.value.trim()) return "Email address is required.";
        else if (e.currentTarget.value.includes("@") === false)
          return "Provide valid email address";
        break;
      case "password":
        if (!e.currentTarget.value.trim()) return "Password is required";
        else if (e.currentTarget.value.trim().length < 7)
          return "Password  must be of 7 characters";
        break;
      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const loginData = { ...this.state.data };
    loginData[e.currentTarget.name] = e.currentTarget.value;

    this.setState({ data: loginData, errors: errors });
  };
  isFormvalid = () => {
    let errs = this.validate();

    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };
  render() {
    const { data, errors } = this.state;
    return (
      <React.Fragment>
        <div className="row text-center mt-5 ml-4">
          <div className="col-4"></div>
          <div
            className="col-4"
            style={{
              border: "5px solid lightgray",
              backgroundColor: "#fff3e6",
            }}
          >
            <h2 className="text-center">Sign In</h2>
            <h6 className="text-center">UserEmail:</h6>
            <div className="text-center">
              <form onSubmit={this.handleSubmit}>
                <input
                  value={data.email}
                  type="text"
                  onChange={this.handleChange}
                  className="col-8"
                  id="email"
                  placeholder="Enter Email"
                  name="email"
                />
                {errors.email ? (
                  <div className="text-danger text-center">{errors.email}</div>
                ) : (
                  ""
                )}
                <h6 className="text-center mt-3">Password:</h6>
                <input
                  value={data.password}
                  type="password"
                  onChange={this.handleChange}
                  className="col-8"
                  id="password"
                  placeholder="Enter Password"
                  name="password"
                />
                {errors.password ? (
                  <div className="text-danger text-center">
                    {errors.password}
                  </div>
                ) : (
                  ""
                )}
                <div className="mt-3">
                  <button
                    type="submit"
                    className="btn btn-primary"
                    disabled={this.isFormvalid()}
                  >
                    Login
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default SignIn;
