import React from "react";
import { Route, Redirect } from "react-router-dom";
const ProtectedRoute = ({ component: Component, render, ...rest }) => {
  const user = JSON.parse(localStorage.getItem("user"));
  return (
    <Route
      {...rest}
      render={(props) => {
        if (user && user.Role === "admin") {
          return Component ? <Component {...props} /> : render(props);
        } else {
          return <Redirect to={{ pathname: "/sign-in" }} />;
        }
      }}
    />
  );
};

export default ProtectedRoute;
