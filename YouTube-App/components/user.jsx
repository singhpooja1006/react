import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
class User extends Component {
  state = {
    userFavoriteList: { user: {}, favList: [] },
  };

  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    if (user && user.Role === "user") {
      const { userFavoriteList } = this.state;
      let apiEndpoint = config.apiEndPoint + "/getFavorite?email=" + user.Email;
      const { data: favList } = await axios.get(apiEndpoint);
      userFavoriteList.user = user;
      userFavoriteList.favList = [];
      for (let index = 0; index < favList.length; index++) {
        apiEndpoint =
          config.apiEndPoint +
          "/getPlayListDetails?q=" +
          favList[index].playlistId;
        const { data: playlist } = await axios.get(apiEndpoint);
        userFavoriteList.favList.push(playlist[0]);
      }
      console.log("userFavoriteList::", userFavoriteList);
      this.setState({ userFavoriteList });
    } else {
      window.location = "/sign-in";
    }
  }
  handleViewPlaylist = (fav) => {
    let id = fav.id;
    window.location = "/home/viewPlaylist?id=" + id;
  };
  render() {
    return (
      <div className="container">
        {this.state.userFavoriteList.favList.length > 0
          ? this.renderUserFavoriteList()
          : ""}
      </div>
    );
  }
  renderUserFavoriteList = () => {
    const { userFavoriteList } = this.state;
    return (
      <React.Fragment>
        <h2>All Favorites</h2>
        <div className="row">
          {userFavoriteList.favList && userFavoriteList.favList.length > 0
            ? userFavoriteList.favList.map((fav) => (
                <div className="col-md-6 col-lg-4">
                  <div className="row">
                    <div className="text-truncate col-10 h6">
                      {fav.snippet.title}
                    </div>
                  </div>
                  <img
                    src={
                      fav.snippet.thumbnails.default
                        ? fav.snippet.thumbnails.default.url
                        : ""
                    }
                    style={{
                      width: "70%",
                      height: "100px",
                    }}
                    onClick={() => this.handleViewPlaylist(fav)}
                  />
                  <h5
                    className="mb-4"
                    onClick={() => this.handleViewPlaylist(fav)}
                  >
                    View Full playlist
                  </h5>
                </div>
              ))
            : ""}
        </div>
      </React.Fragment>
    );
  };
}

export default User;
