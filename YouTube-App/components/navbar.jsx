import React, { Component } from "react";
import { faUserCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";

class Navbar extends Component {
  state = {
    searchText: "",
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    this.setState({ searchText: input.value });
  };
  handleSearch = (e) => {
    e.preventDefault();
    const searchText = this.state.searchText;
    window.location = "/home?q=" + searchText;
  };
  render() {
    return (
      <div className="container">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <a className="navbar-brand" href="#" style={{ fontSize: "25px" }}>
            <i
              className="fa fa-youtube-play"
              style={{ color: "red" }}
              aria-hidden="true"
            ></i>
            YouTube
          </a>

          <form
            className="form-inline my-2 my-lg-0"
            onSubmit={this.handleSearch}
          >
            <input
              className="form-control mr-sm-2"
              onChange={this.handleChange}
              type="text"
              id="search"
              name="search"
              value={this.state.searchText}
              aria-label="Search"
              placeholder="Search.."
            />
            <button type="submit" className="btn btn-outline-secondary">
              <i className="fa fa-search"></i>
            </button>
          </form>
          <ul className="navbar-nav ml-auto">
            {this.props.user ? (
              this.renderUser()
            ) : (
              <Link to="/sign-in" className="nav-link">
                <button className="btn btn-outline-primary">
                  <FontAwesomeIcon icon={faUserCheck} /> SIGN IN
                </button>
              </Link>
            )}
          </ul>
        </nav>
      </div>
    );
  }
  renderUser = () => {
    const { user } = this.props;
    if (user) {
      if (user.Role === "admin") {
        return (
          <React.Fragment>
            <li className="nav-item">
              <Link to="/myAccount" className="nav-link">
                <button className="btn btn-outline-primary">
                  <FontAwesomeIcon icon={faUserCheck} /> My Account
                </button>
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/signOut" className="nav-link">
                <button className="btn btn-outline-primary">
                  <FontAwesomeIcon icon={faUserCheck} /> Sign Out
                </button>
              </Link>
            </li>
          </React.Fragment>
        );
      } else if (user.Role === "user") {
        return (
          <React.Fragment>
            <li className="nav-item">
              <Link to="/signOut" className="nav-link">
                <button className="btn btn-outline-primary">
                  <FontAwesomeIcon icon={faUserCheck} /> Sign Out
                </button>
              </Link>
            </li>
          </React.Fragment>
        );
      }
    }
  };
}

export default Navbar;
