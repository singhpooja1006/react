import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
import queryString from "query-string";
class AllPlaylist extends Component {
  state = {
    playlistsData: [],
    searchStr: "",
    channelDetail: [],
  };
  async componentDidMount() {
    let { q } = queryString.parse(this.props.location.search);
    let apiEndPoint = config.apiEndPoint + "/playlists?q=" + q;
    const { data: playlistsData } = await axios.get(apiEndPoint);
    apiEndPoint = config.apiEndPoint + "/getChannelDetails?q=" + q;
    const { data: channelDetail } = await axios.get(apiEndPoint);
    this.setState({ playlistsData, channelDetail, searchStr: q });
  }
  handleViewPlaylist = (playlist) => {
    let id = playlist.id;
    window.location = "/home/viewPlaylist?id=" + id;
  };
  handlefavorites = async (playlist) => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let apiEndpoint = config.apiEndPoint + "/addfavorite";
      const { data: favorite } = await axios.post(apiEndpoint, {
        email: user.Email,
        playlistId: playlist.id,
      });
      window.location = "/user";
    } else {
      this.props.history.push({
        pathname: "/sign-in",
        state: {
          redirectUrl: "/home?q=" + this.state.searchStr,
        },
      });
      //window.location = "/sign-in";
    }
  };
  render() {
    const { playlistsData, channelDetail } = this.state;
    return (
      <div className="container">
        {playlistsData.length > 0 && channelDetail.length > 0 ? (
          <React.Fragment>
            <div className="row">
              <div className="col-12">
                {channelDetail.map((channel) => (
                  <div className="row">
                    <div className="col-2">
                      <img
                        id="earth"
                        style={{
                          width: "85px",
                        }}
                        src={channel.snippet.thumbnails.default.url}
                      />
                    </div>
                    <div className="col-6">
                      <div className="row">
                        <h3>{channel.snippet.title}</h3>
                      </div>
                      <div className="row">
                        {channel.statistics.subscriberCount}
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>

            <h4 className="mt-3">All Playlist</h4>
            <div className="row">
              <div className="col-lg-12 col-12">
                {this.renderPlaylistsData()}
              </div>
            </div>
          </React.Fragment>
        ) : (
          ""
        )}
      </div>
    );
  }
  renderPlaylistsData = () => {
    let { playlistsData } = this.state;
    return (
      <React.Fragment>
        <div className="row">
          {playlistsData
            ? playlistsData.map((playlist) =>
                this.renderPlaylistViewData(playlist)
              )
            : ""}
        </div>
      </React.Fragment>
    );
  };
  renderPlaylistViewData = (playlist) => {
    return (
      <React.Fragment>
        <div className="col-md-6 col-lg-4">
          <div className="row">
            <div className="text-truncate col-10 h6">
              {playlist.snippet.title}
            </div>
            <div className="col-2" style={{ cursor: "pointer" }}>
              <i
                className="fa fa-bookmark-o"
                aria-hidden="true"
                onClick={() => this.handlefavorites(playlist)}
              ></i>
            </div>
          </div>
          <img
            onClick={() => this.handleViewPlaylist(playlist)}
            src={
              playlist.snippet.thumbnails.default
                ? playlist.snippet.thumbnails.default.url
                : ""
            }
            style={{
              width: "95%",
              height: "150px",
            }}
          />
          <h5
            className="mb-4"
            onClick={() => this.handleViewPlaylist(playlist)}
          >
            View Full playlist
          </h5>
        </div>
      </React.Fragment>
    );
  };
}

export default AllPlaylist;
