import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
import "./text.css";
import queryString from "query-string";
class ViewFullPlaylist extends Component {
  state = {
    playlist: [],
    selectedVideo: {},
    channelDetail: [],
  };
  async componentDidMount() {
    let { id } = queryString.parse(this.props.location.search);

    let apiEndPoint = config.apiEndPoint + "/playlistVideos?id=" + id;
    const { data: playlist } = await axios.get(apiEndPoint);
    if (playlist.length > 0) {
      apiEndPoint =
        config.apiEndPoint +
        "/getChannelDetails?q=" +
        playlist[0].snippet.channelId;
      const { data: channelDetail } = await axios.get(apiEndPoint);
      this.setState({
        playlist,
        selectedVideo: playlist.length > 0 ? playlist[0] : {},
        channelDetail,
      });
    }
  }
  handleSelectVideo = (item) => {
    this.setState({ selectedVideo: item });
  };
  handleWatchVideo = async () => {
    const { selectedVideo } = this.state;
    const user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let apiEndpoint = config.apiEndPoint + "/addHistory";
      const { data: history } = await axios.post(apiEndpoint, {
        email: user.Email,
        title: selectedVideo.snippet.title,
      });
    }
    let videoId = selectedVideo.snippet.resourceId.videoId;
    window.open("https://www.youtube.com/watch?v=" + videoId);
  };
  render() {
    const { playlist, selectedVideo, channelDetail } = this.state;
    return (
      <div className="container">
        <div className="row">
          <div className="col-lg-7 col-12">
            <h4>Playlist Title</h4>
            <div className="row bg-dark">
              {channelDetail.map((channel) => (
                <div className="col-2 mt-2">
                  <img
                    id="earth"
                    style={{
                      width: "80px",
                    }}
                    src={channel.snippet.thumbnails.default.url}
                  />
                </div>
              ))}
              <div className="col-8 col-lg-8">
                <img
                  style={{
                    width: "320px",
                    height: "200px",
                  }}
                  src={
                    selectedVideo.snippet
                      ? selectedVideo.snippet.thumbnails.medium.url
                      : ""
                  }
                />
                <span className="img-grad text-truncate ng-scope mt-4 mr-1">
                  <a class="text-white ng-binding">
                    {selectedVideo.snippet ? selectedVideo.snippet.title : ""}
                  </a>
                </span>
              </div>

              <div className="col-2 text-right text-white mt-3">
                <h6 className="mb-5">
                  <i className="fa fa-info-circle" aria-hidden="true"></i>
                  <i className="fa fa-ellipsis-v ml-3" aria-hidden="true"></i>
                </h6>
                <h6 style={{ fontSize: "15px", marginTop: "130px" }}>
                  <i className="fa fa-youtube-play" aria-hidden="true"></i>
                  YouTube
                </h6>
              </div>
            </div>

            <div className="row mt-2">
              <div className="col-5">Channel Image</div>
              <div className="col-6">
                <button
                  className="btn btn-danger"
                  onClick={() => this.handleWatchVideo()}
                >
                  Watch on Youtube
                </button>
              </div>
            </div>
            <div className="mt-3">
              {selectedVideo.snippet ? selectedVideo.snippet.description : ""}
            </div>
          </div>

          <div className="col-4 mt-3">
            {playlist.map((item, index) => (
              <div
                className={
                  item.snippet.thumbnails.medium.url ===
                  selectedVideo.snippet.thumbnails.medium.url
                    ? "row bg-light"
                    : "row"
                }
              >
                <div className="col-1">{index + 1}</div>
                <div className="col-4">
                  <img
                    className="mb-3"
                    src={
                      item.snippet.thumbnails.default
                        ? item.snippet.thumbnails.default.url
                        : ""
                    }
                    style={{
                      width: "120px",
                      height: "90px",
                    }}
                    onClick={() => this.handleSelectVideo(item)}
                  />
                </div>
                <div className="col-7">{item.snippet.title}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}
export default ViewFullPlaylist;
