import React, { Component } from "react";
import { faUserPlus, faUsers } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import config from "./config.json";
import axios from "axios";
class MyAccount extends Component {
  state = {
    data: { name: "", email: "", password: "", role: "user" },
    errors: {},
    view: 0,
    usersDetail: [],
    selectedUserFavoriteList: { user: {}, favList: [] },
    historyList: [],
    isShow: 0,
  };

  handleFavorite = async (user) => {
    const { selectedUserFavoriteList } = this.state;
    let apiEndpoint = config.apiEndPoint + "/getFavorite?email=" + user.Email;
    const { data: favList } = await axios.get(apiEndpoint);
    selectedUserFavoriteList.user = user;
    selectedUserFavoriteList.favList = [];
    for (let index = 0; index < favList.length; index++) {
      apiEndpoint =
        config.apiEndPoint +
        "/getPlayListDetails?q=" +
        favList[index].playlistId;
      const { data: playlist } = await axios.get(apiEndpoint);
      selectedUserFavoriteList.favList.push(playlist[0]);
    }
    this.setState({ selectedUserFavoriteList, isShow: 1 });
  };
  handleHistory = async (user) => {
    let apiEndpoint = config.apiEndPoint + "/getHistory?email=" + user.Email;
    const { data: historyList } = await axios.get(apiEndpoint);
    historyList.user = user;
    this.setState({ historyList, isShow: 2 });
  };

  handleaddUser = () => {
    this.setState({ view: 1, isShow: 0 });
  };
  handleShowUsers = async () => {
    let apiEndpoint = config.apiEndPoint + "/allUsers";
    const { data: usersDetail } = await axios.get(apiEndpoint);
    this.setState({ usersDetail, view: 2 });
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    let resetData = { name: "", email: "", password: "", role: "user" };
    try {
      let apiEndpoint = config.apiEndPoint + "/createUser";
      const { data: user } = await axios.post(apiEndpoint, {
        ...this.state.data,
      });
      this.setState({
        view: 0,
        data: resetData,
      });
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        const errors = { ...this.state.errors };
        if (ex.response.data.includes("email id already exist")) {
          errors.msg = "User Creation Failed. Check for the email id.";
        } else {
          errors.msg = ex.response.data;
        }
        if (errors.msg) {
          this.setState({ errors });
        }
      }
    }
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const userData = { ...this.state.data };
    userData[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data: userData, errors: errors });
  };
  validate = () => {
    let errs = {};
    if (!this.state.data.name.trim()) errs.name = "Name is required.";
    if (!this.state.data.email.trim())
      errs.email = "Email address is required.";
    else if (this.state.data.email.includes("@") === false)
      errs.email = "Provide valid email address";

    if (!this.state.data.password.trim())
      errs.password = "Password is required";
    else if (this.state.data.password.trim().length < 7)
      errs.password = "Password  must be of 7 characters";
    return errs;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "name":
        if (!e.currentTarget.value.trim()) return "Name is required.";
        break;
      case "email":
        if (!e.currentTarget.value.trim()) return "Email address is required.";
        else if (e.currentTarget.value.includes("@") === false)
          return "Provide valid email address";
        break;
      case "password":
        if (!e.currentTarget.value.trim()) return "Password is required";
        else if (e.currentTarget.value.trim().length < 7)
          return "Password  must be of 7 characters";
        break;
      default:
        break;
    }
    return "";
  };
  render() {
    const { data, errors } = this.state;
    return (
      <div className="container">
        <div className="row mt-3">
          <div className="col-3"></div>
          <div className="col-4">
            <FontAwesomeIcon
              icon={faUserPlus}
              style={{ fontSize: "35px", color: "red" }}
              onClick={() => this.handleaddUser()}
            />
            {this.state.view === 1 ? (
              <React.Fragment>
                <div className="row text-center">
                  <div
                    className="col-12 mt-2"
                    style={{
                      border: "5px solid lightgray",
                      backgroundColor: "#fff3e6",
                    }}
                  >
                    <h2 className="text-center">Add User</h2>
                    {errors.msg ? (
                      <div className="text-danger text-center">
                        {errors.msg}
                      </div>
                    ) : (
                      ""
                    )}
                    <h6 className="text-center">Name:</h6>
                    <div className="text-center">
                      <form onSubmit={this.handleSubmit}>
                        <input
                          value={data.name}
                          type="text"
                          onChange={this.handleChange}
                          className="col-8"
                          id="name"
                          placeholder="Enter Name"
                          name="name"
                        />
                        {errors.name ? (
                          <div className="text-danger text-center">
                            {errors.name}
                          </div>
                        ) : (
                          ""
                        )}
                        <h6 className="text-center mt-3">Email:</h6>
                        <input
                          value={data.email}
                          type="text"
                          onChange={this.handleChange}
                          className="col-8"
                          id="email"
                          placeholder="Enter Email"
                          name="email"
                        />
                        {errors.email ? (
                          <div className="text-danger text-center">
                            {errors.email}
                          </div>
                        ) : (
                          ""
                        )}
                        <h6 className="text-center mt-3">Password:</h6>
                        <input
                          value={data.password}
                          type="password"
                          onChange={this.handleChange}
                          className="col-8"
                          id="password"
                          placeholder="Enter Password"
                          name="password"
                        />
                        {errors.password ? (
                          <div className="text-danger text-center">
                            {errors.password}
                          </div>
                        ) : (
                          ""
                        )}
                        <div className="mt-3">
                          <button type="submit" className="btn btn-primary">
                            Add User
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </React.Fragment>
            ) : (
              ""
            )}
          </div>
          <div className="col-3"></div>
          <div className="col-2">
            <FontAwesomeIcon
              onClick={() => this.handleShowUsers()}
              icon={faUsers}
              style={{ fontSize: "35px", color: "red" }}
            />
          </div>
        </div>
        {this.state.view === 2 ? <div>{this.renderAllUser()}</div> : ""}
        {this.state.isShow === 1 ? (
          <div>
            {this.state.selectedUserFavoriteList.favList.length > 0
              ? this.renderFavorites()
              : ""}
          </div>
        ) : (
          ""
        )}
        {this.state.isShow === 2 ? (
          <div>
            {this.state.historyList.length > 0 ? this.renderHistory() : ""}
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
  renderAllUser = () => {
    const { usersDetail } = this.state;
    return (
      <React.Fragment>
        <h2>All Users</h2>
        <div className="row">
          <div className="col-1">S.No.</div>
          <div className="col-2">Name</div>
          <div className="col-3">Email</div>
          <div className="col-3"></div>
          <div className="col-3"></div>
        </div>
        {usersDetail.map((user, index) => (
          <div className="row">
            <div className="col-1">{index + 1}</div>
            <div className="col-2">{user.Name}</div>
            <div className="col-3">{user.Email}</div>
            <div className="col-3">
              <button
                className="btn btn-danger mt-1"
                onClick={() => this.handleFavorite(user)}
              >
                Favorites
              </button>
            </div>
            <div className="col-3">
              <button
                className="btn btn-danger"
                onClick={() => this.handleHistory(user)}
              >
                History
              </button>
            </div>
          </div>
        ))}
      </React.Fragment>
    );
  };
  renderFavorites = () => {
    const { selectedUserFavoriteList } = this.state;
    return (
      <React.Fragment>
        <h2>All Favorites of User {selectedUserFavoriteList.user.Name}</h2>
        <div className="row">
          {selectedUserFavoriteList.favList &&
          selectedUserFavoriteList.favList.length > 0
            ? selectedUserFavoriteList.favList.map((fav) => (
                <div className="col-md-6 col-lg-4">
                  <div className="row">
                    <div className="text-truncate col-10 h6">
                      {fav.snippet.title}
                    </div>
                  </div>
                  <img
                    src={
                      fav.snippet.thumbnails.default
                        ? fav.snippet.thumbnails.default.url
                        : ""
                    }
                    style={{
                      width: "40%",
                      height: "100px",
                    }}
                  />
                </div>
              ))
            : ""}
        </div>
      </React.Fragment>
    );
  };
  renderHistory = () => {
    const { historyList } = this.state;
    return (
      <React.Fragment>
        <h2>Watch history of the User {historyList.user.Name}</h2>

        {historyList.map((history) => (
          <div className="row border">
            <React.Fragment>
              <div className="col-12 h6">{history.Title}</div>
            </React.Fragment>
          </div>
        ))}
      </React.Fragment>
    );
  };
}

export default MyAccount;
