import React, { Component } from "react";
class SignOut extends Component {
  componentDidMount() {
    localStorage.removeItem("user");
    window.location = "/sign-in";
  }
  render() {
    return null;
  }
}

export default SignOut;
