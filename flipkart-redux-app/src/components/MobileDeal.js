import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

const MobileDeal = (props) => { 
    const url = props.mobile.brand + "/" + props.mobile.id;
    return (
        <React.Fragment>
            <div className="col-lg-2 col-3 text-center">
            <div className="row  ml-lg-3">
                <Link to={`/home/Mobiles/${url}`}><img
                    routerlinkactive="router-link-active"
                    style={{ height: "150px", cursor: "pointer" }}
                    tabindex="0"
                    src={props.mobile.img}
                />
                </Link>
            </div>
            <div
                className="row ml-lg-3 text-truncate"
                style={{
                fontSize: "12px",
                fontWeight: "500",
                marginTop: "15px",
                }}
            >
                {props.mobile.name}
            </div>
            <div className="row" id="abc">
                <div
                className="col"
                id="abc"
                style={{ color: "#388e3c", fontSize: "10px" }}
                >
                {props.mobile.discount} % Off
                </div>
            </div>
            </div>
        </React.Fragment>
        );
}

//export default connect()(MobileDeal);
export default MobileDeal;