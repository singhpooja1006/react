import React, { Component } from "react";
class SlideBar extends Component {
  render() {
    return (
      <div
        id="carouselExampleIndicators"
        className="carousel slide"
        data-ride="carousel"
      >
        <ol className="carousel-indicators">
          <li
            data-target="#carouselExampleIndicators"
            data-slide-to="0"
            className="active"
          ></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img
              className="d-block w-100"
              src="https://i.ibb.co/tq9j6V7/4dfdf0c59f26c4a1.jpg"
              alt=""
            />
          </div>
          <div className="carousel-item">
            <img
              className="d-block w-100"
              src="https://i.ibb.co/vQZhcvT/68af1ae7331acd1c.jpg"
              alt=""
            />
          </div>

          <div className="carousel-item">
            <img
              className="d-block w-100"
              src="https://i.ibb.co/qxHzVsp/30d7dffe1a1eae09.jpg"
              alt=""
            />
          </div>
          <div className="carousel-item">
            <img
              className="d-block w-100"
              style={{ width: "452px", height: "225px" }}
              src="https://i.ibb.co/4m9zL2Y/aa8947d0a8f758f2.jpg"
              alt=""
            />
          </div>
        </div>
        <a
          className="carousel-control-prev"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="sr-only">Previous</span>
        </a>
        <a
          className="carousel-control-next"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="sr-only">Next</span>
        </a>
      </div>
    );
  }
}

export default SlideBar;
