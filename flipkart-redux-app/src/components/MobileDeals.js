import React, {Component} from 'react';
import axios from "axios";
import config from "./config.json";
import MobileDeal from './MobileDeal';
class MobileDeals extends Component {
    state = {
        mobileData: [],
        pageIndex: 0,
      };
    async componentDidMount() {
        let apiEndpoint = config.apiEndPoint + "/deals";
        const { data: mobileData } = await axios.get(apiEndpoint);
        this.setState({
            mobileData,
        });
    }
    handleRightBtn = () => {
        let { pageIndex } = this.state;
        pageIndex += 1;
        this.setState({ pageIndex });
    };
    handleLeftBtn = () => {
        let { pageIndex } = this.state;
        pageIndex -= 1;
        this.setState({ pageIndex });
    };
    handleClick = (mobile) => {
        window.location = "/home/Mobiles/" + mobile.brand + "/" + mobile.id;
    };

    render() {
        const { mobileData, pageIndex } = this.state;
        let dispayMobileList = [];
        let startIndex = pageIndex === 0 ? 0 : pageIndex * 5;
        let lastIndex = pageIndex === 0 ? 5 : pageIndex * 5 + 5;
        console.log("Page", pageIndex, startIndex, lastIndex);
        for (let index = startIndex; index < lastIndex; index++) {
          let data = mobileData[index];
          if (data) dispayMobileList.push(data);
        }
        return (
            <React.Fragment>
              <div className="row ">
                {pageIndex > 0 ? (
                  <div className="col-1 text-left">
                    <button
                      style={{
                        alignSelf: "center",
                        padding: "60px 5px",
                        color: "black",
                        boxShadow: "1px 2px 10px -1px rgba(0,0,0,.3)",
                        backgroundColor: "hsla(0,0%,100%,.98)",
                        cursor: "pointer",
                        display: "flex",
                        position: "absolute;z-index: 1",
                      }}
                    >
                      <i
                        className="fa fa-angle-left"
                        onClick={() => this.handleLeftBtn()}
                      ></i>
                    </button>
                  </div>
                ) : (
                  ""
                )}
                {dispayMobileList.map((mobile) => <MobileDeal key={mobile.id} mobile={mobile} />)}
                {dispayMobileList.length % 5 === 0 ? (
                  <div className="col-1 text-right ">
                    <button
                      style={{
                        alignSelf: "center",
                        padding: "60px 5px",
                        color: "black",
                        boxShadow: "1px 2px 10px -1px rgba(0,0,0,.3)",
                        backgroundColor: "hsla(0,0%,100%,.98)",
                        cursor: "pointer",
                        display: "flex",
                        position: "absolute;z-index: 1",
                      }}
                    >
                      <i
                        className="fa fa-angle-right"
                        onClick={() => this.handleRightBtn()}
                      ></i>
                    </button>
                  </div>
                ) : (
                  ""
                )}
              </div>
            </React.Fragment>
          );
    }
}


export default MobileDeals;