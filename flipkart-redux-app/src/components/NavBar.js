import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./nav.css";
import {connect} from 'react-redux';
import config from "./config.json";
import http from "./services/httpService";
class NavBar extends Component {
  state = {
    searchText: "",
  };

  handleAddCart = () => {
    window.location = "/home/checkout";
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    this.setState({ searchText: input.value });
  };
  handleKeyDown = async (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      const searchText = this.state.searchText;
      this.setState({ searchText: "" });
      let api = config.apiEndPoint + "/products/Mobiles?q=" + searchText;
      const { data: mobilesData } = await http.get(api);
      if(mobilesData.data.length > 0){
        let path = "/home/Mobiles/?q=" + searchText+ "&page=1";
        window.location = path;
      }else {
        api = config.apiBaseUrl + "products/Laptops?q=" + searchText;
        const { data: laptopsData } = await http.get(api);
        if(laptopsData.data.length > 0){
          let path = "/home/Laptops/?q=" + searchText+ "&page=1";
          window.location = path;
        }else{
          api = config.apiBaseUrl + "products/Cameras?q=" + searchText;
          const { data: camerasData } = await http.get(api);
          if(camerasData.data.length > 0){
            let path = "/home/Cameras/?q=" + searchText+ "&page=1";
            window.location = path;
          }else{
            let path = "/home/Mobiles/?q=" + searchText+ "&page=1";
            window.location = path;
          }
        }
      }
    }
  };
  getSearchData = async (api) =>{
    const { data: productsData } = await http.get(api);
    return productsData;
  }
  render() {
    return (
        <div className=''>
          <div className="row" style={{ backgroundColor: "#2874f0" }}>
            <div className="col-lg-2 col-4 mt-1 p-0 text-right">
              <Link to="/home">
                <div className="row ml-1 ">
                  <img
                    className="img-fluid"
                    routerlinkactive="router-link-active"
                    src="https://i.ibb.co/qs8BK6Y/flipkart-plus-4ee2f9.png"
                    style={{ width: "100px", cursor: "pointer" }}
                    tabindex="0"
                  />
                </div>
              </Link>
              <div className="row text-primary ml-1">
                <a className="text-white" href="#" style={{ fontSize: "10px" }}>
                  <i>
                    Explore{" "}
                    <span style={{ color: "yellow" }}>
                      Plus
                      <img
                        src="https://i.ibb.co/t2WXyzj/plus-b13a8b.png"
                        style={{ width: "10px" }}
                      />
                    </span>
                  </i>
                </a>
              </div>
            </div>
            <div className="col-lg-4 col-5 mt-2 p-0">
              
              <input
                className="form-control"
                placeholder="Search for products, brands and more"
                value={this.state.searchText}
                onChange={this.handleChange}
                type="text"
                id="search"
                name="search"
                aria-label="Search"
                onKeyDown={this.handleKeyDown}
              />
            </div>
           
            <div
              className="col-2 text-white mt-1 text-center"
              style={{ minHeight: "20px" }}
            >
               {this.props.user ? (
              <div className="dropdown">
                <div className="dropbtn1">
                  My Account
                  <span>
                    <i
                      id="onhover"
                      style={{ fontSize: "10px", color: "lightgrey" }}
                      className="fa fa-chevron-down"
                    ></i>
                  </span>
                </div>
                <div className="dropdown-content">
                  <div>
                    <a href="/">My Profile</a>
                  </div>
                  <div>
                    <a href="/">Orders</a>
                  </div>
                  <div>
                    <a href="/">WishList</a>
                  </div>
                </div>
              </div>
              ) : (
                <div className="col-lg-2 col-md-3 col-5 text-white mt-1" 
                style={{fontSize: "13px",minHeight: "20px"}}>
                <button className="btn">
                Login
              </button>
                
                </div>
           )}
            </div>
           
            <div
              className="col-1 text-white mt-1  p-0 d-none d-lg-block"
              style={{ minHeight: "20px" }}
            >
              <div className="dropdown">
                <div className="dropbtn1">
                  {" "}
                  More{" "}
                  <i
                    id="onhover"
                    style={{ fontSize: "10px", color: "lightgrey" }}
                    className="fa fa-chevron-down"
                  ></i>
                </div>
                <div className="dropdown-content">
                  <div>
                    <a href="/">Notifications</a>
                  </div>
                  <div>
                    <a href="/">Sell on FlipKart</a>
                  </div>
                  <div>
                    <a href="/">24X7 Customer Care</a>
                  </div>
                  <div>
                    <a href="/">Advertise</a>
                  </div>
                </div>
              </div>
            </div>
            <div
              className="col-lg-2 col-3 mt-3 p-0"
              routerlinkactive="router-link-active"
              style={{ cursor: "pointer" }}
              tabindex="0"
            >
              <Link to={"/home/checkout"} className='text-white'><i
                className="fa fa-shopping-cart"
                routerlinkactive="router-link-active"
                tabindex="0"
              ></i>
              <span
                id={
                  this.props.totalQty > 0
                    ? "cartcss"
                    : ""
                }
              >
                {this.props.totalQty > 0
                  ? this.props.totalQty
                  : ""}
              </span>{" "}
              &nbsp;&nbsp;&nbsp;
              <span>Cart</span></Link>
              
            </div>
          </div>
          <div
            className="row bg-white"
            style={{
              color: "#212121",
              minHeight: "25px",
              fontFamily: "Roboto,Arial,sans-serif",
            }}
          >
            <div className="col-4 text-center" id="onhover">
              <div className="dropdown">
                <div className="dropbtn1">
                  {" "}
                  Mobiles{" "}
                  <i
                    className="fa fa-chevron-down"
                    id="onhover"
                    style={{ fontSize: "10px", color: "lightgrey" }}
                  ></i>
                </div>
                <div className="dropdown-content">
                  <div>
                    <Link to="/home/Mobiles/Mi?page=1">Mi</Link>
                  </div>
                  <div>
                    <Link to="/home/Mobiles/RealMe?page=1">RealMe</Link>
                  </div>
                  <div>
                    <Link to="/home/Mobiles/Samsung?page=1">Samsung</Link>
                  </div>
                  <div>
                    <Link to="/home/Mobiles/OPPO?page=1">OPPO</Link>
                  </div>
                  <div>
                    <Link to="/home/Mobiles/Apple?page=1">Apple</Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-4 text-center" id="onhover">
              <div className="dropdown">
                <div className="dropbtn1">
                  {" "}
                  Laptops{" "}
                  <i
                    id="onhover"
                    style={{ fontSize: "10px", color: "lightgrey" }}
                    className="fa fa-chevron-down"
                  ></i>
                </div>
                <div className="dropdown-content">
                  <div>
                    <Link to="/home/Laptops/Apple?page=1">Apple</Link>
                  </div>

                  <div>
                    <Link to="/home/Laptops/Hp?page=1">HP</Link>
                  </div>
                  <div>
                    <Link to="/home/Laptops/Dell?page=1">Dell</Link>
                  </div>
                  <div>
                    <Link to="/home/Laptops/Acer?page=1">Acer</Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-4 text-center" id="onhover">
              <div className="dropdown">
                <div className="dropbtn1">
                  {" "}
                  Cameras{" "}
                  <i
                    id="onhover"
                    style={{ fontSize: "10px", color: "lightgrey" }}
                    className="fa fa-chevron-down"
                  ></i>
                </div>
                <div className="dropdown-content">
                  <div>
                    <Link to="/home/Cameras/DSLR?page=1">DSLR</Link>
                  </div>
                  <div>
                    <Link to="/home/Cameras/Lens?page=1">Lens</Link>
                  </div>
                  <div>
                    <Link to="/home/Cameras/Tripods?page=1">Tripods</Link>
                  </div>
                  <div>
                    <Link to="/home/Cameras/Compact?page=1">Compact</Link>
                  </div>
                  <div>
                    <Link to="/home/Cameras/Accessories?page=1">Accessories</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
            
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      totalQty: state.cart.reduce((acc, curr) => acc + curr.quantity, 0)
  }
};

export default connect(mapStateToProps, null)(NavBar);
