import React, { Component } from 'react';
import config from "./config.json";
import LeftPanel from "./leftPanel";
import http from "./services/httpService";
import queryString from "query-string";
import { Link } from "react-router-dom";
import Like from "./like";
class LaptopList extends Component {
    state = { 
        laptopsData:{},
        rams: [
            { name: "6 GB and More", value: ">=6", check: false },
            { name: "4 GB", value: "<=4", check: false },
            { name: "3 GB", value: "<=3", check: false },
            { name: "2 GB", value: "<=2", check: false },
          ],
          ratings: [
            { name: "4 ★ & above", value: ">4", check: false },
            { name: "3 ★ & above", value: ">3", check: false },
            { name: "2 ★ & above", value: ">2", check: false },
            { name: "1 ★ & above", value: ">1", check: false },
          ],
          prices: [
            { name: "0-5,000", value: "0-5000", check: false },
            { name: "5,000-10,000", value: "5000-10000", check: false },
            { name: "10,000-20,000", value: "10000-20000", check: false },
            { name: "More than 20,000", value: "20000+", check: false },
          ],
      
          assured: [
            {
              check: false,
            },
          ],
        };
        async componentDidMount() {
            let brand = this.props.match.params.brand;
            let api = config.apiBaseUrl+"products/Laptops/";
            if (brand) {
              api += brand;
            }
            api = api + this.props.location.search;
            const { data: laptopsData } = await http.get(api);
        
            this.setState({ laptopsData });
          }        
          async componentDidUpdate(prevProps, prevState) {
            if (prevProps !== this.props) {
              let brand = this.props.match.params.brand;
              let api = config.apiBaseUrl+"products/Laptops/";
              if (brand) {
                api += brand;
              }
              api = api + this.props.location.search;
              const { data: laptopsData } = await http.get(api);
              this.setState({ laptopsData });
            }
          }
          addToParams(params, newParamName, newParamValue) {
            if (newParamValue) {
              params = params ? params + "&" : params + "?";
              params = params + newParamName + "=" + newParamValue;
            }
            return params;
          }
          handleOptionChange = (rams, ratings, prices, assured) => {
            let ram = this.buildQueryString(rams);
            let rating = this.buildQueryString(ratings);
            let price = this.buildQueryString(prices);
        
            this.setState({ rams, ratings, prices, assured });
            let { sort,q  } = queryString.parse(this.props.location.search);
            this.calURL("", q, 1, sort, assured.check, ram, rating, price);
          };
          buildQueryString(optionsData) {
            let filterData = optionsData.filter((n1) => n1.check);
            let arrayData = filterData.map((n1) => n1.value);
            return arrayData.join(",");
          }
          calURL = (params, q, page, sort, assured, ram, rating, price) => {
            let path = "/home/Laptops";
            const brand = this.props.match.params.brand;
            if (brand) path = path + "/" + brand;
            params = this.addToParams(params, "q", q);
            params = this.addToParams(params, "page", page);
            params = this.addToParams(params, "sort", sort);
            if (assured === true) {
              params = this.addToParams(params, "assured", assured);
            }
        
            params = this.addToParams(params, "ram", ram);
            params = this.addToParams(params, "rating", rating);
            params = this.addToParams(params, "price", price);
            this.props.history.push({
              pathname: path,
              search: params,
            });
          };
          handleSortBy = (sortBy) => {
            let { ram, rating, price, page, assured,q, sort } = queryString.parse(
              this.props.location.search
            );
            if(sort && sort === sortBy){
              sort = "";
            }else{
              sort = sortBy
            }
            this.calURL("",q, page, sort, assured, ram, rating, price);
          };

          handleNumberBtn = (pageNo) => {
            let { q, ram, rating, price, sort, assured } = queryString.parse(
              this.props.location.search
            );
            this.calURL("", q, pageNo, sort, assured, ram, rating, price);
          };
          handleNextBtn = () => {
            let { q, ram, rating, price, sort, page, assured } = queryString.parse(
              this.props.location.search
            );
            let currPage = +page + 1;
            this.calURL("", q, currPage, sort, assured, ram, rating, price);
          };
        handleLike = (laptop) => {
            const { laptopsData } = this.state;
            const laptops = [...laptopsData.data];
            const index = laptops.indexOf(laptop);
            laptops[index].liked = laptops[index].liked ? !laptops[index].liked : true;
            laptopsData.data = laptops;
            this.setState({ laptopsData });
          };      
    render() { 
        const brand = this.props.match.params.brand;
        let { sort } = queryString.parse(this.props.location.search);
        let sortByCss = "col-2 d-none d-lg-block";
    
        return (
          <div className="container-fluid">
            <div className="row" style={{paddingTop:"7px"}}>
              <div className="col-2 ml-2 d-none d-lg-block">
              <LeftPanel
                  rams={this.state.rams}
                  ratings={this.state.ratings}
                  prices={this.state.prices}
                  assured={this.state.assured}
                  onOptionChange={this.handleOptionChange}
                />
              </div>
             <div className="col-lg-9 col-12 bg-white ml-1">
                <div className="row">
                  <div className="col">
                    <nav
                      aria-label="breadcrumb"
                      style={{ fontSize: "10px", backgroundColor: "white" }}
                    >
                      <ol className="breadcrumb bg-white">
                        <li className="breadcrumb-item">
                          <a href="/">Home</a>
                        </li>
                        <li className="breadcrumb-item">
                          <a
                            href="/home/Laptops/"
                            routerlinkactive="router-link-active"
                          >
                            Laptops
                          </a>
                        </li>
                        <li aria-current="page" className="breadcrumb-item active">
                          {brand ? brand : ""}
                        </li>
                      </ol>
                    </nav>
                  </div>
                </div>
                <div className="row">
                  <div className="col">{brand ? brand : ""} Laptops</div>
                </div>
    
                <div
                  className="row pb-1 border-bottom "
                  style={{ fontSize: "14px" }}
                >
                  <div className="col-2 d-none d-lg-block">
                    <strong>Sort By</strong>
                  </div>
                  <div
                    style={{ cursor: "pointer", borderBottom: "5px" }}
                    className={
                      sort === "popularity"
                        ? sortByCss + " text-primary"
                        : sortByCss
                    }
                    onClick={() => this.handleSortBy("popularity")}
                  >
                    Popularity
                  </div>
                  <div
                    style={{ cursor: "pointer" }}
                    className={
                      sort === "desc" ? sortByCss + " text-primary" : sortByCss
                    }
                    onClick={() => this.handleSortBy("desc")}
                  >
                    Price High to Low
                  </div>
                  <div
                    style={{ cursor: "pointer" }}
                    className={
                      sort === "asc" ? sortByCss + " text-primary" : sortByCss
                    }
                    onClick={() => this.handleSortBy("asc")}
                  >
                    Price Low to High
                  </div>
                </div>
                {this.renderFilterLaptops()}
              </div>
            </div>
          </div>
        );
      }
      renderFilterLaptops = () => {
        const { laptopsData } = this.state;
    if(laptopsData.data){
        let pageArray = [];
      for (let index = 0; index < laptopsData.pageInfo.numberOfPages; index++) {
        pageArray.push(index + 1);
      }
        return (
            <React.Fragment>
            {laptopsData.data.map((laptop) => (
            <React.Fragment>
              <div className="row">
                <div className="col-lg-2 col-9 text-center">
                <Link to={"/home/Laptops/" + laptop.brand + "/" + laptop.id}>
                    <img
                      alt=""
                      routerlinkactive="router-link-active"
                      style={{
                      maxHeight: "100%",
                      maxWidth: "100%",
                      cursor: "pointer",
                      }}
                      tabindex="0"
                      src={laptop.img}
                    />
                  </Link>
                </div>
                
                <Like
                  liked={laptop.liked}
                  onClick={() => this.handleLike(laptop)}
                />

                <div className="col-lg-5 col-12 text-left">
                  <div className="row">
                    <div
                      className="col"
                      style={{ fontSize: "16px", cursor: "pointer" }}
                    >
                    <Link to={"/home/Laptops/"  + laptop.brand + "/" + laptop.id}>
                      <span
                        id="namecss"
                        routerlinkactive="router-link-active"
                        tabindex="0">{laptop.name}
                      </span>
                    </Link> 
                     
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <span
                        style={{
                          lineHeight: "normal",
                          display: "inline-block",
                          color: "#fff",
                          padding: "2px 4px 2px 6px",
                          borderRadius: "3px",
                          fontWeight: "500",
                          fontSize: "12px",
                          verticalAlign: "middle",
                          backgroundColor: "#388e3c",
                        }}
                      >
                        <strong>
                          {laptop.rating}&nbsp;
                          <img
                            alt=""
                            src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                          />
                        </strong>
                      </span>{" "}
                      &nbsp;
                      <span className="text-muted">{laptop.ratingDesc}</span>
                    </div>
                  </div>
                  {laptop.details.map((detail) => (
                    <ul
                      style={{
                        color: "#c2c2c2",
                        fontSize: "12px",
                        display: "inline",
                        padding: "0%",
                      }}
                    >
                      <li>{detail}</li>
                    </ul>
                  ))}
                </div>
                <div className="col-lg-3 col-12">
                  <div className="row">
                    <div className="col" style={{ fontSize: "25px" }}>
                      <strong>₹{laptop.price}</strong>&nbsp;{" "}
                      <span>
                        <img
                          alt=""
                          className="img-fluid"
                          src="https://i.ibb.co/XCQBqSr/fa-8b4b59.png"
                          style={{ width: "70px" }}
                        />
                      </span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <span
                        style={{
                          textDecoration: "line-through",
                          fontSize: "14px",
                          color: "#878787",
                        }}
                      >
                        {laptop.prevPrice}
                      </span>{" "}
                      &nbsp;
                      <span
                        style={{
                          color: "#388e3c",
                          fontSize: "13px",
                          fontWeight: "500",
                        }}
                      >
                        {laptop.discount}%
                      </span>
                    </div>
                  </div>
                  <div className="row" style={{ fontSize: "14px" }}>
                    <div className="col">{laptop.emi}</div>
                  </div>
                  <div className="row" style={{ fontSize: "14px" }}>
                    <div className="col">{laptop.exchange}</div>
                  </div>
                </div>
              </div>
              <hr />
               </React.Fragment> 
              ))}
              {laptopsData.data.length > 0 ? (
            <div className="row">
              <div className="col-lg-2 col-4">
                Page{laptopsData.pageInfo.pageNumber} of{" "}
                {laptopsData.pageInfo.numberOfPages}
              </div>
              <div
                className="col-8 p-0 text-center"
                style={{
                  cursor: "pointer",
                  lineHeight: "32px",
                  height: "32px",
                  width: "32px",
                  borderRadius: "50%",
                }}
              >
                {pageArray.map((page) => (
                  <span
                    className={
                        laptopsData.pageInfo.pageNumber === page ? "pagecss" : ""
                    }
                    onClick={() => this.handleNumberBtn(page)}
                  >
                    {page}&nbsp;&nbsp;
                  </span>
                ))}
                {laptopsData.pageInfo.pageNumber !==
                laptopsData.pageInfo.numberOfPages ? (
                  <a className="text-primary" onClick={() => this.handleNextBtn()}>
                    Next
                  </a>
                ) : (
                  ""
                )}
              </div>
            </div>
          ) : (
            ""
          )}
        </React.Fragment>
            )
        }
          
    }
        
}
 
export default LaptopList;