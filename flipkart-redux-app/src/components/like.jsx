import React from "react";
const Like = (props) => {
  return (
    <div
      className={
        props.liked
          ? "col-lg-1 col-2 text-danger"
          : "col-lg-1 col-2 text-secondary"
      }
    >
      <i
        onClick={props.onClick}
        style={{ cursor: "pointer" }}
        className="fa fa-heart"
        aria-hidden="true"
      ></i>
    </div>
  );
};

export default Like;
