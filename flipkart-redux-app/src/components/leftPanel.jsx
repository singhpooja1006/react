import React, { Component } from "react";
import "./nav.css";
class LeftPanel extends Component {
  state = {
    ramClass: { css: "fa fa-chevron-up", isShow: true },
    ratingClass: { css: "fa fa-chevron-up", isShow: true },
    priceClass: { css: "fa fa-chevron-up", isShow: true },
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    const { rams, ratings, prices } = this.props;
    let assured = this.props.assured;
    if (input.name === "assured") {
      assured.check = assured.check === true ? false : true;
    }
    let ram = rams.find((n1) => n1.name === input.name);
    if (ram) ram.check = input.checked;
    let rating = ratings.find((n1) => n1.name === input.name);
    if (rating) rating.check = input.checked;
    let price = prices.find((n1) => n1.name === input.name);
    if (price) price.check = input.checked;
    this.props.onOptionChange(rams, ratings, prices, assured);
  };
  handleRams = () => {
    let { ramClass } = this.state;
    if (ramClass.css.includes("up")) {
      ramClass.css = "fa fa-chevron-down";
      ramClass.isShow = false;
    } else {
      ramClass.css = "fa fa-chevron-up";
      ramClass.isShow = true;
    }

    this.setState({ ramClass });
  };
  handleRatings = () => {
    let { ratingClass } = this.state;
    if (ratingClass.css.includes("up")) {
      ratingClass.css = "fa fa-chevron-down";
      ratingClass.isShow = false;
    } else {
      ratingClass.css = "fa fa-chevron-up";
      ratingClass.isShow = true;
    }
    this.setState({ ratingClass });
  };
  handlePrices = () => {
    let { priceClass } = this.state;
    if (priceClass.css.includes("up")) {
      priceClass.css = "fa fa-chevron-down";
      priceClass.isShow = false;
    } else {
      priceClass.css = "fa fa-chevron-up";
      priceClass.isShow = true;
    }
    this.setState({ priceClass });
  };
  render() {
    const { rams, ratings, prices, assured } = this.props;
    return (
      <React.Fragment>
        <div className="row bg-white border-bottom pt-2 pb-2">
          <div
            className="col"
            style={{
              fontSize: "18px",
              textTransform: "capitalize",
              width: "67%",
              fontWeight: "500",
            }}
          >
            {" "}
            Filters{" "}
          </div>
        </div>
        <div className="row bg-white pt-2 pb-2">
          <div className="col-9">
            <div className="checkbox">
              <label>
                <input
                  name="assured"
                  type="checkbox"
                  className="ng-valid ng-dirty ng-touched"
                  onChange={this.handleChange}
                  checked={assured.check}
                />
                &nbsp;
                <img
                  src="https://i.ibb.co/t8bPSBN/fa-8b4b59.png"
                  style={{ width: "80px", height: "21px" }}
                />
              </label>
            </div>
          </div>
          <div className="col-3">
            <span className="question"> ? </span>
          </div>
        </div>
        <div className="row bg-white border-top">
          <div className="col-10">
            <div
              className="row ml-1 pb-2"
              style={{
                fontSize: "13px",
                fontWeight: "500",
                textTransform: "uppercase",
                letterSpacing: ".3px",
                display: "inline-block",
              }}
            >
              RAM
            </div>
          </div>
          <div className="col-2 text-right">
            <span>
              <i
                style={{
                  fontSize: "10px",
                  color: "lightgrey",
                  cursor: "pointer",
                }}
                className={this.state.ramClass.css}
                onClick={() => this.handleRams()}
              ></i>
            </span>
          </div>
          {this.state.ramClass.isShow ? (
            <div>
              {rams.map((ram) => (
                <div
                  className="form-check ml-4 mr-2 pb-2 d-none d-lg-block bg-white"
                  style={{ borderRadius: "3px" }}
                >
                  <label className="form-check-label ml-4" for={ram.name}>
                    <input
                      value={ram.name}
                      onChange={this.handleChange}
                      className="form-check-input ng-untouched ng-pristine ng-valid"
                      name={ram.name}
                      type="checkbox"
                      id={ram.name}
                      checked={ram.check}
                    />
                    {ram.name}
                  </label>
                </div>
              ))}
            </div>
          ) : (
            ""
          )}
        </div>
        <div className="row  bg-white border-top">
          <div className="col-10">
            <div
              className="row ml-1 pb-2"
              style={{
                fontSize: "13px",
                fontWeight: "500",
                textTransform: "uppercase",
                letterSpacing: ".3px",
                display: "inline-block",
              }}
            >
              CUSTOMER RATING
            </div>
          </div>
          <div className="col-2 text-right">
            <span>
              <i
                style={{
                  fontSize: "10px",
                  color: "lightgrey",
                  cursor: "pointer",
                }}
                className={this.state.ratingClass.css}
                onClick={() => this.handleRatings()}
              ></i>
            </span>
          </div>
          {this.state.ratingClass.isShow ? (
            <div>
              {ratings.map((rating) => (
                <div
                  className="form-check ml-3 mr-2 pb-2 d-none d-lg-block bg-white"
                  style={{ borderRadius: "3px" }}
                >
                  <label className="form-check-label ml-3" for={rating.name}>
                    <input
                      value={rating.name}
                      onChange={this.handleChange}
                      className="form-check-input ng-untouched ng-pristine ng-valid"
                      name={rating.name}
                      type="checkbox"
                      id={rating.name}
                      checked={rating.check}
                    />
                    {rating.name}
                  </label>
                </div>
              ))}
            </div>
          ) : (
            ""
          )}
        </div>
        <div className="row  bg-white border-top">
          <div className="col-10">
            <div
              className="row ml-1 pb-2"
              style={{
                fontSize: "13px",
                fontWeight: "500",
                textTransform: "uppercase",
                letterSpacing: ".3px",
                display: "inline-block",
              }}
            >
              PRICE
            </div>
          </div>
          <div className="col-2 text-right">
            <span>
              <i
                style={{
                  fontSize: "10px",
                  color: "lightgrey",
                  cursor: "pointer",
                }}
                className={this.state.priceClass.css}
                onClick={() => this.handlePrices()}
              ></i>
            </span>
          </div>
          {this.state.priceClass.isShow ? (
            <div>
              {prices.map((price) => (
                <div
                  className="form-check ml-3 mr-2 pb-2 d-none d-lg-block bg-white"
                  style={{ borderRadius: "3px" }}
                >
                  <label className="form-check-label ml-3" for={price.name}>
                    <input
                      value={price.name}
                      onChange={this.handleChange}
                      className="form-check-input ng-untouched ng-pristine ng-valid"
                      name={price.name}
                      type="checkbox"
                      id={price.name}
                      checked={price.check}
                    />
                    {price.name}
                  </label>
                </div>
              ))}
            </div>
          ) : (
            ""
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPanel;
