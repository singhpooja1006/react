import React, { Component } from "react";
import { connect } from "react-redux";
import { decrementCartQuantity, incrementCartQuantity } from "./action";
class MobileCheckOut extends Component {
  state = {};
  incrementOrDecrement = (cart, type) => {
    if (type === "inc") {
      this.props.dispatch(incrementCartQuantity(cart.id));
    }

    if (type === "desc" && cart.quantity > 1) {
      this.props.dispatch(decrementCartQuantity(cart.id));
    }
  };
  render() {
    return <div className="container-fluid">{this.renderMobileCart()}</div>;
  }
  renderMobileCart = () => {
    return (
      <div className="row  mt-2">
        <div className="col-lg-8 col-12 bg-white ml-1 mr-1">
          <div className="row border-bottom">
            <div
              className="col-6"
              style={{
                fontSize: "18px",
                lineHeight: "56px",
                padding: "0 24px",
                fontWeight: "500",
              }}
            >
              My Cart({this.props.cartItemCount})
            </div>
          </div>
          <div>
            {this.props.cartItems.map((cart) => (
              <React.Fragment>
                <div className="row mt-1 bg-white">
                  <div className="col-lg-2 col-4">
                    <div className="row">
                      <div className="col text-center">
                        <img
                          style={{
                            maxWidth: "100%",
                            height: "92px",
                          }}
                          src={cart.img}
                        />
                      </div>
                    </div>
                    <div className="row mt-1">
                      <div className="col-12 text-center">
                        <button
                          className="btn btn-sm"
                          style={{
                            backgroundColor: "white",
                            borderColor: "#e0e0e0",
                            cursor: "auto",
                            borderRadius: "50%",
                            cursor: "pointer",
                            fontSize: "12px",
                          }}
                          onClick={() => {
                            this.incrementOrDecrement(cart, "desc");
                          }}
                        >
                          -
                        </button>
                        <span id="rect">{cart.quantity}</span>
                        <button
                          className="btn btn-sm"
                          style={{
                            backgroundColor: "white",
                            borderColor: "#e0e0e0",
                            cursor: "auto",
                            borderRadius: "50%",
                            cursor: "pointer",
                            fontSize: "12px",
                          }}
                          onClick={() => {
                            this.incrementOrDecrement(cart, "inc");
                          }}
                        >
                          {" "}
                          +{" "}
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-5 col-4">
                    <div className="row">
                      <div
                        className="col"
                        style={{
                          fontSize: "16px",
                          color: "#212121",
                        }}
                      >
                        {cart.name}{" "}
                      </div>
                    </div>
                    <div className="row">
                      <div
                        className="col"
                        style={{
                          fontSize: "14px",
                          color: "#878787",
                        }}
                      >
                        {" "}
                        {cart.brand} &nbsp;&nbsp;
                        <span>
                          {cart.assured === true ? (
                            <img
                              className="img-fluid"
                              src="https://i.ibb.co/XCQBqSr/fa-8b4b59.png"
                              style={{ width: "70px" }}
                            />
                          ) : (
                            ""
                          )}
                        </span>
                      </div>
                    </div>
                    <div className="row mt-2">
                      <div className="col" style={{ fontWeight: "500" }}>
                        {" "}
                        ₹{cart.price} &nbsp;
                        <span
                          style={{
                            textDecoration: "line-through",
                            fontSize: "16px",
                            color: "#878787",
                          }}
                        >
                          ₹{cart.prevPrice}
                        </span>
                        &nbsp;
                        <span
                          style={{
                            color: "#388e3c",
                            fontSize: "16px",
                            fontWeight: "500",
                          }}
                        >
                          {cart.discount}%
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3 col-4">
                    <div className="row">
                      <div className="col" style={{ fontSize: "14px" }}>
                        {" "}
                        Delivery in 2 days | Free
                        <span style={{ textDecoration: "line-through" }}>
                          ₹40
                        </span>
                      </div>
                    </div>
                    <div className="row">
                      <div
                        className="col"
                        tyle={{ fontSize: "14px", color: "gray" }}
                      >
                        {" "}
                        10 Days Replacement Policy{" "}
                      </div>
                    </div>
                  </div>
                </div>
                <hr _ngcontent-tlb-c9=""></hr>
              </React.Fragment>
            ))}
          </div>
        </div>

        <div className="col-lg-3 col-12  ml-1">
          <div className="row bg-white">
            <div className="col">
              <div
                className="row border-bottom"
                style={{
                  fontSize: "14px",
                  fontWeight: "500",
                  color: "#878787",
                  minHeight: "47px",
                }}
              >
                <div className="col pt-2">PRICE DETAILS</div>
              </div>
              <div className="row pt-2 pb-2">
                <div className="col-6">
                  Price({this.props.cartItemCount} items)
                </div>
                <div className="col-6 text-right">₹{this.props.totalPrice}</div>
              </div>
              <div className="row pt-2 pb-2">
                <div className="col-6"> Delivery </div>
                <div className="col-6 text-success text-right"> FREE </div>
              </div>
              <div
                className="row pt-2 pb-2"
                style={{
                  fontWeight: "500",
                  borderTop: "1px dashed e0e0e0",
                  fontSize: "18px",
                }}
              >
                <div className="col-6"> Total Payable </div>
                <div className="col-6 text-right">₹{this.props.totalPrice}</div>
              </div>
            </div>
          </div>
          <div className="row mt-2">
            <div className="col-1">
              <img
                src="https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/shield_435391.svg"
                style={{ width: "25px" }}
              />
            </div>
            <div
              className="col-9"
              style={{
                fontSize: "14px",
                textAlign: "left",
                fontWeight: "500",
                display: "inline-block",
                color: "#878787",
              }}
            >
              {" "}
              Safe and Secure Payments.Easy returns.100% Authentic products.{" "}
            </div>
          </div>
        </div>
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {
    cartItems: state.cart,
    cartItemCount: state.cart.reduce((count, curItem) => {
      return count + curItem.quantity;
    }, 0),
    totalPrice: state.cart.reduce((count, curItem) => {
      return count + curItem.price * curItem.quantity;
    }, 0),
  };
};
export default connect(mapStateToProps, null)(MobileCheckOut);
