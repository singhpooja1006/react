import React, { Component } from 'react';
import config from "./config.json";
import axios from "axios";
import { Link } from "react-router-dom";
import {connect} from 'react-redux';
import {addProductToCart} from "./action";
class CameraDetail extends Component {
    state = { 
        cameraData: {},
        bankOffers: [],
        selectedImg: "",
        cardData: {},
      };
    async componentDidMount() {
        const id = this.props.match.params.id;
        let bankOffersApi = config.apiBaseUrl + "bankOffers";
        let cameraApi = config.apiBaseUrl + "product/" + id;
        const { data: cameraData } = await axios.get(cameraApi);
        const { data: bankOffers } = await axios.get(bankOffersApi);
        this.setState({
            cameraData,
          bankOffers,
          selectedImg: cameraData.prod.pics ? cameraData.prod.pics.imgList[0] : "",
          cardData: cameraData.prod ? cameraData.prod : {},
        });
      }
      handleBuyNow = () => {
        this.props.dispatch(addProductToCart(this.state.cameraData));
      };
      handleAddToCart = () => {
        this.props.dispatch(addProductToCart(this.state.cameraData.prod));
      };
      handleSelectImg = (imgUrl) => {
        this.setState({ selectedImg: imgUrl });
      };
      render() {
        return (
          <React.Fragment>{this.renderCartProduct()}</React.Fragment>
        );
      }
      renderCartProduct = () => {
        const { cameraData, bankOffers, selectedImg } = this.state;
        if (cameraData.prod) {
          return (
            <React.Fragment>
              <div className="row mt-4">
                <div className="col-lg-5 col-12">
                  <div className="row">
                    <div className="col-lg-2 col-4 text-center">
                    {cameraData.prod.pics.imgList.map((img) => (
                        <div
                          style={{
                            height: "64px",
                            width: "64px",
                            textAlign: "center",
                            borderWidth: "2px solid",
                          }}
                          className={
                            img === selectedImg
                              ? "row border ml-lg-2 ml-0 border-primary"
                              : "row border ml-lg-2 ml-0"
                          }
                        >
                          <div className="col text-center">
                            <img
                              style={{ width: "40px", height: "50px" }}
                              src={img}
                              onClick={() => this.handleSelectImg(img)}
                            />
                          </div>
                        </div>
                      ))}
                    </div>
    
                    <div className="col-lg-8 col-8 border p-0 text-center">
                      <img className="img-fluid" src={selectedImg} />
                    </div>
                  </div>
    
              <div className="row">
                    <div className="col-lg-2 col-4"></div>
                    <div className="col-lg-4 col-4 text-sm-center">
                      <button
                        className="btn btn-sm btn-warning text-white"
                        onClick={() => this.handleAddToCart()}
                      >
                        <svg
                          className="_3oJBMI"
                          height="16"
                          viewBox="0 0 16 15"
                          width="16"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            className=""
                            d="M15.32 2.405H4.887C3 2.405 2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644 1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493 1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06 0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0 1.805-.75 1.91-1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86"
                            fill="#fff"
                          ></path>
                        </svg>{" "}
                        Add to Cart
                      </button>
                    </div>
                    <div className="col-lg-4 col-4">
                      <Link
                        to="/home/checkout"
                        className="btn btn-sm text-white"
                        style={{ backgroundColor: "#fb641b" }}
                        onClick={() =>  this.props.dispatch(addProductToCart(this.state.cameraData.prod))}
                      >
                        <i className="fa fa-bolt"></i> Buy Now
                      </Link>
                    </div>
                  </div>
                </div>
                <div className="col-lg-7 col-12">
                  <div className="row"></div>
                  <div
                    className="row"
                    style={{
                      color: "#212121",
                      fontSize: "18px",
                      fontWeight: "400",
                      padding: "0",
                      lineHeight: "1.4",
                      fontSize: "inherit",
                      fontWeight: "inherit",
                      display: "inline-block",
                    }}
                  >
                    <div className="col"> {cameraData.prod.name}</div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <span
                        style={{
                          lineHeight: "normal",
                          display: "inline-block",
                          color: "#fff",
                          padding: "2px 4px 2px 6px",
                          borderRadius: "3px",
                          fontWeight: "500",
                          fontSize: "12px",
                          verticalAlign: "middle",
                          backgroundColor: "#388e3c",
                        }}
                      >
                        <strong>
                          {cameraData.prod.rating}&nbsp;
                          <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg==" />
                        </strong>
                      </span>{" "}
                      &nbsp;
                      <span
                        className="text-muted"
                        style={{ fontSize: "14px", fontWeight: "500" }}
                      >
                        {cameraData.prod.ratingDesc}
                      </span>{" "}
                      &nbsp;&nbsp;
                      <span>
                        <img
                          className="img-fluid"
                          src="https://i.ibb.co/XCQBqSr/fa-8b4b59.png"
                          style={{ width: "70px" }}
                        />
                      </span>
                    </div>
                  </div>
                  <div className="row mt-2">
                    <div
                      className="col"
                      style={{
                        fontSize: "28px",
                        verticalAlign: "sub",
                        fontWeight: "500",
                      }}
                    >
                      ₹{cameraData.prod.price} &nbsp;
                      <span
                        style={{
                          textDecoration: "line-through",
                          fontSize: "16px",
                          color: "#878787",
                        }}
                      >
                        ₹{cameraData.prod.prevPrice}
                      </span>
                      &nbsp;
                      <span
                        style={{
                          color: "#388e3c",
                          fontSize: "16px",
                          fontWeight: "500",
                        }}
                      >
                        {cameraData.prod.discount}%
                      </span>
                    </div>
                  </div>
                  <div className="row">
                    <div
                      className="col"
                      style={{
                        fontSize: "16px",
                        fontWeight: "500",
                        marginLeft: "12px",
                        verticalAlign: "middle",
                        color: "black",
                      }}
                    >
                      {" "}
                      Available Offers{" "}
                    </div>
                  </div>
                  {bankOffers.map((offers) => (
                    <div className="row">
                      <div className="col-12">
                        <img
                          style={{ height: "18px", width: "18px" }}
                          src={offers.img}
                        />
                        &nbsp;
                        <span
                          style={{
                            color: "#212121",
                            fontSize: "14px",
                            fontWeight: "500",
                            paddingRight: "4px",
                          }}
                        >
                          {offers.type}
                        </span>
                        &nbsp;
                        <span style={{ fontSize: "14px" }}>{offers.detail}</span>
                      </div>
                    </div>
                  ))}
                  <div className="row mt-2">
                    <div className="col-lg-1 col-3 border text-center ml-2">
                     
                    </div>
    
                    <div
                      className="col-8 ml-3 d-none d-lg-block"
                      style={{ fontSize: "14px" }}
                    >
                      {cameraData.prod.details[5]}
                    </div>
                  </div>
                  <div className="row mt-2">
                    <div
                      className="col-1 d-none d-lg-block"
                      style={{
                        fontWeight: "500",
                        color: "#878787",
                        width: "110px",
                        paddingRight: "10px",
                        float: "left",
                        fontSize: "14px",
                      }}
                    >
                      {" "}
                      Highlights{" "}
                    </div>
    
                    <div className="col-5 d-none d-lg-block">
                      <ul>
                        {cameraData.prod.details.map((detail) => (
                          <li
                            style={{
                              fontSize: "14px",
                              color: "grey",
                              lineHeight: "1.4",
                            }}
                          >
                            <span style={{ color: "black" }}>{detail}</span>
                          </li>
                        ))}
                      </ul>
                    </div>
                    <div
                      className="col-2 d-none d-lg-block"
                      style={{
                        fontWeight: "500",
                        color: "#878787",
                        width: "110px",
                        paddingRight: "10px",
                        float: "left",
                        fontSize: "14px",
                      }}
                    >
                      {" "}
                      Easy Payment Options{" "}
                    </div>
                    <div className="col-4 d-none d-lg-block">
                      <ul
                        style={{
                          fontSize: "14px",
                          color: "grey",
                          lineHeight: "1.4",
                        }}
                      >
                        <li>
                          <span style={{ color: "black" }}>
                            No cost EMI NaN/month
                          </span>
                        </li>
                        <li>
                          <span style={{ color: "black" }}>
                            Debit/Flipkart EMI available
                          </span>
                        </li>
                        <li>
                          <span style={{ color: "black" }}>Cash on Delivery</span>
                        </li>
                        <li>
                          <span style={{ color: "black" }}>
                            Net Banking &amp; Credit/Debit/ATM Card
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="row">
                    <div
                      className="col-lg-1 col-3"
                      style={{
                        fontWeight: "500",
                        color: "#878787",
                        width: "110px",
                        paddingRight: "10px",
                        float: "left",
                        fontSize: "14px",
                      }}
                    >
                      {" "}
                      Seller{" "}
                    </div>
                    <div className="col-lg-9 col-9">
                      <span
                        style={{
                          color: "#2874f0",
                          fontSize: "14px",
                          fontWeight: "500",
                        }}
                      >
                        SuperComNet&nbsp;&nbsp;
                      </span>
                      <span
                        style={{
                          lineHeight: "normal",
                          display: "inline-block",
                          color: "#fff",
                          padding: "2px 4px 2px 6px",
                          fontWeight: "500",
                          fontSize: "12px",
                          borderRadius: "4px",
                          backgroundColor: "#2874f0",
                        }}
                      >
                        4.2 &nbsp;
                        <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg==" />
                      </span>
                      <ul className="d-none d-lg-block">
                        <li
                          style={{
                            fontSize: "14px",
                            color: "grey",
                            lineHeight: "1.4",
                          }}
                        >
                          {" "}
                          10 Day replacement{" "}
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <img
                        className="img-fluid"
                        src="https://i.ibb.co/j8CMRbn/CCO-PP-2019-07-14.png"
                        style={{ width: "300px", height: "85px" }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </React.Fragment>
          );
        }
      };
    }
 
export default connect()(CameraDetail);