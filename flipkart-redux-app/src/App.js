import React, { Component } from 'react';
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import NavBar from './components/NavBar'
import Home from "./components/Home";
import MobileList from './components/MobileList';
import MobileDetail from './components/MobileDetail';
import { Provider } from 'react-redux'
import reducer from './components/reducers/index';
import { createStore } from 'redux'
import MobileCheckOut from './components/MobileCheckOut';
import LaptopList from './components/LaptopList';
import LaptopDetail from './components/LaptopDetail';
import CameraList from './components/CameraList';
import CameraDetail from './components/CameraDetail'; 
export const  store = createStore(reducer);
window.store = store

class App extends Component {
  render() {
    return (
          <Provider store={store}>
            <BrowserRouter>
              <div className="container-fluid">
                <NavBar/>
                <Switch>
                    <Route exact path={'/'} render={() => {
                        return <Redirect to={'/home'}/>
                    }}/>
                    <Route exact path={'/home'} component={Home}/>
                    <Route exact path={'/home/checkout'} component={MobileCheckOut}/>
                    <Route path="/home/Mobiles/:brand/:id" component={MobileDetail} />
                    <Route path="/home/Mobiles/:brand" component={MobileList} />
                    <Route path="/home/Mobiles" component={MobileList} />
                    <Route path="/home/Laptops/:brand/:id" component={LaptopDetail} />
                    <Route path="/home/Laptops/:brand" component={LaptopList} />
                    <Route path="/home/Laptops" component={LaptopList} />
                    <Route path="/home/Cameras/:brand/:id" component={CameraDetail} />
                    <Route path="/home/Cameras/:brand" component={CameraList} />
                    <Route path="/home/Cameras" component={CameraList} />
                </Switch>
              </div>
            </BrowserRouter>
          </Provider>
        
    );
  }
}

export default App;
