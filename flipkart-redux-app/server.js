
var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
  next();
});
var port = process.env.PORT || 2410;
app.listen(port,()=>console.log("Listening on port : ",port))
let dataList = 
[{id:"L1",category:"Laptops",brand:"Hp",name:"HP 14s Core i3 10th Gen - (8 GB/256 GB SSD/Windows 10 Home) 14s-cf3074TU Thin and Light Laptop  (14 inch, Jet Black, 1.47 kg, With MS Office)",
img:"https://i.ibb.co/tXMWNJf/hp-na-thin-and-light-laptop-original-imaftv7fhbftxnmq.jpg",
rating:4.2,ratingDesc:"1,667 Ratings & 189 Reviews",
details:["Intel Core i3 Processor (10th Gen)",
"8 GB DDR4 RAM",
"64 bit Windows 10 Operating System",
"256 GB SSD",
"35.56 cm (14 inch) Display",
"HP Documentation, HP CoolSense, HP SSRM, HP Smart, HP Support Assistant, Microsoft Office Home and Student 2019",
"1 Year Onsite Warranty"],
pics:{"brand":"Hp",imgList:["https://i.ibb.co/48QxMyC/hp-original-imaftyzafphshjge.jpg",
"https://i.ibb.co/yR0H9TJ/hp-original-imaftyzay6ep3m2e.jpg",
"https://i.ibb.co/jVsJNhP/hp-original-imaftyza2ndykswh.jpg",
"https://i.ibb.co/CwzQqg7/hp-original-imaftyzap6ypruev.jpg"],
brandImg:"https://i.ibb.co/DzCD7h4/4e0d4a37f0f20d78413f7315017041415b0fe6010ba327ff598715c7db2142b7.jpg"},
price:35990,
prevPrice:39039,discount:7,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:4,popularity:1667},
{id:"L2",category:"Laptops",brand:"Hp",name:"HP 15 Ryzen 3 Dual Core 3200U - (4 GB/1 TB HDD/Windows 10 Home) 15-db1069AU Laptop  (15.6 inch, Jet Black, 2.04 kg, With MS Office)",
img:"https://i.ibb.co/vk216VQ/hp-na-original-imafz4duhht4zbnx.jpg",
rating:4.1,ratingDesc:"2,109 Ratings & 282 Reviews",
details:["AMD Ryzen 3 Dual Core Processor",
"4 GB DDR4 RAM",
"64 bit Windows 10 Operating System",
"1 TB HDD",
"39.62 cm (15.6 inch) Display",
"HP Audio Switch, HP ePrint, Dropbox, HP Support Assistant, HP Documentation, HP JumpStart, Microsoft Office Home and Student 2019",
"1 Year Onsite Warranty"],
pics:{"brand":"Hp",imgList:["https://i.ibb.co/HFzkXKq/hp-na-original-imafz4duhht4zbnx.jpg",
"https://i.ibb.co/cCjhyh8/hp-na-original-imafz256dyjwjztd.jpg","https://i.ibb.co/12nhFm0/hp-na-original-imafz256fnzgntsm.jpg",
"https://i.ibb.co/W586y6P/hp-na-original-imafz256y5byyaee.jpg"],
brandImg:"https://i.ibb.co/DzCD7h4/4e0d4a37f0f20d78413f7315017041415b0fe6010ba327ff598715c7db2142b7.jpg"},
price:29990,
prevPrice:32182,discount:6,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:4,popularity:2109},
{id:"L3",category:"Laptops",brand:"Hp",name:"HP 15s Ryzen 5 Quad Core 3450U - (8 GB/1 TB HDD/Windows 10 Home) 15s-GR0010AU Thin and Light Laptop  (15.6 inch, Jet Black, 1.76 kg, With MS Office)",
img:"https://i.ibb.co/s92k593/hp-original-imafxtarxygbevbh.jpg",
rating:4.2,ratingDesc:"183 Ratings & 26 Reviews",
details:["AMD Ryzen 5 Quad Core Processor",
"8 GB DDR4 RAM",
"64 bit Windows 10 Operating System",
"1 TB HDD",
"39.62 cm (15.6 inch) Display",
"Microsoft Office Home and Student 2019,HP Documentation,HP CoolSense,HP SSRM, HP Smart, HP Support Assistant",
"1 Year Onsite Warranty"],
pics:{"brand":"Hp",imgList:["https://i.ibb.co/Y7Cw9k7/hp-original-imafxtarxygbevbh.jpg",
"https://i.ibb.co/RPvhN00/hp-original-imafxtarxq89dhgq.jpg","https://i.ibb.co/t4tcS6s/hp-original-imafxtarabjjhazg.jpg",
"https://i.ibb.co/W586y6P/hp-na-original-imafz256y5byyaee.jpg"],
brandImg:"https://i.ibb.co/DzCD7h4/4e0d4a37f0f20d78413f7315017041415b0fe6010ba327ff598715c7db2142b7.jpg"},
price:39990,
prevPrice:42554,discount:6,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:8,popularity:183},
{id:"L4",category:"Laptops",brand:"Hp",name:"HP 14s Core i5 11th Gen - (8 GB/512 GB SSD/Windows 10 Home) 14s-DR2006TU Thin and Light Laptop  (14 inch, Pale Gold, 1.46 kg, With MS Office)",
img:"https://i.ibb.co/Zdrtc7G/hp-original-imafxdr8jqwhqcqc.jpg",
rating:4.4,ratingDesc:"45 Ratings & 8 Reviews",
details:["Intel Core i5 Processor (11th Gen)",
"8 GB DDR4 RAM",
"64 bit Windows 10 Operating System",
"512 GB SSD",
"35.56 cm (14 inch) Display",
"Microsoft Office Home and Student 2019,HP Documentation,HP Smart, HP Support Assistant",
"1 Year Onsite Warranty"],
pics:{"brand":"Hp",imgList:["https://i.ibb.co/ZhbM29m/hp-original-imafxdr8jqwhqcqc.jpg",
"https://i.ibb.co/r3dLBwq/hp-original-imafxdr8ysesqsry.jpg","https://i.ibb.co/hCGSt6w/hp-original-imafxdr8gkubgguz.jpg",
"https://i.ibb.co/mvfg3Mr/hp-original-imafxdr8qc4hewqc.jpg"],
brandImg:"https://i.ibb.co/DzCD7h4/4e0d4a37f0f20d78413f7315017041415b0fe6010ba327ff598715c7db2142b7.jpg"},
price:58990,
prevPrice:60218,discount:2,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:8,popularity:45},
{id:"L5",category:"Laptops",brand:"Hp",name:"HP Pavilion x360 Core i3 10th Gen - (8 GB/512 GB SSD/Windows 10 Home) 14-dh1178TU 2 in 1 Laptop",
img:"https://i.ibb.co/DQtM8T0/windows-10-home-10th-gen-8ga79pa-acj-a-8-gb-core-i5-na-14-original-imafk64ya8dccvt8.jpg",
rating:4.4,ratingDesc:"582 Ratings & 62 Reviews",
details:["Intel Core i3 Processor (10th Gen)",
"8 GB DDR4 RAM",
"64 bit Windows 10 Operating System",
"512 GB SSD",
"35.56 cm (14 inch)	Touchscreen Display",
"Microsoft Office Home and Student 2019, HP Audio Switch, HP ePrint, HP Connection Optimizer, HP Support Assistant, HP Recovery Manager, HP Documentation, HP System Event Utility, HP Jumpstart, Built-in Alexa",
"1 Year Onsite Warranty"],
pics:{"brand":"Hp",imgList:["https://i.ibb.co/hR3NkBB/windows-10-home-10th-gen-8ga79pa-acj-a-8-gb-core-i5-na-14-original-imafk64ya8dccvt8.jpg",
"https://i.ibb.co/2cSqh6W/windows-10-home-10th-gen-8ga79pa-acj-a-8-gb-core-i5-na-14-original-imafk64ybcgffzgh.jpg",
"https://i.ibb.co/jTctGB1/hp-na-2-in-1-laptop-original-imafvezjfmhc4hzz.jpg","https://i.ibb.co/YNgBvr1/hp-na-2-in-1-laptop-original-imaft8py37z94d6y.jpg",
 "https://i.ibb.co/gjzVrg7/hp-na-2-in-1-laptop-original-imaft8pyjah2pte6.jpg"],
brandImg:"https://i.ibb.co/DzCD7h4/4e0d4a37f0f20d78413f7315017041415b0fe6010ba327ff598715c7db2142b7.jpg"},
price:51990,
prevPrice:63179,discount:17,emi:"No Cost EMI",assured:true,exchange:"",ram:8,popularity:582},
{id:"L6",category:"Laptops",brand:"Apple",name:"Apple MacBook Air Core i3 10th Gen - (8 GB/256 GB SSD/Mac OS Catalina) MWTJ2HN/A",
img:"https://i.ibb.co/wyqVjxr/apple-na-original-imafr5acuz95sa5g.jpg",
rating:4.6,ratingDesc:"643 Ratings & 84 Reviews",
details:["Intel Core i3 Processor (10th Gen)",
"8 GB LPDDR4X RAM",
"64 bit Windows 10 Operating System",
"256 GB SSD",
"33.78 cm (13.3 inch) Display",
"Built-in Apps: Photos, iMovie, GarageBand, Pages, Numbers, Keynote, Siri, Safari, Mail, FaceTime, Messages, Maps, News, Stocks, Home, Voice Memos, Notes, Calendar, Contacts, Reminders, Photo Booth, Preview, Books, App Store, Time Machine, TV, Music, Podcasts, Find My, QuickTime Player",
"1 Year Limited Warranty"],
pics:{"brand":"Apple",imgList:["https://i.ibb.co/tM32zKn/apple-na-original-imafr5acuz95sa5g.jpg",
"https://i.ibb.co/ZKM42h0/apple-na-original-imafr5acdst67cpu.jpg",
"https://i.ibb.co/W6dDFyQ/apple-na-original-imafr5acxr9zhcwb.jpg","https://i.ibb.co/G5Nd9YL/apple-na-original-imafr5acsty2nxxv.jpg"],
brandImg:"https://i.ibb.co/vBWYDtj/9d5696196cfb3f4440ca99b1018c8ff91a53716d1948ba73ee3bb68f36571d7a.jpg"},
price:83990,
prevPrice:92990,discount:9,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:8,popularity:643},
{id:"L7",category:"Laptops",brand:"Apple",name:"Apple MacBook Pro with Touch Bar Core i5 8th Gen - (8 GB/512 GB SSD/Mac OS Catalina) MXK72HN/A",
img:"https://i.ibb.co/7gvRFJX/apple-na-thin-and-light-laptop-original-imafs5nhyzx7mku8.jpg",
rating:4.5,ratingDesc:"35 Ratings & 11 Reviews",
details:["Intel Core i5 Processor (8th Gen)",
"8 GB LPDDR3 RAM",
"Mac OS Operating System",
"512 GB SSD",
"33.02 cm (13 inch) Display",
"Built-in Apps: Photos, iMovie, GarageBand, Pages, Numbers, Keynote, Siri, Safari, Mail, FaceTime, Messages, Maps, News, Stocks, Home, Voice Memos, Notes, Calendar, Contacts, Reminders, Photo Booth, Preview, Music, Podcasts, TV, Books, App Store, Time Machine, Find My, QuickTime Player",
"1 Year Limited Hardware Warranty"],
pics:{"brand":"Apple",imgList:["https://i.ibb.co/QFpRLmn/apple-na-thin-and-light-laptop-original-imafs5nmfuxmhnhh.jpg",
"https://i.ibb.co/W3XMHSj/apple-na-thin-and-light-laptop-original-imafs5nmtwjgfyqf.jpg",
"https://i.ibb.co/sHyCXmV/apple-na-thin-and-light-laptop-original-imafs5nmer7kfmhc.jpg",
"https://i.ibb.co/m4YsbRG/apple-na-thin-and-light-laptop-original-imafs5nmszn5vmkt.jpg",
"https://i.ibb.co/G5Nd9YL/apple-na-original-imafr5acsty2nxxv.jpg"],
brandImg:"https://i.ibb.co/vBWYDtj/9d5696196cfb3f4440ca99b1018c8ff91a53716d1948ba73ee3bb68f36571d7a.jpg"},
price:142990,
prevPrice:144990,discount:5,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:8,popularity:35},
{id:"L8",category:"Laptops",brand:"Apple",name:"Apple MacBook Pro with Touch Bar Core i5 8th Gen - (8 GB/512 GB SSD/Mac OS Catalina) MXK72HN/A",
img:"https://i.ibb.co/Jdfsf4Z/apple-na-thin-and-light-laptop-original-imafs5nmg3kxcqnz.jpg",
rating:4.8,ratingDesc:"68 Ratings & 5 Reviews",
details:["Intel Core i5 Processor (10th Gen)",
"16 GB LPDDR4X RAM",
"Mac OS Operating System",
"1 TB SSD",
"33.02 cm (13 inch) Display",
"Built-in Apps: Photos, iMovie, GarageBand, Pages, Numbers, Keynote, Siri, Safari, Mail, FaceTime, Messages, Maps, News, Stocks, Home, Voice Memos, Notes, Calendar, Contacts, Reminders, Photo Booth, Preview, Music, Podcasts, TV, Books, App Store, Time Machine, Find My, QuickTime Player",
"1 Year Limited Hardware Warranty"],
pics:{"brand":"Apple",imgList:["https://i.ibb.co/QFpRLmn/apple-na-thin-and-light-laptop-original-imafs5nmfuxmhnhh.jpg",
"https://i.ibb.co/W3XMHSj/apple-na-thin-and-light-laptop-original-imafs5nmtwjgfyqf.jpg",
"https://i.ibb.co/sHyCXmV/apple-na-thin-and-light-laptop-original-imafs5nmer7kfmhc.jpg",
"https://i.ibb.co/m4YsbRG/apple-na-thin-and-light-laptop-original-imafs5nmszn5vmkt.jpg",
"https://i.ibb.co/G5Nd9YL/apple-na-original-imafr5acsty2nxxv.jpg"],
brandImg:"https://i.ibb.co/vBWYDtj/9d5696196cfb3f4440ca99b1018c8ff91a53716d1948ba73ee3bb68f36571d7a.jpg"},
price:174900,
prevPrice:178990,discount:5,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:16,popularity:68},
{id:"L9",category:"Laptops",brand:"Apple",name:"Apple MacBook Pro with Touch Bar Core i5 8th Gen - (8 GB/512 GB SSD/Mac OS Catalina) MXK72HN/A",
img:"https://i.ibb.co/cbXdnmk/apple-na-thin-and-light-laptop-original-imafgwev6abfznds.jpg",
rating:4.7,ratingDesc:"125 Ratings & 18 Reviews",
details:["Intel Core i5 Processor (8th Gen)",
"8 GB DDR3 RAM",
"Mac OS Operating System",
"128 GB SSD",
"33.78 cm (13.3 inch) Display",
"Built-in Apps: Photos, iMovie, GarageBand, Pages, Numbers, Keynote, Siri, Safari, Mail, FaceTime, Messages, Maps, News, Stocks, Home, Voice Memos, Notes",
"1 Year Onsite Warranty"],
pics:{"brand":"Apple",imgList:["https://i.ibb.co/VY5ZzbK/apple-na-thin-and-light-laptop-original-imafgwev6abfznds.jpg",
"https://i.ibb.co/NLKCDmH/apple-na-thin-and-light-laptop-original-imafgwevwuzkck7s.jpg",
"https://i.ibb.co/rMwh1zX/apple-na-thin-and-light-laptop-original-imafgwevxrkmpqht.jpg"],
brandImg:"https://i.ibb.co/vBWYDtj/9d5696196cfb3f4440ca99b1018c8ff91a53716d1948ba73ee3bb68f36571d7a.jpg"},
price:97040,
prevPrice:119900,discount:19,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:8,popularity:125},
{id:"L10",category:"Laptops",brand:"Apple",name:"Apple MacBook Air Core i5 10th Gen - (8 GB/512 GB SSD/Mac OS Catalina) MVH22HN/A",
img:"https://i.ibb.co/xX0L7qn/apple-na-original-imafr5acuz95sa5g.jpg",
rating:4.5,ratingDesc:"232 Ratings & 16 Reviews",
details:["Intel Core i5 Processor (10th Gen)",
"8 GB LPDDR4X RAM",
"Mac OS Operating System",
"512 GB SSD",
"33.78 cm (13.3 inch) Display",
"Built-in Apps: Photos, iMovie, GarageBand, Pages, Numbers, Keynote, Siri, Safari, Mail, FaceTime, Messages, Maps, News, Stocks, Home, Voice Memos, Notes, Calendar, Contacts, Reminders, Photo Booth, Preview, Music, Podcasts, TV, Books, App Store, Time Machine, Find My, QuickTime Player",
"1 Year Limited Hardware Warranty"],
pics:{"brand":"Apple",imgList:["https://i.ibb.co/DkqRrSK/apple-na-thin-and-light-laptop-original-imafcnwgnnwbjpyj.jpg",
"https://i.ibb.co/NLKCDmH/apple-na-thin-and-light-laptop-original-imafgwevwuzkck7s.jpg",
"https://i.ibb.co/py8tt9k/apple-na-thin-and-light-laptop-original-imafcnwgbxzugkyw.jpg"],
brandImg:"https://i.ibb.co/vBWYDtj/9d5696196cfb3f4440ca99b1018c8ff91a53716d1948ba73ee3bb68f36571d7a.jpg"},
price:118990,
prevPrice:122900,discount:3,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:8,popularity:232},
{id:"L11",category:"Laptops",brand:"Dell",name:"Dell Inspiron Core i5 10th Gen - (8 GB/512 GB SSD/Windows 10 Home/2 GB Graphics) INS 5408 Thin and Light Laptop",
img:"https://i.ibb.co/Nj3RCD6/dell-original-imafv9tnwfhskwbx.jpg",
rating:4.3,ratingDesc:"132 Ratings & 15 Reviews",
details:["Intel Core i5 Processor (10th Gen)",
"8 GB DDR4 RAM",
"64 bit Windows 10 Operating System",
"512 GB SSD",
"35.56 cm (14 inch) Display",
"Microsoft Office Home and Student 2019",
"1 Year Onsite Warranty"],
pics:{"brand":"Dell",imgList:["https://i.ibb.co/VYpd613/dell-original-imafv9tnwfhskwbx.jpg",
"https://i.ibb.co/ynDkh48/dell-na-thin-and-light-laptop-original-imafw8hnhtm7n9qw.jpg",
"https://i.ibb.co/4KQCvr9/dell-na-thin-and-light-laptop-original-imafw8hncumwugtp.jpg",
"https://i.ibb.co/j5kWP41/dell-na-thin-and-light-laptop-original-imafw8hndz3vhxhp.jpg",
"https://i.ibb.co/0rvMF5G/dell-original-imafv9tnfqgxpnxk.jpg"],
brandImg:"https://rukminim1.flixcart.com/image/160/160/prod-fk-cms-brand-images/b99107335669131b5797b6b986ab6825278d6cde72b862b48a13febe474d6a18.jpg"},
price:65990,
prevPrice:73375,discount:10,emi:"No Cost EMI",assured:true,exchange:"",ram:8,popularity:132},
{id:"L12",category:"Laptops",brand:"Dell",name:"Dell Vostro Core i3 10th Gen - (4 GB/1 TB HDD/Windows 10 Home) Vostro 3491 Thin and Light Laptop",
img:"https://i.ibb.co/XZYW1TB/dell-original-imaftuvzyhyfmvzy.jpg",
rating:3.8,ratingDesc:"1867 Ratings & 236 Reviews",
details:["Intel Core i3 Processor (10th Gen)",
"4 GB DDR4 RAM",
"64 bit Windows 10 Operating System",
"1 TB HDD",
"35.56 cm (14 inch) Display",
"Microsoft Office Home and Student 2019",
"1 Year Limited Hardware Warranty, In Home Service After Remote Diagnosis - Retail"],
pics:{"brand":"Dell",imgList:["https://i.ibb.co/GcwKrhY/dell-original-imaftuvzyhyfmvzy.jpg",
"https://i.ibb.co/xjj9RJN/dell-na-thin-and-light-laptop-original-imafvzjyyshqyhez.jpg",
"https://i.ibb.co/GRWxYMs/dell-original-imaftuvz3kqhzuvz.jpg",
"https://i.ibb.co/f9sJfQM/dell-na-thin-and-light-laptop-original-imafvzjy574uqhdp.jpg"],
brandImg:"https://rukminim1.flixcart.com/image/160/160/prod-fk-cms-brand-images/b99107335669131b5797b6b986ab6825278d6cde72b862b48a13febe474d6a18.jpg"},
price:32990,
prevPrice:35213	,discount:6,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:4,popularity:1867},
{id:"L13",category:"Laptops",brand:"Dell",name:"Dell Vostro Core i3 10th Gen - (4 GB/1 TB HDD/Windows 10 Home) Vostro 3401 Thin and Light Laptop",
img:"https://i.ibb.co/Jnjyx82/dell-original-imafvwfmqup6fzfm.jpg",
rating:3.7,ratingDesc:"1165 Ratings & 175 Reviews",
details:["Intel Core i3 Processor (10th Gen)",
"4 GB DDR4 RAM",
"64 bit Windows 10 Operating System",
"1 TB HDD",
"35.56 cm (14 inch) Display",
"Microsoft Office Home and Student 2019",
"1 Year Limited Hardware Warranty, In Home Service After Remote Diagnosis - Retail"],
pics:{"brand":"Dell",imgList:["https://i.ibb.co/9rYzzx5/dell-original-imafvwfmqup6fzfm.jpg",
"https://i.ibb.co/t2RRNBR/dell-na-thin-and-light-laptop-original-imafw37yvtc9brwg.jpg",
"https://i.ibb.co/NC0XY16/dell-na-thin-and-light-laptop-original-imafw378juzyzjuj.jpg",
"https://i.ibb.co/WPF92W7/dell-na-thin-and-light-laptop-original-imafw37ymzhgg7cy.jpg"],
brandImg:"https://rukminim1.flixcart.com/image/160/160/prod-fk-cms-brand-images/b99107335669131b5797b6b986ab6825278d6cde72b862b48a13febe474d6a18.jpg"},
price:33990,
prevPrice:38703	,discount:12,emi:"No Cost EMI",assured:true,exchange:"",ram:4,popularity:1165},
{id:"L14",category:"Laptops",brand:"Dell",name:"Dell Inspiron Core i5 10th Gen - (8 GB/512 GB SSD/Windows 10 Home) Inspiron 5300 Thin and Light Laptop",
img:"https://i.ibb.co/HNRBXdK/dell-na-thin-and-light-laptop-original-imafv9twgs5esgvh.jpg",
rating:4.7,ratingDesc:"11 Ratings & 2 Reviews",
details:["Intel Core i5 Processor (10th Gen)",
"8 GB DDR4 RAM",
"64 bit Windows 10 Operating System",
"512 GB SSD",
"33.78 cm (13.3 inch) Display",
"Microsoft Office Home and Student 2019",
"1 Year Onsite Warranty"],
pics:{"brand":"Dell",imgList:["https://i.ibb.co/yffJrZT/dell-na-thin-and-light-laptop-original-imafv9twgs5esgvh.jpg",
"https://i.ibb.co/PQMP2nN/dell-na-thin-and-light-laptop-original-imafv9twtyurf28p.jpg",
"https://i.ibb.co/SKgh3fn/dell-na-thin-and-light-laptop-original-imafv9twz2g6sy6s.jpg",
"https://i.ibb.co/g9nkYHS/dell-na-thin-and-light-laptop-original-imafv9twjugqrthv.jpg"],
brandImg:"https://rukminim1.flixcart.com/image/160/160/prod-fk-cms-brand-images/b99107335669131b5797b6b986ab6825278d6cde72b862b48a13febe474d6a18.jpg"},
price:73990,
prevPrice:75234	,discount:1,emi:"No Cost EMI",assured:true,exchange:"",ram:4,popularity:11},
{id:"L15",category:"Laptops",brand:"Dell",name:"Dell Inspiron Ryzen 3 Dual Core 3250U 2nd Gen - (4 GB/1 TB HDD/Windows 10 Home) Inspiron 3505 Laptop",
img:"https://i.ibb.co/NtWf5jW/dell-inspiron-3505-laptop-amd-ryzen-3-3250u-4gb-ram-1tb-hdd-15-6-original-imafynyafscsdrka.jpg",
rating:3.5,ratingDesc:"112 Ratings & 22 Reviews",
details:["AMD Ryzen 3 Dual Core Processor (2nd Gen)",
"4 GB DDR4 RAM",
"64 bit Windows 10 Operating System",
"1 TB HDD",
"39.62 cm (15.6 inch) Display",
"Microsoft Office Home and Student 2019",
"1 Year Onsite Warranty"],
pics:{"brand":"Dell",imgList:["https://i.ibb.co/ctYYQbt/dell-inspiron-3505-laptop-amd-ryzen-3-3250u-4gb-ram-1tb-hdd-15-6-original-imafynyafscsdrka.jpg",
"https://i.ibb.co/SxqLXNz/dell-inspiron-3505-laptop-amd-ryzen-3-3250u-4gb-ram-1tb-hdd-15-6-original-imafynya3cb4dnba.jpg", "https://i.ibb.co/ZH5k7ft/dell-inspiron-3505-laptop-amd-ryzen-3-3250u-4gb-ram-1tb-hdd-15-6-original-imafyhuucnayamfb.jpg", "https://i.ibb.co/M8kcjvt/dell-inspiron-3505-laptop-amd-ryzen-3-3250u-4gb-ram-1tb-hdd-15-6-original-imafyhuuez7z8hhb.jpg"],
brandImg:"https://rukminim1.flixcart.com/image/160/160/prod-fk-cms-brand-images/b99107335669131b5797b6b986ab6825278d6cde72b862b48a13febe474d6a18.jpg"},
price:33990,
prevPrice:35234	,discount:2,emi:"No Cost EMI",assured:true,exchange:"",ram:4,popularity:112},
{id:"L16",category:"Laptops",brand:"Acer",name:"Acer Aspire 7 Core i5 9th Gen - (8 GB/512 GB SSD/Windows 10 Home/4 GB Graphics/NVIDIA Geforce GTX 1650 Ti) A715-75G-51H8 Gaming Laptop",
img:"https://i.ibb.co/VYxVWqw/acer-na-gaming-laptop-original-imafthr8aa4rsjpn.jpg",
rating:4.4,ratingDesc:"233 Ratings & 44 Reviews",
details:["Intel Core i5 Processor (9th Gen)",
"8 GB DDR4 RAM",
"64 bit Windows 10 Operating System",
"512 GB SSD",
"39.62 cm (15.6 inch) Display",
"Acer Collection, Acer Product Registration , Quick Access, Acer Care Center",
"1 Year International Travelers Warranty (ITW)"],
pics:{"brand":"Acer",imgList:["https://i.ibb.co/z2TfdDb/acer-na-gaming-laptop-original-imafs5prytwgrcyf.jpg",
"https://i.ibb.co/gTsM2gj/acer-na-gaming-laptop-original-imafrzzvrjxrfhnt.jpg",
"https://i.ibb.co/M91ZYst/acer-na-gaming-laptop-original-imafrczgkedbamcx.jpg",
"https://i.ibb.co/6RgwtSc/acer-na-gaming-laptop-original-imafrczhrywshugf.jpg"],
brandImg:"https://rukminim1.flixcart.com/image/160/160/prod-fk-cms-brand-images/e6ef82a91ed1d0bac7eff768025543ab00d969308640f932e2f1d934cd052540.jpg"},
price:59990,
prevPrice:84999,discount:29,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:8,popularity:233},
{id:"L17",category:"Laptops",brand:"Acer",name:"Acer Predator Helios 300 Core i5 10th Gen - (16 GB/1 TB HDD/256 GB SSD/Windows 10 Home/6 GB Graphics/NVIDIA Geforce RTX 2060/144 Hz) PH315-53-594S Gaming Laptop",
img:"https://i.ibb.co/Kq5NRmW/acer-na-gaming-laptop-original-imafrcpya8phvptc.jpg",
rating:4.6,ratingDesc:"554 Ratings & 85 Reviews",
details:["Intel Core i5 Processor (9th Gen)",
"16 GB DDR4 RAM",
"64 bit Windows 10 Operating System",
"1TB HDD|256 GB SSD",
"39.62 cm (15.6 inch) Display",
"PredatorSense,Acer Collection, Acer Product Registration , Quick Access, Acer Care Center",
"1 Year International Travelers Warranty (ITW)"],

pics:{"brand":"Acer",imgList:["https://i.ibb.co/njGkQpS/acer-na-gaming-laptop-original-imafrcpya8phvptc.jpg",
"https://i.ibb.co/D98YW7L/acer-na-gaming-laptop-original-imafrcpyh3p3ehhe.jpg",
"https://i.ibb.co/3kkMXcx/acer-na-gaming-laptop-original-imafrcpfdhazwgyr.jpg",
"https://i.ibb.co/W2rXLkT/acer-na-gaming-laptop-original-imafrpf3zsaejwsm.jpg"],
brandImg:"https://rukminim1.flixcart.com/image/160/160/prod-fk-cms-brand-images/e6ef82a91ed1d0bac7eff768025543ab00d969308640f932e2f1d934cd052540.jpg"},
price:94990,
prevPrice:130990,discount:27,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:8,popularity:554},
{id:"L18",category:"Laptops",brand:"Acer",name:"Acer Aspire 5 Core i3 10th Gen - (4 GB + 32 GB Optane/512 GB SSD/Windows 10 Home) A514-53-316M Thin and Light Laptop",
img:"https://i.ibb.co/YbBJJJG/acer-na-thin-and-light-laptop-original-imafu5t5tghggqyz.jpg",
rating:4.3,ratingDesc:"441 Ratings & 62 Reviews",
details:["Intel Core i3 Processor (10th Gen)",
"4 GB DDR4 RAM",
"64 bit Windows 10 Operating System",
"512 GB SSD",
"35.56 cm (14 inch) Display",
"Acer Collection, Acer Product Registration , Quick Access, Acer Care Center",
"1 Year International Travelers Warranty (ITW)"],
pics:{"brand":"Acer",imgList:["https://i.ibb.co/zNrBhNN/acer-original-imafrhdaywnzkuxc.jpg",
"https://i.ibb.co/1QFVGWy/acer-original-imafrhdabx3npmfs.jpg",
"https://i.ibb.co/94BS00S/acer-na-laptop-original-imafw7h7c8fhg8aq.jpg",
"https://i.ibb.co/pf6NTFb/acer-original-imafrhdacmwyb4dt.jpg"],
brandImg:"https://rukminim1.flixcart.com/image/160/160/prod-fk-cms-brand-images/e6ef82a91ed1d0bac7eff768025543ab00d969308640f932e2f1d934cd052540.jpg"},
price:39990,
prevPrice:55999,discount:28,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:8,popularity:441},
{id:"L19",category:"Laptops",brand:"Apple",name:"Apple MacBook Air M1-(8 GB/256 GB SSD/Mac OS Big Sur) MGND3HN/A(13.3 inch, Gold, 1.29 kg)",
img:"https://i.ibb.co/CnS5Qgr/apple-original-imafxfyqykyghjg6.jpg",
rating:4,ratingDesc:"90 Ratings & 65 Reviews",
details:["Apple M1 Processor",
"8 GB DDR4 RAM",
"Mac OS Operating System",
"256 GB SSD",
"33.78 cm (13.3 inch) Display",
"Built-in Apps: Photos, iMovie, GarageBand, Pages, Numbers, Keynote, Siri, Safari, Mail, FaceTime, Messages, Maps, News, Stocks, Home, Voice Memos, Notes, Calendar, Contacts, Reminders, Photo Booth, Preview, Books, App Store, Time Machine, TV, Music, Podcasts, Find My, QuickTime Player",
"1 Year Limited Warranty"],
pics:{"brand":"Apple",imgList:["https://i.ibb.co/2s0bXC4/apple-original-imafxfyqykyghjg6.jpg",
"https://i.ibb.co/kcn8KGz/apple-original-imafxfyq8vkgsve7.jpg",
"https://i.ibb.co/KjVrs7D/apple-original-imafxfyqygm3e56b.jpg",
"https://i.ibb.co/F4YXG6Y/apple-original-imafxfyqzthd9ssa.jpg"],
brandImg:"https://i.ibb.co/vBWYDtj/9d5696196cfb3f4440ca99b1018c8ff91a53716d1948ba73ee3bb68f36571d7a.jpg"},
price:142900,
prevPrice:155990,discount:18,emi:"No Cost EMI",assured:true,exchange:"Upto ₹15,650 Off on Exchange",ram:8,popularity:90},


{id:"C1",category:"Cameras",brand:"DSLR",name:"Canon EOS 3000D DSLR Camera Single Kit with 18-55 lens (16 GB Memory Card & Carry Case)",
img:"https://i.ibb.co/myTSFQr/canon-eos-eos-3000d-dslr-original-imaf3t5h9yuyc5zu.jpg",
rating:4.4,ratingDesc:"19228 Ratings & 3102 Reviews",
details:["Self-Timer | Type C and Mini HDMI, |9 Auto Focus Points | 35x Optical Zoom., Effective Pixels: 18 MP APS-C CMOS sensor-which is 25 times larger than a typical Smartphone sensor., WiFi | Full HD | Video Recording at 1080 p on 30fps.",
"Effective Pixels: 18 MP",
"Sensor Type: CMOS",
"WiFi Available",
"Full HD",
"Product is covered under one-year standard warranty and one-year additional warranty from the date of Product purchased by the customer. Warranty validation / Warranty period confirmation would be done through either Online Warranty Serial No. Tracking system (Service Edge) or Warranty Card and Customer Invoice date."],
pics:{"brand":"DSLR",imgList:["https://i.ibb.co/ypY8vdC/canon-eos-eos-3000d-dslr-original-imaf3t5h9yuyc5zu.jpg",
"https://i.ibb.co/kDty1Vs/canon-eos-3000d-original-imaf3t5hjrz6zzzc.jpg",
"https://i.ibb.co/yY0PPLb/eos-eos-3000d-canon-original-imaf9n6fqhmgt4cv.jpg",
"https://i.ibb.co/9yvY3rb/eos-eos-3000d-canon-original-imaf9n6fg2xkhqh3.jpg"],
},
price:28611,
prevPrice:29495,discount:2,emi:"No Cost EMI",assured:true,exchange:"Upto ₹13,000 Off on Exchange",ram:16,popularity:19228},
{id:"C2",category:"Cameras",brand:"DSLR",name:"Canon EOS 80D DSLR Camera Body with Single Lens: EF-S 18-135 IS USM (16 GB SD Card)",
img:"https://i.ibb.co/mNhJmnG/eos-80d-80d-canon-original-imaf329wfqqyawwx.jpg",
rating:4.6,ratingDesc:"590 Ratings & 102 Reviews",
details:[
"Effective Pixels: 24 MP",
"Sensor Type: CMOS",
"WiFi Available",
"1080p at 60p + Time-Lapse",
"Product is covered under one-year standard warranty and one-year additional warranty from the date of Product purchased by the customer. Warranty validation / Warranty period confirmation would be done through either Online Warranty Serial No. Tracking system (Service Edge) or Warranty Card and Customer Invoice date."],
pics:{"brand":"DSLR",imgList:["https://i.ibb.co/JFD7LYf/eos-80d-80d-canon-original-imaf329wfqqyawwx.jpg",
"https://i.ibb.co/9TXJQXk/eos-80d-80d-canon-original-imaf329w88h358eb.jpg",
"https://i.ibb.co/VYv0kQS/eos-80d-80d-canon-original-imaf329w6rn36yre.jpg",
"https://i.ibb.co/PYpVNcr/eos-80d-80d-canon-original-imaf329wsarqsgcy.jpg"],
},
price:91990,
prevPrice:105495,discount:12,emi:"No Cost EMI",assured:true,exchange:"Upto ₹13,000 Off on Exchange",ram:16,popularity:19228},
{id:"C3",category:"Cameras",brand:"Lens",name:"NICK JONES Mobile Phone Telephoto Lens 0.45X Super Wide Angle Lens 0.45X Professional HD Camera Lens Kit Mobile Phone Lens",
img:"https://i.ibb.co/KVcx9zk/mobile-phone-telephoto-lens-0-45x-super-wide-angle-lens-0-45x-original-imafhwx2e9vhqvyx.jpg",
rating:3.1,ratingDesc:"159 Ratings & 29 Reviews",
details:[
"Effective Pixels: 24 MP",
"Sensor Type: CMOS",
"WiFi Available",
"1080p at 60p + Time-Lapse",],
pics:{"brand":"Lens",imgList:["https://i.ibb.co/87Q0PdG/mobile-phone-telephoto-lens-0-45x-super-wide-angle-lens-0-45x-original-imafhwx2e9vhqvyx.jpg",
"https://i.ibb.co/ww2FqpC/mobile-phone-telephoto-lens-0-45x-super-wide-angle-lens-0-45x-original-imafhwy6a99hdmwy.jpg",
"https://i.ibb.co/7Ncv87S/mobile-phone-telephoto-lens-0-45x-super-wide-angle-lens-0-45x-original-imafhwzmgvxpanxf.jpg",
"https://i.ibb.co/2MwWQhv/mobile-phone-telephoto-lens-0-45x-super-wide-angle-lens-0-45x-original-imafhxy4emkaznyq.jpg"],
},
price:369,
prevPrice:1200,discount:69,emi:"No Cost EMI",assured:true,exchange:"Upto ₹500 Off on Exchange",ram:4,popularity:159},
{id:"C4",category:"Cameras",brand:"Tripods",name:"CartBug 10 Selfie Ring Light, 26 Colors RGB Ring Light with Adjustable Tripod Stand/Phone Holder/Shutter Dimmable LED Circle Light for YouTube, Makeup,Photography Tripod, Monopod",
img:"https://i.ibb.co/YhZvq8R/10-selfie-ring-light-26-colors-rgb-ring-light-with-adjustable-original-imafx5zfkv3zetdz.jpg",
rating:4.1,ratingDesc:"159 Ratings & 29 Reviews",
details:[
"Effective Pixels: 24 MP",
"Sensor Type: CMOS",
"WiFi Available"],
pics:{"brand":"Tripods",imgList:["https://i.ibb.co/4j6k39D/10-inch-rgb-ring-light-360-full-color-13-dynamic-rgb-13-static-original-imafyvvhb7yre8zg.jpg",
"https://i.ibb.co/zGVP2YH/10-rgb-ring-light-with-stand-mini-led-dimmable-circle-light-original-imafyvxrhg8fca7t.jpg",
"https://i.ibb.co/drVtqwc/10-rgb-ring-light-with-stand-mini-led-dimmable-circle-light-original-imafyvxrfypngqbz.jpg"],
},
price:1999,
prevPrice:2999,discount:33,emi:"No Cost EMI",assured:true,exchange:"Upto ₹500 Off on Exchange",ram:4,popularity:159},
{id:"C5",category:"Cameras",brand:"Compact",name:"BLAPOXE Portable Compact Mini Pocket 10x25 with Powerful Lens 101 to 1000m Vision Binoculars For Sports Hunting Camping Binoculars",
img:"https://i.ibb.co/44KWyLm/blapoxe-portable-compact-mini-pocket-10x25-with-powerful-lens-original-imafgv8vzzvfjwzz.jpg",
rating:3.2,ratingDesc:"15 Ratings & 5 Reviews",
details:["Type:Binoculars",
"Objective Diameter: 15mm",
"Water Proof",
"Magnification: 30 x"],
pics:{"brand":"Compact",imgList:["https://i.ibb.co/qMFCrSg/blapoxe-portable-compact-mini-pocket-10x25-with-powerful-lens-original-imafgv8vzzvfjwzz.jpg",
"https://i.ibb.co/Zd7j4PZ/blapoxe-portable-compact-mini-pocket-10x25-with-powerful-lens-original-imafgwd5krmybehr.jpg",
"https://i.ibb.co/WsBYhRk/blapoxe-portable-compact-mini-pocket-10x25-with-powerful-lens-original-imafgs4fhchvwwdg.jpg"],
},
price:649,
prevPrice:999,discount:35,emi:"No Cost EMI",assured:true,exchange:"Upto ₹300 Off on Exchange",ram:4,popularity:15},
{id:"C6",category:"Cameras",brand:"Accessories",name:"Canon M50 Mirrorless Camera Body with Single Lens EF-M 15-45 mm IS STM",
img:"https://i.ibb.co/brgmVps/eos-m50-canon-original-imaf54t3aqfb6rpf.jpg",
rating:4.6,ratingDesc:"922 Ratings & 135 Reviews",
details:["Type:Binoculars",
"Objective Diameter: 15mm",
"Water Proof",
"Magnification: 30 x"],
pics:{"brand":"Accessories",imgList:["https://i.ibb.co/nkqCVq1/eos-m50-canon-original-imaf54t3aqfb6rpf.jpg",
"https://i.ibb.co/GsBwjJv/eos-m50-canon-original-imaf54t3ehuks6ck.jpg",
"https://i.ibb.co/wW0v29c/eos-m50-canon-original-imaf54t3d9adgpzu.jpg",
"https://i.ibb.co/jvqx9Tg/eos-m50-canon-original-imaf54t3a75wzhkw.jpg",
"https://i.ibb.co/Qc63Grp/eos-m50-canon-original-imaf54t3saheqr78.jpg"],
},
price:51911,
prevPrice:54995,discount:5,emi:"No Cost EMI",assured:true,exchange:"Upto ₹1000 Off on Exchange",ram:4,popularity:922}];

let bankOffer = [{img:"https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",type:"Bank Offer",detail:"5% Unlimited Cashback on Flipkart Axis Bank Credit Card"},
{img:"https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",type:"Partner Offer",detail:"Get a free Lifetime Premium membership of HP Print Learn Centre content on purchase of this product"},
{img:"https://i.ibb.co/zVRyyTn/49f16fff-0a9d-48bf-a6e6-5980c9852f11.png",type:"No cost EMI ₹11,332/month.",detail:"Standard EMI also available"},
{img:"https://i.ibb.co/zVRyyTn/49f16fff-0a9d-48bf-a6e6-5980c9852f11.png",type:"Bank Offer",detail:"Upto 40% off on Complete Laptop Protection. Save up on defects & damages for 2 years"}];

app.get("/products/Laptops",function(req,res) { 
  console.log("in get request for laptopsData: ");
  let page = +req.query.page;
  let {q, sort,ram,rating,price} = req.query;
  page = isNaN(page) ? 1 : page;
  let laptopsData = dataList.filter(obj=> obj.category === "Laptops");
  console.log(q)
  if(q){
    laptopsData = [...laptopsData.filter(obj=> obj.brand.toLowerCase().includes(q.toLowerCase()) || obj.name.toLowerCase().includes(q.toLowerCase()))];
  }
  if(sort === 'popularity'){
    laptopsData = laptopsData.sort((a,b) => (a.popularity < b.popularity) ? 1 : ((b.popularity < a.popularity) ? -1 : 0)); 
  }else if(sort === 'asc'){
    laptopsData = laptopsData.sort((a,b) => (a.price > b.price) ? 1 : ((b.price > a.price) ? -1 : 0)); 
  }else if(sort === 'desc'){
    laptopsData = laptopsData.sort((a,b) => (a.price < b.price) ? 1 : ((b.price < a.price) ? -1 : 0)); 
  }
  if(ram) {
    let ramList = ram.split(",");
    let filterData = [];
    for (let index in ramList) {
      if(ramList[index] === '>=6'){
        filterData.push(...laptopsData.filter((data) => data.ram >= 6)) ;
      }else if(ramList[index] === '<=4'){
        filterData.push(...laptopsData.filter((data) => data.ram === 4)) ;
      }else if(ramList[index] === '<=3'){
        filterData.push(...laptopsData.filter((data) => data.ram === 3)) ;
      }else if(ramList[index] === '<=2'){
        filterData.push(...laptopsData.filter((data) => data.ram === 2)) ;
      }
    }
    laptopsData = [...filterData];
  }
  if(rating) {
    let ratingList = rating.split(",");
    let filterData = [];
    for (let index in ratingList) {
      if(ratingList[index] === '>4'){
        filterData.push(...laptopsData.filter((data) => data.rating >= 4)) ;
      }else if(ratingList[index] === '>3'){
        filterData.push(...laptopsData.filter((data) => data.rating >= 3)) ;
      }else if(ratingList[index] === '>2'){
        filterData.push(...laptopsData.filter((data) => data.rating >= 2)) ;
      }else if(ratingList[index] === '>1'){
        filterData.push(...laptopsData.filter((data) => data.rating >= 1)) ;
      }
    }
    laptopsData = [...filterData];
  }
  if(price) {
    
    let priceList = price.split(",");
    let filterData = [];
    console.log("Price", priceList)
    for (let index in priceList) {
      if(priceList[index].trim() === '20000'){
        filterData.push(...laptopsData.filter((data) => data.price >=20000)) ;
      }else if(priceList[index] === '10000-20000'){
        filterData.push(...laptopsData.filter((data) => data.price >= 10000 && data.price < 20000)) ;
      }else if(priceList[index] === '5000-10000'){
        filterData.push(...laptopsData.filter((data) => data.price  >= 5000 && data.price < 10000)) ;
      }else if(priceList[index] === '0-5000'){
        filterData.push(...laptopsData.filter((data) => data.price <= 5000)) ;
      }
    }
    laptopsData = [...filterData];
  }
  let respArr = pagination(laptopsData, page);
  let len = laptopsData.length;
  let quo = Math.floor(len / 5);
  let rem = len % 5;
  let extra = rem === 0 ? 0 : 1;
  let numofpages = quo + extra;
  let pageInfo = {
      pageNumber: page,
      numberOfPages: numofpages,
      numOfItems: 5,
      totalItemCount: laptopsData.length,
  };
  res.json({
    data: respArr,
    pageInfo: pageInfo,
  });
});

app.get("/products/Laptops/:brand",function(req,res) { 
  let page = +req.query.page;
  let {sort,ram,rating,price} = req.query;
  page = isNaN(page) ? 1 : page;
  let brand = req.params.brand;
  console.log("in get request for brand: ",brand);
  let laptopsData= dataList.filter(obj=> obj.brand === brand && obj.category === "Laptops");
  if(sort === 'popularity'){
    laptopsData = laptopsData.sort((a,b) => (a.popularity < b.popularity) ? 1 : ((b.popularity < a.popularity) ? -1 : 0)); 
  }else if(sort === 'asc'){
    laptopsData = laptopsData.sort((a,b) => (a.price > b.price) ? 1 : ((b.price > a.price) ? -1 : 0)); 
  }else if(sort === 'desc'){
    laptopsData = laptopsData.sort((a,b) => (a.price < b.price) ? 1 : ((b.price < a.price) ? -1 : 0)); 
  }
  if(ram) {
    let ramList = ram.split(",");
    let filterData = [];
    for (let index in ramList) {
      if(ramList[index] === '>=6'){
        filterData.push(...laptopsData.filter((data) => data.ram >= 6)) ;
      }else if(ramList[index] === '<=4'){
        filterData.push(...laptopsData.filter((data) => data.ram === 4)) ;
      }else if(ramList[index] === '<=3'){
        filterData.push(...laptopsData.filter((data) => data.ram === 3)) ;
      }else if(ramList[index] === '<=2'){
        filterData.push(...laptopsData.filter((data) => data.ram === 2)) ;
      }
    }
    laptopsData = [...filterData];
  }
  if(rating) {
    let ratingList = rating.split(",");
    let filterData = [];
    for (let index in ratingList) {
      if(ratingList[index] === '>4'){
        filterData.push(...laptopsData.filter((data) => data.rating >= 4)) ;
      }else if(ratingList[index] === '>3'){
        filterData.push(...laptopsData.filter((data) => data.rating >= 3)) ;
      }else if(ratingList[index] === '>2'){
        filterData.push(...laptopsData.filter((data) => data.rating >= 2)) ;
      }else if(ratingList[index] === '>1'){
        filterData.push(...laptopsData.filter((data) => data.rating >= 1)) ;
      }
    }
    laptopsData = [...filterData];
  }
  if(price) {
    
    let priceList = price.split(",");
    let filterData = [];
    console.log("Price", priceList)
    for (let index in priceList) {
      if(priceList[index].trim() === '20000'){
        filterData.push(...laptopsData.filter((data) => data.price >=20000)) ;
      }else if(priceList[index] === '10000-20000'){
        filterData.push(...laptopsData.filter((data) => data.price >= 10000 && data.price < 20000)) ;
      }else if(priceList[index] === '5000-10000'){
        filterData.push(...laptopsData.filter((data) => data.price  >= 5000 && data.price < 10000)) ;
      }else if(priceList[index] === '0-5000'){
        filterData.push(...laptopsData.filter((data) => data.price <= 5000)) ;
      }
    }
    laptopsData = [...filterData];
  }
  let respArr = pagination(laptopsData, page);
  let len = laptopsData.length;
  let quo = Math.floor(len / 5);
  let rem = len % 5;
  let extra = rem === 0 ? 0 : 1;
  let numofpages = quo + extra;
  let pageInfo = {
      pageNumber: page,
      numberOfPages: numofpages,
      numOfItems: 5,
      totalItemCount: laptopsData.length,
  };
  res.json({
    data: respArr,
    pageInfo: pageInfo,
  });
});

function pagination(obj, page) {
  var resArr = obj;
  resArr = resArr.slice(page * 5 - 5, page * 5);
  return resArr;
}

app.get("/product/:id",function(req,res) { 
  let id = req.params.id;
  console.log("in get request for id: ",id);
  let data = dataList.find(obj=> obj.id === id);
  res.send({prod : data});
});
app.get("/bankOffers",function(req,res) { 
  console.log("in get request for bankOffers: ");
  res.send(bankOffer)
});

app.get("/products/Cameras",function(req,res) { 
  console.log("in get request for CamerasData: ");
  let page = +req.query.page;
  let {q,sort,ram,rating,price} = req.query;
  page = isNaN(page) ? 1 : page;
  let camerasData = dataList.filter(obj=> obj.category === "Cameras");
  if(q){
    camerasData = [...camerasData.filter(obj=> obj.brand.toLowerCase().includes(q.toLowerCase()) || obj.name.toLowerCase().includes(q.toLowerCase()))];
  }
  if(sort === 'popularity'){
    camerasData = camerasData.sort((a,b) => (a.popularity < b.popularity) ? 1 : ((b.popularity < a.popularity) ? -1 : 0)); 
  }else if(sort === 'asc'){
    camerasData = camerasData.sort((a,b) => (a.price > b.price) ? 1 : ((b.price > a.price) ? -1 : 0)); 
  }else if(sort === 'desc'){
    camerasData = camerasData.sort((a,b) => (a.price < b.price) ? 1 : ((b.price < a.price) ? -1 : 0)); 
  }
  if(ram) {
    let ramList = ram.split(",");
    let filterData = [];
    for (let index in ramList) {
      if(ramList[index] === '>=6'){
        filterData.push(...camerasData.filter((data) => data.ram >= 6)) ;
      }else if(ramList[index] === '<=4'){
        filterData.push(...camerasData.filter((data) => data.ram === 4)) ;
      }else if(ramList[index] === '<=3'){
        filterData.push(...camerasData.filter((data) => data.ram === 3)) ;
      }else if(ramList[index] === '<=2'){
        filterData.push(...camerasData.filter((data) => data.ram === 2)) ;
      }
    }
    camerasData = [...filterData];
  }
  if(rating) {
    let ratingList = rating.split(",");
    let filterData = [];
    for (let index in ratingList) {
      if(ratingList[index] === '>4'){
        filterData.push(...camerasData.filter((data) => data.rating >= 4)) ;
      }else if(ratingList[index] === '>3'){
        filterData.push(...camerasData.filter((data) => data.rating >= 3)) ;
      }else if(ratingList[index] === '>2'){
        filterData.push(...camerasData.filter((data) => data.rating >= 2)) ;
      }else if(ratingList[index] === '>1'){
        filterData.push(...camerasData.filter((data) => data.rating >= 1)) ;
      }
    }
    camerasData = [...filterData];
  }
  if(price) {
    let priceList = price.split(",");
    let filterData = [];
    console.log("Price", priceList)
    for (let index in priceList) {
      if(priceList[index].trim() === '20000'){
        filterData.push(...camerasData.filter((data) => data.price >=20000)) ;
      }else if(priceList[index] === '10000-20000'){
        filterData.push(...camerasData.filter((data) => data.price >= 10000 && data.price < 20000)) ;
      }else if(priceList[index] === '5000-10000'){
        filterData.push(...camerasData.filter((data) => data.price  >= 5000 && data.price < 10000)) ;
      }else if(priceList[index] === '0-5000'){
        filterData.push(...camerasData.filter((data) => data.price <= 5000)) ;
      }
    }
    camerasData = [...filterData];
  }
  let respArr = pagination(camerasData, page);
  let len = camerasData.length;
  let quo = Math.floor(len / 5);
  let rem = len % 5;
  let extra = rem === 0 ? 0 : 1;
  let numofpages = quo + extra;
  let pageInfo = {
      pageNumber: page,
      numberOfPages: numofpages,
      numOfItems: 5,
      totalItemCount: camerasData.length,
  };
  res.json({
    data: respArr,
    pageInfo: pageInfo,
  });
});
app.get("/products/Cameras/:brand",function(req,res) { 
  let page = +req.query.page;
  let {sort,ram,rating,price} = req.query;
  page = isNaN(page) ? 1 : page;
  let brand = req.params.brand;
  console.log("in get request for brand: ",brand);
  let camerasData= dataList.filter(obj=> obj.brand === brand && obj.category === "Cameras");
  if(sort === 'popularity'){
    camerasData = camerasData.sort((a,b) => (a.popularity < b.popularity) ? 1 : ((b.popularity < a.popularity) ? -1 : 0)); 
  }else if(sort === 'asc'){
    camerasData = camerasData.sort((a,b) => (a.price > b.price) ? 1 : ((b.price > a.price) ? -1 : 0)); 
  }else if(sort === 'desc'){
    camerasData = camerasData.sort((a,b) => (a.price < b.price) ? 1 : ((b.price < a.price) ? -1 : 0)); 
  }
  if(ram) {
    let ramList = ram.split(",");
    let filterData = [];
    for (let index in ramList) {
      if(ramList[index] === '>=6'){
        filterData.push(...camerasData.filter((data) => data.ram >= 6)) ;
      }else if(ramList[index] === '<=4'){
        filterData.push(...camerasData.filter((data) => data.ram === 4)) ;
      }else if(ramList[index] === '<=3'){
        filterData.push(...camerasData.filter((data) => data.ram === 3)) ;
      }else if(ramList[index] === '<=2'){
        filterData.push(...camerasData.filter((data) => data.ram === 2)) ;
      }
    }
    camerasData = [...filterData];
  }
  if(rating) {
    let ratingList = rating.split(",");
    let filterData = [];
    for (let index in ratingList) {
      if(ratingList[index] === '>4'){
        filterData.push(...camerasData.filter((data) => data.rating >= 4)) ;
      }else if(ratingList[index] === '>3'){
        filterData.push(...camerasData.filter((data) => data.rating >= 3)) ;
      }else if(ratingList[index] === '>2'){
        filterData.push(...camerasData.filter((data) => data.rating >= 2)) ;
      }else if(ratingList[index] === '>1'){
        filterData.push(...camerasData.filter((data) => data.rating >= 1)) ;
      }
    }
    camerasData = [...filterData];
  }
  if(price) {
    let priceList = price.split(",");
    let filterData = [];
    console.log("Price", priceList)
    for (let index in priceList) {
      if(priceList[index].trim() === '20000'){
        filterData.push(...camerasData.filter((data) => data.price >=20000)) ;
      }else if(priceList[index] === '10000-20000'){
        filterData.push(...camerasData.filter((data) => data.price >= 10000 && data.price < 20000)) ;
      }else if(priceList[index] === '5000-10000'){
        filterData.push(...camerasData.filter((data) => data.price  >= 5000 && data.price < 10000)) ;
      }else if(priceList[index] === '0-5000'){
        filterData.push(...camerasData.filter((data) => data.price <= 5000)) ;
      }
    }
    camerasData = [...filterData];
  }
  let respArr = pagination(camerasData, page);
  let len = camerasData.length;
  let quo = Math.floor(len / 5);
  let rem = len % 5;
  let extra = rem === 0 ? 0 : 1;
  let numofpages = quo + extra;
  let pageInfo = {
      pageNumber: page,
      numberOfPages: numofpages,
      numOfItems: 5,
      totalItemCount: camerasData.length,
  };
  res.json({
    data: respArr,
    pageInfo: pageInfo,
  });
});