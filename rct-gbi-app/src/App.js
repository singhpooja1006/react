import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import "./App.css";
import NavBar from "./components/navBar";
import LoginForm from "./components/login";
import Logout from "./components/logout";
import Admin from "./components/admin";
import ViewAllCustomers from "./components/allCustomers";
import AddCustomers from "./components/addCustomers";
import AllCheque from "./components/allCheque";
import NetBanking from "./components/netBanking";
import Customer from "./components/customer";
import ViewCheque from "./components/viewCheque";
import ViewNetBanking from "./components/viewNetBanking";
import AddPayee from "./components/addPayee";
import ChequeForm from "./components/chequeForm";
import NetBankingForm from "./components/netBankingForm";
import CustomerDetails from "./components/customerDetails";
import NomineeDetails from "./components/nomineeDetails";

class App extends Component {
  state = {};
  componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    this.setState({ user });
  }
  render() {
    const { user } = this.state;
    return (
      <React.Fragment>
        <NavBar user={user} />
        <Switch>
          <Route path="/customer" component={Customer} />
          <Route path="/viewCheque" component={ViewCheque} />
          <Route path="/viewNet" component={ViewNetBanking} />
          <Route path="/customerDetails" component={CustomerDetails} />
          <Route path="/nomineeDetails" component={NomineeDetails} />
          <Route path="/allCheques" component={AllCheque} />
          <Route path="/allNet" component={NetBanking} />
          <Route path="/addPayee" component={AddPayee} />
          <Route path="/cheque" component={ChequeForm} />
          <Route path="/netBanking" component={NetBankingForm} />
          <Route path="/addCustomer" component={AddCustomers} />
          <Route path="/allCustomers" component={ViewAllCustomers} />
          <Route path="/admin" component={Admin} />
          <Route path="/logout" component={Logout} />
          <Route path="/login" component={LoginForm} />
          <Route path="/" component={LoginForm} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;
