import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import axios from "axios";
import queryString from "query-string";
class ViewCheque extends Component {
  state = {
    customerChequeData: {},
    perPageSize: 5,
  };

  async componentDidMount() {
    //let { page } = queryString.parse(this.props.location.search);
    const user = JSON.parse(localStorage.getItem("user"));
    const { data: customerChequeData } = await axios.get(
      apiBaseURL + "/getChequeByName/" + user.name + this.props.location.search
    );
    this.setState({
      customerChequeData,
    });
  }
  pageNavigate = (value) => {
    let { page } = queryString.parse(this.props.location.search);
    let currPage = page ? +page : 1;
    currPage = currPage + value;
    this.calURL("", currPage);
  };
  calURL = (params, page) => {
    params = this.addToParams(params, "page", page);
    this.props.history.push({
      search: params,
    });
  };

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.location.search !== this.props.location.search) {
      const user = JSON.parse(localStorage.getItem("user"));
      const { data: customerChequeData } = await axios.get(
        apiBaseURL +
          "/getChequeByName/" +
          user.name +
          this.props.location.search
      );
      this.setState({
        customerChequeData,
      });
    }
  }
  render() {
    const { customerChequeData } = { ...this.state };
    return (
      <div className="container">
        <h2>All Cheque Details</h2>
        {customerChequeData.items && customerChequeData.items.length === 0 ? (
          <h3 style={{ color: "red" }}>No Transation to show</h3>
        ) : (
          this.renderViewAllCustomersCheque()
        )}
      </div>
    );
  }
  renderViewAllCustomersCheque = () => {
    const { customerChequeData, perPageSize } = { ...this.state };

    if (customerChequeData.items) {
      let startIndex, lastIndex;
      startIndex =
        customerChequeData.page === 1
          ? 1
          : customerChequeData.page * perPageSize < customerChequeData.totalNum
          ? customerChequeData.page * perPageSize -
            customerChequeData.totalItems +
            1
          : (customerChequeData.page - 1) * perPageSize + 1;
      lastIndex =
        customerChequeData.page * perPageSize < customerChequeData.totalNum
          ? customerChequeData.page * perPageSize
          : customerChequeData.totalNum;
      return (
        <React.Fragment>
          <p>
            {startIndex} - {lastIndex} of {customerChequeData.totalNum}
          </p>
          <table className="table text-center">
            <tr>
              <th>Cheque Number</th>
              <th>Bank Name</th>
              <th>Branch</th>
              <th>Amount</th>
            </tr>

            <tbody>
              {customerChequeData.items.map((customerCheque, index) =>
                index % 2 === 0 ? (
                  <tr
                    key={
                      customerCheque.chequeNumber +
                      "" +
                      customerCheque.bankName +
                      "" +
                      customerCheque.amount
                    }
                    className="bg-light"
                  >
                    <td>{customerCheque.chequeNumber}</td>
                    <td>{customerCheque.bankName}</td>
                    <td>{customerCheque.branch}</td>
                    <td>{customerCheque.amount}</td>
                  </tr>
                ) : (
                  <tr
                    key={
                      customerCheque.chequeNumber +
                      "" +
                      customerCheque.bankName +
                      "" +
                      customerCheque.amount
                    }
                  >
                    <td>{customerCheque.chequeNumber}</td>
                    <td>{customerCheque.bankName}</td>
                    <td>{customerCheque.branch}</td>
                    <td>{customerCheque.amount}</td>
                  </tr>
                )
              )}
            </tbody>
          </table>
          <div className="row">
            <div className="col-11">
              {customerChequeData.page > 1 ? (
                <button
                  className="btn btn-secondary"
                  onClick={() => this.pageNavigate(-1)}
                >
                  Prev
                </button>
              ) : (
                ""
              )}
            </div>
            <div className="col-1 float-right">
              {startIndex < customerChequeData.totalNum ? (
                <button
                  className="btn btn-secondary m-1 "
                  onClick={() => this.pageNavigate(1)}
                >
                  Next
                </button>
              ) : (
                ""
              )}
            </div>
          </div>
        </React.Fragment>
      );
    }
  };
}

export default ViewCheque;
