import React, { Component } from "react";
import "./requiredField.css";
import { apiBaseURL } from "./config/config.json";
import axios from "axios";
import http from "./services/httpService";
class CustomerDetails extends Component {
  state = {
    customerData: {},
    genders: ["Male", "Female"],
    days: this.generateDay(),
    months: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
    years: this.generateYear(),

    errors: {},
  };
  generateYear() {
    let yearList = [];
    let currentYear = new Date().getFullYear();
    for (let year = 1980; year < currentYear; year++) {
      yearList.push(year);
    }
    return yearList;
  }

  generateDay() {
    let dayList = [];

    for (let day = 1; day <= 31; day++) {
      dayList.push(day);
    }
    return dayList;
  }
  removeDuplicatesFromArrayByProperty = (arr, prop) =>
    arr.reduce((accumulator, currentValue) => {
      if (!accumulator.find((obj) => obj[prop] === currentValue[prop])) {
        accumulator.push(currentValue);
      }
      return accumulator;
    }, []);

  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    const { data: state } = await axios.get(apiBaseURL + "/statecity");
    const stateData = this.removeDuplicatesFromArrayByProperty(
      state,
      "stateName"
    );
    const { data: customer } = await axios.get(
      apiBaseURL + "/getCustomer/" + user.name
    );
    let customerData = {};
    if (customer.name !== user.name) {
      customerData = {
        name: "",
        gender: "",
        addressLine1: "",
        addressLine2: "",
        state: "",
        city: "",
        dob: "",
        PAN: "",
        year: "",
        month: "",
        day: "",
      };
    } else {
      customerData = customer;
    }
    this.setState({ customerData, stateData });
  }

  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { customerData } = this.state;
    customerData[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ customerData, errors });
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "gender":
        if (!e.currentTarget.value.trim()) return "Gender is required";
        break;
      case "year":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select Year"
        )
          return "Year is required";

        break;
      case "month":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select Month"
        )
          return "Month is required";
        break;
      case "day":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select Day"
        )
          return "Day is required";
        break;
      case "PAN":
        if (!e.currentTarget.value.trim()) return "PAN No is required";
        break;
      case "state":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select State"
        )
          return "State is required";
        break;
      case "city":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select City"
        )
          return "City is required";
        break;

      default:
        break;
    }
    return "";
  };
  validate = () => {
    let errs = {};
    if (!this.state.customerData.gender.trim())
      errs.gender = "Gender is required";
    if (
      !this.state.customerData.year.trim() ||
      this.state.customerData.year === "Select Year"
    )
      errs.year = "Year is required";
    if (
      !this.state.customerData.month.trim() ||
      this.state.customerData.month === "Select Month"
    )
      errs.month = "Month is required";
    if (
      !this.state.customerData.day.trim() ||
      this.state.customerData.day === "Select Day"
    )
      errs.day = "Day is required";
    if (!this.state.customerData.PAN.trim()) errs.PAN = "PAN No is required";
    if (
      !this.state.customerData.state.trim() ||
      this.state.customerData.state === "Select State"
    )
      errs.state = "State is required";
    if (
      !this.state.customerData.city.trim() ||
      this.state.customerData.city === "Select City"
    )
      errs.city = "City is required";
    return errs;
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const { customerData } = this.state;
      const user = JSON.parse(localStorage.getItem("user"));
      const apiEndpoint = apiBaseURL + "/customerDetails";
      await http.post(apiEndpoint, {
        name: user.name,
        addressLine1: customerData.addressLine1,
        addressLine2: customerData.addressLine2,
        gender: customerData.gender,
        state: customerData.state,
        city: customerData.city,
        PAN: customerData.PAN,
        dob:
          customerData.day + "-" + customerData.month + "-" + customerData.year,
      });
      alert(user.name + " details Added Successfully");
      window.location = "/customer";
    } catch (ex) {}
  };
  render() {
    const {
      genders,
      days,
      months,
      years,
      stateData,
      customerData,
      errors,
    } = this.state;
    const dob = customerData.dob;
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          <h5>Customer Details</h5>
          <div className="form-group required">
            <div className="row mb-3">
              <label htmlFor="gender" className="control-label col-2">
                Gender
              </label>

              {genders.map((gender) => (
                <div className="form-check-inline col-2">
                  <input
                    value={gender}
                    onChange={this.handleChange}
                    id="gender"
                    name="gender"
                    type="radio"
                    checked={customerData.gender === gender}
                    className="form-check-input"
                  />

                  <label className="form-check-label" htmlFor={gender}>
                    {gender}
                  </label>
                </div>
              ))}
            </div>
            {errors.gender ? (
              <div className="text-danger">{errors.gender}</div>
            ) : (
              ""
            )}
            <div className="col text-secondary text-left border-bottom"></div>
            <div className="form-group required">
              <label htmlFor="dob" className="control-label mt-2">
                Date of Birth
              </label>
              <div className="row ml-1">
                <select
                  value={
                    dob
                      ? dob.substring(dob.lastIndexOf("-") + 1, dob.length)
                      : customerData.year
                      ? customerData.year
                      : "Select Year"
                  }
                  name="year"
                  onChange={this.handleChange}
                  className="browser-default custom-select col-3 m-2"
                >
                  <option>Select Year</option>
                  {years.map((year) => (
                    <option key={year}>{year}</option>
                  ))}
                </select>

                <select
                  value={
                    dob
                      ? dob.substring(
                          dob.indexOf("-") + 1,
                          dob.lastIndexOf("-")
                        )
                      : customerData.month
                      ? customerData.month
                      : "Select Month"
                  }
                  onChange={this.handleChange}
                  name="month"
                  className="browser-default custom-select col-3 m-2"
                >
                  <option>Select Month</option>
                  {months.map((month) => (
                    <option key={month}>{month}</option>
                  ))}
                </select>
                <select
                  value={
                    dob
                      ? dob.substring(0, dob.indexOf("-"))
                      : customerData.day
                      ? customerData.day
                      : "Select Day"
                  }
                  name="day"
                  onChange={this.handleChange}
                  className="browser-default custom-select col-3 m-2"
                >
                  <option>Select Day</option>
                  {days.map((day) => (
                    <option key={day}>{day}</option>
                  ))}
                </select>
              </div>
              <div className="row">
                <div className="text-danger col-3">
                  {errors.year ? errors.year : ""}
                </div>
                <div className="text-danger col-3">
                  {errors.month ? errors.month : ""}
                </div>
                <div className="text-danger col-3">
                  {errors.day ? errors.day : ""}
                </div>
              </div>
            </div>
            <div className="form-group required">
              <label htmlFor="PAN" className="control-label">
                PAN
              </label>
              <input
                value={customerData.PAN}
                onChange={this.handleChange}
                type="text"
                id="PAN"
                name="PAN"
                className="form-control"
              />
              {errors.PAN ? (
                <div className="text-danger">{errors.PAN}</div>
              ) : (
                ""
              )}
            </div>
            <div className="form-group">
              <label htmlFor="address">Address</label>
              <div className="row">
                <div className="col">
                  <input
                    id="addressLine1"
                    name="addressLine1"
                    type="text"
                    className="form-control"
                    placeholder="Line 1"
                    value={customerData.addressLine1}
                    onChange={this.handleChange}
                  />
                </div>
                <div className="col">
                  <input
                    id="addressLine2"
                    name="addressLine2"
                    type="text"
                    className="form-control"
                    placeholder="Line 2"
                    value={customerData.addressLine2}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
            </div>
            <div className="form-group required">
              <div className="row">
                <div className="col">
                  <label htmlFor="state" className="control-label">
                    State
                  </label>
                  <select
                    value={customerData.state}
                    onChange={this.handleChange}
                    name="state"
                    className="custom-select col"
                  >
                    <option>Select State</option>
                    {stateData
                      ? stateData.map((state) => (
                          <option key={state.stateName}>
                            {state.stateName}
                          </option>
                        ))
                      : ""}
                  </select>
                </div>
                <div className="col">
                  <label htmlFor="city" className="control-label">
                    City
                  </label>
                  <select
                    value={
                      customerData.city
                        ? customerData.city.trim()
                        : "Select City"
                    }
                    name="city"
                    onChange={this.handleChange}
                    className="browser-default custom-select col"
                  >
                    <option>Select City</option>
                    {stateData ? this.renderCityDropDown() : ""}
                  </select>
                </div>
              </div>
              <div className="row">
                <div className="text-danger col">
                  {errors.state ? errors.state : ""}
                </div>
                <div className="text-danger col">
                  {errors.city ? errors.city : ""}
                </div>
              </div>
            </div>
          </div>

          {!customerData.name ? (
            <button className="btn btn-primary">Add Details</button>
          ) : (
            ""
          )}
        </form>
      </div>
    );
  }

  renderCityDropDown() {
    const { stateData, customerData } = this.state;
    let citys = stateData.find((state) =>
      state.stateName.trim() === customerData.state
        ? customerData.state.trim()
        : ""
    );
    if (citys) {
      return (
        <React.Fragment>
          {citys.cityArr.map((city) => (
            <option key={city.trim()}>{city.trim()}</option>
          ))}
        </React.Fragment>
      );
    }
  }
}

export default CustomerDetails;
