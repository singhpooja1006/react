import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import axios from "axios";
import http from "./services/httpService";
import "./requiredField.css";
class AddPayee extends Component {
  state = {
    data: { payeeName: "", accNumber: "", bankName: "", IFSC: "" },
    payeeBankList: [
      { id: "GBI", value: "Same Bank" },
      { id: "Other", value: "Other Bank" },
    ],
    bankNameList: [],
    selectedBank: "GBI",
    errors: {},
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const { data } = this.state;
      const user = JSON.parse(localStorage.getItem("user"));
      const apiEndpoint = apiBaseURL + "/addPayee";
      await http.post(apiEndpoint, {
        payeeName: data.payeeName,
        accNumber: data.accNumber,
        bankName: data.bankName,
        IFSC: data.IFSC,
        name: user.name,
      });
      alert("Payee added to your list ::" + data.payeeName);
      window.location = "/customer";
    } catch (ex) {}
  };
  async componentDidMount() {
    const { data: bankNames } = await axios.get(apiBaseURL + "/getBanks");
    const bankNameList = bankNames.filter((bankName) => bankName !== "GBI");
    this.setState({ bankNameList });
  }
  validate = () => {
    let errs = {};
    if (!this.state.data.payeeName.trim())
      errs.payeeName = "Payee Name is required";

    if (!this.state.data.accNumber.trim())
      errs.accNumber = "Account Number is required";
    if (this.state.selectedBank !== "GBI" && !this.state.data.IFSC.trim())
      errs.IFSC = "IFSC Code is required";
    if (this.state.selectedBank !== "GBI" && !this.state.data.bankName.trim())
      errs.bankName = "Bank Name is required";
    return errs;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "payeeName":
        if (!e.currentTarget.value.trim()) return "Payee Name is required";
        break;
      case "accNumber":
        if (!e.currentTarget.value.trim()) return "Account Number is required";

        break;
      case "IFSC":
        if (this.state.selectedBank !== "GBI" && !e.currentTarget.value.trim())
          return "IFSC Code is required";
        break;
      case "bankName":
        if (
          this.state.selectedBank !== "GBI" &&
          e.currentTarget.value === "Select Bank"
        )
          return "Bank Name is required";

      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    let selectedBank = this.state.selectedBank;
    const { data } = this.state;

    if (e.currentTarget.type === "radio") {
      selectedBank = e.currentTarget.value;
    } else if (e.currentTarget.name === "bankName") {
      data[e.currentTarget.name] = e.currentTarget.value;
    } else {
      data[e.currentTarget.name] = e.currentTarget.value;
    }
    this.setState({ data, errors: errors, selectedBank });
  };
  render() {
    const {
      data,
      payeeBankList,
      bankNameList,
      selectedBank,
      errors,
    } = this.state;
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          <h5>Add Payee</h5>
          <div className="form-group required">
            <label htmlFor="payeeName" className="control-label">
              Payee Name
            </label>
            <input
              value={data.payeeName}
              onChange={this.handleChange}
              type="text"
              id="payeeName"
              name="payeeName"
              placeholder="Enter payee name"
              className="form-control"
            />
            {errors.payeeName ? (
              <div className="text-danger">{errors.payeeName}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group required">
            <label htmlFor="accNumber" className="control-label">
              Account Number
            </label>
            <input
              value={data.accNumber}
              onChange={this.handleChange}
              type="text"
              id="accNumber"
              name="accNumber"
              placeholder="Enter payee account number"
              className="form-control"
            />
            {errors.accNumber ? (
              <div className="text-danger">{errors.accNumber}</div>
            ) : (
              ""
            )}
          </div>
          {payeeBankList.map((payeeBank) => (
            <div className=" col-8 mb-2 mt-2 ml-2 form-check">
              <input
                value={payeeBank.id}
                onChange={this.handleChange}
                id="bank"
                name="bank"
                type="Radio"
                checked={selectedBank === payeeBank.id}
                className="form-check-input"
              />
              <label className="form-check-label" htmlFor={payeeBank.id}>
                {payeeBank.value}
              </label>
            </div>
          ))}
          {selectedBank !== "GBI" ? (
            <React.Fragment>
              <div className="form-group">
                <select
                  value={bankNameList.bank}
                  onChange={this.handleChange}
                  id="bankName"
                  name="bankName"
                  className="browser-default custom-select form-control"
                >
                  <option>Select Bank</option>
                  {bankNameList.map((bank) => (
                    <option key={bank}>{bank}</option>
                  ))}
                </select>
                {errors.bankName ? (
                  <div className="text-danger">{errors.bankName}</div>
                ) : (
                  ""
                )}
              </div>
              <div className="form-group required ">
                <label htmlFor="IFSC" className="control-label">
                  IFSC Code
                </label>
                <input
                  value={data.IFSC}
                  onChange={this.handleChange}
                  type="text"
                  id="IFSC"
                  name="IFSC"
                  placeholder="Enter IFSC Code"
                  className="form-control"
                />
                {errors.IFSC ? (
                  <div className="text-danger">{errors.IFSC}</div>
                ) : (
                  ""
                )}
              </div>
            </React.Fragment>
          ) : (
            ""
          )}

          <button className="btn btn-primary">Add Payee</button>
        </form>
      </div>
    );
  }
}

export default AddPayee;
