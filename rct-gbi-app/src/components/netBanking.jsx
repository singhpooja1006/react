import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import axios from "axios";
import queryString from "query-string";
class NetBanking extends Component {
  state = {
    bankNameList: [],
    allNetBankingData: {},
    amountList: ["<10000", ">=10000"],
    selectedBank: "",
    selectedAmount: "",
    perPageSize: 5,
  };

  async componentDidMount() {
    const { data: bankNameList } = await axios.get(apiBaseURL + "/getBanks");
    let { page } = queryString.parse(this.props.location.search);
    const { data: allNetBankingData } = await axios.get(
      apiBaseURL + "/getAllNetBankings?page=" + page
    );
    this.setState({ bankNameList, allNetBankingData });
  }
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let { selectedBank, selectedAmount } = this.state;
    if (input.name === "bank") {
      selectedBank = input.value;
    }
    if (input.name === "amount") {
      selectedAmount = input.value;
    }
    this.calURL("", 1, selectedBank, selectedAmount);
    this.setState({ selectedBank, selectedAmount });
  };
  calURL = (params, page, selectedBank, selectedAmount) => {
    params = this.addToParams(params, "page", page);
    params = this.addToParams(params, "bank", selectedBank);
    params = this.addToParams(params, "amount", selectedAmount);
    this.props.history.push({
      search: params,
    });
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  pageNavigate = (value) => {
    let { page, bank, amount } = queryString.parse(this.props.location.search);
    let currPage = page ? +page : 1;
    currPage = currPage + value;
    this.calURL("", currPage, bank, amount);
  };
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.location.search !== this.props.location.search) {
      const { data: allNetBankingData } = await axios.get(
        apiBaseURL + "/getAllNetBankings" + this.props.location.search
      );

      this.setState({ allNetBankingData: allNetBankingData });
    }
  }
  render() {
    const {
      bankNameList,
      amountList,
      selectedBank,
      selectedAmount,
    } = this.state;

    return (
      <div className="container">
        <h3>All Net Banking Transactions</h3>
        <div className="row">
          <div className="col-2">
            <div className="mt-3 ml-3 row border bg-light">
              <div className="col-8 mb-2 mt-2 h6">Bank</div>
            </div>
            {bankNameList.map((bank) => (
              <div className="row ml-3 border">
                <div className=" col-8 mb-2 mt-2 ml-2 form-check">
                  <input
                    value={bank}
                    onChange={this.handleChange}
                    id="bank"
                    name="bank"
                    type="Radio"
                    checked={selectedBank === bank}
                    className="form-check-input"
                  />
                  <label className="form-check-label" htmlFor={bank}>
                    {bank}
                  </label>
                </div>
              </div>
            ))}
            <br />
            <div className="row ml-3 border bg-light">
              <div class="col-8 mb-2 mt-2 h6">Amount</div>
            </div>
            {amountList.map((amount) => (
              <div className="row ml-3 border">
                <div className=" col-8 mb-2 mt-2 ml-2 form-check">
                  <input
                    value={amount}
                    onChange={this.handleChange}
                    id="amount"
                    name="amount"
                    type="Radio"
                    checked={selectedAmount === amount}
                    className="form-check-input"
                  />
                  <label className="form-check-label" htmlFor={amount}>
                    {amount}
                  </label>
                </div>
              </div>
            ))}
          </div>

          {this.renderTableView()}
        </div>
      </div>
    );
  }
  renderTableView = () => {
    const { allNetBankingData, perPageSize } = this.state;
    let startIndex, lastIndex;
    startIndex =
      allNetBankingData.page === 1
        ? 1
        : allNetBankingData.page * perPageSize < allNetBankingData.totalNum
        ? allNetBankingData.page * perPageSize -
          allNetBankingData.totalItems +
          1
        : (allNetBankingData.page - 1) * perPageSize + 1;
    lastIndex =
      allNetBankingData.page * perPageSize < allNetBankingData.totalNum
        ? allNetBankingData.page * perPageSize
        : allNetBankingData.totalNum;
    if (allNetBankingData.items) {
      return (
        <React.Fragment>
          <div className="col-10">
            <div class="col-8 mb-2 mt-2 h6">
              {startIndex} - {lastIndex} of {allNetBankingData.totalNum}
            </div>
            <table className="table ">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Payee Name</th>
                  <th>Amount</th>
                  <th>Bank Name</th>
                  <th>Comment</th>
                </tr>
              </thead>

              <tbody>
                {allNetBankingData.items.map((netBank, index) =>
                  index % 2 === 0 ? (
                    <tr
                      key={
                        netBank.payeeName +
                        "" +
                        netBank.name +
                        "" +
                        netBank.bankName +
                        "" +
                        netBank.amount
                      }
                      className="bg-light"
                    >
                      <td>{netBank.name}</td>
                      <td>{netBank.payeeName}</td>
                      <td>{netBank.amount}</td>
                      <td>{netBank.bankName}</td>
                      <td>{netBank.comment}</td>
                    </tr>
                  ) : (
                    <tr
                      key={
                        netBank.payeeName +
                        "" +
                        netBank.name +
                        "" +
                        netBank.bankName +
                        "" +
                        netBank.amount
                      }
                    >
                      <td>{netBank.name}</td>
                      <td>{netBank.payeeName}</td>
                      <td>{netBank.amount}</td>
                      <td>{netBank.bankName}</td>
                      <td>{netBank.comment}</td>
                    </tr>
                  )
                )}
              </tbody>
            </table>
            <div className="row">
              <div className="col-10">
                {allNetBankingData.page > 1 ? (
                  <button
                    className="btn btn-secondary m-1"
                    onClick={() => this.pageNavigate(-1)}
                  >
                    Previous
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div className="col-2 text-right">
                {lastIndex < allNetBankingData.totalNum ? (
                  <button
                    className="btn btn-secondary m-1"
                    onClick={() => this.pageNavigate(1)}
                  >
                    Next
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </React.Fragment>
      );
    }
  };
}

export default NetBanking;
