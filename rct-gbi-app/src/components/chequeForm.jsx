import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import axios from "axios";
import http from "./services/httpService";
import "./requiredField.css";
class ChequeForm extends Component {
  state = {
    data: { chequeNumber: "", bankName: "", branch: "", amount: "" },
    bankNameList: [],
    errors: {},
  };
  async componentDidMount() {
    const { data: bankNameList } = await axios.get(apiBaseURL + "/getBanks");
    this.setState({ bankNameList });
  }
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const { data } = this.state;
      const user = JSON.parse(localStorage.getItem("user"));
      const apiEndpoint = apiBaseURL + "/postCheque";
      await http.post(apiEndpoint, {
        chequeNumber: data.chequeNumber,
        bankName: data.bankName,
        branch: data.branch,
        amount: data.amount,
        name: user.name,
      });
      alert("Details added successfully");
      window.location = "/customer";
    } catch (ex) {}
  };
  validate = () => {
    let errs = {};
    if (!this.state.data.chequeNumber.trim())
      errs.chequeNumber = "Cheque Number is required";
    else if (this.state.data.chequeNumber.trim().length < 11)
      errs.chequeNumber = "Enter your 11 digit Cheque Number.";
    if (!this.state.data.branch.trim()) errs.branch = "Branch is required";
    else if (this.state.data.branch.trim().length < 4)
      errs.branch = "Enter 4 digit code of Branch.";
    if (!this.state.data.amount.trim()) errs.amount = "Amount is required";
    else if (this.state.data.amount.trim() <= 0)
      errs.amount = "Enter Amount greter than zero.";
    return errs;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "chequeNumber":
        if (!e.currentTarget.value.trim()) return "Cheque Number is required";
        else if (e.currentTarget.value.trim().length < 11)
          return "Enter your 11 digit Cheque Number.";
        break;
      case "branch":
        if (!e.currentTarget.value.trim()) return "Branch is required";
        else if (e.currentTarget.value.trim().length < 4)
          return "Enter 4 digit code of Branch.";
        break;
      case "amount":
        if (!e.currentTarget.value.trim()) return "Amount is required";
        else if (e.currentTarget.value.trim() <= 0)
          return "Enter Amount greter than zero.";
        break;

      default:
        break;
    }
    return "";
  };

  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const addChequeData = { ...this.state.data };
    addChequeData[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data: addChequeData, errors: errors });
  };

  render() {
    const { data, bankNameList, errors } = this.state;
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          <h5>Deposit Cheque</h5>
          <div className="form-group required">
            <label htmlFor="chequeNumber" className="control-label">
              Cheque Number
            </label>
            <input
              value={data.chequeNumber}
              onChange={this.handleChange}
              type="number"
              id="chequeNumber"
              name="chequeNumber"
              placeholder="Enter Cheque Number"
              className="form-control"
            />
            {errors.chequeNumber ? (
              <div className="text-danger">{errors.chequeNumber}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group required">
            <label htmlFor="bankName" className="control-label">
              Bank Name
            </label>
            <select
              value={bankNameList.bank}
              onChange={this.handleChange}
              id="bankName"
              name="bankName"
              className="browser-default custom-select form-control"
            >
              <option>Select Bank</option>
              {bankNameList.map((bank) => (
                <option key={bank}>{bank}</option>
              ))}
            </select>
          </div>
          <div className="form-group required">
            <label htmlFor="branch" className="control-label">
              Branch
            </label>
            <input
              value={data.branch}
              onChange={this.handleChange}
              type="text"
              id="branch"
              name="branch"
              placeholder="Enter Branch Code"
              className="form-control"
            />
            {errors.branch ? (
              <div className="text-danger">{errors.branch}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group required">
            <label htmlFor="amount" className="control-label">
              Amount
            </label>
            <input
              value={data.amount}
              onChange={this.handleChange}
              type="number"
              id="amount"
              name="amount"
              placeholder="Enter Amount"
              className="form-control"
            />
            {errors.amount ? (
              <div className="text-danger">{errors.amount}</div>
            ) : (
              ""
            )}
          </div>
          <button className="btn btn-primary">Add Cheque</button>
        </form>
      </div>
    );
  }
}

export default ChequeForm;
