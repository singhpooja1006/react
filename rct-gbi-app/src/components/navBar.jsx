import React, { Component } from "react";

import { Link } from "react-router-dom";
import "./dropdown.css";
class NavBar extends Component {
  state = {};
  render() {
    return (
      <nav className="navbar navbar-expand-sm navbar-light bg-warning">
        <Link className="navbar-brand" to="/">
          Home
        </Link>
        {!this.props.user ? (
          <Link to="/login" className="nav-link">
            Login
          </Link>
        ) : (
          this.renderMenuBar()
        )}

        <ul className="navbar-nav ml-auto">
          {this.props.user ? (
            <React.Fragment>
              <li className="nav-item">
                <Link to="#" className="nav-link">
                  Welcome {this.props.user.name}
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/logout" className="nav-link">
                  Logout
                </Link>
              </li>
            </React.Fragment>
          ) : (
            ""
          )}
        </ul>
      </nav>
    );
  }
  renderMenuBar() {
    const { user } = this.props;
    if (user) {
      if (user.role === "manager") {
        return (
          <React.Fragment>
            <div>
              <form className="form-inline my-2 my-lg-0">
                <a className="navbar-brand">
                  <div className="dropdown">
                    <div className="dropbtn">
                      Customers&nbsp;
                      <i
                        className="fa fa-chevron-down"
                        id="onhover"
                        style={{ fontSize: "10px" }}
                      ></i>
                    </div>
                    <div className="dropdown-content">
                      <div>
                        <Link to="/addCustomer">Add Customers</Link>
                      </div>
                      <div>
                        <Link to="/allCustomers?page=1">
                          View All Customers
                        </Link>
                      </div>
                    </div>
                  </div>
                </a>
              </form>
            </div>
            <div>
              <form className="form-inline my-2 my-lg-0">
                <a className="navbar-brand">
                  <div className="dropdown">
                    <div className="dropbtn">
                      Transactions&nbsp;
                      <i
                        className="fa fa-chevron-down"
                        id="onhover"
                        style={{ fontSize: "10px" }}
                      ></i>
                    </div>
                    <div className="dropdown-content">
                      <div>
                        <Link to="/allCheques?page=1">Cheques</Link>
                      </div>
                      <div>
                        <Link to="/allNet?page=1">Net Banking</Link>
                      </div>
                    </div>
                  </div>
                </a>
              </form>
            </div>
          </React.Fragment>
        );
      } else if (user.role === "customer") {
        return (
          <React.Fragment>
            <div>
              <form className="form-inline my-2 my-lg-0">
                <a className="navbar-brand">
                  <div className="dropdown">
                    <div className="dropbtn">
                      View&nbsp;
                      <i
                        className="fa fa-chevron-down"
                        id="onhover"
                        style={{ fontSize: "10px" }}
                      ></i>
                    </div>
                    <div className="dropdown-content">
                      <div>
                        <Link to="/viewCheque?page=1">Cheque</Link>
                      </div>
                      <div>
                        <Link to="/viewNet?page=1">Net Banking</Link>
                      </div>
                    </div>
                  </div>
                </a>
              </form>
            </div>
            <div>
              <form className="form-inline my-2 my-lg-0">
                <a className="navbar-brand">
                  <div className="dropdown">
                    <div className="dropbtn">
                      Details&nbsp;
                      <i
                        className="fa fa-chevron-down"
                        id="onhover"
                        style={{ fontSize: "10px" }}
                      ></i>
                    </div>
                    <div className="dropdown-content">
                      <div>
                        <Link to="/customerDetails">Customer</Link>
                      </div>
                      <div>
                        <Link to="/nomineeDetails">Nominee</Link>
                      </div>
                    </div>
                  </div>
                </a>
              </form>
            </div>
            <div>
              <form className="form-inline my-2 my-lg-0">
                <a className="navbar-brand">
                  <div className="dropdown">
                    <div className="dropbtn">
                      Transaction&nbsp;
                      <i
                        className="fa fa-chevron-down"
                        id="onhover"
                        style={{ fontSize: "10px" }}
                      ></i>
                    </div>
                    <div className="dropdown-content">
                      <div>
                        <Link to="/addPayee">Add Payee</Link>
                      </div>
                      <div>
                        <Link to="/cheque">Cheque</Link>
                      </div>
                      <div>
                        <Link to="/netBanking">Net Banking</Link>
                      </div>
                    </div>
                  </div>
                </a>
              </form>
            </div>
          </React.Fragment>
        );
      }
    }
  }
}

export default NavBar;
