import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import http from "./services/httpService";
class AddCustomers extends Component {
  state = {
    data: { name: "", password: "", cnfpwd: "" },
    errors: {},
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const apiEndpoint = apiBaseURL + "/register";
      await http.post(apiEndpoint, {
        name: this.state.data.name,
        password: this.state.data.password,
        cnfpwd: this.state.data.cnfpwd,
        role: "customer",
      });
      alert("Customer added successfully");
      window.location = "/admin";
    } catch (ex) {}
  };
  validate = () => {
    let errs = {};
    if (!this.state.data.name.trim()) errs.name = "Name is required";

    if (!this.state.data.password.trim())
      errs.password = "Password is required";
    else if (this.state.data.password.trim().length < 7)
      errs.password =
        "Password can not be blank.Minimum length should be 7 characters.";
    if (!this.state.data.cnfpwd.trim())
      errs.cnfpwd = "confirm password is required";
    else if (this.state.data.password !== this.state.data.cnfpwd)
      errs.reEnter = "confirm Password do not Match";
    return errs;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "name":
        if (!e.currentTarget.value.trim()) return "Name is required";
        break;
      case "password":
        if (!e.currentTarget.value.trim()) return "Password is required";
        else if (e.currentTarget.value.trim().length < 7)
          return "Password can not be blank.Minimum length should be 7 characters.";
        break;
      case "cnfpwd":
        if (!e.currentTarget.value.trim())
          return "confirm password is required";
        else if (e.currentTarget.value !== this.state.data.password)
          return "confirm Password do not Match";
        break;

      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const addCustomerData = { ...this.state.data };
    addCustomerData[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data: addCustomerData, errors: errors });
  };
  render() {
    const { data, errors } = this.state;
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          <h5>New Customer</h5>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              value={data.name}
              onChange={this.handleChange}
              type="text"
              id="name"
              name="name"
              placeholder="Enter customer name"
              className="form-control"
            />
            {errors.name ? (
              <div className="text-danger">{errors.name}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              value={data.password}
              onChange={this.handleChange}
              type="password"
              id="password"
              name="password"
              className="form-control"
            />
            {errors.password ? (
              <div className="text-danger">{errors.password}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="cnfpwd">Confirm Password</label>
            <input
              value={data.cnfpwd}
              onChange={this.handleChange}
              type="password"
              id="cnfpwd"
              name="cnfpwd"
              className="form-control"
            />
            {errors.cnfpwd ? (
              <div className="text-danger">{errors.cnfpwd}</div>
            ) : (
              ""
            )}
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    );
  }
}

export default AddCustomers;
