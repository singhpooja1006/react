import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import http from "./services/httpService";
class LoginForm extends Component {
  state = {
    data: { name: "", password: "" },
    errors: {},
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const apiEndpoint = apiBaseURL + "/login";
      const { data: user } = await http.post(apiEndpoint, {
        name: this.state.data.name,
        password: this.state.data.password,
      });
      localStorage.setItem("user", JSON.stringify(user));
      if (user.role === "manager") {
        window.location = "/admin";
      } else if (user.role === "customer") {
        window.location = "/customer";
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        const errors = { ...this.state.errors };
        if (ex.response.data.includes("Login Failed")) {
          errors.msg = "Login Failed. Check the username and password.";
        } else {
          errors.msg = ex.response.data;
        }

        this.setState({ errors });
      }
    }
  };
  validate = () => {
    let errs = {};

    if (!this.state.data.name.trim()) errs.email = "Name is required";

    if (!this.state.data.password.trim())
      errs.password = "Password is required";
    else if (this.state.data.password.trim().length < 7)
      errs.password = "Password  must be of 7 characters";
    return errs;
  };
  isFormvalid = () => {
    let errs = this.validate();

    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "name":
        if (!e.currentTarget.value.trim()) return "Name is required";

        break;
      case "password":
        if (!e.currentTarget.value.trim()) return "Password is required";
        else if (e.currentTarget.value.trim().length < 7)
          return "Password  must be of 7 characters";
        break;
      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const loginData = { ...this.state.data };
    loginData[e.currentTarget.name] = e.currentTarget.value;

    this.setState({ data: loginData, errors: errors });
  };
  render() {
    const { data, errors } = this.state;
    return (
      <React.Fragment>
        <h2 className="text-center">Welcome to GBI Bank</h2>
        <h6 className="text-center">User Name</h6>
        <div className="text-center">
          <form onSubmit={this.handleSubmit}>
            <input
              value={data.name}
              type="text"
              onChange={this.handleChange}
              className="col-4"
              id="name"
              placeholder="Enter user name"
              name="name"
            />
            <div>We'll never share your user name with anyone else.</div>
            {errors.name ? (
              <div className="text-danger text-center">{errors.name}</div>
            ) : (
              ""
            )}
            <h6 className="text-center mt-3">Password</h6>
            <input
              value={data.password}
              type="password"
              onChange={this.handleChange}
              className="col-4"
              id="password"
              placeholder="Password"
              name="password"
            />
            {this.state.errors.password ? (
              <div className="text-danger text-center">
                {this.state.errors.password}
              </div>
            ) : (
              ""
            )}
            <div className="mt-3">
              <button className="btn btn-primary" disabled={this.isFormvalid()}>
                Login
              </button>
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default LoginForm;
