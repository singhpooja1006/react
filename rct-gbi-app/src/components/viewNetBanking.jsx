import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import axios from "axios";
import queryString from "query-string";
class ViewNetBanking extends Component {
  state = {
    netBankingData: {},
    perPageSize: 5,
  };
  async componentDidMount() {
    //let { page } = queryString.parse(this.props.location.search);
    const user = JSON.parse(localStorage.getItem("user"));
    const { data: netBankingData } = await axios.get(
      apiBaseURL +
        "/getNetBankingByName/" +
        user.name +
        this.props.location.search
    );
    this.setState({
      netBankingData,
    });
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.location.search !== this.props.location.search) {
      const user = JSON.parse(localStorage.getItem("user"));
      const { data: netBankingData } = await axios.get(
        apiBaseURL +
          "/getNetBankingByName/" +
          user.name +
          this.props.location.search
      );
      this.setState({
        netBankingData,
      });
    }
  }
  pageNavigate = (value) => {
    let { page } = queryString.parse(this.props.location.search);
    let currPage = page ? +page : 1;
    currPage = currPage + value;
    this.calURL("", currPage);
    //this.setState({ previousLength: this.state.customersData.totalItems });
  };
  calURL = (params, page) => {
    params = this.addToParams(params, "page", page);
    this.props.history.push({
      search: params,
    });
  };

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  render() {
    return <div className="container">{this.renderViewNetBanking()}</div>;
  }
  renderViewNetBanking = () => {
    const { netBankingData, perPageSize } = { ...this.state };
    if (netBankingData.items) {
      let startIndex, lastIndex;
      startIndex =
        netBankingData.page === 1
          ? 1
          : netBankingData.page * perPageSize < netBankingData.totalNum
          ? netBankingData.page * perPageSize - netBankingData.totalItems + 1
          : (netBankingData.page - 1) * perPageSize + 1;
      lastIndex =
        netBankingData.page * perPageSize < netBankingData.totalNum
          ? netBankingData.page * perPageSize
          : netBankingData.totalNum;
      return (
        <React.Fragment>
          <h2>All Net Banking Details</h2>
          <p>
            {startIndex} - {lastIndex} of {netBankingData.totalNum}
          </p>
          <table className="table text-center">
            <tr>
              <th>Payee Name</th>
              <th>Amount</th>
              <th>Bank</th>
              <th>Comment</th>
            </tr>

            <tbody>
              {netBankingData.items.map((netBank, index) =>
                index % 2 === 0 ? (
                  <tr
                    key={
                      netBank.payeeName +
                      "" +
                      netBank.amount +
                      "" +
                      netBank.bankName
                    }
                    className="bg-light"
                  >
                    <td>{netBank.payeeName}</td>
                    <td>{netBank.amount}</td>
                    <td>{netBank.bankName}</td>
                    <td>{netBank.comment}</td>
                  </tr>
                ) : (
                  <tr
                    key={
                      netBank.payeeName +
                      "" +
                      netBank.amount +
                      "" +
                      netBank.bankName
                    }
                  >
                    <td>{netBank.payeeName}</td>
                    <td>{netBank.amount}</td>
                    <td>{netBank.bankName}</td>
                    <td>{netBank.comment}</td>
                  </tr>
                )
              )}
            </tbody>
          </table>
          <div className="row">
            <div className="col-11">
              {netBankingData.page > 1 ? (
                <button
                  className="btn btn-secondary"
                  onClick={() => this.pageNavigate(-1)}
                >
                  Prev
                </button>
              ) : (
                ""
              )}
            </div>
            <div className="col-1 float-right">
              {lastIndex < netBankingData.totalNum ? (
                <button
                  className="btn btn-secondary m-1 "
                  onClick={() => this.pageNavigate(1)}
                >
                  Next
                </button>
              ) : (
                ""
              )}
            </div>
          </div>
        </React.Fragment>
      );
    }
  };
}

export default ViewNetBanking;
