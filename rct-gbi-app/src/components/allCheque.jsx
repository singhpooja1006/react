import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import axios from "axios";
import queryString from "query-string";

class AllCheque extends Component {
  state = {
    bankNameList: [],
    allChequesData: {},
    amountList: ["<10000", ">=10000"],
    selectedBank: "",
    selectedAmount: "",
    perPageSize: 5,
  };

  async componentDidMount() {
    const { data: bankNameList } = await axios.get(apiBaseURL + "/getBanks");
    let { page } = queryString.parse(this.props.location.search);
    const { data: allChequesData } = await axios.get(
      apiBaseURL + "/getAllCheques?page=" + page
    );
    this.setState({ bankNameList, allChequesData });
  }
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let { selectedBank, selectedAmount } = this.state;
    if (input.name === "bank") {
      selectedBank = input.value;
    }
    if (input.name === "amount") {
      selectedAmount = input.value;
    }
    this.calURL("", 1, selectedBank, selectedAmount);
    this.setState({ selectedBank, selectedAmount });
  };
  calURL = (params, page, selectedBank, selectedAmount) => {
    params = this.addToParams(params, "page", page);
    params = this.addToParams(params, "bank", selectedBank);
    params = this.addToParams(params, "amount", selectedAmount);
    this.props.history.push({
      search: params,
    });
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  pageNavigate = (value) => {
    let { page, bank, amount } = queryString.parse(this.props.location.search);
    let currPage = page ? +page : 1;
    currPage = currPage + value;
    this.calURL("", currPage, bank, amount);
  };
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.location.search !== this.props.location.search) {
      const { data: allChequesData } = await axios.get(
        apiBaseURL + "/getAllCheques" + this.props.location.search
      );

      this.setState({ allChequesData: allChequesData });
    }
  }
  render() {
    const {
      bankNameList,
      amountList,
      selectedBank,
      selectedAmount,
      perPageSize,
    } = this.state;

    return (
      <div className="container">
        <h3>All Cheque Transactions</h3>
        <div className="row">
          <div className="col-2">
            <div className="mt-3 ml-3 row border bg-light">
              <div className="col-8 mb-2 mt-2 h6">Bank</div>
            </div>
            {bankNameList.map((bank) => (
              <div className="row ml-3 border">
                <div className=" col-8 mb-2 mt-2 ml-2 form-check">
                  <input
                    value={bank}
                    onChange={this.handleChange}
                    id="bank"
                    name="bank"
                    type="Radio"
                    checked={selectedBank === bank}
                    className="form-check-input"
                  />
                  <label className="form-check-label" htmlFor={bank}>
                    {bank}
                  </label>
                </div>
              </div>
            ))}
            <br />
            <div className="row ml-3 border bg-light">
              <div class="col-8 mb-2 mt-2 h6">Amount</div>
            </div>
            {amountList.map((amount) => (
              <div className="row ml-3 border">
                <div className=" col-8 mb-2 mt-2 ml-2 form-check">
                  <input
                    value={amount}
                    onChange={this.handleChange}
                    id="amount"
                    name="amount"
                    type="Radio"
                    checked={selectedAmount === amount}
                    className="form-check-input"
                  />
                  <label className="form-check-label" htmlFor={amount}>
                    {amount}
                  </label>
                </div>
              </div>
            ))}
          </div>

          {this.renderTableView()}
        </div>
      </div>
    );
  }
  renderTableView = () => {
    const { allChequesData, perPageSize } = this.state;
    let startIndex, lastIndex;
    startIndex =
      allChequesData.page === 1
        ? 1
        : allChequesData.page * perPageSize < allChequesData.totalNum
        ? allChequesData.page * perPageSize - allChequesData.totalItems + 1
        : (allChequesData.page - 1) * perPageSize + 1;
    lastIndex =
      allChequesData.page * perPageSize < allChequesData.totalNum
        ? allChequesData.page * perPageSize
        : allChequesData.totalNum;
    if (allChequesData.items) {
      return (
        <React.Fragment>
          <div className="col-10">
            <div class="col-8 mb-2 mt-2 h6">
              {startIndex} - {lastIndex} of {allChequesData.totalNum}
            </div>
            <table className="table ">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Cheque Number</th>
                  <th>Bank Name</th>
                  <th>Branch</th>
                  <th>Amount</th>
                </tr>
              </thead>

              <tbody>
                {allChequesData.items.map((cheque, index) =>
                  index % 2 === 0 ? (
                    <tr
                      key={
                        cheque.chequeNumber +
                        "" +
                        cheque.name +
                        "" +
                        cheque.bankName +
                        "" +
                        cheque.amount
                      }
                      className="bg-light"
                    >
                      <td>{cheque.name}</td>
                      <td>{cheque.chequeNumber}</td>
                      <td>{cheque.bankName}</td>
                      <td>{cheque.branch}</td>
                      <td>{cheque.amount}</td>
                    </tr>
                  ) : (
                    <tr
                      key={
                        cheque.chequeNumber +
                        "" +
                        cheque.name +
                        "" +
                        cheque.bankName +
                        "" +
                        cheque.amount
                      }
                    >
                      <td>{cheque.name}</td>
                      <td>{cheque.chequeNumber}</td>
                      <td>{cheque.bankName}</td>
                      <td>{cheque.branch}</td>
                      <td>{cheque.amount}</td>
                    </tr>
                  )
                )}
              </tbody>
            </table>
            <div className="row">
              <div className="col-10">
                {allChequesData.page > 1 ? (
                  <button
                    className="btn btn-secondary m-1"
                    onClick={() => this.pageNavigate(-1)}
                  >
                    Previous
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div className="col-2 text-right">
                {lastIndex < allChequesData.totalNum ? (
                  <button
                    className="btn btn-secondary m-1"
                    onClick={() => this.pageNavigate(1)}
                  >
                    Next
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </React.Fragment>
      );
    }
  };
}

export default AllCheque;
