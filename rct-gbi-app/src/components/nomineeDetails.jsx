import React, { Component } from "react";
import "./requiredField.css";
import http from "./services/httpService";
import { apiBaseURL } from "./config/config.json";
import axios from "axios";
class NomineeDetails extends Component {
  state = {
    nomineeData: {
      name: "",
      nomineeName: "",
      gender: "",
      dob: "",
      relationship: "",
      jointsignatory: false,
      year: "",
      month: "",
      day: "",
    },
    genders: ["Male", "Female"],
    days: this.generateDay(),
    months: [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ],
    years: this.generateYear(),
    errors: {},
  };
  generateYear() {
    let yearList = [];
    let currentYear = new Date().getFullYear();
    for (let year = 1980; year < currentYear; year++) {
      yearList.push(year);
    }
    return yearList;
  }

  generateDay() {
    let dayList = [];

    for (let day = 1; day <= 31; day++) {
      dayList.push(day);
    }
    return dayList;
  }
  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    const { data: nominee } = await axios.get(
      apiBaseURL + "/getNominee/" + user.name
    );
    let nomineeData = {};
    if (nominee.name !== user.name) {
      nomineeData = {
        name: "",
        nomineeName: "",
        gender: "",
        dob: "",
        relationship: "",
        jointsignatory: "",
        year: "",
        month: "",
        day: "",
      };
    } else {
      nomineeData = nominee;
    }
    console.log("Nomini Data", nomineeData);
    this.setState({ nomineeData });
  }
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "nomineeName":
        if (!e.currentTarget.value.trim()) return "Name is required";
        break;
      case "gender":
        if (!e.currentTarget.value.trim()) return "Gender is required";
        break;
      case "year":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select Year"
        )
          return "Year is required";

        break;
      case "month":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select Month"
        )
          return "Month is required";
        break;
      case "day":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select Day"
        )
          return "Day is required";
        break;

      case "relationship":
        if (!e.currentTarget.value.trim()) return "Relationship is required";
        break;

      default:
        break;
    }
    return "";
  };
  validate = () => {
    let errs = {};
    if (!this.state.nomineeData.nomineeName.trim())
      errs.nomineeName = "Name is required";
    if (!this.state.nomineeData.gender.trim())
      errs.gender = "Gender is required";
    if (
      !this.state.nomineeData.year.trim() ||
      this.state.nomineeData.year === "Select Year"
    )
      errs.year = "Year is required";
    if (
      !this.state.nomineeData.month.trim() ||
      this.state.nomineeData.month === "Select Month"
    )
      errs.month = "Month is required";
    if (
      !this.state.nomineeData.day.trim() ||
      this.state.nomineeData.day === "Select Day"
    )
      errs.day = "Day is required";
    if (!this.state.nomineeData.relationship.trim())
      errs.relationship = "Relationship is required";
    return errs;
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    console.log("Error", errors);
    if (errCount > 0) return;
    try {
      const { nomineeData } = this.state;
      const user = JSON.parse(localStorage.getItem("user"));
      const apiEndpoint = apiBaseURL + "/nomineeDetails";
      await http.post(apiEndpoint, {
        name: user.name,
        nomineeName: nomineeData.nomineeName,
        gender: nomineeData.gender,
        relationship: nomineeData.relationship,
        jointsignatory: nomineeData.jointsignatory,
        dob: nomineeData.day + "-" + nomineeData.month + "-" + nomineeData.year,
      });
      alert(user.name + " Your nominee ::" + nomineeData.nomineeName);
      window.location = "/customer";
    } catch (ex) {}
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    console.log("Error", errors);
    const { nomineeData } = this.state;
    nomineeData[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ nomineeData, errors });
  };
  render() {
    const { nomineeData, genders, days, months, years, errors } = this.state;
    console.log("Data Render", nomineeData);
    const dob = nomineeData.dob;
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          <h5>Nominee Details</h5>
          <div className="form-group required">
            <label htmlFor="nomineeName" className="control-label">
              Name
            </label>
            <input
              value={nomineeData.nomineeName}
              onChange={this.handleChange}
              type="text"
              id="nomineeName"
              name="nomineeName"
              className="form-control"
            />
            {errors.nomineeName ? (
              <div className="text-danger">{errors.nomineeName}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group required">
            <div className="row mb-3">
              <label htmlFor="gender" className="control-label col-2 ">
                Gender
              </label>
              {genders.map((gender) => (
                <div className="form-check-inline col-2">
                  <input
                    value={gender}
                    onChange={this.handleChange}
                    id="gender"
                    name="gender"
                    type="radio"
                    //checked={nomineeData.gender === gender}
                    className="form-check-input"
                  />
                  <label className="form-check-label" htmlFor={gender}>
                    {gender}
                  </label>
                </div>
              ))}
            </div>
            {errors.gender ? (
              <div className="text-danger">{errors.gender}</div>
            ) : (
              ""
            )}
            <div className="col text-secondary text-left border-bottom"></div>
            <div className="form-group required">
              <label htmlFor="dob" className="control-label mt-2">
                Date of Birth
              </label>
              <div className="row">
                <select
                  value={
                    dob
                      ? dob.substring(dob.lastIndexOf("-") + 1, dob.length)
                      : nomineeData.year
                      ? nomineeData.year
                      : "Select Year"
                  }
                  name="year"
                  onChange={this.handleChange}
                  className="browser-default custom-select col-3 m-2"
                >
                  <option>Select Year</option>
                  {years.map((year) => (
                    <option key={year}>{year}</option>
                  ))}
                </select>
                <select
                  value={
                    dob
                      ? dob.substring(
                          dob.indexOf("-") + 1,
                          dob.lastIndexOf("-")
                        )
                      : nomineeData.month
                      ? nomineeData.month
                      : "Select Month"
                  }
                  name="month"
                  onChange={this.handleChange}
                  className="browser-default custom-select col-3 m-2"
                >
                  <option>Select Month</option>
                  {months.map((month) => (
                    <option key={month}>{month}</option>
                  ))}
                </select>
                <select
                  value={
                    dob
                      ? dob.substring(0, dob.indexOf("-"))
                      : nomineeData.day
                      ? nomineeData.day
                      : "Select Day"
                  }
                  name="day"
                  onChange={this.handleChange}
                  className="browser-default custom-select col-3 m-2"
                >
                  <option>Select Day</option>
                  {days.map((day) => (
                    <option key={day}>{day}</option>
                  ))}
                </select>
              </div>
              <div className="row">
                <div className="text-danger col-3">
                  {errors.year ? errors.year : ""}
                </div>
                <div className="text-danger col-3">
                  {errors.month ? errors.month : ""}
                </div>
                <div className="text-danger col-3">
                  {errors.day ? errors.day : ""}
                </div>
              </div>
            </div>
            <div className="col text-secondary text-left border-bottom"></div>
            <div className="form-group required">
              <label htmlFor="relationship" className="control-label">
                Relationship
              </label>
              <input
                value={nomineeData.relationship}
                onChange={this.handleChange}
                type="text"
                id="relationship"
                name="relationship"
                className="form-control"
              />
              {errors.relationship ? (
                <div className="text-danger">{errors.relationship}</div>
              ) : (
                ""
              )}
            </div>

            <div class="form-check ">
              <input
                value={nomineeData.jointsignatory}
                class="form-check-input"
                type="checkbox"
                id="jointsignatory"
                name="jointsignatory"
                checked={nomineeData.jointsignatory}
                onChange={this.handleChange}
              />
              <label htmlFor="jointsignatory" className="form-check-label">
                Joint Signatory
              </label>
            </div>
          </div>
          {!nomineeData.name ? (
            <button className="btn btn-primary">Add Nominee</button>
          ) : (
            ""
          )}
        </form>
      </div>
    );
  }
}

export default NomineeDetails;
