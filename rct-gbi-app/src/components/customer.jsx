import React, { Component } from "react";
import bankImg from "./bankImg.jpg";
class Customer extends Component {
  state = {
    componentDidMount() {
      const user = JSON.parse(localStorage.getItem("user"));
      this.setState({ user });
    },
  };
  render() {
    return (
      <div className="container">
        <h1 className="text-center text-danger">
          Welcome to GBI BANK customer portal
        </h1>
        <div className="row">
          <img
            src={bankImg}
            alt=""
            style={{
              marginLeft: "auto",
              marginRight: "auto",
              width: "35%",
              height: "50%",
            }}
          />
        </div>
      </div>
    );
  }
}

export default Customer;
