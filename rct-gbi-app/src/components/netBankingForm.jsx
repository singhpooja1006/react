import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import axios from "axios";
import http from "./services/httpService";
import "./requiredField.css";
class NetBankingForm extends Component {
  state = {
    data: { payeeName: "", amount: "", bankName: "", comment: "" },
    payeeDetails: [],
    errors: {},
  };
  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    const { data: payeeDetails } = await axios.get(
      apiBaseURL + "/getPayees/" + user.name
    );
    this.setState({ payeeDetails });
  }
  validate = () => {
    let errs = {};
    if (!this.state.data.payeeName.trim())
      errs.payeeName = "Payee Name is required";
    if (!this.state.data.amount.trim()) errs.amount = "Amount is required";
    else if (this.state.data.amount <= 0)
      errs.amount = "Enter Amount greter than zero.";
    return errs;
  };
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "payeeName":
        if (e.currentTarget.value === "Select Payee")
          return "Payee Name is required";
        break;
      case "amount":
        if (!e.currentTarget.value.trim()) return "Amount is required";
        else if (e.currentTarget.value.trim() <= 0)
          return "Enter Amount greter than zero.";
        break;
      default:
        break;
    }
    return "";
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const { data } = this.state;
      const user = JSON.parse(localStorage.getItem("user"));
      const apiEndpoint = apiBaseURL + "/postNet";
      await http.post(apiEndpoint, {
        bankName: this.state.payeeDetails.find(
          (payee) => payee.payeeName === data.payeeName
        ).bankName,
        payeeName: data.payeeName,
        comment: data.comment,
        amount: data.amount,
        name: user.name,
      });
      alert("Details added successfully");
      window.location = "/customer";
    } catch (ex) {}
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { data } = this.state;
    data[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data, errors: errors });
  };
  render() {
    const { data, payeeDetails, errors } = this.state;
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          <h5>Net Banking Details</h5>
          <div className="form-group required">
            <label htmlFor="payeeName" className="control-label">
              Payee Name
            </label>
            <select
              value={payeeDetails.payee}
              onChange={this.handleChange}
              id="payeeName"
              name="payeeName"
              className="browser-default custom-select form-control"
            >
              <option>Select Payee</option>
              {payeeDetails.map((payee) => (
                <option key={payee.payeeName}>{payee.payeeName}</option>
              ))}
            </select>
            {errors.payeeName ? (
              <div className="text-danger">{errors.payeeName}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group required">
            <label htmlFor="amount" className="control-label">
              Amount
            </label>
            <input
              value={data.amount}
              onChange={this.handleChange}
              type="number"
              id="amount"
              name="amount"
              placeholder="Enter Amount"
              className="form-control"
            />
            {errors.amount ? (
              <div className="text-danger">{errors.amount}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="comment">Comment</label>
            <input
              value={data.comment}
              onChange={this.handleChange}
              type="text"
              id="comment"
              name="comment"
              placeholder="Enter Comment"
              className="form-control"
            />
          </div>
          <button className="btn btn-primary">Add Transaction</button>
        </form>
      </div>
    );
  }
}

export default NetBankingForm;
