import React, { Component } from "react";
import { apiBaseURL } from "./config/config.json";
import axios from "axios";
import queryString from "query-string";
class ViewAllCustomers extends Component {
  state = {
    customersData: {},
    perPageSize: 5,
  };
  async componentDidMount() {
    let { page } = queryString.parse(this.props.location.search);
    const { data: customersData } = await axios.get(
      apiBaseURL + "/getCustomers?page=" + page
    );
    this.setState({
      customersData,
      pageLength: customersData.length,
    });
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.location !== this.props.location) {
      let { page } = queryString.parse(this.props.location.search);
      const { data: customersData } = await axios.get(
        apiBaseURL + "/getCustomers?page=" + page
      );
      this.setState({
        customersData,
      });
    }
  }
  pageNavigate = (value) => {
    let { page } = queryString.parse(this.props.location.search);
    let currPage = page ? +page : 1;
    currPage = currPage + value;
    this.calURL("", currPage);
    //this.setState({ previousLength: this.state.customersData.totalItems });
  };
  calURL = (params, page) => {
    params = this.addToParams(params, "page", page);
    this.props.history.push({
      search: params,
    });
  };

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  render() {
    return <div className="container">{this.renderViewAllCustomers()}</div>;
  }
  renderViewAllCustomers = () => {
    const { customersData, perPageSize } = { ...this.state };

    let startIndex, lastIndex;
    startIndex =
      customersData.page === 1
        ? 1
        : customersData.page * perPageSize < customersData.totalNum
        ? customersData.page * perPageSize - customersData.totalItems + 1
        : (customersData.page - 1) * perPageSize + 1;
    lastIndex =
      customersData.page * perPageSize < customersData.totalNum
        ? customersData.page * perPageSize
        : customersData.totalNum;
    if (customersData.items) {
      return (
        <React.Fragment>
          <h2>All Customers</h2>
          <p>
            {startIndex} - {lastIndex} of {customersData.totalNum}
          </p>
          <table className="table ">
            <tr>
              <th>Name</th>
              <th>State</th>
              <th>City</th>
              <th>PAN</th>
              <th>DOB</th>
            </tr>

            <tbody>
              {customersData.items.map((customer, index) =>
                index % 2 === 0 ? (
                  <tr key={customer.name} className="bg-light">
                    <td>{customer.name}</td>
                    <td>{customer.state}</td>
                    <td>{customer.city}</td>
                    <td>{customer.PAN}</td>
                    <td>{customer.dob}</td>
                  </tr>
                ) : (
                  <tr key={customer.name}>
                    <td>{customer.name}</td>
                    <td>{customer.state}</td>
                    <td>{customer.city}</td>
                    <td>{customer.PAN}</td>
                    <td>{customer.dob}</td>
                  </tr>
                )
              )}
            </tbody>
          </table>
          <div className="row">
            <div className="col-11">
              {customersData.page > 1 ? (
                <button
                  className="btn btn-secondary"
                  onClick={() => this.pageNavigate(-1)}
                >
                  Previous
                </button>
              ) : (
                ""
              )}
            </div>
            <div className="col-1 float-right">
              {lastIndex < customersData.totalNum ? (
                <button
                  className="btn btn-secondary m-1 "
                  onClick={() => this.pageNavigate(1)}
                >
                  Next
                </button>
              ) : (
                ""
              )}
            </div>
          </div>
        </React.Fragment>
      );
    }
  };
}

export default ViewAllCustomers;
