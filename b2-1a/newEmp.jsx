import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
class AddNewEmp extends Component {
  state = {
    data: {
      id: "",
      name: "",
      department: "",
      designation: "",
      salary: "",
      gender: "",
    },
    departments: ["Finance", "Operations", "Technology", "HR"],
    designations: [
      "Trainee",
      "Junior Manager",
      "Manager",
      "General Manager",
      "Vice President",
    ],

    genders: ["Male", "Female"],
    errors: {},
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { data } = this.state;
    data[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ data, errors });
  };
  async componentDidMount() {
    const { id } = this.props.match.params;
    if (id) {
      const apiEndpoint = config.apiEndPoint + "/employees/" + id;
      const { data } = await http.get(apiEndpoint);
      this.setState({ data });
    }
  }
  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "id":
        if (!e.currentTarget.value.trim()) return "ID is required";
        break;
      case "name":
        if (!e.currentTarget.value.trim()) return "Name is required";
        break;
      case "department":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select Department"
        )
          return "Department is required";
        break;
      case "designation":
        if (
          !e.currentTarget.value.trim() ||
          e.currentTarget.value === "Select Designation"
        )
          return "Designation is required";
        break;
      case "salary":
        if (!e.currentTarget.value.trim()) return "Salary is required";
        else if (+e.currentTarget.value <= 0) return "Salary is grater than 0";
        break;
      case "gender":
        if (!e.currentTarget.value.trim()) return "Gender is required";
        break;

      default:
        break;
    }
    return "";
  };
  validate = () => {
    let errs = {};
    if (!this.state.data.id.trim()) errs.id = "ID is required";
    if (!this.state.data.name.trim()) errs.gender = "Name is required";
    if (
      !this.state.data.department.trim() ||
      this.state.data.department === "Select Department"
    )
      errs.department = "Department is required";
    if (
      !this.state.data.designation.trim() ||
      this.state.data.designation === "Select Designation"
    )
      errs.designation = "Department is required";
    if (+this.state.data.salary <= 0) errs.salary = "Salary is grater than 0";
    if (!this.state.data.gender.trim()) errs.gender = "Gender is required";
    return errs;
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const { data } = this.state;
      const { id } = this.props.match.params;
      if (id) {
        const apiEndpoint = config.apiEndPoint + "/employees/" + id;
        await http.put(apiEndpoint, data);
      } else {
        const apiEndpoint = config.apiEndPoint + "/employees";
        await http.post(apiEndpoint, data);
      }

      window.location = "/employees";
    } catch (ex) {}
  };
  render() {
    const { data, departments, designations, genders, errors } = this.state;
    const { id } = this.props.match.params;
    return (
      <div className="container">
        <h2>Employee Details</h2>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="id">Customer ID</label>
            <input
              value={data.id}
              onChange={this.handleChange}
              type="text"
              id="id"
              name="id"
              className="form-control"
              placeholder="Enter Id"
              disabled={id ? true : false}
            />
            {errors.id ? <div className="text-danger">{errors.id}</div> : ""}
          </div>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              value={data.name}
              onChange={this.handleChange}
              type="text"
              id="name"
              name="name"
              placeholder="Enter Nmae"
              className="form-control"
            />
            {errors.name ? (
              <div className="text-danger">{errors.name}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="department">Department</label>
            <select
              value={data.department}
              onChange={this.handleChange}
              id="department"
              name="department"
              className="browser-default custom-select mb-1"
            >
              <option>Select Department</option>
              {departments.map((department) => (
                <option key={department}>{department}</option>
              ))}
            </select>
            {errors.department ? (
              <div className="text-danger">{errors.department}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="designation">Deignations</label>
            <select
              value={data.designation}
              onChange={this.handleChange}
              id="designation"
              name="designation"
              className="browser-default custom-select mb-1"
            >
              <option>Select Designation</option>
              {designations.map((designation) => (
                <option key={designation}>{designation}</option>
              ))}
            </select>
            {errors.designation ? (
              <div className="text-danger">{errors.designation}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="salary">Salary</label>
            <input
              value={data.salary}
              onChange={this.handleChange}
              type="number"
              id="salary"
              name="salary"
              placeholder="Enter Salary"
              className="form-control"
            />
            {errors.salary ? (
              <div className="text-danger">{errors.salary}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label htmlFor="gender" className="control-label col-2">
              <b>Gender</b>
            </label>
            {genders.map((gender) => (
              <div className="form-check-inline col-2">
                <input
                  value={gender}
                  onChange={this.handleChange}
                  id="gender"
                  name="gender"
                  type="radio"
                  checked={data.gender === gender}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={gender}>
                  {gender}
                </label>
              </div>
            ))}
            {errors.gender ? (
              <div className="text-danger">{errors.gender}</div>
            ) : (
              ""
            )}
          </div>
          <button className="btn btn-primary">Submit</button>
        </form>
      </div>
    );
  }
}

export default AddNewEmp;
