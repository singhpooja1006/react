import React, { Component } from "react";
class LeftPanel extends Component {
  state = {};
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let { deptData, desgData, salaryData, gendersData } = { ...this.props };
    if (input.name === "department") {
      deptData.selected = input.value;
    }
    if (input.name === "designation") {
      desgData.selected = input.value;
    }
    if (input.name === "gender") {
      gendersData.selected = input.value;
    }
    if (input.name === "salary") {
      salaryData.selected = input.value;
    }

    this.props.onRadioButtonEvent(deptData, desgData, salaryData, gendersData);
  };
  render() {
    const { deptData, desgData, salaryData, gendersData } = this.props;
    return (
      <React.Fragment>
        <div className="row border ">
          <div className="col-1 m-3 h6">Options</div>
        </div>
        <div className="row border ">
          <div className="col-12 m-3">
            <h6>Department</h6>
            {deptData.departments.map((department) => (
              <div className="form-check mt-1">
                <input
                  value={department}
                  onChange={this.handleChange}
                  id="department"
                  name="department"
                  type="radio"
                  checked={deptData.selected === department ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={department}>
                  {department}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>Designation</h6>
            {desgData.designations.map((designation) => (
              <div className="form-check mt-1">
                <input
                  value={designation}
                  onChange={this.handleChange}
                  id="designation"
                  name="designation"
                  type="radio"
                  checked={desgData.selected === designation ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={designation}>
                  {designation}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>Gender</h6>
            {gendersData.genders.map((gender) => (
              <div className="form-check mt-1">
                <input
                  value={gender}
                  onChange={this.handleChange}
                  id="gender"
                  name="gender"
                  type="radio"
                  checked={gendersData.selected === gender ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={gender}>
                  {gender}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="row border mt-3">
          <div className="col-12 m-3">
            <h6>Salry Range</h6>
            {salaryData.salaries.map((salary) => (
              <div className="form-check mt-1">
                <input
                  value={salary}
                  onChange={this.handleChange}
                  id="salary"
                  name="salary"
                  type="radio"
                  checked={salaryData.selected === salary ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={salary}>
                  {salary}
                </label>
              </div>
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPanel;
