var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
    next();
});

const port  = 2410;
app.listen(port,()=>console.log("Listening on port : ",port))

let employeesData = 
[{id : "EM01", name : "Anita Singh", department : "Finance", designation : "Trainee", salary: 5000, gender : "Female"},
{id : "EM02", name : "Juleea", department : "Finance", designation : "Trainee", salary: 5000, gender : "Female"},
{id : "EM03", name : "Sunita", department : "Technology", designation : "Trainee", salary: 5000, gender : "Female"},
{id : "EM04", name : "Punita", department : "Technology", designation : "Trainee", salary: 5000, gender : "Female"},
{id : "EM05", name : "Anoop Singh", department : "Technology", designation : "Trainee", salary: 5000, gender : "Male"},
{id : "EM06", name : "Reena Verma", department : "Finance", designation : "Vice President", salary: 35000, gender : "Female"},
{id : "EM07", name : "Aakash", department : "Finance", designation : "Vice President", salary: 35000, gender : "Male"},
{id : "EM08", name : "Anushka", department : "Technology", designation : "Vice President", salary: 35000, gender : "Female"},
{id : "EM09", name : "Rajeev", department : "Technology", designation : "Vice President", salary: 35000, gender : "Male"},
{id : "EM10", name : "Raju Kumar", department : "Technology", designation : "Vice President", salary: 35000, gender : "Male"},
{id : "EM11", name : "Priya Jain", department : "Finance", designation : "General Manager", salary: 25000, gender : "Female"},
{id : "EM12", name : "Amrita", department : "Finance", designation : "General Manager", salary: 25000, gender : "Female"},
{id : "EM13", name : "Sanjna", department : "Operations", designation : "General Manager", salary: 25000, gender : "Female"},
{id : "EM14", name : "Suruti tiwari", department : "Operations", designation : "General Manager", salary: 25000, gender : "Female"},
{id : "EM15", name : "Monika Dube", department : "Finance", designation : "General Manager", salary: 25000, gender : "Female"},
{id : "EM16", name : "Payal Parihar", department : "Finance", designation : "General Manager", salary: 25000, gender : "Female"},
{id : "EM17", name : "Sapna Pandey", department : "Operations", designation : "Manager", salary: 15000, gender : "Female"},
{id : "EM18", name : "Deepali", department : "Operations", designation : "Manager", salary: 15000, gender : "Female"},
{id : "EM19", name : "Ananya Pandey", department : "Operations", designation : "Manager", salary: 15000, gender : "Female"},
{id : "EM20", name : "Rupesh Rai", department : "Operations", designation : "Manager", salary: 15000, gender : "Male"},
{id : "EM21", name : "Jack Smith", department : "Operations", designation : "Manager", salary: 15000, gender : "Male"},
{id : "EM22", name : "Harsh Tiwari", department : "Finance", designation : "Trainee", salary: 5000, gender : "Male"},
{id : "EM23", name : "Vivek Rai", department : "Finance", designation : "Junior Manager", salary: 10000, gender : "Male"},
{id : "EM24", name : "Aaditya Gupta", department : "Finance", designation : "Junior Manager", salary: 10000, gender : "Male"},
{id : "EM25", name : "Aaryan Pandey", department : "Finance", designation : "Junior Manager", salary: 10000, gender : "Male"},
{id : "EM26", name : "Sagar Sahu", department : "HR", designation : "Junior Manager", salary: 10000, gender : "Male"},
{id : "EM27", name : "Arun Giri", department : "HR", designation : "Junior Manager", salary: 10000, gender : "Male"},
{id : "EM28", name : "Ashish Singh", department : "HR", designation : "Trainee", salary: 5000, gender : "Male"},
{id : "EM29", name : "Vikash Singh", department : "HR", designation : "Trainee", salary: 5000, gender : "Male"},
{id : "EM30", name : "Aman Pandey", department : "HR", designation : "Trainee", salary: 5000, gender : "Male"}]

app.get("/employees",function(req,res) { 
    let {sortBy,department,designation,salary,gender} = req.query;
    console.log("in get request for /employees: ",sortBy );
    let result = [...employeesData];
    if(department) {
        result = result.filter((data) => data.department === department) ;
    }
    if(designation) {
        result = result.filter((data) => data.designation === designation) ;
    }
    if(salary){
        if(salary === "<15000")
            result = result.filter((data) => data.salary < 15000) ;
        if(salary === "15000-25000")
            result = result.filter((data) => data.salary >= 15000 && data.salary <= 25000) ;
        if(salary === ">25000")
            result = result.filter((data) => data.salary > 25000) ;
    }
    if(gender){
        result = result.filter((data) => data.gender === gender) ;
    }
    if(sortBy === "id"){
        result = result.sort((emp1,emp2) => 
            (emp1.id > emp2.id)  ? 
                1
            : ((emp1.id < emp2.id)  ? 
              -1 : 0)
        );
    }
    if(sortBy === "name"){
        result = result.sort((emp1,emp2) => 
            (emp1.name > emp2.name)  ? 
                1
            : ((emp1.name < emp2.name)  ? 
              -1 : 0)
        );
    }
    if(sortBy === "department"){
        result = result.sort((emp1,emp2) => 
            (emp1.department > emp2.department)  ? 
                1
            : ((emp1.department < emp2.department)  ? 
              -1 : 0)
        );
    }
    if(sortBy === "designation"){
        result = result.sort((emp1,emp2) => 
            (emp1.designation > emp2.designation)  ? 
                1
            : ((emp1.designation < emp2.designation)  ? 
              -1 : 0)
        );
    }
    if(sortBy === "salary"){
        result = result.sort((emp1,emp2) => emp1.salary - emp2.salary);
    }
    if(sortBy === "gender"){
        result = result.sort((emp1,emp2) => 
            (emp1.gender > emp2.gender)  ? 
                1
            : ((emp1.gender < emp2.gender)  ? 
              -1 : 0)
        );
    }
    res.send(result);
})


app.get("/employees/:id",function(req,res) { 
    let id = req.params.id;
    console.log("in get request for /employees/:id : ",id);
    let index = employeesData.findIndex((data)=> data.id === id);
    if (index === -1) res.status(404).send(id + " Not Found");
    else {
        res.send(employeesData[index]);
    }
});

app.post("/employees",function(req,res) { 
    console.log("in post request for /employees ");
    employeesData.push(req.body)
    res.send(req.body);
});

app.put("/employees/:id",function(req,res) { 
    let id = req.params.id;
    console.log("in put request for /employees/:id ", id);
    let index = employeesData.findIndex((data)=> data.id === id);
    if (index === -1) res.status(404).send(id + " Not Found");
    else {
        employeesData[index] = req.body;
        res.send(res.body);
    }
});

app.delete("/employees/:id",function(req,res) { 
    let id = req.params.id;
    console.log("in delete request for /employees/:id", id );
    let employeeIndex = employeesData.findIndex((data)=> data.id === id);
    const employee = employeesData[employeeIndex];
    if (employeeIndex === -1) 
        return res.status(404).send("The employee with the given ID was not found.");
    employeesData.splice(employeeIndex,1);
    res.send(employee);
    
});