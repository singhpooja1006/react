import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
import queryString from "query-string";
import LeftPanel from "./leftPanel";

class Employees extends Component {
  state = {
    employeesData: [],
    sortBy: "",
    deptData: {
      departments: ["Finance", "Operations", "Technology", "HR"],
      selected: "",
    },
    desgData: {
      designations: [
        "Trainee",
        "Junior Manager",
        "Manager",
        "General Manager",
        "Vice President",
      ],
      selected: "",
    },
    salaryData: {
      salaries: ["<15000", "15000-25000", ">25000"],
      selected: "",
    },
    gendersData: { genders: ["Male", "Female"], selected: "" },
  };
  async componentDidMount() {
    let { sortBy } = queryString.parse(this.props.location.search);
    let apiEndpoint = config.apiEndPoint + "/employees";
    if (sortBy) {
      apiEndpoint = apiEndpoint + "?sortBy=" + sortBy;
    }
    const { data: employeesData } = await http.get(apiEndpoint);
    this.setState({ employeesData });
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      let apiEndpoint =
        config.apiEndPoint + "/employees" + this.props.location.search;
      const { data: employeesData } = await http.get(apiEndpoint);
      this.setState({ employeesData });
    }
  }
  handleSortBy = (sortBy) => {
    this.setState({ sortBy });

    this.calURL(
      "",
      this.state.deptData.selected,
      this.state.desgData.selected,
      this.state.salaryData.selected,
      this.state.gendersData.selected,
      sortBy
    );
  };
  calURL = (params, department, designation, salary, gender, sortBy) => {
    let path = "/employees";
    params = this.addToParams(params, "department", department);
    params = this.addToParams(params, "designation", designation);
    params = this.addToParams(params, "salary", salary);
    params = this.addToParams(params, "gender", gender);
    params = this.addToParams(params, "sortBy", sortBy);
    this.props.history.push({
      pathname: path,
      search: params,
    });
  };

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  handleEditDetails = (id) => {
    window.location = "/employees/" + id;
  };
  handleDeleteDetails = async (id) => {
    const originalEmployees = this.state.employeesData;
    try {
      const apiEndpoint = config.apiEndPoint + "/employees";
      await http.delete(apiEndpoint + "/" + id);
      const { data: employeesData } = await http.get(apiEndpoint);
      this.setState({ employeesData });
    } catch (ex) {
      if (ex.response && ex.response.status !== 404) {
        this.setState({ employeesData: originalEmployees });
      }
    }
  };
  handleRadioButtonEvent = (deptData, desgData, salaryData, gendersData) => {
    this.setState({ deptData, desgData, salaryData, gendersData });
    this.calURL(
      "",
      deptData.selected,
      desgData.selected,
      salaryData.selected,
      gendersData.selected,
      this.state.sortBy
    );
  };
  render() {
    return (
      <div className="container">
        <div className="row m-1">
          <div className="col-2 mr-1">
            <LeftPanel
              deptData={this.state.deptData}
              desgData={this.state.desgData}
              salaryData={this.state.salaryData}
              gendersData={this.state.gendersData}
              onRadioButtonEvent={this.handleRadioButtonEvent}
            />
          </div>
          <div className="col-9 ml-2">{this.renderEmployeesData()}</div>
        </div>
      </div>
    );
  }
  renderEmployeesData = () => {
    const { employeesData } = this.state;
    if (employeesData) {
      return (
        <React.Fragment>
          <div className="row border bg-dark text-center text-white mt-2">
            <div
              className="col-1 border"
              onClick={() => this.handleSortBy("id")}
            >
              ID
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("name")}
            >
              Name
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("department")}
            >
              Department
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("designation")}
            >
              Designation
            </div>
            <div
              className="col-1 border"
              onClick={() => this.handleSortBy("salary")}
            >
              Salary
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("gender")}
            >
              Gender
            </div>
            <div className="col-2 border"></div>
          </div>
          {employeesData.map((employee) => (
            <div className="row border text-center bg-light" key={employee.id}>
              <div className="col-1 border">{employee.id}</div>
              <div className="col-2 border">{employee.name}</div>
              <div className="col-2 border">{employee.department}</div>
              <div className="col-2 border">{employee.designation}</div>
              <div className="col-1 border">{employee.salary}</div>
              <div className="col-2 border">{employee.gender}</div>
              <div className="col-1 border">
                <i
                  className="fa fa-pencil-square-o"
                  aria-hidden="true"
                  onClick={() => this.handleEditDetails(employee.id)}
                ></i>
              </div>
              <div className="col-1 border">
                <i
                  className="fa fa-trash"
                  aria-hidden="true"
                  onClick={() => this.handleDeleteDetails(employee.id)}
                ></i>
              </div>
            </div>
          ))}
        </React.Fragment>
      );
    }
  };
}

export default Employees;
