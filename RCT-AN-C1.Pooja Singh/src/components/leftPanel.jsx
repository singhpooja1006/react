import React, { Component } from "react";
class LeftPanel extends Component {
  state = {};
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let { sectionsData, selectedOrder } = this.props;
    if (input.name === "selectedSection") {
      sectionsData[input.name] = input.id;
    } else {
      selectedOrder = input.value;
    }
    this.props.onOptionChange(sectionsData, selectedOrder);
  };
  render() {
    const { sectionsData, dropDownData, selectedOrder } = this.props;
    return (
      <React.Fragment>
        <div className="container">
          <div className="mt-3 ml-3 row border bg-light">
            <div class="col-8 mt-2 h6">Order By</div>
          </div>
          <div className="row ml-3">
            <select
              className="form-control"
              value={selectedOrder}
              id="selectedOrder"
              name="selectedOrder"
              onChange={this.handleChange}
            >
              {dropDownData.map((item) => (
                <option key={item}>{item}</option>
              ))}
            </select>
          </div>
          <div className="mt-3 ml-3 row border bg-light">
            <div class="col-8 mb-2 mt-2 h6">Sections</div>
          </div>
          {sectionsData.sections.map((section) => (
            <div className="row ml-3 border">
              <div className="col-8 mb-2 mt-2 ml-2 form-check">
                <input
                  value={section.value}
                  onChange={this.handleChange}
                  id={section.id}
                  name="selectedSection"
                  type="Radio"
                  checked={section.id === sectionsData.selectedSection}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={section.value}>
                  {section.value}
                </label>
              </div>
            </div>
          ))}
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPanel;
