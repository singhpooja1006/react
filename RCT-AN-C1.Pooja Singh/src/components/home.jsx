import React, { Component } from "react";
import config from "./configUrl.json";
import LeftPanel from "./leftPanel";
import queryString from "query-string";
import axios from "axios";

class HomePage extends Component {
  state = {
    searchText: "",
    sectionsData: {
      sections: [
        { id: "business", value: "Business" },
        { id: "technology", value: "Technology" },
        { id: "politics", value: "Politics" },
        { id: "lifeStyle", value: "LifeStyle" },
      ],
      selectedSection: "",
    },

    dropDownData: ["Order By", "None", "newest", "oldest", "relevance"],
    selectedOrder: "Order By",
  };
  handleSubmit = (e) => {
    e.preventDefault();
    let { sectionsData } = this.state;
    if (sectionsData.selectedSection !== "") {
      sectionsData.selectedSection = "";
    }
    this.setState({ sectionsData });
    this.calURL(
      "",
      this.state.searchText,
      "1",
      this.state.sectionsData.selectedSection,
      this.state.selectedOrder
    );
  };
  calURL = (params, searchText, nextPage, section, orderBy) => {
    params = this.addToParams(params, "q", searchText);
    params = this.addToParams(params, "page", nextPage);
    params = this.addToParams(params, "section", section);
    params = this.addToParams(params, "order-by", orderBy);
    this.props.history.push({
      search: params,
    });
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.location.search !== this.props.location.search) {
      let { q, section } = queryString.parse(this.props.location.search);
      let url =
        config.apiEndPoint + this.props.location.search + "&api-key=test";
      const { data: dataList } = await axios.get(url);
      const { sectionsData } = this.state;
      if (section === undefined) {
        sectionsData.selectedSection = "";
      }

      this.setState({
        dataResult: dataList.response,
        searchText: q,
        sectionsData,
      });
    }
  }

  async componentDidMount() {
    let { q } = queryString.parse(this.props.location.search);
    let url = config.apiEndPoint + this.props.location.search + "&api-key=test";
    const { data: dataList } = await axios.get(url);
    this.setState({ dataResult: dataList.response, searchText: q });
  }

  handleChange = (e) => {
    const { currentTarget: input } = e;
    this.setState({ searchText: input.value });
  };
  pageNavigate = (value) => {
    const { dataResult, searchText } = this.state;
    let nextPage = dataResult.currentPage + value;
    this.setState({ dataResult });
    this.calURL(
      "",
      searchText,
      nextPage,
      this.state.sectionsData.selectedSection,
      this.state.selectedOrder
    );
  };
  handleOptionChange = (sectionsData, selectedOrder) => {
    const { q } = queryString.parse(this.props.location.search);
    if (selectedOrder === "None" || selectedOrder === "Order By") {
      this.calURL("", q, "1", sectionsData.selectedSection, "");
    } else {
      this.calURL("", q, "1", sectionsData.selectedSection, selectedOrder);
    }
    this.setState({ sectionsData, selectedOrder });
  };
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-3">
            <LeftPanel
              sectionsData={this.state.sectionsData}
              dropDownData={this.state.dropDownData}
              selectedOrder={this.state.selectedOrder}
              onOptionChange={this.handleOptionChange}
            />
          </div>
          <div className="col-8">
            <div className="row ">
              <div className="col-12 mt-2">
                <form onSubmit={this.handleSubmit}>
                  <div className="form-inline text-center">
                    <input
                      value={this.state.searchText}
                      onChange={this.handleChange}
                      name="searchText"
                      type="text"
                      className="form-control col-10"
                      placeholder="Enter Search Text"
                    />
                    <button className="btn btn-light m-2">Submit</button>
                  </div>
                </form>
              </div>
            </div>
            {this.renderDataResult()}
          </div>
        </div>
      </div>
    );
  }
  renderDataResult() {
    if (this.state.dataResult && this.state.dataResult.results.length > 0) {
      const { dataResult } = this.state;
      let maxPage = dataResult.startIndex + dataResult.pageSize - 1;
      let currentPageDetails = dataResult.startIndex + " - ";
      currentPageDetails += maxPage + " of " + dataResult.total;
      return (
        <React.Fragment>
          <div className="row ">
            <div className="col-12 h6">{currentPageDetails}</div>
          </div>
          <div className="row ">
            {this.returnCellData(dataResult.results[0])}
            {this.returnCellData(dataResult.results[1])}
            {this.returnCellData(dataResult.results[2])}
          </div>
          <div className="row ">
            {this.returnCellData(dataResult.results[3])}
            {this.returnCellData(dataResult.results[4])}
            {this.returnCellData(dataResult.results[5])}
          </div>
          <div className="row ">
            {this.returnCellData(dataResult.results[6])}
            {this.returnCellData(dataResult.results[7])}
            {this.returnCellData(dataResult.results[8])}
          </div>
          <div className="row ">
            {this.returnCellData(dataResult.results[9])}
          </div>
          <div className="row mt-3">
            <div className="col-6">
              {dataResult.currentPage > 1 ? (
                <button
                  className="btn btn-danger"
                  onClick={() => this.pageNavigate(-1)}
                >
                  Prev
                </button>
              ) : (
                ""
              )}
            </div>
            <div className="col-6">
              <button
                className="btn btn-danger float-right"
                onClick={() => this.pageNavigate(1)}
              >
                Next
              </button>
            </div>
          </div>
        </React.Fragment>
      );
    }
  }

  returnCellData(data) {
    if (data) {
      return (
        <React.Fragment>
          <div className="col-3 text-center bg-light m-1">
            {data.webTitle}
            <br />
            <b>Source : {data.pillarName}</b>
            <br />
            <b>{data.webPublicationDate}</b>
            <br />
            <a href={data.webUrl} rel="noopener noreferrer" target="_blank">
              Preview
            </a>
          </div>
        </React.Fragment>
      );
    }
  }
}

export default HomePage;
