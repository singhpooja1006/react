import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import HomePage from "./home";
import NavBar from "./navbar";
class MainComponent extends Component {
  state = {};
  render() {
    return (
      <div>
        <NavBar />
        <Switch>
          <Route path="/home" component={HomePage} />
          <Route path="/" component={HomePage} />
          <Redirect path="/home" />
        </Switch>
      </div>
    );
  }
}

export default MainComponent;
