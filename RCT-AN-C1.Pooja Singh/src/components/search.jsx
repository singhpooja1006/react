import React, { Component } from "react";
class SearchText extends Component {
  state = {};
  render() {
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          <div className="form-inline text-center">
            <input
              // value={this.state.searchText}
              onChange={this.handleChange}
              name="searchText"
              type="text"
              className="form-control col-7"
              placeholder="Enter Search Text"
            />
            <button className="btn btn-light m-2">Search</button>
          </div>
        </form>
      </div>
    );
  }
}

export default SearchText;
