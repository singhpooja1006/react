import React, { Component } from "react";
import { Link } from "react-router-dom";
class NavBar extends Component {
  state = {};
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-danger">
        <Link className="navbar-brand" to="/">
          NewsSite
        </Link>

        <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
          <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
            <li className="nav-item">
              <Link className="nav-link text-white" to="/home/?q=sports&page=1">
                Sports
              </Link>
            </li>
            <li className="nav-item">
              <Link
                className="nav-link text-white"
                to="/home/?q=cricket&page=1"
              >
                Cricket
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link text-white" to="/home/?q=movies&page=1">
                Movies
              </Link>
            </li>
            <li className="nav-item">
              <Link
                className="nav-link text-white"
                to="/home/?q=education&page=1"
              >
                Education
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavBar;
