import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
class SignIn extends React.Component {
  state = {
    data: [
      { email: "user@user.com", password: "user1234", role: "user" },
      { email: "admin@admin.com", password: "user1234", role: "admin" },
    ],
  };
  render() {
    return (
      <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        validationSchema={Yup.object().shape({
          email: Yup.string()
            .email("Email is invalid")
            .required("Email is required"),
          password: Yup.string()
            .min(6, "Password must be at least 6 characters")
            .required("Password is required"),
        })}
        onSubmit={(fields) => {
          let userData = this.state.data.find(
            (user) =>
              user.email === fields.email && user.password === fields.password
          );
          if (userData) {
            localStorage.setItem("user", JSON.stringify(userData));
            window.location = "/products";
          } else {
            alert("Invalid username or password!");
          }
        }}
        render={({ errors, status, touched }) => (
          <div className="container">
            <Form>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <Field
                  name="email"
                  type="text"
                  className={
                    "form-control" +
                    (errors.email && touched.email ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="email"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <Field
                  name="password"
                  type="password"
                  className={
                    "form-control" +
                    (errors.password && touched.password ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="password"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <button type="submit" className="btn btn-primary mr-2">
                  Submit
                </button>
              </div>
            </Form>
          </div>
        )}
      />
    );
  }
}

export default SignIn;
