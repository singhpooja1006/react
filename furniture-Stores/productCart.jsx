import React, { Component } from "react";
class Cart extends Component {
  state = {
    cartProduct: [],
  };
  componentDidMount() {
    const cartProduct = JSON.parse(localStorage.getItem("cartProduct"));
    this.setState({ cartProduct });
  }
  handleIncrement = (prodCode) => {
    const cartProduct = [...this.state.cartProduct];
    const index = cartProduct.findIndex(
      (product) => product.prodCode === prodCode
    );
    cartProduct[index].qty += 1;
    this.setState({ cartProduct });
  };
  handleDecrement = (prodCode) => {
    const cartProduct = [...this.state.cartProduct];
    const index = cartProduct.findIndex(
      (product) => product.prodCode === prodCode
    );
    cartProduct[index].qty -= 1;
    if (cartProduct[index].qty === 0) {
      cartProduct.splice(index, 1);
    }
    if (cartProduct.length === 0) {
      localStorage.removeItem("cartProduct");
    }
    this.setState({ cartProduct });
  };
  render() {
    const { cartProduct } = this.state;
    return (
      <div className="container">
        {this.state.cartProduct ? (
          <React.Fragment>
            <div className="row">
              <h2 className="col-12 text-center">Products in Shopping Cart</h2>
            </div>
            {cartProduct
              ? cartProduct.map((product) => (
                  <div className="row bg-light">
                    <div className="col-lg-3 col-12">
                      <div
                        className="card"
                        style={{
                          width: "25%",
                          height: "25%",
                          objectFit: "cover",
                        }}
                      >
                        <img
                          className="card-img-top text-center"
                          alt=""
                          src={product.img}
                        />
                      </div>
                    </div>
                    <div className="col-3">
                      <b>{product.title}</b>
                    </div>
                    <div className="col-4"></div>
                    <div className="col-2">
                      <button
                        className="btn btn-danger"
                        onClick={() => this.handleDecrement(product.prodCode)}
                      >
                        -
                      </button>
                      <button className="btn btn-secondary">
                        {product.qty}
                      </button>
                      <button
                        className="btn btn-success"
                        onClick={() => this.handleIncrement(product.prodCode)}
                      >
                        +
                      </button>
                    </div>
                  </div>
                ))
              : ""}
            <div className="row">
              <h2 className="col-12 text-center">List of Items in Cart</h2>
            </div>
            <div className="row border bg-dark text-white ml-5 mr-5">
              <div className="col-6 text-center">Item Name</div>
              <div className="col-6 text-center">Count</div>
            </div>
            {this.renderListOfItemsInCart()}
          </React.Fragment>
        ) : (
          ""
        )}
      </div>
    );
  }
  renderListOfItemsInCart = () => {
    const cartProduct = [...this.state.cartProduct];
    let ingList = [];
    for (
      let productIndex = 0;
      productIndex < cartProduct.length;
      productIndex++
    ) {
      for (
        let ingIndex = 0;
        ingIndex < cartProduct[productIndex].ingredients.length;
        ingIndex++
      ) {
        let index = ingList.findIndex(
          (ing) =>
            ing.ingName ===
            cartProduct[productIndex].ingredients[ingIndex].ingName
        );
        if (index === -1) {
          const indProduct = {
            ...cartProduct[productIndex].ingredients[ingIndex],
          };
          indProduct.qty = indProduct.qty * cartProduct[productIndex].qty;
          ingList.push(indProduct);
        } else {
          ingList[index].qty =
            ingList[index].qty +
            cartProduct[productIndex].ingredients[ingIndex].qty *
              cartProduct[productIndex].qty;
        }
      }
    }
    return (
      <React.Fragment>
        {ingList.map((ing) => (
          <div className="row border bg-light ml-5 mr-5">
            <div className="col-6 text-center border">{ing.ingName}</div>
            <div className="col-6 text-center border">{ing.qty}</div>
          </div>
        ))}
      </React.Fragment>
    );
  };
}

export default Cart;
