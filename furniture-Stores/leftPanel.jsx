import React, { Component } from "react";
class LeftPanel extends Component {
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let { categoryData } = { ...this.props };
    if (input.name === "category") {
      categoryData.selected = input.value;
    }

    this.props.onOptionChange(categoryData);
  };
  render() {
    const { categoryData } = this.props;
    return (
      <React.Fragment>
        <div className="row border bg-light">
          <div className="col-1 m-3 h6">Options</div>
        </div>
        <div className="row border">
          <div className="col-12 m-3">
            {categoryData.categories.map((category) => (
              <div className="form-check mt-1">
                <input
                  value={category}
                  onChange={this.handleChange}
                  id="category"
                  name="category"
                  type="radio"
                  checked={categoryData.selected === category ? true : false}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={category}>
                  {category}
                </label>
              </div>
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPanel;
