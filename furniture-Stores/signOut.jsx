import React, { Component } from "react";
class SignOut extends Component {
  componentDidMount() {
    localStorage.removeItem("user");
    localStorage.removeItem("cartProduct");
    localStorage.removeItem("newProduct");
    window.location = "/sign-in";
  }
  render() {
    return null;
  }
}

export default SignOut;
