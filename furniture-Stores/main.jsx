import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import NavBar from "./navBar";
import FurnitureData from "./showFurnitureData";
import SignIn from "./signInForm";
import SignOut from "./signOut";
import Cart from "./productCart";
import AddNewProduct from "./new";

class MainComp extends Component {
  state = {};
  componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    this.setState({ user });
  }
  render() {
    const { user } = this.state;
    return (
      <React.Fragment>
        <NavBar user={user} />
        <Switch>
          <Route path="/products/new" component={AddNewProduct} />

          <Route path="/products" component={FurnitureData} />
          <Route path="/cart" component={Cart} />
          <Route path="/sign-in" component={SignIn} />
          <Route path="/signout" component={SignOut} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default MainComp;
