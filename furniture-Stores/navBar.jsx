import React, { Component } from "react";
import { Link } from "react-router-dom";
class NavBar extends Component {
  state = {};
  render() {
    return (
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
        <Link className="navbar-brand" to="/">
          FurnitureStore
        </Link>
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link to="/products" className="nav-link">
              Products
            </Link>
          </li>
        </ul>
        <ul className="navbar-nav">
          <li className="nav-item">
            {this.props.user && this.props.user.role === "user" ? (
              <Link to="/cart" className="nav-link">
                Cart
              </Link>
            ) : (
              ""
            )}
          </li>
        </ul>
        <ul className="navbar-nav">
          <li className="nav-item">
            {this.props.user && this.props.user.role === "admin" ? (
              <Link to="/products/new" className="nav-link">
                Add a New Product
              </Link>
            ) : (
              ""
            )}
          </li>
        </ul>

        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            {this.props.user ? (
              <Link to="/signout" className="nav-link">
                Sign out
              </Link>
            ) : (
              <Link to="/sign-in" className="nav-link">
                Sign In
              </Link>
            )}
          </li>
        </ul>
      </nav>
    );
  }
}

export default NavBar;
