import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import config from "./config.json";
import axios from "axios";
class AddNewProduct extends React.Component {
  constructor() {
    super();
    this.state = {
      showDesc: false,
      showItems: false,
      desc: [],
      items: [],
      imgUrl: "",
      title: "",
      prodCode: "",
    };
  }
  async componentDidMount() {
    if (this.props.product) {
      this.setState({
        prodCode: this.props.product.prodCode,
        imgUrl: this.props.product.img,
        title: this.props.product.title,
        showDesc: true,
        showItems: true,
        desc: this.props.product.desc,
        items: this.props.product.ingredients,
      });
    }
  }
  render() {
    return (
      <Formik
        initialValues={{
          prodCode: this.props.product ? this.props.product.prodCode : "",
          title: this.props.product ? this.props.product.title : "",
          img: this.props.product ? this.props.product.img : "",
          category: this.props.product ? this.props.product.category : "",
        }}
        validationSchema={Yup.object().shape({
          prodCode: Yup.string().required("Product Code required"),
          title: Yup.string().required("This field is required"),
          img: Yup.string().required("Image URL is required"),
          category: Yup.string().required("Category is required"),
        })}
        render={({ errors, status, touched, values, setValues }) => (
          <div className="container">
            <div className="row">
              <div className="col-md-6 ">
                <div>
                  <Form>
                    <div className="form-group">
                      <label htmlFor="prodCode">Prodcut Code</label>
                      <Field
                        name="prodCode"
                        type="text"
                        className={
                          "form-control" +
                          (errors.prodCode && touched.prodCode
                            ? " is-invalid"
                            : "")
                        }
                        disabled={this.props.product ? true : false}
                        onChange={(e) => {
                          setValues({ prodCode: e.target.value });
                          this.setState({ prodCode: e.target.value });
                        }}
                      />
                      <ErrorMessage
                        name="prodCode"
                        component="div"
                        className="invalid-feedback"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="title">Name</label>
                      <Field
                        name="title"
                        type="text"
                        className={
                          "form-control" +
                          (errors.title && touched.title ? " is-invalid" : "")
                        }
                        onChange={(e) => {
                          setValues({ title: e.target.value });
                          this.setState({ title: e.target.value });
                        }}
                      />
                      <ErrorMessage
                        name="title"
                        component="div"
                        className="invalid-feedback"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="img">Image URL</label>
                      <Field
                        name="img"
                        type="text"
                        className={
                          "form-control" +
                          (errors.img && touched.img ? " is-invalid" : "")
                        }
                        onChange={(e) => {
                          setValues({ img: e.target.value });
                          this.setState({ imgUrl: e.target.value });
                        }}
                      />
                      <ErrorMessage
                        name="img"
                        component="div"
                        className="invalid-feedback"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="category">Category</label>
                      <Field
                        as="select"
                        id="category"
                        name="category"
                        className={
                          "form-control" +
                          (errors.category && touched.category
                            ? " is-invalid"
                            : "")
                        }
                      >
                        <option value="" label="Select Category " />
                        <option value="Dining" label="Dining" />
                        <option value="Drawing" label="Drawing" />
                        <option value="Bedroom" label="Bedroom" />
                        <option value="Study" label="Study" />
                      </Field>
                      <ErrorMessage
                        name="category"
                        component="div"
                        className="invalid-feedback"
                      />
                    </div>
                    <div className="form-group">
                      <button
                        className="btn btn-secondary mt-2"
                        onClick={(e) => {
                          e.preventDefault();
                          let desc = this.state.desc;
                          if (desc) {
                            desc.push("");
                          }
                          this.setState({
                            showDesc: true,
                            desc,
                          });
                        }}
                      >
                        Add Description
                      </button>
                      {this.state.showDesc === true
                        ? this.state.desc.map((line, index) => (
                            <div className="form-group">
                              <Field
                                name="desc"
                                type="text"
                                className="col-10"
                                value={line}
                                placeholder={"Line " + (index + 1)}
                                onChange={(e) => {
                                  let desc = [...this.state.desc];
                                  desc[index] = e.target.value;
                                  this.setState({ desc });
                                }}
                              />
                              <button
                                className="btn btn-danger ml-4"
                                onClick={() => {
                                  let desc = this.state.desc;
                                  desc.splice(index, 1);
                                  this.setState({
                                    desc,
                                  });
                                }}
                              >
                                <i
                                  className="fa fa-times"
                                  aria-hidden="true"
                                ></i>
                              </button>
                            </div>
                          ))
                        : ""}
                    </div>
                    <div className="form-group">
                      <button
                        className="btn btn-secondary mt-2"
                        onClick={(e) => {
                          e.preventDefault();
                          let items = this.state.items;
                          if (items) {
                            items.push({ ingName: "", qty: "" });
                          }
                          this.setState({
                            showItems: true,
                            items,
                          });
                        }}
                      >
                        Add Items shipped with product
                      </button>
                      {this.state.showItems === true
                        ? this.state.items.map((item, index) => (
                            <div className="form-group">
                              <Field
                                name="itemName"
                                type="text"
                                className="col-6"
                                value={item.ingName}
                                placeholder={"Item Name"}
                                onChange={(e) => {
                                  let items = [...this.state.items];
                                  items[index].ingName = e.target.value;
                                  this.setState({ items });
                                }}
                              />
                              <Field
                                name="itemQty"
                                type="number"
                                className="col-4 ml-3"
                                value={item.qty}
                                placeholder={"Quantity"}
                                onChange={(e) => {
                                  let items = [...this.state.items];
                                  items[index].qty = e.target.value;
                                  this.setState({ items });
                                }}
                              />
                              <button
                                className="btn btn-danger ml-3"
                                onClick={() => {
                                  let items = this.state.items;
                                  items.splice(index, 1);
                                  this.setState({
                                    items,
                                  });
                                }}
                              >
                                <i
                                  className="fa fa-times"
                                  aria-hidden="true"
                                ></i>
                              </button>
                            </div>
                          ))
                        : ""}
                    </div>
                    <button
                      className="btn btn-success mt-2"
                      onClick={async (e) => {
                        e.preventDefault();

                        let product = {};
                        product.prodCode = this.state.prodCode;
                        product.category = values.category;
                        product.title = this.state.title;
                        product.img = this.state.imgUrl;
                        product.desc = this.state.desc;
                        product.ingredients = this.state.items;
                        if (
                          product.prodCode === "" ||
                          product.category === "" ||
                          product.title === "" ||
                          product.imgUrl === ""
                        ) {
                          alert(
                            "Ensure that all the values have been entered properly"
                          );
                        } else {
                          localStorage.setItem(
                            "newProduct",
                            JSON.stringify(product)
                          );
                          if (this.props.product) {
                            const { data: productsData } = await axios.put(
                              config.apiEndPoint +
                                "/products/" +
                                product.prodCode,
                              product
                            );
                          } else {
                            const { data: productsData } = await axios.post(
                              config.apiEndPoint + "/products",
                              product
                            );
                          }

                          window.location = "/products";
                        }
                      }}
                    >
                      Save
                    </button>
                  </Form>
                </div>
              </div>

              {this.state.imgUrl ? (
                <div className="col-lg-4 col-12 mt-4 ml-5">
                  <div
                    style={{
                      width: "100%",
                      height: "100%",
                      objectFit: "cover",
                    }}
                  >
                    <img
                      className="card-img-top text-center"
                      alt=""
                      src={this.state.imgUrl}
                    />
                  </div>
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
        )}
      />
    );
  }
}

export default AddNewProduct;
