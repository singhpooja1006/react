import React, { Component } from "react";
import config from "./config.json";
import axios from "axios";
import LeftPanel from "./leftPanel";
import AddNewProduct from "./new";
class FurnitureData extends Component {
  state = {
    categoryData: {
      categories: ["Dining", "Drawing", "Bedroom", "Study"],
      selected: "",
    },
    addCartProductList: [],
    productsData: [],
    editIndex: -1,
    view: 0,
  };
  async componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    const { data: productsData } = await axios.get(
      config.apiEndPoint + "/products"
    );

    this.setState({
      productsData,
      user,
    });
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      let pathParam = this.props.location.pathname.split("/");

      if (pathParam.length === 2) {
        const { data: productsData } = await axios.get(
          config.apiEndPoint + "/products"
        );
        this.setState({
          productsData,
          categoryData: {
            categories: ["Dining", "Drawing", "Bedroom", "Study"],
            selected: "",
          },
        });
      } else if (this.state.categoryData.selected) {
        const { data: productsData } = await axios.get(
          config.apiEndPoint + "/products/" + this.state.categoryData.selected
        );
        this.setState({
          productsData,
        });
      }
    } else if (prevState !== this.state) {
    }
  }
  showProductDetails = (productData) => {
    const { categoryData } = this.state;
    categoryData.selected = productData.category;
    this.setState({ categoryData });
    this.calURL(categoryData.selected, productData.prodCode);
  };
  handleOptionChange = (categoryData) => {
    this.setState({ categoryData });
    this.calURL(categoryData.selected, "");
  };
  calURL = (category, prodCode) => {
    let path = "/products/" + category;
    if (prodCode) {
      path += "/" + prodCode;
    }
    this.props.history.push({
      pathname: path,
    });
  };
  addProductToCart = (product) => {
    let { addCartProductList } = this.state;
    let index = addCartProductList.find(
      (prd) => prd.prodCode === product.prodCode
    );
    if (index === undefined) {
      product.qty = 1;
      addCartProductList.push(product);
    }
    this.setState({ addCartProductList });
    localStorage.setItem("cartProduct", JSON.stringify(addCartProductList));
  };

  editProductDetails = async (prodCode) => {
    console.log("URL", this.props.location.pathname);
    this.setState({ editProdcutCode: prodCode });
    this.props.history.push({
      pathname: this.props.location.pathname + "/edit",
      editProdcutCode: prodCode,
    });
  };

  render() {
    let pathParam = this.props.location.pathname.split("/");
    let product;

    if (pathParam.length === 4) {
      product = this.state.productsData.find(
        (prd) => prd.prodCode === pathParam[3]
      );
    }
    let editProduct = {};
    if (this.state.editProdcutCode) {
      editProduct = this.state.productsData.find(
        (prd) => prd.prodCode === this.state.editProdcutCode
      );
    }

    return (
      <div className="container-fluid">
        {!this.state.editProdcutCode ? (
          <div className="row ml-1">
            <div className="col-2">
              <LeftPanel
                categoryData={this.state.categoryData}
                onOptionChange={this.handleOptionChange}
              />
            </div>

            <div className="col-lg-6 col-12 mt-2">
              {product ? (
                ""
              ) : (
                <div className="col-lg-12 col-12 ml-5 text-right">
                  &nbsp;&nbsp;&nbsp;Choose a product!
                </div>
              )}

              {this.renderProductsData()}
            </div>
            {product &&
            product.ingredients &&
            this.state.categoryData.selected ? (
              <div className="col-lg-4 col-12">
                <div className="row">
                  <div className="mt-1">
                    {this.state.user && this.state.user.role === "admin" ? (
                      <React.Fragment>
                        <button
                          className="btn btn-secondary"
                          onClick={() =>
                            this.editProductDetails(product.prodCode)
                          }
                        >
                          Edit Product
                        </button>
                        <button className="btn btn-secondary ml-3">
                          Delete Product
                        </button>
                      </React.Fragment>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="mt-4">
                    <div
                      className="card"
                      style={{
                        width: "100%",
                        height: "100%",
                        objectFit: "cover",
                      }}
                    >
                      <img
                        className="card-img-top text-center"
                        alt=""
                        src={product.img}
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <b>{product.title}</b>
                </div>
                <div className="row">{product.desc}</div>
                <div className="row">
                  <b>Items in product</b>
                </div>
                {product.ingredients.map((data) => (
                  <div className="row">
                    -{data.ingName} : {data.qty}{" "}
                  </div>
                ))}
                {this.state.user && this.state.user.role === "user" ? (
                  <div
                    className="btn btn-success"
                    onClick={() => this.addProductToCart(product)}
                  >
                    Add to Cart
                  </div>
                ) : (
                  ""
                )}
              </div>
            ) : (
              ""
            )}
          </div>
        ) : (
          <AddNewProduct product={editProduct} />
        )}
      </div>
    );
  }
  renderProductsData = () => {
    const { productsData } = this.state;
    return (
      <React.Fragment>
        <div className="row">
          {productsData
            ? productsData.map((product) => this.renderProductViewData(product))
            : ""}
        </div>
      </React.Fragment>
    );
  };
  renderProductViewData = (productData) => {
    if (productData) {
      return (
        <React.Fragment>
          <div
            className="mb-2 col-5"
            onClick={() => this.showProductDetails(productData)}
          >
            <div
              className="card"
              style={{
                width: "100%",
                height: "70%",
                objectFit: "cover",
              }}
            >
              <img
                className="card-img-top text-center"
                alt=""
                src={productData.img}
              />
            </div>
          </div>
        </React.Fragment>
      );
    }
  };
}

export default FurnitureData;
