import React ,{ Component }from 'react';
import './App.css';
import { Switch, Route, Redirect } from "react-router-dom";
import NavBar from './components/navBar';
import Students from './components/students';
import AddNewStudent from './components/newStudent';
import LoginForm from './components/login';
import Logout from './components/logOut';
class App extends Component {
  state = {};

  componentDidMount() {
    const user = JSON.parse(localStorage.getItem("user"));
    this.setState({ user });
  }
  render() {
    const { user } = this.state;
    return (
      <React.Fragment>
        <NavBar user={user} />
		<Switch>
    
    <Route path="/new" component={AddNewStudent} />
    <Route path="/students/:id" component={AddNewStudent} />
    <Route path="/students" component={Students} />
    <Route path="/logOut" component={Logout} />
    <Route path="/login" component={LoginForm} />
    <Redirect from="/" to="/login"/>
    
    </Switch>
      </React.Fragment>
    );
  }
}

export default App;