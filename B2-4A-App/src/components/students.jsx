import React, { Component } from "react";
import config from "./config.json";
import http from "./services/httpService";
import queryString from "query-string";
import LeftPanel from "./leftPanel";
class Students extends Component {
  state = {
    studnetsData: [],
    sortBy: "",
    pageInfo: {},
    maths: [
      { name: "Excellent", isSelected: false },
      { name: "Good", isSelected: false },
      { name: "Average", isSelected: false },
      { name: "Poor", isSelected: false },
    ],
    english: [
      { name: "Excellent", isSelected: false },
      { name: "Good", isSelected: false },
      { name: "Average", isSelected: false },
      { name: "Poor", isSelected: false },
    ],
    computers: [
      { name: "Excellent", isSelected: false },
      { name: "Good", isSelected: false },
      { name: "Average", isSelected: false },
      { name: "Poor", isSelected: false },
    ],

    sections: [
      { name: "A", isSelected: false },
      { name: "B", isSelected: false },
      { name: "C", isSelected: false },
      { name: "D", isSelected: false },
    ],
  };
  async componentDidMount() {
    let { q } = queryString.parse(this.props.location.search);
    let apiEndpoint = config.apiEndPoint + "/students";
    let { data: studnetsData } = await http.get(apiEndpoint);
    if (q) {
      studnetsData = studnetsData.filter(
        (std) => std.name.toUpperCase().indexOf(q.toUpperCase()) >= 0
      );
    }
    let returnData = this.returnPaginationData(studnetsData, 1);
    this.setState({
      studnetsData: returnData.data,
      pageInfo: returnData.pageInfo,
    });
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      let {
        maths,
        english,
        computers,
        sections,
        sortBy,
        page,
        q,
      } = queryString.parse(this.props.location.search);
      let apiEndpoint = config.apiEndPoint + "/students";
      if (sortBy) {
        apiEndpoint += "?sortBy=" + sortBy;
      }
      const { data: studnetsData } = await http.get(apiEndpoint);
      let result = [...studnetsData];
      if (q) {
        result = result.filter(
          (std) => std.name.toUpperCase().indexOf(q.toUpperCase()) >= 0
        );
      }
      if (sections) {
        let sectionList = sections.split(",");
        result = result.filter((data) =>
          sectionList.find((section) => section === data.section)
        );
      }
      if (maths) {
        let mathsList = maths.split(",");
        let ressArr = [];
        let dataFound = false;
        for (let index = 0; index < mathsList.length; index++) {
          let objs;
          switch (mathsList[index]) {
            case "Excellent":
              objs = result.filter(
                (std) =>
                  std.marksDetails.filter((data) => data.maths >= 80).length > 0
              );
              break;
            case "Good":
              objs = result.filter(
                (std) =>
                  std.marksDetails.filter(
                    (data) => data.maths >= 60 && data.maths < 80
                  ).length > 0
              );
              break;
            case "Average":
              objs = result.filter(
                (std) =>
                  std.marksDetails.filter(
                    (data) => data.maths >= 40 && data.maths < 60
                  ).length > 0
              );
              break;
            case "Poor":
              objs = result.filter(
                (std) =>
                  std.marksDetails.filter((data) => data.maths < 40).length > 0
              );
              break;
          }
          if (objs) {
            objs.forEach((obj) => ressArr.push(obj));
            dataFound = true;
          }
        }
        if (dataFound === true) {
          result = [...ressArr];
        }
      }
      if (english) {
        let englishList = english.split(",");
        let ressArr = [];
        let dataFound = false;
        for (let index = 0; index < englishList.length; index++) {
          let objs;
          switch (englishList[index]) {
            case "Excellent":
              objs = result.filter(
                (std) =>
                  std.marksDetails.filter((data) => data.english >= 80).length >
                  0
              );
              break;
            case "Good":
              objs = result.filter(
                (std) =>
                  std.marksDetails.filter(
                    (data) => data.english >= 60 && data.english < 80
                  ).length > 0
              );
              break;
            case "Average":
              objs = result.filter(
                (std) =>
                  std.marksDetails.filter(
                    (data) => data.english >= 40 && data.english < 60
                  ).length > 0
              );
              break;
            case "Poor":
              objs = result.filter(
                (std) =>
                  std.marksDetails.filter((data) => data.english < 40).length >
                  0
              );
              break;
          }
          if (objs) {
            objs.forEach((obj) => ressArr.push(obj));
            dataFound = true;
          }
        }
        if (dataFound === true) {
          result = [...ressArr];
        }
      }
      if (computers) {
        let computersList = computers.split(",");
        let ressArr = [];
        let dataFound = false;
        for (let index = 0; index < computersList.length; index++) {
          let objs;
          switch (computersList[index]) {
            case "Excellent":
              objs = result.filter(
                (std) =>
                  std.marksDetails.filter((data) => data.computers >= 80)
                    .length > 0
              );
              break;
            case "Good":
              objs = result.filter(
                (std) =>
                  std.marksDetails.filter(
                    (data) => data.computers >= 60 && data.computers < 80
                  ).length > 0
              );
              break;
            case "Average":
              objs = result.filter(
                (std) =>
                  std.marksDetails.filter(
                    (data) => data.computers >= 40 && data.computers < 60
                  ).length > 0
              );

              break;
            case "Poor":
              objs = result.filter(
                (std) =>
                  std.marksDetails.filter((data) => data.computers < 40)
                    .length > 0
              );
              break;
          }
          if (objs) {
            objs.forEach((obj) => ressArr.push(obj));
            dataFound = true;
          }
        }
        if (dataFound === true) {
          result = [...ressArr];
        }
      }
      let returnData = this.returnPaginationData(result, page ? page : 1);
      this.setState({
        studnetsData: returnData.data,
        pageInfo: returnData.pageInfo,
      });
    }
  }

  pagination(obj, page) {
    var resArr = obj;
    resArr = resArr.slice(page * 6 - 6, page * 6);
    return resArr;
  }

  returnPaginationData(result, page) {
    let respArr = this.pagination(result, page);
    let len = result.length;
    let quo = Math.floor(len / 6);
    let rem = len % 6;
    let extra = rem === 0 ? 0 : 1;
    let numofpages = quo + extra;
    let pageInfo = {
      pageNumber: page,
      numberOfPages: numofpages,
      numOfItems: 6,
      totalItemCount: result.length,
    };
    return {
      data: respArr,
      pageInfo: pageInfo,
    };
  }

  handleSortBy = (sortBy) => {
    this.setState({ sortBy });
    let { maths, english, computers, sections, q } = queryString.parse(
      this.props.location.search
    );
    this.calURL("", q, maths, english, computers, sections, sortBy, 1);
  };
  calURL = (params, q, maths, english, computers, sections, sortBy, page) => {
    let path = "/students";
    params = this.addToParams(params, "q", q);
    params = this.addToParams(params, "maths", maths);
    params = this.addToParams(params, "english", english);
    params = this.addToParams(params, "computers", computers);
    params = this.addToParams(params, "sections", sections);
    params = this.addToParams(params, "sortBy", sortBy);
    params = this.addToParams(params, "page", page);

    this.props.history.push({
      pathname: path,
      search: params,
    });
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  handleCheckBoxEvent = (maths, english, computers, sections) => {
    this.setState({ maths, english, computers, sections });
    let mathsString = this.buildQueryString(maths);
    let englishString = this.buildQueryString(english);
    let computersString = this.buildQueryString(computers);
    let sectionString = this.buildQueryString(sections);
    let { q } = queryString.parse(this.props.location.search);
    this.calURL(
      "",
      q ? q : "",
      mathsString,
      englishString,
      computersString,
      sectionString,
      this.state.sortBy,
      1
    );
  };
  buildQueryString(optionsData) {
    let filterData = optionsData.filter((n1) => n1.isSelected);
    let arrayData = filterData.map((n1) => n1.name);
    return arrayData.join(",");
  }
  handleEditDetails = async (id) => {
    const apiEndpoint = config.apiEndPoint + "/students/" + id;
    const { data } = await http.get(apiEndpoint);
    const user = JSON.parse(localStorage.getItem("user"));
    if (
      data &&
      user &&
      (user.email === data[0].email || user.role === "admin")
    ) {
      window.location = "/students/" + id;
    }
  };
  handleDeleteDetails = async (id) => {
    const originalstudnetsData = this.state.studnetsData;
    const apiEndpoint = config.apiEndPoint + "/students/" + id;
    const { data } = await http.get(apiEndpoint);
    const user = JSON.parse(localStorage.getItem("user"));
    if (
      data &&
      user &&
      (user.email === data[0].email || user.role === "admin")
    ) {
      try {
        const apiEndpoint = config.apiEndPoint + "/students";
        await http.delete(apiEndpoint + "/" + id);
        const { data: studnetsData } = await http.get(apiEndpoint);
        this.setState({
          studnetsData: studnetsData,
        });
      } catch (ex) {
        if (ex.response && ex.response.status !== 404) {
          this.setState({ studnetsData: originalstudnetsData });
        }
      }
    }
  };
  pageNavigate = (value) => {
    const { pageInfo } = { ...this.state };
    let { maths, english, computers, sections, sortBy, q } = queryString.parse(
      this.props.location.search
    );
    let currPage = +pageInfo.pageNumber + value;
    this.calURL("", q, maths, english, computers, sections, sortBy, currPage);
  };
  render() {
    return (
      <div className="container">
        <div className="row m-1">
          <div className="col-2 mr-1">
            <LeftPanel
              mathsData={this.state.maths}
              englishData={this.state.english}
              computersData={this.state.computers}
              sectionsData={this.state.sections}
              onCheckBoxButtonEvent={this.handleCheckBoxEvent}
            />
          </div>
          <div className="col-8 ml-2">{this.renderstudnetsData()}</div>
        </div>
      </div>
    );
  }
  renderstudnetsData = () => {
    const { studnetsData, pageInfo } = this.state;

    if (studnetsData) {
      let { page } = queryString.parse(this.props.location.search);
      page = page ? +page : 1;
      return (
        <React.Fragment>
          <div className="row">
            {pageInfo.pageNumber === 1
              ? 1
              : pageInfo.numOfItems * pageInfo.pageNumber -
                pageInfo.numOfItems +
                1}{" "}
            to{" "}
            {pageInfo.numOfItems * pageInfo.pageNumber < pageInfo.totalItemCount
              ? pageInfo.numOfItems * pageInfo.pageNumber
              : pageInfo.totalItemCount}{" "}
            of {pageInfo.totalItemCount}
          </div>
          <div className="row border bg-dark text-center text-white mt-2">
            <div
              className="col-1 border"
              onClick={() => this.handleSortBy("id")}
            >
              ID
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("name")}
            >
              Name
            </div>
            <div
              className="col-1 border"
              onClick={() => this.handleSortBy("section")}
            >
              Sec
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("maths")}
            >
              Maths
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("english")}
            >
              English
            </div>
            <div
              className="col-2 border"
              onClick={() => this.handleSortBy("computers")}
            >
              Computers
            </div>
            <div className="col-2 border"></div>
          </div>
          {studnetsData.map((student) => (
            <div className="row border text-center bg-light" key={student.id}>
              <div className="col-1 border">{student.id}</div>
              <div className="col-2 border text-truncate">{student.name}</div>
              <div className="col-1 border">{student.section}</div>
              {student.marksDetails.map((mark) => (
                <React.Fragment>
                  <div className="col-2 border">{mark.maths}</div>
                  <div className="col-2 border">{mark.english}</div>
                  <div className="col-2 border">{mark.computers}</div>
                </React.Fragment>
              ))}

              <div className="col-1 border">
                <i
                  className="fa fa-pencil-square-o"
                  aria-hidden="true"
                  onClick={() => this.handleEditDetails(student.id)}
                ></i>
              </div>
              <div className="col-1 border">
                <i
                  className="fa fa-trash"
                  aria-hidden="true"
                  onClick={() => this.handleDeleteDetails(student.id)}
                ></i>
              </div>
            </div>
          ))}
          <div className="row m-1">
            <div>
              {page > 1 ? (
                <button
                  className="btn btn-primary m-1"
                  onClick={() => this.pageNavigate(-1)}
                >
                  Previous
                </button>
              ) : (
                ""
              )}
            </div>
            <div>
              {page < pageInfo.numberOfPages ? (
                <button
                  className="btn btn-primary m-1"
                  onClick={() => this.pageNavigate(1)}
                >
                  Next
                </button>
              ) : (
                ""
              )}
            </div>
          </div>
        </React.Fragment>
      );
    }
  };
}

export default Students;
