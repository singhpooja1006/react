import React, { Component } from "react";
import { apiEndPoint } from "./config.json";
import http from "./services/httpService";
class LoginForm extends Component {
  state = {
    data: { email: "", password: "" },
    errors: {},
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      const apiEndpoint =
        apiEndPoint + "/getUserByEmail/" + this.state.data.email;
      const { data: user } = await http.get(apiEndpoint);
      if (user && user.password === this.state.data.password) {
        localStorage.setItem("user", JSON.stringify(user));
        window.location = "/students";
      } else {
        alert("Please enter Valid Email & Password.");
      }
    } catch (ex) {
      if (ex.response.status >= 400 && ex.response.status <= 500) {
        const errors = { ...this.state.errors };
        if (ex.response.data.includes("Not Found")) {
          errors.msg = "Login Failed. Check the username and password.";
        } else {
          errors.msg = ex.response.data;
        }
        this.setState({ errors });
      }
    }
  };
  validate = () => {
    let errs = {};

    if (!this.state.data.email.trim()) errs.email = "Email is required";

    if (!this.state.data.password.trim())
      errs.password = "Password is required";
    else if (this.state.data.password.trim().length < 7)
      errs.password = "Password  must be of 7 characters";
    return errs;
  };
  isFormvalid = () => {
    let errs = this.validate();

    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };
  validateInput = (e) => {
    switch (e.currentTarget.email) {
      case "email":
        if (!e.currentTarget.value.trim()) return "Email is required";

        break;
      case "password":
        if (!e.currentTarget.value.trim()) return "Password is required";
        else if (e.currentTarget.value.trim().length < 7)
          return "Password  must be of 7 characters";
        break;
      default:
        break;
    }
    return "";
  };
  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const loginData = { ...this.state.data };
    loginData[e.currentTarget.name] = e.currentTarget.value;

    this.setState({ data: loginData, errors: errors });
  };
  render() {
    const { data, errors } = this.state;
    return (
      <React.Fragment>
        <h6 className="text-center">Email ID</h6>
        <div className="text-center">
          <form onSubmit={this.handleSubmit}>
            <input
              value={data.email}
              type="text"
              onChange={this.handleChange}
              className="col-4"
              id="email"
              name="email"
              placeholder="Enter Email"
            />

            {errors.email ? (
              <div className="text-danger text-center">{errors.email}</div>
            ) : (
              ""
            )}
            <h6 className="text-center mt-3">Password</h6>
            <input
              value={data.password}
              type="password"
              onChange={this.handleChange}
              className="col-4"
              id="password"
              placeholder="Password"
              name="password"
            />
            {this.state.errors.password ? (
              <div className="text-danger text-center">
                {this.state.errors.password}
              </div>
            ) : (
              ""
            )}
            <div className="mt-3">
              <button className="btn btn-primary" disabled={this.isFormvalid()}>
                Login
              </button>
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default LoginForm;
