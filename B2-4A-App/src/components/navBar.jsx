import React, { Component } from "react";
import { Link } from "react-router-dom";
class NavBar extends Component {
  state = {
    searchText: "",
  };
  handleChange = (ele) => {
    const data = { ...this.state.searchText };
    data[ele.currentTarget.name] = ele.currentTarget.value;
    this.setState({ searchText: data.searchText });
  };
  handleKeyDown = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      const searchText = this.state.searchText;
      //this.setState({ searchText: "" });
      window.location = "/students?q=" + searchText;
    }
  };
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="collapse navbar-collapse" id="navbarNav">
          {!this.props.user ? (
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to="/login" className="nav-link">
                  Login
                </Link>
              </li>
            </ul>
          ) : (
            <React.Fragment>
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link className="nav-link" to="/students">
                    Show Students
                  </Link>
                </li>

                {this.props.user.role === "admin" ? (
                  <li className="nav-item">
                    <Link className="nav-link" to="/new">
                      Add a New Student
                    </Link>
                  </li>
                ) : (
                  ""
                )}
              </ul>

              <input
                className="form-control col-4 border-0 mr-5"
                type="text"
                placeholder="Search...."
                id="searchText"
                name="searchText"
                value={this.state.searchText}
                onChange={this.handleChange}
                onKeyDown={this.handleKeyDown}
              />
            </React.Fragment>
          )}
          <ul className="navbar-nav ml-auto">
            {this.props.user ? (
              <React.Fragment>
                <li className="nav-item">
                  <Link to="/logout" className="nav-link">
                    Logout
                  </Link>
                </li>
              </React.Fragment>
            ) : (
              ""
            )}
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavBar;
