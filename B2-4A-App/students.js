var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
    next();
});
const port  = 2410;
app.listen(port,()=>console.log("Listening on port : ",port))
const MongoClient = require("mongodb").MongoClient;
let ObjectId = require("mongodb").ObjectId;
const url = "mongodb://localhost:27017";
const dbName = "stddb";
const client = new MongoClient(url);
const userDetails = [
  {email : 'admin@gmail.com', password : "admin1234", role : "admin"},
  {email : 'anita@gmail.com', password : "anita1234", role : "student"},
  {email : 'meghna@gmail.com', password : "meghna1234", role : "student"},
  {email : 'arpita@gmail.com', password : "arpita1234", role : "student"},
  {email : 'janhavi@gmail.com', password : "janhavi1234", role : "student"},];
app.post("/students",function(req,res) { 
    let obj = {...req.body};
    client.connect(function(err,client){
    console.log("Post : /students",obj);
    const db = client.db(dbName);
    let stdObj = {id : obj.id, name: obj.name,section:obj.section,contact:obj.contact,city:obj.city,email:obj.email }
    let markObj = {id : obj.id, maths:obj.maths,english:obj.english,computers:obj.computers  }
    db.collection("students").insertOne(stdObj , function(err,result){
        console.log(result);
    });
    db.collection("marks").insertOne(markObj , function(err,result){
      console.log(result);
    });
    res.send(obj)
  });
});

app.delete("/students/:id",function(req,res) { 
  let id = req.params.id;
  console.log("Delete : /students/:id",id);
  client.connect(function(err,client){
    
    const db = client.db(dbName);
    db.collection("students").removeOne({id: id});
    db.collection("marks").removeOne({id: id}, function(err,result){
      res.send(result);
    })});
});

app.put("/students/:id",function(req,res) { 
  let obj = {...req.body};
  let id = req.params.id;
  client.connect(function(err,client){
      console.log("Put : /students/:id",id,obj);
  const db = client.db(dbName);
  let stdObj = {id : obj.id, name: obj.name,section:obj.section,contact:obj.contact,city:obj.city,email:obj.email }
  let markObj = {id : obj.id, maths:obj.maths,english:obj.english,computers:obj.computers  }
  db.collection("students").updateOne({id: id},{$set:stdObj});
  db.collection("marks").updateOne({id: id},{$set:markObj});
  res.send(obj);
});
});

app.get("/students/:id",function(req,res) { 
  let id = req.params.id;
  client.connect(function(err,client){
  console.log("Get by id: /students/:id",id);
  const db = client.db(dbName);
  
  db.collection('students').aggregate([
    {$match: {id: id}},
    { $lookup:
      {
        from: 'marks',
        localField: 'id',
        foreignField: 'id',
        as: 'marksDetails'
      }
    }]).toArray(function(err,data){
      console.log(data);
      res.send(data);
  });
});
});


app.get("/getUserByEmail/:email",function(req,res) { 
  let email = req.params.email;
  let index = userDetails.findIndex((data)=> data.email === email);
  if (index === -1) res.status(404).send(email + " Not Found");
  else {
      res.send(userDetails[index]);
  }
});


app.get("/students",function(req,res) { 
  let sortBy = req.query.sortBy;
  let q = req.query.q;
  let page = +req.query.page;
  page = isNaN(page) ? 1 : page;

  client.connect(function(err,client){
  console.log("Get by name: /students?sortBy",sortBy);
  const db = client.db(dbName);
  if(sortBy === "maths")
      db.collection('students').aggregate([
        { $lookup:
          {
            from: 'marks',
            localField: 'id',
            foreignField: 'id',
            as: 'marksDetails'
          }
        },{$sort:
          {
            "marksDetails.maths": 1
          }}
      ]).toArray(function(
              err,result
          ){
              console.log(err);
              res.send(result);
          });
  else if(sortBy === "english")
      db.collection('students').aggregate([
        { $lookup:
          {
            from: 'marks',
            localField: 'id',
            foreignField: 'id',
            as: 'marksDetails'
          }
        },{$sort:
          {
            "marksDetails.english": 1
          }}
      ]).toArray(function(
              err,result
          ){
              console.log(err);
              res.send(result);
          });
  else if(sortBy === "computers")
      db.collection('students').aggregate([
        { $lookup:
          {
            from: 'marks',
            localField: 'id',
            foreignField: 'id',
            as: 'marksDetails'
          }
        },{$sort:
          {
            "marksDetails.computers": 1
          }}
      ]).toArray(function(
              err,result
          ){
              console.log(result);
              res.send(result);
          });
  else if(sortBy === "name")
      db.collection('students').aggregate([
        { $lookup:
          {
            from: 'marks',
            localField: 'id',
            foreignField: 'id',
            as: 'marksDetails'
          }
        },{$sort:
          {
            name: 1
          }}
      ]).toArray(function(
              err,result
          ){
              console.log(result);
              res.send(result);
          });
  else if(sortBy === "id")
      db.collection('students').aggregate([
        { $lookup:
          {
            from: 'marks',
            localField: 'id',
            foreignField: 'id',
            as: 'marksDetails'
          }
        },{$sort:
          {
            id: 1
          }}
      ]).toArray(function(
              err,result
          ){
              console.log(result);
              res.send(result);
          });
  else if(sortBy === "section")
      db.collection('students').aggregate([
        { $lookup:
          {
            from: 'marks',
            localField: 'id',
            foreignField: 'id',
            as: 'marksDetails'
          }
        },{$sort:
          {
            section: 1
          }}
      ]).toArray(function(
              err,result
          ){
              console.log(result);
              res.send(result);
          });
  else 
      db.collection('students').aggregate([
        { $lookup:
          {
            from: 'marks',
            localField: 'id',
            foreignField: 'id',
            as: 'marksDetails'
          }
        }
      ]).toArray(function(err,result){
              console.log(result);
              res.send(result);
              
          });
          
    });
   
});


function pagination(obj, page) {
  var resArr = obj;
  resArr = resArr.slice(page * 6 - 6, page * 6);
  return resArr;
}

function returnPaginationData(result, page){
    let respArr = pagination(result, page);
    let len = result.length;
    let quo = Math.floor(len / 6);
    let rem = len % 6;
    let extra = rem === 0 ? 0 : 1;
    let numofpages = quo + extra;
    let pageInfo = {
        pageNumber: page,
        numberOfPages: numofpages,
        numOfItems: 6,
        totalItemCount: result.length,
    };
    return {
        data: respArr,
        pageInfo: pageInfo,
      };
}